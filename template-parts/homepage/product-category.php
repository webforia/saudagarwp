<?php
// category
if (rt_option('homebuilder_product_category_hide_child', false)) {
    $terms = get_terms([
        'taxonomy' => 'product_cat',
        'hide_empty' => rt_option('homebuilder_product_category_hide_empty', false),
        'parent' => 0,
    ]);
} else {
    $terms = get_terms([
        'taxonomy' => 'product_cat',
        'hide_empty' => rt_option('homebuilder_product_category_hide_empty', false),
    ]);
}
?>
<?php if (rt_is_woocommerce()) : ?>
    <?php $background_type = rt_option('homebuilder_product_category_background_type', 'background-primary') ?>

    <section id="homepage-product-category" class="<?php echo "home-section {$background_type}" ?>">
        <div class="page-container">

            <?php
            echo rt_header_block([
                'title' => rt_option('homebuilder_product_category_title', 'Categories Description'),
                'desc' => rt_option('homebuilder_product_category_desc', 'Categories description'),
                'class' => 'rt-header-block--center',
            ]);

            if (rt_option('homebuilder_product_category_carousel', true)) {
                if (rt_option('homebuilder_product_category_style', 'style-1') == 'style-1') {
                    echo rt_before_slider([
                        'id' => 'home_pruduct_category',
                        'items-lg' => rt_option('homebuilder_product_category_slider_show', 3),
                        'items-md' => rt_option('homebuilder_product_category_slider_show_tablet', 2),
                        'items-sm' => rt_option('homebuilder_product_category_slider_show_mobile', 2),
                        "slider_loop" => rt_option("homebuilder_product_category_slider_loop", false),
                        "slider_auto_play" => rt_option("homebuilder_product_category_slider_autoplay", false),
                        "slider_gap" => 20,
                    ]);
                } else {
                    echo rt_before_slider([
                        'id' => 'home_pruduct_category',
                        'class' => 'rt-swiper--pagination-outer',
                        'breakpoints' => [
                            '960' => [
                                'spaceBetween' => 30,
                            ],
                            '720' => [
                                'spaceBetween' => 20,
                            ],
                            '320' => [
                                'spaceBetween' => 15,
                            ],
                        ],
                        "slider_loop" => rt_option("homebuilder_product_category_slider_loop", false),
                        "slider_auto_play" => rt_option("homebuilder_product_category_slider_autoplay", false),
                        'centerInsufficientSlides' => true,
                        'navigation' => false,
                        'pagination' => [
                            'el' => '.swiper-pagination',
                            'clickable' => true,
                        ],
                    ]);
                }
            } else {
                $classes[] = 'grids';
                $classes[] = 'grids-md-' . rt_option('homebuilder_product_category_column', 3);
                $classes[] = 'grids-sm-' . rt_option('homebuilder_product_category_column_tablet', 3);
                $classes[] = 'grids-xs-' . rt_option('homebuilder_product_category_column_mobile', 2);
                $column_class = implode(' ', $classes);

                echo '<div class="' . $column_class . '">';
            }
            ?>

            <?php if (!empty($terms) && !is_wp_error($terms)) : ?>

                <?php foreach ($terms as $key => $term) : ?>


                    <?php if ($term->slug != 'uncategorized') : ?>

                        <?php $thumbnail_id = get_term_meta($term->term_id, 'thumbnail_id', true); ?>

                        <?php if (rt_option('homebuilder_product_category_style', 'style-1') == 'style-1') : ?>

                            <?php if (rt_option('homebuilder_product_category_carousel', true)) : ?>
                                <div class="swiper-slide">
                                <?php endif ?>
                                <div class="rt-banner mb-0 <?php echo "rt-banner-category-{$term->slug}" ?>">
                                    <a href="<?php echo get_term_link($term->slug, 'product_cat') ?>">

                                        <span class="rt-banner__badges"><?php echo wp_sprintf(__('%s products', 'saudagarwp'), $term->count) ?></span>

                                        <div class="rt-banner__thumbnail">
                                            <?php
                                            if (wp_get_attachment_image($thumbnail_id, 'featured_medium')) {
                                                echo wp_get_attachment_image($thumbnail_id, 'featured_medium');
                                            } else {
                                                echo wc_placeholder_img('featured_medium');
                                            }
                                            ?>
                                        </div>
                                        <span class="rt-banner__overlay"></span>
                                        <div class="rt-banner__body">
                                            <h5 class="rt-banner__title"><?php echo esc_html($term->name); ?></h5>
                                        </div>
                                    </a>
                                </div>

                                <?php if (rt_option('homebuilder_product_category_carousel', true)) : ?>
                                </div>
                            <?php endif ?>
                        <?php endif ?>

                        <?php if (rt_option('homebuilder_product_category_style', 'style-1') == 'style-2') : ?>
                            <?php if (rt_option('homebuilder_product_category_carousel', true)) : ?>
                                <div class="swiper-slide" style="width: auto">
                                <?php endif ?>


                                <div class="rt-img-box <?php echo "rt-banner-category-{$term->slug}" ?>">
                                    <a href="<?php echo get_term_link($term->slug, 'product_cat') ?>">
                                        <div class="rt-img-box__thumbnail rt-img">
                                            <?php
                                            if (wp_get_attachment_image($thumbnail_id, 'thumbnail')) {
                                                echo wp_get_attachment_image($thumbnail_id, 'thumbnail');
                                            } else {
                                                echo wc_placeholder_img('thumbnail');
                                            }
                                            ?>
                                        </div>

                                        <div class="rt-img-box__body">
                                            <h5 class="rt-img-box__title"><?php echo $term->name ?></h5>
                                            <span class="rt-img-box__meta"><?php echo wp_sprintf(__('%s items', 'saudagarwp'), $term->count) ?></span>
                                        </div>
                                    </a>
                                </div>

                                <?php if (rt_option('homebuilder_product_category_carousel', true)) : ?>
                                </div>
                            <?php endif ?>

                        <?php endif ?>


                    <?php endif ?>
                <?php endforeach ?>

            <?php endif; ?>

            <?php if (rt_option('homebuilder_product_category_carousel', true)) : ?>
                <?php echo rt_after_slider(); ?>
            <?php else : ?>
        </div>
    <?php endif ?>


    </div>
    </section>

<?php endif ?>