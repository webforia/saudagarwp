<?php
$index = $args["index"];

$background_type = rt_option("homebuilder_product_{$index}_background_type", 'background-primary');

if (rt_option("homebuilder_product_{$index}_carousel", false)) {
    $class[] = "rt-swiper--card";
}
$class[] = "products woocommerce";
?>
<?php if (rt_is_woocommerce()): ?>
<section id="homepage-product-<?php echo esc_attr($index) ?>" class="<?php echo "home-section {$background_type}" ?>">
    <div class="page-container">

    <?php
    echo rt_header_block([
        "title" => rt_option("homebuilder_product_{$index}_title", "Product Catalog"),
        "desc" => rt_option("homebuilder_product_{$index}_desc"),
        'class' => 'rt-header-block--center',
    ]);
    
    rt_template_loop([
        "id" => "home_product_{$index}",
        "class" => implode(' ', $class),
        "post_type" => "product",
        "template_part" => "woocommerce/content-product",
        "posts_per_page" => rt_option("homebuilder_product_{$index}_posts_per_page", 8),
        "query_by" => rt_option("homebuilder_product_{$index}_query_by"),
        "category" => rt_option("homebuilder_product_{$index}_category"),
        "post_id" => rt_option("homebuilder_product_{$index}_id"),
        "orderby" => rt_option("homebuilder_product_{$index}_orderby"),
        "setting_column" => rt_option("homebuilder_product_{$index}_column", 3),
        "setting_column_tablet" => rt_option("homebuilder_product_{$index}_column_tablet", 2),
        "setting_column_mobile" => rt_option("homebuilder_product_{$index}_column_mobile", 2),
        "pagination_style" => rt_option("homebuilder_product_{$index}_pagination", 'none'),
        "carousel" => rt_option("homebuilder_product_{$index}_carousel", false),
        "slider_item" => rt_option("homebuilder_product_{$index}_slider_show", 3),
        "slider_item_tablet" => rt_option("homebuilder_product_{$index}_slider_show_tablet", 2),
        "slider_item_mobile" => rt_option("homebuilder_product_{$index}_slider_show_mobile", 2),
        "slider_loop" => rt_option("homebuilder_product_{$index}_slider_loop"),
        "slider_auto_play" => rt_option("homebuilder_product_{$index}_slider_autoplay"),
        "slider_link" => rt_option("homebuilder_product_{$index}_slider_link", false),
        "link_text" => rt_option("homebuilder_product_{$index}_pagination_link_text", __('View More', 'saudagarwp')),
        "link_url" => rt_option("homebuilder_product_{$index}_pagination_link_url"),
    ]);

    ?>

    </div>
</section>
<?php endif?>