<?php
$premium_homepage = array(
    'product-2',
    'product-3',
    'product-category',
    'blog-1',
    'banner',
    'image',
    'image-content',
    'text-content',
    'the-content',
);
$homepage = rt_option('homebuilder_layout', ['product-1']);

if (!rt_is_premium()) {

    $homepage = array_diff($homepage, $premium_homepage);

}

if (!empty($homepage)) {
    foreach ($homepage as $key => $layout) {

        $wraper_id = str_replace('-', '_', $layout);

        // before wrapper preview
        if (is_customize_preview()) {
            echo rt_html_open(['id' => "homebuilder_{$wraper_id}_wrapper"]);
        }

        if (in_array($layout, array('product-1', 'product-2', 'product-3'))) {
            // get number
            $arg['index'] = str_replace('product-', '', $layout);
            rt_get_template_part("homepage/product", $arg);
        } else if ($layout == 'blog-1') {
            rt_get_template_part("homepage/blog");
        } else {
            rt_get_template_part("homepage/{$layout}");
        }

        // after wrapper preview
        if (is_customize_preview()) {
            echo rt_html_close();
        }

    }
}
