<?php
$style = rt_option('homebuilder_welcome_style', 'style-1');
$image_id = rt_option('homebuilder_welcome_image');
$sub_title = rt_option('homebuilder_welcome_text_intro', 'Sub Title');
$title = rt_option('homebuilder_welcome_title', 'Title');
$content = rt_option('homebuilder_welcome_content', 'Text content');
$link_url = rt_option('homebuilder_welcome_link_url', '#');
$link_label = rt_option('homebuilder_welcome_link_label', 'Read More');

?>
<section id="homepage-welcome-area" class="rt-welcome-area rt-welcome-area--<?php echo $style ?> home-section">
    <div class="page-container">

        <span class="rt-welcome-area__background"></span>
        
        <?php if (rt_option('homebuilder_welcome_background_overlay', false)) : ?>
            <span class="rt-welcome-area__overlay"></span>
        <?php endif ?>

        <div class="rt-welcome-area__inner">

            <?php if ($image_id && in_array($style, ['style-1', 'style-3'])) : ?>
                <div class="rt-welcome-area__thumbnail">
                    <?php echo (wp_get_attachment_image($image_id, 'full')) ? wp_get_attachment_image($image_id, 'full') : ''; ?>
                </div>
            <?php endif ?>

            <div class="rt-welcome-area__body">

                <?php if ($sub_title) : ?>
                    <span class="rt-welcome-area__intro"><?php echo $sub_title ?></span>
                <?php endif ?>

                <h2 class="rt-welcome-area__title"><?php echo esc_html($title) ?></h2>

                <?php if ($content) : ?>
                    <div class="rt-welcome-area__content"><?php echo $content; ?></div>
                <?php endif ?>

                <?php if ($link_label) : ?>
                    <div class="rt-welcome-area__buttons">
                        <a class="rt-btn rt-btn--primary" href="<?php echo esc_url($link_url) ?>"><?php echo $link_label ?></a>
                    </div>
                <?php endif ?>


            </div>

            <?php if ($style == 'style-2') : ?>
                <div class="rt-welcome-area__thumbnail">
                    <?php echo (wp_get_attachment_image($image_id, 'full')) ? wp_get_attachment_image($image_id, 'full') : ''; ?>
                </div>
            <?php endif ?>


        </div>

    </div>
</section>