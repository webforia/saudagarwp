<?php
if (rt_option('homebuilder_slider_nav', true)) {
    $navigation = [
        'nextEl' => '.swiper-button-next',
        'prevEl' => '.swiper-button-prev',
    ];
} else {
    $navigation = false;
}
$slider_args = array(
    'id' => 'home_slider',
    'items-lg' => 1,
    'items-md' => 1,
    'items-sm' => 1,
    'navigation' => $navigation,
    'spaceBetween' => 0,
    'pagination' => [
        'el' => '.swiper-pagination',
        'clickable' => true,
    ],
    'loop' => rt_option('homebuilder_slider_loop', false),
    'autoplay' => rt_option('homebuilder_slider_autoplay', false),

);
?>


<?php if (rt_option('homebuilder_slider_full_width', true) == true) : ?>
    <section id="homepage-slider" class="section-full">
        <?php echo rt_before_slider($slider_args); ?>
        <?php $index = 0; ?>
        <?php foreach (rt_option('homebuilder_slider') as $key => $slider) : ?>
            <div class="swiper-slide">

                <?php if (!empty($slider['link_url'])) : ?><a href="<?php echo esc_url($slider['link_url']) ?>"><?php endif ?>

                    <div class="rt-img rt-img--full rt-img--rounded">
                        <?php
                        if ($index === 0) {
                            echo wp_get_attachment_image($slider['image'], rt_option('homebuilder_slider_image_size', 'full'), false, ['loading' => 'no-lazy']);
                        } else {
                            echo wp_get_attachment_image($slider['image'], rt_option('homebuilder_slider_image_size', 'full'));
                        }
                        ?>
                    </div>

                    <?php if (!empty($slider['link_url'])) : ?>
                    </a><?php endif ?>

            </div>
            <?php $index++; ?>
        <?php endforeach; ?>
        <?php echo rt_after_slider(); ?>
    </section>
<?php endif ?>

<?php if (rt_option('homebuilder_slider_full_width', true) == false) : ?>
    <section id="homepage-slider" class="home-section">
        <div class="page-container">
            <?php echo rt_before_slider(wp_parse_args(['class' => 'rt-swiper--rounded'], $slider_args)); ?>

            <?php $index = 0; ?>
            <?php foreach (rt_option('homebuilder_slider') as $key => $slider) : ?>
                <div class="swiper-slide">

                    <?php if (!empty($slider['link_url'])) : ?><a href="<?php echo esc_url($slider['link_url']) ?>"><?php endif ?>

                        <div class="rt-img rt-img--full rt-img--rounded">
                            <?php
                            if ($index === 0) {
                                echo wp_get_attachment_image($slider['image'], rt_option('homebuilder_slider_image_size', 'full'), false, ['loading' => 'no-lazy']);
                            } else {
                                echo wp_get_attachment_image($slider['image'], rt_option('homebuilder_slider_image_size', 'full'));
                            }
                            ?>
                        </div>

                        <?php if (!empty($slider['link_url'])) : ?>
                        </a><?php endif ?>

                </div>
                <?php $index++; ?>
            <?php endforeach; ?>
            <?php echo rt_after_slider(); ?>
        </div>
    </section>
<?php endif; ?>