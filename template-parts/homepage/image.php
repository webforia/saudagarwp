<section id="homepage-image">

    <?php if(!rt_option('homebuilder_image_full_width', false)): ?>
    <div class="page-container">
    <?php endif ?>

        <a class="rt-img rt-img--rounded rt-img--full" href="<?php echo esc_url(rt_option('homebuilder_image_link_url', '#')) ?>">
            <?php echo wp_get_attachment_image(rt_option('homebuilder_image'), 'full'); ?>
        </a>

    <?php if(!rt_option('homebuilder_image_full_width', false)): ?>
    </div>
    <?php endif ?>

</section>