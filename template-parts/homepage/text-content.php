<?php
$background_type = rt_option('homebuilder_text_content_text_background_type', 'background-primary');
$content = rt_option('homebuilder_text_content_text', 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eveniet, totam eius perferendis dolores repellendus, quos tempora molestiae excepturi, sit exercitationem quasi maiores necessitatibus laudantium voluptatibus facilis in cum sunt fuga!');
?>

<section id="homepage-text-content" class="<?php echo "home-section {$background_type}" ?>">
    <div class="page-container">
        <?php echo apply_filters('the_content', $content); ?>
    </div>
</section>


