<?php
$sidebar = rt_option('woocommerce_archive_layout', 'sidebar-right');
$class[] = "home-section";
$class[] = "page-{$sidebar}";
?>
<?php if (rt_is_woocommerce()): ?>
<section id="homepage-product-archive" class="<?php echo esc_attr(implode(' ', $class)) ?>">
    <div class="page-container">
         <?php
            echo rt_header_block([
                "title" => rt_option("homebuilder_product_archive_title", "Shop"),
                "desc" => rt_option("homebuilder_product_archive_desc", "Lastest Product"),
                'class' => 'rt-header-block--center',
            ]);
            ?>
    </div>
    <div class="page-container">
        <div class="flex flex-row">
            <div id="page-content" class="page-content">
            <?php
            rt_template_loop([
                "id" => "home_product_archive",
                "post_type" => "product",
                "template_part" => "woocommerce/content-product",
                "posts_per_page" => rt_woocommerce_per_page(),
                "setting_column" => rt_option('woocommerce_archive_options_column', 3),
                "setting_column_tablet" => rt_option('woocommerce_archive_options_column_tablet', 2),
                "setting_column_mobile" => rt_option("woocommerce_archive_options_column_mobile", 2),
                "pagination_style" => rt_option('woocommerce_archive_pagination', 'number'),
                'format' => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'add_args' => [
                    "post_type" => "product",
                ],
            ]);
            ?>
            </div>

            <?php
            if ($sidebar == 'sidebar-right' || $sidebar == 'sidebar-left') {
                get_sidebar('shop');
            }
            ?>
        </div>
    </div>

</section>
<?php endif?>