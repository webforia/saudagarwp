<?php $background_type = rt_option('homebuilder_infobox_background_type', 'background-primary')?>

<section id="homepage-infobox" class="<?php echo "home-section {$background_type}"?>">
    <div class="page-container">

    <?php
     echo rt_header_block([
        'title' => rt_option('homebuilder_infobox_title', 'title'),
        'desc' => rt_option('homebuilder_infobox_desc', 'description'),
        'class' => 'rt-header-block--center',
    ]);

    rt_template_loop([
        'id' => 'info-box',
        'post_type' => 'content',
        'content' => rt_option('homebuilder_infobox'),
        'template_part' => 'homepage/infobox-content',
        'setting_column' => rt_option('homebuilder_infobox_column', 3),
        'setting_column_tablet' => rt_option('homebuilder_infobox_column_tablet', 1),
        'setting_column_mobile' => rt_option('homebuilder_infobox_column_mobile', 1),
        'carousel' => rt_option('homebuilder_infobox_carousel', false),
        'slider_item' => rt_option('homebuilder_infobox_slider_show', 3),
        'slider_item_tablet' => rt_option('homebuilder_infobox_slider_show_tablet', 1),
        'slider_item_mobile' => rt_option('homebuilder_infobox_slider_show_mobile', 1),
        'slider_gap' => 20,
        'slider_loop' => rt_option('homebuilder_infobox_slider_loop'),
        'slider_auto_play' => rt_option('homebuilder_infobox_slider_autoplay'),
    ]); 
    ?>

    </div>
</section>