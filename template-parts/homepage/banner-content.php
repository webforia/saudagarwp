<div class="rt-banner">

    <?php if (!empty($args['link'])): ?><a href="<?php echo esc_url_raw($args['link']) ?>"><?php endif?>

    <?php if ($args['image']): ?>
    <div class="rt-banner__thumbnail">
        <?php echo wp_get_attachment_image($args['image'], rt_option('homebuilder_banner_image_size', 'featured_medium')); ?>
    </div>
    <?php endif?>
    
    <?php if (!empty($args['link'])): ?></a><?php endif?>
</div>