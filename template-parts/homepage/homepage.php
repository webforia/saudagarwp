<?php
if (is_customize_preview()) {
    echo rt_html_open(['id' => "homebuilder_layout_wrapper"]);
}

rt_get_template_part('homepage/homepage-content');

if (is_customize_preview()) {
    echo rt_html_close();
}
?>