<?php 
 if(rt_option('homebuilder_infobox_card', false)){
    $css_class[] = 'rt-infobox--card';
 }
 $css_class[] = 'rt-infobox--'.rt_option('homebuilder_infobox_style', 'stack');
?>
<div class="rt-infobox <?php echo implode(" ",  $css_class) ?>">
    <div class="rt-infobox__thumbnail rt-img rt-img--rounded">
        <?php echo wp_get_attachment_image($args['image'], 'thumbnail'); ?>
    </div>

    <div class="rt-infobox__body">
        
        <h5 class="rt-infobox__title">
            <?php if ($args['link_url']) : ?>
                <a class="#" href="<?php echo esc_url_raw($args['link_url']) ?>">
                <?php endif ?>
                <?php echo $args['title'] ?>
                <?php if ($args['link_url']) : ?>
                </a>
            <?php endif ?>
        </h5>

        <div class="rt-infobox__desc"><?php echo $args['content'] ?></div>
    </div>


</div>