<?php $background_type = rt_option('homebuilder_banner_background_type', 'background-primary')?>

<section id="homepage-banner" class="<?php echo "home-section {$background_type}"?>">
    <div class="page-container">

    <?php
     echo rt_header_block([
        'title' => rt_option('homebuilder_banner_title', 'title'),
        'desc' => rt_option('homebuilder_banner_desc', 'description'),
        'class' => 'rt-header-block--center',
    ]);

    rt_template_loop([
        'id' => 'content-box',
        'post_type' => 'content',
        'content' => rt_option('homebuilder_banner'),
        'template_part' => 'homepage/banner-content',
        'layout' => rt_option('homebuilder_banner_layout', 1),
        'setting_column' => rt_option('homebuilder_banner_column', 3),
        'setting_column_tablet' => rt_option('homebuilder_banner_column_tablet', 2),
        'setting_column_mobile' => rt_option('homebuilder_banner_column_mobile', 2),
        'carousel' => rt_option('homebuilder_banner_carousel', false),
        'slider_item' => rt_option('homebuilder_banner_slider_show', 3),
        'slider_item_tablet' => rt_option('homebuilder_banner_slider_show_tablet', 2),
        'slider_item_mobile' => rt_option('homebuilder_banner_slider_show_mobile', 2),
        'slider_gap' => 20,
        'slider_loop' => rt_option('homebuilder_banner_slider_loop'),
        'slider_auto_play' => rt_option('homebuilder_banner_slider_autoplay'),
    ]); 
    ?>

    </div>
</section>