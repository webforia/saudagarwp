<?php $background_type = rt_option('homebuilder_blog_1_background_type', 'background-primary') ?>

<section id="homepage-blog-1" class="<?php echo "home-section {$background_type}"?>">
    <div class="page-container">
    
    <?php 
    echo rt_header_block([
        'title' => rt_option('homebuilder_blog_1_title', 'Lastest Post'),
        'desc' => rt_option('homebuilder_blog_1_desc'),
        'class' => 'rt-header-block--center',
    ]);

    rt_template_loop([
        'id' => 'home_blog_1',
        'class' => (rt_option('homebuilder_blog_1_carousel', false))?'rt-swiper--card': '',
        'post_type' => 'post',
        'template_part' => 'template-parts/post/post',
        'posts_per_page' => rt_option('homebuilder_blog_1_posts_per_page', 8),
        'query_by' => rt_option('homebuilder_blog_1_query_by'),
        'category' => rt_option('homebuilder_blog_1_category'),
        'post_id' => rt_option('homebuilder_blog_1_id'),
        'orderby' => rt_option('homebuilder_blog_1_orderby'),
        'layout' => rt_option('homebuilder_blog_1_layout', 'grid'),
        'setting_column' => rt_option('homebuilder_blog_1_column', 3),
        'setting_column_tablet' => rt_option('homebuilder_blog_1_column_tablet', 2),
        'setting_column_mobile' => rt_option('homebuilder_blog_1_column_mobile', 2),
        'pagination_style' => rt_option('homebuilder_blog_1_pagination'),
        'carousel' => rt_option('homebuilder_blog_1_carousel', false),
        'slider_item' => rt_option('homebuilder_blog_1_slider_show', 3),
        'slider_item_tablet' => rt_option('homebuilder_blog_1_slider_show_tablet', 2),
        'slider_item_mobile' => rt_option('homebuilder_blog_1_slider_show_mobile', 2),
        'slider_loop' => rt_option('homebuilder_blog_1_slider_loop'),
        'slider_auto_play' => rt_option('homebuilder_blog_1_slider_autoplay'),
        "slider_link" => rt_option("homebuilder_blog_1_slider_link", false),
        "link_text" => rt_option("homebuilder_blog_1_pagination_link_text", __('View More', 'saudagarwp')),
        "link_url" => rt_option("homebuilder_blog_1_pagination_link_url"),
    ]);
    
     // link action
    $link_text = rt_option("homebuilder_blog_1_pagination_link_text");
    $link_url = esc_url_raw(rt_option("homebuilder_blog_1_pagination_link_url"));

    if($link_url){
        echo "<div class='rt-pagination'>";
        echo "<a class='rt-pagination__button rt-btn rt-btn--border' href='{$link_url}'>{$link_text}</a>";
        echo "</div>";
    }

    ?>

    </div>
</section>