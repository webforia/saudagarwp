<?php
$background_type = rt_option('homebuilder_infobox_background_type', 'background-primary');
$image_id = rt_option('homebuilder_image_content_item');
$title = rt_option('homebuilder_image_content_title', 'Title Image Content');
$content = rt_option('homebuilder_image_content_content', 'Add rich text to the section. Ideally the text should be short and to the point in order to prevent if from being taller than the image.');
$button = rt_option('homebuilder_image_content_link_style', 'button-link');

if ($button == 'button-primary') {
    $button_class = 'mt-20 rt-btn rt-btn--primary';
} elseif ($button == 'button-action') {
    $button_class = 'mt-20 rt-btn rt-btn--action';
} else {
    $button_class = 'rt-image-content__link mt-20';
}
?>

<section id="homepage-image-content" class="<?php echo "rt-image-content home-section {$background_type}" ?>">
    <div class="page-container">

        <div class="rt-image-content__inner">
            <div class="rt-image-content__img rt-img rt-img--full rt-img--rounded">
                <?php
                if (wp_get_attachment_image($image_id, 'full')) {
                    echo wp_get_attachment_image($image_id, 'full');
                } else {
                    rt_image_placeholder();
                }
                ?>
            </div>
            <div class="rt-image-content__body">
                <h2 class="rt-image-content__title"><?php echo esc_html($title) ?></h2>
                <div class="rt-image-content__desc rt-entry-content"> <?php echo $content; ?></div>
                <?php if (rt_option('homebuilder_image_content_link_url', site_url())) : ?>
                    <a class="<?php echo $button_class ?>" href="<?php echo esc_url(rt_option('homebuilder_image_content_link_url', site_url())) ?>"><?php echo rt_option('homebuilder_image_content_link_text', 'Read more') ?></a>
                <?php endif ?>
            </div>
        </div>

    </div>
</section>