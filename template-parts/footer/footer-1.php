<footer id="page-footer" class="page-footer page-footer--style-1">

    <?php do_action('rt_footer_prepend')?>

    <?php if (rt_option('footer_widget', false)): ?>

    <?php do_action('rt_before_footer_widget')?>

    <div id="footer-1" class="page-footer__widget">
        <div class="page-container">

            <div class="flexs">

                <div id="widget-footer-1" class="flex-md-3 flex-sm-12">
                    <?php rt_footer_widget_item('retheme_footer_1'); ?>
                </div>

                <div id="widget-footer-2" class="flex-md-3 flex-sm-12">
                    <?php rt_footer_widget_item('retheme_footer_2'); ?>
                </div>

                <div id="widget-footer-3" class="flex-md-3 flex-sm-12">
                    <?php rt_footer_widget_item('retheme_footer_3'); ?>
                </div>

                <div id="widget-footer-4" class="flex-md-3 flex-sm-12">
                <?php rt_footer_widget_item('retheme_footer_4'); ?>
                </div>

            </div>

        </div>

    </div>

    <?php do_action('rt_after_footer_widget')?>

    <?php endif;?>

    <?php rt_get_template_part('footer/footer-bottom');?>

     <?php do_action('rt_footer_append')?>

</footer>