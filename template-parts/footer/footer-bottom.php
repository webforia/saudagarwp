<?php if (rt_option('footer_bottom', true)): ?>

	 <?php do_action('rt_before_footer_bottom')?>

	<div class="page-footer__bottom">
      <div class="page-container">
         <?php
        $elements = rt_option('footer_bottom_element', array('html-1'));

        if (!empty($elements) && is_array($elements) && rt_is_premium()) {
            foreach ($elements as $element) {
                rt_get_template_part('footer/element/' . $element);
            }
        } else {
            rt_get_template_part('footer/element/html-1');
        }
        ?>
      </div>
	</div>

	 <?php do_action('rt_after_footer_bottom')?>
<?php endif?>
