<footer id="page-footer" class="page-footer page-footer--style-3">

    <?php do_action('rt_footer_prepend')?>

    <?php if (rt_option('footer_widget', false)): ?>

    <?php do_action('rt_before_footer_widget')?>

    <div id="footer-3" class="page-footer__widget">
        <div class="page-container">
            
            <div class="flex flex-row">
        
                <div id="widget-footer-1" class="flex-md-6 flex-sm-12 mal-0">
                    <div class="flex-md-10 flex-sm-12">
                        <?php rt_footer_widget_item('retheme_footer_1');?>
                    </div>
                </div>
        
                <div id="widget-footer-2" class="flex-md-3 flex-sm-12">
                    <?php rt_footer_widget_item('retheme_footer_2');?>
                </div>

                </div>

            </div>

        </div>

    </div>

    <?php do_action('rt_after_footer_widget')?>

    <?php endif;?>

    <?php rt_get_template_part('footer/footer-bottom');?>

     <?php do_action('rt_footer_append')?>

</footer>