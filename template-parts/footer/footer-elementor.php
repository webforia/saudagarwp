<?php if (rt_option('footer_widget_layout_elementor') && rt_option('footer_widget_layout_elementor') != get_the_ID() && did_action('elementor/loaded')): ?>

<footer id="page-footer" class="page-footer page-footer--builder">
    <?php echo Elementor\Plugin::instance()->frontend->get_builder_content_for_display(rt_option('footer_widget_layout_elementor')); ?>
</footer>

<?php endif?>
