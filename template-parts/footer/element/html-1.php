<?php 
$text_option = rt_option('footer_bottom_text', '@Copyright '.get_bloginfo('name').'. All Rights Reserved');
$text_format = str_replace(array('{{site_name}}', '{{year}}'), array(get_bloginfo('name'), date("Y")), $text_option);
?>
<?php if($text_option && !empty(get_option("saudagarwp_key")) && rt_is_premium()): ?>
    <span class="rt-footer__bottom-text"><?php echo $text_format ?></span>
<?php else: ?>
    <span class="rt-footer__bottom-text">@Copyright <a href="https://saudagarwp.com/" target="_blank">Saudagar WP</a>. All Rights Reserved</span>
<?php endif ?>
