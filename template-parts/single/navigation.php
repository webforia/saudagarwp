<?php
$nextPost = get_next_post();
$prevPost = get_previous_post();
?>

<?php if (rt_single_option('navigation') && (!empty($nextPost) || !empty($prevPost))) : ?>
    <div class="rt-single-nav">

        <?php if (isset($prevPost) && $prevPost != null) : ?>
            <a href="<?php echo esc_url(get_the_permalink($prevPost->ID)) ?>" class="rt-single-nav-prev">
                <div class="rt-single-nav__label">
                    <i class="rt-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"m" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
                        </svg>
                    </i>
                    <?php echo __('Previous Post', 'saudagarwp') ?>
                </div>
                <div class="rt-single-nav__body">
                    <div class="rt-img"><?php echo get_the_post_thumbnail($prevPost->ID, 'thumbnail') ?></div>
                    <h4 class="rt-single-nav__title"><?php echo rt_limited_string(get_the_title($prevPost->ID), 9) ?></h4>
                </div>
            </a>
        <?php else : ?>
            <span class="rt-single-nav-prev"></span>
        <?php endif ?>

        <?php if (isset($nextPost) && $nextPost != null) : ?>
            <a href="<?php echo esc_url(get_the_permalink($nextPost->ID)) ?>" class="rt-single-nav-next">
                <div class="rt-single-nav__label"><?php echo __('Newer Post', 'saudagarwp') ?>
                    <i class="rt-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
                        </svg>
                    </i>
                </div>
                <div class="rt-single-nav__body">
                    <div class="rt-img"><?php echo get_the_post_thumbnail($nextPost->ID, 'thumbnail') ?></div>
                    <h4 class="rt-single-nav__title"><?php echo rt_limited_string(get_the_title($nextPost->ID), 9) ?></h4>
                </div>
            </a>
        <?php else : ?>
            <span class="rt-single-nav-next"></span>
        <?php endif ?>

    </div>
<?php endif ?>