<?php
if (get_the_category() && rt_single_option('related')) {
    $results = [
        'id' => 'post_related',
        'class' => 'rt-swiper--card',
        'post__not_in' => array(get_the_ID()),
        'post_type' => 'post',
        'posts_per_page' => rt_option('single_related_count', 6),
        'carousel' => true,
        'template_part' => 'template-parts/single/related-content',
        'slider_item' => rt_option('single_related_show', 3),
        'slider_item_tablet' => rt_option('single_related_show_tablet', 2),
        'slider_item_mobile' => rt_option('single_related_show_mobile', 2),
        'tax_query' => [
            [
                'taxonomy' => 'doc_category',
                'field' => 'term_id',
                'terms' => wp_get_post_terms(get_the_ID(), 'doc_category', array('fields' => 'ids')),
                'operator' => 'IN',
            ],
        ],
    ];


    if (count(get_posts($results))) {
        echo rt_header_block([
            "title" => __("You also like", 'saudagarwp'),
        ]);

        rt_template_loop($results);
    }
}
