<div class="rt-single__content rt-entry-content">
  <?php 
  
  the_content(); 
  
  wp_link_pages(
		array(
			'before' => '<div class="rt-page-links">' . esc_html__( 'Pages:', 'saudagarwp'),
			'after'  => '</div>',
		)
	);
  ?>
</div>
