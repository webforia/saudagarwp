<article class="rt-single">
    <?php 
    rt_get_template_part('single/breadcrumbs');
    rt_get_template_part('single/title');
    rt_get_template_part('single/meta');
    rt_get_template_part('global/share');
    rt_get_template_part('single/thumbnail');
    rt_get_template_part('single/content');
    rt_get_template_part('single/tags');
    rt_get_template_part('global/share');
    ?>
</article>

<?php  
rt_get_template_part('single/navigation');
rt_get_template_part('single/author-info');
rt_get_template_part('single/related');
rt_get_template_part('single/comment');
?>

