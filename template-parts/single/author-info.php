<?php if (rt_single_option('authorbox')) : ?>

  <?php
  $author_id = get_the_author_meta('ID');
  $author_avatar     = get_avatar($author_id, 70, '', '', ['class' => 'rounded-full']);
  $author_avatar_url = get_user_meta($author_id, 'avatar', true);
  $first_name        = get_the_author_meta('first_name');
  $last_name         = get_the_author_meta('last_name');
  $author_desc       = get_the_author_meta('description');
  $author_blog       = get_the_author_meta('blog_title');
  $author_url        = get_the_author_meta('url');
  $author_website    = get_the_author_meta('url');
  $author_job = '';
  ?>


  <div class="rt-author-box">

    <div class="rt-author-box__thumbnail ">
      <?php
      if ($author_avatar_url) {
        echo wp_get_attachment_image($author_avatar_url, 'thumbnail', "", ['class' => 'rounded-full']);
      } else {
        echo $author_avatar;
      }
      ?>
    </div>


    <div class="rt-author-box__body">

      <h4 class="rt-author-box__title">
        <a href="<?php echo get_author_posts_url($author_id) ?>">
          <?php echo "{$first_name} {$last_name}" ?>
        </a>
      </h4>

      <?php if ($author_job): ?>
        <span class="rt-author-box__job"><?php echo $author_job ?></span>
      <?php endif ?>

      <p class="rt-author-box__content">
        <?php echo $author_desc ?>
      </p>

    </div>
  </div>

<?php endif ?>