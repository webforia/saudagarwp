<?php if (rt_single_option('thumbnail')) : ?>
<div class="rt-single__thumbnail">
  <?php rt_the_post_thumbnail('medium-large') ?>
</div>
<?php endif ?>