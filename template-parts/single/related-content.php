<?php 
$post_class[] = 'rt-post';
$post_class[] = 'rt-post--'.rt_option("blog_archive_style", "grid");
$post_class[] = (rt_option("blog_archive_card", false) && rt_option("blog_archive_style", "grid") == 'grid') ? 'rt-post--card' : '';
?>

<div  id="post-<?php echo get_the_ID()?>" <?php post_class($post_class) ?>>

    <?php rt_get_template_part('post/thumbnail'); ?>

    <div class="rt-post__body">
        <?php
        rt_get_template_part('post/category');
        rt_get_template_part('post/title');
        rt_get_template_part('post/meta');
        rt_get_template_part('post/content');
        rt_get_template_part('post/footer');
        ?>
    </div>

</div>
