<?php $url = get_permalink() ?>

<div class="rt-share">
    <div class="rt-socmed rt-socmed--simple">

        <?php if (rt_option('connect_share_facebook', true)) : ?>
            <a class="rt-socmed__item facebook" target="_blank" rel="noopener noreferrer nofollow" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url($url); ?>">
                <i class="rt-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
                        <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z" />
                    </svg>
                </i>
            </a>
        <?php endif; ?>

        <?php if (rt_option('connect_share_twitter', true)) : ?>
            <a class="rt-socmed__item twitter" target="_blank" rel="noopener noreferrer nofollow" href="https://x.com/intent/post?url=<?php echo esc_url($url); ?>">
                <i class="rt-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-twitter-x" viewBox="0 0 16 16">
                        <path d="M12.6.75h2.454l-5.36 6.142L16 15.25h-4.937l-3.867-5.07-4.425 5.07H.316l5.733-6.57L0 .75h5.063l3.495 4.633L12.601.75Zm-.86 13.028h1.36L4.323 2.145H2.865z" />
                    </svg>
                </i>
            </a>
        <?php endif; ?>

        <?php if (rt_option('connect_share_pinterest', true)) : ?>
            <a class="rt-socmed__item pinterest" target="_blank" rel="noopener noreferrer nofollow" href="https://pinterest.com/pin/create/bookmarklet/?media=<?php the_post_thumbnail_url(); ?>&amp;url=<?php echo esc_url($url); ?>">
                <i class="rt-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-pinterest" viewBox="0 0 16 16">
                        <path d="M8 0a8 8 0 0 0-2.915 15.452c-.07-.633-.134-1.606.027-2.297.146-.625.938-3.977.938-3.977s-.239-.479-.239-1.187c0-1.113.645-1.943 1.448-1.943.682 0 1.012.512 1.012 1.127 0 .686-.437 1.712-.663 2.663-.188.796.4 1.446 1.185 1.446 1.422 0 2.515-1.5 2.515-3.664 0-1.915-1.377-3.254-3.342-3.254-2.276 0-3.612 1.707-3.612 3.471 0 .688.265 1.425.595 1.826a.24.24 0 0 1 .056.23c-.061.252-.196.796-.222.907-.035.146-.116.177-.268.107-1-.465-1.624-1.926-1.624-3.1 0-2.523 1.834-4.84 5.286-4.84 2.775 0 4.932 1.977 4.932 4.62 0 2.757-1.739 4.976-4.151 4.976-.811 0-1.573-.421-1.834-.919l-.498 1.902c-.181.695-.669 1.566-.995 2.097A8 8 0 1 0 8 0z" />
                    </svg>
                </i>
            </a>
        <?php endif; ?>

        <?php if (rt_option('connect_share_email', true)) : ?>
            <a class="rt-socmed__item email" href="mailto:?subject=<?php echo wp_strip_all_tags(get_the_title()) ?>" body="<?php echo $url ?>" title="<?php echo wp_strip_all_tags(get_the_title()) ?>">
                <i class="rt-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                        <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z" />
                    </svg>
                </i>
            </a>
        <?php endif; ?>

    </div>
</div>