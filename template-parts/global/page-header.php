<?php $classes = 'page-header page-header--' . rt_option('page_header_layout', 'in-content'); ?>

<?php if(rt_option('page_header_layout', 'in-content') == 'in-content'): ?>
  <div id="page-header" class="<?php echo esc_attr( $classes )?>">
<?php else: ?>
  <section id="page-header" class="<?php echo esc_attr( $classes )?>">
    <div class="page-container">
<?php endif ?>

      <?php do_action('rt_page_header_prepend')?>

      <?php rt_breadcrumb()?>

      <?php if (rt_get_page_title()): ?>
      <div class="page-header__inner">

        <h1 class="page-header__title"><?php echo rt_get_page_title() ?></h1>

        <?php if (rt_get_page_desc()): ?>
        <div class="page-header__desc">
          <?php echo rt_get_page_desc() ?>
        </div>
        <?php endif;?>

      </div>
      <?php endif?>

      
      <?php do_action('rt_page_header_append')?>

<?php if(rt_option('page_header_layout', 'in-content') == 'in-content'): ?>
  </div>
<?php else: ?>
  </div>
</section>
<?php endif?>