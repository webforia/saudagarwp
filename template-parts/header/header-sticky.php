
<?php if (rt_option('header_sticky', true)): ?>
<?php 
    $elements = rt_get_theme('header_builder_option');
    $args = array( 'sticky' => true );
?>
  <div id="header-sticky" class="rt-header__sticky">
      <div class="page-container">

        <div id="header_sticky_left" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['sticky_left_alignment']) ?>" data-display="<?php esc_attr_e($elements['sticky_left_display']) ?>">
          <?php if (!empty($elements['sticky_left_element'])) {
                foreach ($elements['sticky_left_element'] as $element) {
                    rt_get_template_part("header/element/{$element}", $args);
                }
            }?>
      </div>
      
       <div id="header_sticky_center" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['sticky_center_alignment']) ?>" data-display="<?php esc_attr_e($elements['sticky_center_display']) ?>">
          <?php if (!empty($elements['sticky_center_element'])) {
                foreach ($elements['sticky_center_element'] as $element) {
                    rt_get_template_part("header/element/{$element}", $args);
                }
            }?>
      </div>

       <div id="header_sticky_right" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['sticky_right_alignment']) ?>" data-display="<?php esc_attr_e($elements['sticky_right_display']) ?>">
          <?php if (!empty($elements['sticky_right_element'])) {
                foreach ($elements['sticky_right_element'] as $element) {
                    rt_get_template_part("header/element/{$element}", $args);
                }
            }?>
      </div>


      </div>
  </div>
<?php endif; ?>
