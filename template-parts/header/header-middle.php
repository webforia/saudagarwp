
<?php if (rt_option('header_middle', false)): ?>

<?php $elements = rt_get_theme('header_builder_option'); ?>

  <?php do_action('rt_before_header_middle')?>

  <div id="header-middle" class="rt-header__middle">
      <div class="page-container">

        <div id="header_middle_left" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['middle_left_alignment']) ?>" data-display="<?php esc_attr_e($elements['middle_left_display'])  ?>">
          <?php if (!empty($elements['middle_left_element'])) {
              foreach ($elements['middle_left_element'] as $element) {
                  rt_get_template_part("header/element/{$element}");
              }
          }?>
        </div>
        
        <div id="header_middle_center" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['middle_center_alignment'])?>" data-display="<?php esc_attr_e($elements['middle_center_display']) ?>">
           <?php if (!empty($elements['middle_center_element'])) {
              foreach ($elements['middle_center_element'] as $element) {
                  rt_get_template_part("header/element/{$element}");
              }
          }?>
        </div>

        <div id="header_middle_right" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['middle_right_alignment'])?>" data-display="<?php esc_attr_e($elements['middle_right_display'])  ?>">
          <?php if (!empty($elements['middle_right_element'])) {
              foreach ($elements['middle_right_element'] as $element) {
                  rt_get_template_part("header/element/{$element}");
              }
          }?>
        </div>

      </div>
  </div>

  <?php do_action('rt_after_header_middle')?>
  
<?php endif; ?>
