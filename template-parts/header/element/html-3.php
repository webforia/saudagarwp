<?php if (rt_option('header_html_3')): ?>
  <div class="rt-header__element rt-header-html-3">

    <?php echo wp_kses_post(do_shortcode(rt_option('header_html_3'))) ?>

  </div>
<?php endif?>