<nav class="rt-menu rt-menu--horizontal menu-primary js-menu" data-animatein='<?php echo rt_option('header_menu_primary_submenu_animation', 'zoomIn') ?>' data-animateout='<?php echo rt_animate_reverse(rt_option('header_menu_primary_submenu_animation', 'zoomIn')) ?>' data-duration='<?php echo rt_option('header_menu_primary_submenu_animation', '300') ?>'>
    <?php
    if (has_nav_menu('primary')) {
        wp_nav_menu([
            'container' =>  '',
            'menu_class' => 'rt-menu__main',
            'theme_location' => 'primary',
            'fallback_cb' => 'rt_menu_fallback',
        ]);
    } else {
        rt_menu_default();
    }
    ?>
</nav>