  <div class="rt-header__element rt-header-menu-toggle rt-header-menu-toggle--<?php echo rt_option('header_menu_off_canvas_style', 'simple') ?> js-slidepanel" data-target="#off_canvas_menu">
    <span class="rt-header-menu-toggle__bar rt-menu-toggle">
      <span></span>
    </span>
    <?php if (rt_option('header_menu_off_canvas_trigger_label', 'Menu')) : ?>
      <span class="rt-header-menu-toggle__label"><?php echo rt_option('header_menu_off_canvas_label', 'Menu') ?></span>
    <?php endif ?>
  </div>