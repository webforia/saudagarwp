<?php if(rt_option('header_html_1')): ?>
  <div class="rt-header__element rt-header-html-1">

    <?php echo wp_kses_post(do_shortcode(rt_option('header_html_1'))) ?>

  </div>
<?php endif ?>
