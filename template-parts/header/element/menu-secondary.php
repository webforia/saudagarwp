<nav class='rt-menu rt-menu--horizontal menu-secondary js-menu' data-animatein='<?php echo rt_option('header_menu_secondary_submenu_animation', 'fadeIn') ?>' data-animateout='<?php echo rt_animate_reverse(rt_option('header_menu_secondary_submenu_animation', 'fadeIn')) ?>' data-duration='<?php echo rt_option('header_menu_secondary_submenu_animation', '300') ?>'>
  <?php
  if (has_nav_menu('secondary')) {
    wp_nav_menu([
      'container' => '',
      'menu_class' => 'rt-menu__main',
      'theme_location' => 'secondary',
      'fallback_cb' => 'rt_menu_fallback',
      'walker' => ''
    ]);
  } else {
    rt_menu_default();
  }
  ?>
</nav>