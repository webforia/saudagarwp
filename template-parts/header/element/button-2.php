<?php
$target = '';
$size = rt_option('header_button_2_size', 'md');

if (rt_option('header_button_2_link', 'blank') == 'blank') {
  $target = 'target="_blank"';
}
?>
<div  class="rt-header__element rt-header-btn-2">
  <a href="<?php echo esc_url(rt_option('header_button_2_link', '#')) ?>" class="rt-btn rt-btn--2 rt-btn--<?php echo esc_attr($size )?>" <?php echo esc_attr($target)  ?>>
    <?php echo rt_option('header_button_2_text', 'Button 2') ?>
  </a>
</div>
