<?php if (rt_option('header_html_5')): ?>
  <div class="rt-header__element rt-header-html-5">

    <?php echo wp_kses_post(do_shortcode(rt_option('header_html_5'))) ?>

  </div>
<?php endif?>