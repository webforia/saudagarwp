<nav class="rt-menu rt-menu--vertical menu-mobile js-menu">

<?php
if (has_nav_menu('mobile')) {
    wp_nav_menu([
        'container' =>  '',
        'menu_class' => 'rt-menu__main',
        'theme_location' => 'mobile',
        'fallback_cb' => 'rt_menu_fallback',
        'walker' => ''
    ]);
}else{
    wp_nav_menu([
        'container' =>  '',
        'menu_class' => 'rt-menu__main',
        'theme_location' => 'primary',
        'fallback_cb' => 'rt_menu_fallback',
        'walker' => ''
    ]);
}
?>

</nav>
