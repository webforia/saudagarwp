<?php if (rt_option('header_html_4')): ?>
  <div class="rt-header__element rt-header-html-4">

    <?php echo wp_kses_post(do_shortcode(rt_option('header_html_4'))) ?>

  </div>
<?php endif?>