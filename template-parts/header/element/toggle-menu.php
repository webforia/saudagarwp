<?php if (rt_option('header_drawer_menu_style', 'dropdown') === 'sidepanel') : ?>
  <div class="rt-header__element rt-menu-toggle rt-menu-toggle--point js-slidepanel" data-target="#panel_mobile_menu">
    <span></span>
  </div>
<?php else : ?>
  <div class="rt-header__element rt-menu-toggle rt-menu-toggle--point js-animate" data-target="#mobile_menu_drawer">
    <span></span>
  </div>
<?php endif; ?>