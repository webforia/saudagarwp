<?php
$target = '';
$size = rt_option('header_button_1_size', 'md');

if (rt_option('header_button_1_link', 'blank') == 'blank') {
    $target = 'target="_blank"';
}
?>
<div  class="rt-header__element rt-header-btn-1">
  <a href="<?php echo esc_url(rt_option('header_button_1_link', '#')) ?>" class="rt-btn rt-btn--1 rt-btn--<?php echo esc_attr($size) ?>" <?php echo esc_attr($target) ?>>
    <?php echo rt_option('header_button_1_text', 'Button 1') ?>
  </a>
</div>
