<?php if (rt_option('header_html_2')) : ?>
  <div class="rt-header__element rt-header-html-2">

  <?php echo wp_kses_post(do_shortcode(rt_option('header_html_2'))) ?>

  </div>
<?php endif ?>