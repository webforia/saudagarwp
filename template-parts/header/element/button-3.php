<?php
$target = '';
$size = rt_option('header_button_3_size', 'md');

if (rt_option('header_button_3_link', 'blank')  == 'blank') {
  $target = 'target="_blank"';
}

?>
<div  class="rt-header__element rt-header-btn-3">
  <a href="<?php echo esc_url(rt_option('header_button_3_link', '#')) ?>" class="rt-btn rt-btn--3 rt-btn--<?php echo esc_attr($size )?>" <?php echo esc_attr($target)?>>
    <?php echo rt_option('header_button_3_text', 'Button 3') ?>
  </a>
</div>
