<?php
/** Options */
$elements = rt_get_theme('header_builder_option');
$sticky = (rt_is_premium()) ? rt_option('header_mobile_sticky', true) : 'false';
$overlay = (rt_option('header_overlay', false) && is_front_page()) ? 'is-overlay' : 'is-normal';
?>

<div id="header-mobile" class="rt-header-mobile js-header-mobile <?php echo $overlay?>"  data-sticky='<?php echo $sticky?>'>

  <?php do_action('rt_before_header_mobile_main');?>

  <div class="rt-header-mobile__main">
    <div class="page-container">
      
       <div id="header_mobile_left" class="rt-header-mobile__column" data-alignment="<?php esc_attr_e($elements['mobile_left_alignment']) ?>" data-display="<?php esc_attr_e($elements['mobile_left_display']) ?>">
         <?php if (!empty($elements['mobile_left_element'])) {
              foreach ($elements['mobile_left_element'] as $element) {
                  rt_get_template_part("header/element/{$element}");
              }
          }?>
      </div>
      
       <div id="header_mobile_center" class="rt-header-mobile__column" data-alignment="<?php esc_attr_e($elements['mobile_center_alignment']) ?>" data-display="<?php esc_attr_e($elements['mobile_center_display']) ?>">
          <?php if (!empty($elements['mobile_center_element'])) {
              foreach ($elements['mobile_center_element'] as $element) {
                  rt_get_template_part("header/element/{$element}");
              }
          }?>
      </div>

       <div id="header_mobile_right" class="rt-header-mobile__column" data-alignment="<?php esc_attr_e($elements['mobile_right_alignment']) ?>" data-display="<?php esc_attr_e($elements['mobile_right_display']) ?>">
          <?php if (!empty($elements['mobile_right_element'])) {
              foreach ($elements['mobile_right_element'] as $element) {
                  rt_get_template_part("header/element/{$element}");
              }
          }?>
      </div>

    </div>

  </div>

  <?php
  if (rt_option('header_drawer_menu_style', 'dropdown') === 'dropdown') {
      rt_get_template_part('header/header-drawer');
  }
  rt_get_template_part('header/header-search', ['id' => 'mobile_search']);
  ?>

</div>
