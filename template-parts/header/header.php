<?php

/**
 * Header - Aligned (Default)
 */
$sticky = (rt_is_premium()) ? rt_option('header_sticky', true) : false;
$overlay = (rt_option('header_overlay', false) && is_front_page()) ? 'is-overlay' : 'is-normal';
?>
<header class="rt-header js-header <?php echo $overlay?>" data-sticky='<?php echo $sticky?>'>

    <?php
    do_action('rt_before_header');

    if (rt_is_premium()) {
        rt_get_template_part('header/header-topbar');
        rt_get_template_part('header/header-middle');
    }
    rt_get_template_part('header/header-main');
    rt_get_template_part('header/header-sticky');
    rt_get_template_part('header/header-search', ['id' => 'main_search']);

    do_action('rt_after_header');
    ?>
</header>


<?php
if (rt_is_woocommerce('product') && rt_option('header_product', true) && rt_is_premium()) {
    rt_get_template_part('header/header-product');
} else {
    rt_get_template_part('header/header-mobile');
}
?>

<?php if (is_customize_preview()) : ?>
    <script>
        if (typeof toogleAnimate == 'function') {
            toogleAnimate();
        }
    </script>
<?php endif ?>