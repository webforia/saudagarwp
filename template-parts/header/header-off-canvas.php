<div id="off_canvas_menu" class="rt-sidepanel rt-sidepanel--left rt-menu-off-canvas">

    <div class="rt-sidepanel__overlay js-sidepanel-close"></div>

    <div class="rt-sidepanel__inner">

        <div class="rt-sidepanel__header">
            <a class="rt-sidepanel__close js-sidepanel-close">
                <i class="rt-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                        <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
                    </svg>
                </i>
            </a>
            <h4 class="rt-sidepanel__title"><?php echo rt_option('header_menu_off_canvas_panel_label', 'Menu') ?></h4>
        </div>

        <div class="rt-sidepanel__body pall-0">

            <nav class="rt-menu rt-menu--vertical menu-off-canvas js-menu">
                <?php
                wp_nav_menu([
                    'menu' => rt_option('header_menu_off_canvas_menu'),
                    'container' =>  '',
                    'menu_class' => 'rt-menu__main',
                    'fallback_cb' => 'rt_menu_fallback',
                ]);
                ?>
            </nav>

        </div>

    </div>

</div>