<?php

/** Options */
$elements = rt_get_theme('header_builder_option');

$sticky = (rt_is_premium()) ? rt_option('header_sticky', true) : false;
?>

<div id="header-mobile" class="rt-header-mobile js-header-mobile" data-sticky='<?php esc_attr_e($sticky) ?>'>

  <div class="rt-header-mobile__main">
    <div class="page-container">

      <div id="header_mobile_left" class="rt-header-mobile__column">
        <a class="rt-header__element" href="<?php echo wc_get_page_permalink('shop'); ?>">
          <span class="rt-header-cart-icon">
            <i class="rt-icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
              </svg>
            </i>
          </span>
        </a>
      </div>

      <div id="header_mobile_center" class="rt-header-mobile__column">
        <h6 class="rt-header__element rt-header-html mb-0"> <?php echo rt_the_title(20) ?></h6>
      </div>

      <div id="header_mobile_right" class="rt-header-mobile__column">
        <a class="rt-header__element rt-header-cart js-slidepanel mr-5" data-target="#panel_minicart">
          <i class="rt-icon rt-header-cart-icon js-slidepanel" data-target="#panel_minicart">
            <svg width="1em" height="1em" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/">
              <path d="M12.005,12.002C10.905,12.002 10.01,12.899 10.01,14.005C10.01,15.111 10.905,16.008 12.005,16.008C13.106,16.008 14.001,15.111 14.001,14.005C14.001,12.899 13.106,12.002 12.005,12.002ZM4.993,12.002C3.892,12.002 2.997,12.899 2.997,14.005C2.997,15.111 3.892,16.008 4.993,16.008C6.093,16.008 6.988,15.111 6.988,14.005C6.988,12.899 6.093,12.002 4.993,12.002ZM12.005,13.002C12.555,13.002 13.001,13.452 13.001,14.005C13.001,14.558 12.555,15.008 12.005,15.008C11.455,15.008 11.01,14.558 11.01,14.005C11.01,13.452 11.455,13.002 12.005,13.002ZM4.993,13.002C5.543,13.002 5.988,13.452 5.988,14.005C5.988,14.558 5.543,15.008 4.993,15.008C4.443,15.008 3.997,14.558 3.997,14.005C3.997,13.452 4.443,13.002 4.993,13.002ZM0,0.785C-0,0.511 0.226,0.285 0.5,0.285L2,0.285C2.229,0.285 2.43,0.442 2.485,0.664L2.89,2.285L14.5,2.285C14.774,2.285 15,2.511 15,2.785C15,2.818 14.996,2.851 14.99,2.883L13.99,7.883C13.945,8.106 13.753,8.272 13.525,8.284L4.128,8.756L4.415,10.285L13,10.285C13.274,10.285 13.5,10.511 13.5,10.785C13.5,11.059 13.274,11.285 13,11.285L4,11.285C3.76,11.285 3.553,11.112 3.509,10.877L2.01,2.892L1.61,1.285L0.5,1.285C0.226,1.285 0,1.059 0,0.785ZM3.102,3.285L3.942,7.764L13.086,7.305L13.89,3.285L3.102,3.285Z"></path>
            </svg>
          </i>
          <span class="rt-cart-item-count js-cart-total"><?php echo rt_get_cart_contents_count() ?></span>
        </a>
      </div>

    </div>

  </div>

</div>