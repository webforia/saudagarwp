<?php if (rt_option('header_layout_builder_elementor') && rt_option('header_layout_builder_elementor') != get_the_ID() && did_action( 'elementor/loaded' )): ?>

<div class="rt-header-builder">
    <?php echo Elementor\Plugin::instance()->frontend->get_builder_content_for_display(rt_option('header_layout_builder_elementor')); ?>
</div>

<?php endif?>


