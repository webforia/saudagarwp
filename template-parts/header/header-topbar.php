<?php if (rt_option('header_topbar', false)): ?>

<?php $elements = rt_get_theme('header_builder_option'); ?>

  <?php do_action('rt_before_header_topbar') ?>
  <div id="header-topbar" class="rt-header__topbar">
      <div class="page-container">

        <div id="header_topbar_left" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['topbar_left_alignment']) ?>" data-display="<?php esc_attr_e($elements['topbar_left_display']) ?>">
           <?php if (!empty($elements['topbar_left_element'])) {
                foreach ($elements['topbar_left_element'] as $element) {
                    rt_get_template_part("header/element/{$element}");
                }
            }?>
        </div>
        
        <div id="header_topbar_center" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['topbar_center_alignment']) ?>" data-display="<?php esc_attr_e($elements['topbar_center_display']) ?>">
           <?php if (!empty($elements['topbar_center_element'])) {
                foreach ($elements['topbar_center_element'] as $element) {
                    rt_get_template_part("header/element/{$element}");
                }
            }?>
        </div>

        <div id="header_topbar_right" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['topbar_right_alignment']) ?>" data-display="<?php esc_attr_e($elements['topbar_right_display']) ?>">
            <?php if (!empty($elements['topbar_right_element'])) {
                foreach ($elements['topbar_right_element'] as $element) {
                    rt_get_template_part("header/element/{$element}");
                }
            }?>
        </div>


      </div>
  </div>
  <?php do_action('rt_after_header_topbar') ?>
  
<?php endif; ?>
