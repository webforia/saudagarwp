<?php 
$classes[] = 'rt-main-canvas-menu rt-main-canvas-menu--dropdown'; 
$classes[] = 'rt-main-canvas-menu--'.rt_option('header_drawer_menu_schema', 'dark');
?>

<div id="mobile_menu_drawer" class="<?php esc_attr_e( implode(' ', $classes) )?>" data-animatein="slideDown" data-animateout="slideUp">
    <?php if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('mobile_drawer')): 
    $elements = rt_get_theme('header_builder_option');
    foreach ($elements['drawer_element'] as $element) {
        rt_get_template_part("header/element/{$element}");
    } 
    endif ?>
</div>