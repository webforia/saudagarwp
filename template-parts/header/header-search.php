<div id="<?php echo $args['id'] ?>" class="rt-search" data-animatein="slideInDown" data-animateout="slideOutUp">

  <div class="page-container">


    <i class="rt-icon rt-search__icon rt-search__close">
      <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-x-lg js-animate-close" viewBox="0 0 16 16">
        <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
      </svg>
    </i>

    <form class="rt-search__inner" action="<?php echo esc_url(home_url('/')); ?>" method="get">

      <i class="rt-icon rt-search__icon">
        <svg width="1em" height="1em" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/">
          <path d="M10.719,11.426L15.126,15.833C15.32,16.027 15.635,16.027 15.829,15.833C15.831,15.831 15.833,15.83 15.834,15.828C16.029,15.634 16.029,15.319 15.834,15.125L11.426,10.716C12.397,9.58 12.984,8.105 12.984,6.494C12.984,2.904 10.069,-0.01 6.479,-0.01C2.89,-0.01 -0.025,2.904 -0.025,6.494C-0.025,10.084 2.89,12.999 6.479,12.999C8.099,12.999 9.58,12.406 10.719,11.426ZM6.479,0.99C9.517,0.99 11.984,3.456 11.984,6.494C11.984,9.532 9.517,11.999 6.479,11.999C3.441,11.999 0.975,9.532 0.975,6.494C0.975,3.456 3.441,0.99 6.479,0.99Z" />
        </svg>
      </i>

      <input type="search" name="s" value="<?php the_search_query(); ?>" class="rt-search__input" placeholder="<?php echo rt_option('search_main_text', 'Type Something and enter') ?>">
      <button type="submit" class="rt-search__btn"><?php _e('Search', 'saudagarwp') ?></button>

      <input type="hidden" name="post_type" value="<?php echo rt_option('search_main_result', 'product') ?>" />

    </form>

  </div>

</div>