<?php $elements = rt_get_theme('header_builder_option');?>
<?php do_action('rt_before_header_main')?>

<div id="header-main" class="rt-header__main">
    <div class="page-container">

       <div id="header_main_left" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['main_left_alignment']) ?>" data-display="<?php esc_attr_e($elements['main_left_display']) ?>">
          <?php if (!empty($elements['main_left_element'])) {
                foreach ($elements['main_left_element'] as $element) {
                    rt_get_template_part("header/element/{$element}");
                }
            }?>
      </div>

       <div id="header_main_center" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['main_center_alignment']) ?>" data-display="<?php esc_attr_e($elements['main_center_display']) ?>">
          <?php if (!empty($elements['main_center_element'])) {
                foreach ($elements['main_center_element'] as $element) {
                    rt_get_template_part("header/element/{$element}");
                }
            }?>
      </div>

       <div id="header_main_right" class="rt-header__column" data-alignment="<?php esc_attr_e($elements['main_right_alignment']) ?>" data-display="<?php esc_attr_e($elements['main_right_display']) ?>">
          <?php if (!empty($elements['main_right_element'])) {
                foreach ($elements['main_right_element'] as $element) {
                    rt_get_template_part("header/element/{$element}");
                }
            }?>
      </div>

    </div>
  </div>
<?php do_action('rt_after_header_main')?>
