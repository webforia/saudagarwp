<div class="rt-entry-content">
	<p class="mt-20">
	<?php 
	if(is_home() && current_user_can('publish_posts')){
		echo sprintf(esc_html__('Ready to publish your first post? %1$sGet started here%2$s.', 'saudagarwp'), '<a href="'. esc_url(admin_url('post-new.php')) .'" target="_blank">', '</a>');
	}elseif(is_search()){
		esc_html_e("Sorry, but nothing matched your search terms. Please try again with different keywords.", 'saudagarwp');
		get_template_part('searchform');
	}elseif(is_category()){
		esc_html_e("There aren't any posts currently published in this category.", 'saudagarwp');
	}elseif(is_tax()) {
		esc_html_e("There aren't any posts currently published under this taxonomy.", 'saudagarwp');
	}elseif(is_tag()){
		esc_html_e("There aren't any posts currently published under this tag.", 'saudagarwp'); 
	}else{
		esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for.', 'saudagarwp');
	}
	?>
	</p>
</div><!-- .page-content -->