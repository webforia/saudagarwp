<div class="rt-page rt-entry-content">
	<?php

	the_content();

	wp_link_pages([
		'before' => '<div class="rt-page-links">' . esc_html__('Pages:', 'saudagarwp'),
		'after'  => '</div>',
	]);
	?>
</div>