 <?php
  $account_url = '#';
  $trigger_modal_class = '';

  if (rt_is_premium()) {
    $trigger_modal_class = 'js-modal';
  }

  if (is_user_logged_in() || rt_is_woocommerce('account') || !rt_is_premium() || rt_option('woocommerce_user_login', 'modal') == 'account-page') {
    $account_url = get_permalink(get_option('woocommerce_myaccount_page_id'));
    $trigger_modal_class = '';
  }
  ?>
 <?php if (rt_is_woocommerce()) : ?>
   <div class="rt-shop-navbar__item">
     <a class="<?php echo esc_attr($trigger_modal_class) ?>" href="<?php echo esc_url($account_url) ?>" data-target="#modal_login">
       <span class="rt-shop-navbar__icon">
         <i class="rt-icon">
           <svg width="1em" height="1em" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/">
             <g transform="matrix(1.02129,0,0,1.18173,-0.168648,-0.696392)">
               <path d="M2.362,13.719C4.968,13.719 11.039,13.719 13.645,13.686C14.101,13.686 14.536,13.493 14.804,13.167C15.073,12.84 15.142,12.422 14.99,12.044C14.99,12.042 14.989,12.041 14.989,12.04C13.904,9.426 11.191,8.205 8,8.205C4.812,8.205 2.1,9.424 0.978,12.024C0.977,12.027 0.975,12.03 0.974,12.033C0.818,12.421 0.889,12.851 1.165,13.185C1.441,13.52 1.887,13.719 2.362,13.719L2.362,13.719ZM2.362,12.872C2.362,12.872 2.362,12.872 2.362,12.872C2.203,12.872 2.055,12.806 1.963,12.695C1.873,12.585 1.848,12.445 1.896,12.318C2.87,10.068 5.243,9.051 8,9.051C10.756,9.051 13.128,10.066 14.066,12.322C14.113,12.442 14.091,12.573 14.006,12.676C13.922,12.779 13.784,12.84 13.638,12.84C13.636,12.84 13.633,12.84 13.631,12.84C11.028,12.872 4.965,12.872 2.362,12.872Z" />
             </g>
             <g transform="matrix(1.03322,0,0,1.03322,-2.12109,-2.68141)">
               <path d="M9.779,2.986C7.757,2.986 6.116,4.628 6.116,6.649C6.116,8.671 7.757,10.312 9.779,10.312C11.8,10.312 13.441,8.671 13.441,6.649C13.441,4.628 11.8,2.986 9.779,2.986ZM9.779,3.954C11.266,3.954 12.473,5.162 12.473,6.649C12.473,8.136 11.266,9.344 9.779,9.344C8.291,9.344 7.084,8.136 7.084,6.649C7.084,5.162 8.291,3.954 9.779,3.954Z" />
             </g>
           </svg>
         </i>
       </span>
       <span class="rt-shop-navbar__label"><?php echo __('Account', 'saudagarwp') ?></span>
     </a>
   </div>
 <?php endif ?>