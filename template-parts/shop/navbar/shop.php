<?php if (rt_is_woocommerce('shop')) : ?>
    <div class="rt-shop-navbar__item">
        <a href="<?php echo get_home_url(); ?>">
            <span class="rt-shop-navbar__icon">
                <i class="rt-icon">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
                        <path d="M1.725,7.51L1.049,8.166C0.951,8.261 0.818,8.315 0.679,8.315C0.392,8.315 0.155,8.085 0.155,7.806C0.155,7.671 0.211,7.542 0.309,7.447L7.261,0.695C7.666,0.301 8.334,0.301 8.739,0.695L15.691,7.447C15.789,7.542 15.845,7.671 15.845,7.806C15.845,8.085 15.608,8.315 15.321,8.315C15.182,8.315 15.049,8.261 14.951,8.166L14.275,7.51L14.275,13.859C14.275,14.695 13.567,15.383 12.706,15.383L3.294,15.383C2.433,15.383 1.725,14.695 1.725,13.859L1.725,7.51ZM2.771,6.493L8,1.413L13.229,6.493L13.229,13.859C13.229,14.138 12.993,14.367 12.706,14.367L10.909,14.367L10.909,10.447C10.909,9.605 10.207,8.923 9.34,8.923C8.545,8.923 7.455,8.923 6.66,8.923C5.793,8.923 5.091,9.605 5.091,10.447L5.091,14.367L3.294,14.367C3.007,14.367 2.771,14.138 2.771,13.859L2.771,6.493ZM9.863,14.367L6.137,14.367L6.137,10.447C6.137,10.166 6.371,9.939 6.66,9.939L9.34,9.939C9.629,9.939 9.863,10.166 9.863,10.447L9.863,14.367Z" />
                    </svg>
                </i>
            </span>
            <span class="rt-shop-navbar__label"><?php echo __('Home', 'saudagarwp') ?></span>
        </a>
    </div>
<?php else : ?>
    <div class="rt-shop-navbar__item">
        <a href="<?php echo wc_get_page_permalink('shop'); ?>">
            <span class="rt-shop-navbar__icon">
                <i class="rt-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-shop" viewBox="0 0 16 16">
                        <path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h1v-5a1 1 0 0 1 1-1h3a1 1 0 0 1 1 1v5h6V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zM4 15h3v-5H4v5zm5-5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1v-3zm3 0h-2v3h2v-3z" />
                    </svg>
                </i>
            </span>
            <span class="rt-shop-navbar__label"><?php echo __('Shop', 'saudagarwp') ?></span>
        </a>
    </div>
<?php endif; ?>