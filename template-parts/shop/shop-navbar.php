<?php
// get shopbar items
$navbars = rt_option('woocommerce_shop_navbar_bottom', array('shop', 'filter', 'user', 'cart'));
?>
<?php if(count($navbars) > 0): ?>
<div class="rt-shop-navbar">

    <?php 
    do_action('rt_before_shop_navbar'); 

    foreach ($navbars as $key => $navbar) {
        rt_get_template_part("shop/navbar/{$navbar}");
    }

    // Enable if Webforia WA Chat ready
    if(function_exists('wwc_chat_trigger') && get_option('_whatsapp_chat_widget') == true){
      rt_get_template_part("shop/navbar/chat");
    }
    
    do_action('rt_after_shop_navbar'); 
    ?>
 
</div>
<?php endif ?>