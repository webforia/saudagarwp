<div id="panel_minicart" class="rt-panel-cart rt-sidepanel">

    <div class="rt-sidepanel__overlay js-sidepanel-close"></div>

    <div class="rt-sidepanel__inner">

        <div class="rt-sidepanel__header">
            <a class="rt-sidepanel__close js-sidepanel-close">
                <i class="rt-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                        <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
                    </svg>
                </i>
            </a>
            <h4 class="rt-sidepanel__title"><?php echo __('Shopping Cart', 'saudagarwp') ?></h4>
        </div>

        <div class="rt-sidepanel__body woocommerce widget_shopping_cart">
            <div class="widget_shopping_cart_content">
                <?php woocommerce_mini_cart(); ?>
            </div>
        </div>

    </div>

</div>