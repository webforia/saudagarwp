<!DOCTYPE html>
<html class="no-js" <?php language_attributes()?>>
  <head>

     <meta name="viewport" content="width=device-width,initial-scale=1">

    <?php wp_head();?>
  </head>
  <body <?php body_class('template--focus');?>>
    <?php wp_body_open()?>

    <div class='ptf-wrapper'>

       <header class="ptf-header">
          <div class="page-container">
            <?php
            if (!empty(rt_option('focus_logo_primary'))) {
                $logo = rt_option('focus_logo_primary');
            } else {
                $logo = rt_option('brand_logo_primary');
            }
            ?>
            <a href="<?php echo esc_url(home_url()) ?>" class="rt-logo">
               <?php if ($logo): ?>
                  <img src="<?php echo esc_attr($logo) ?>" alt="<?php echo get_bloginfo('name') ?>">
               <?php else: ?>
                  <span class="rt-logo__sitename"><?php echo get_bloginfo('name') ?></span>
               <?php endif?>
            </a>

          </div>
       </header>

       <div class="ptf-body">

          <div class="page-container">

            <?php if (have_posts()): ?>

                  <?php while (have_posts()): the_post();?>

		                <?php the_content()?>

		                <?php endwhile;?>

            <?php else: ?>

              <?php do_action('rt_post_none')?>

            <?php endif;?>

          </div>

       </div>

       <footer class="ptf-footer">
          <div class="page-container">
              <?php rt_get_template_part('footer/element/html-1');?>
          </div>
       </footer>

    </div>

    <?php wp_footer();?>

  </body>
</html>