<div id="panel_product_filter" class="rt-shop-filter-slidepanel rt-sidepanel rt-sidepanel--left">

    <div class="rt-sidepanel__overlay js-sidepanel-close"></div>

    <div class="rt-sidepanel__inner">

        <div class="rt-sidepanel__header">
            <a class="rt-sidepanel__close js-sidepanel-close">
                <i class="rt-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                        <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
                    </svg>
                </i>
            </a>
            <h4 class="rt-sidepanel__title"><?php _e('Filter', 'saudagarwp') ?></h4>
        </div>

        <div class="rt-sidepanel__body">
            <?php
            // sidebar widget filter
            if (is_active_sidebar('retheme_woocommerce_filter')) {
                dynamic_sidebar('retheme_woocommerce_filter');
            } elseif (is_active_sidebar('retheme_woocommerce_sidebar')) {
                dynamic_sidebar('retheme_woocommerce_sidebar');
            }
            ?>
        </div>

    </div>

</div>