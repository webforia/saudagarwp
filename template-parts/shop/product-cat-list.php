<?php 
$terms = get_terms(array(
    'taxonomy' => 'product_cat',
    'hide_empty' => rt_option('woocommerce_archive_product_cat_list_empty', false),
));

?>
<?php if($terms && !is_wp_error($terms )): ?>
    <div class="rt-shop-categories">
    <?php foreach($terms as $key => $term): ?>
        <?php 
        if($term->slug == 'uncategorized') {
            continue;
        }
        ?>
        <a href="<?php echo get_term_link($term)?>" class="rt-btn rt-btn--border rt-btn--sm <?php echo "product-cat-{$term->slug}"?>"><?php echo $term->name ?></a>
    <?php endforeach ?>
    </div>
<?php endif ?>
