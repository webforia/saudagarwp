<?php

/**
 * Login Form Modal
 *
 * This template for modal popup if user icon cliked
 *
 * @package Retheme/Templates
 * @version 1.0.2
 */
?>

<div id="modal_login" class="rt-modal js-account-form" data-animatein='zoomIn' data-animateout='zoomOut'>

	<div class="rt-modal__overlay js-modal-close"></div>

	<div class="rt-modal__inner">

		<div class="rt-modal__header">
			<h3 class="rt-modal__title">
				<span class="account-login-title"><?php esc_html_e('Login', 'woocommerce') ?></span>
				<span class="account-register-title" style="display: none"><?php esc_html_e('Register', 'woocommerce') ?></span>
			</h3>
			<span class="rt-modal__close js-modal-close">
				<i class="rt-icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
						<path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8 2.146 2.854Z" />
					</svg>
				</i>
			</span>
		</div>

		<div class="rt-modal__body">

			<?php do_action('woocommerce_before_customer_login_form'); ?>

			<div class="rt-form-login">

				<form class="woocommerce-form login" method="post">

					<?php do_action('woocommerce_login_form_start'); ?>

					<p class="rt-form rt-form--overlay js-form-overlay woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label class="rt-form__label" for="username"><?php esc_html_e('Username or email address', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
						<input type="text" class="rt-form__input woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" required /><?php // @codingStandardsIgnoreLine 
																																																										?>
					</p>

					<p class="rt-form rt-form--overlay js-form-overlay woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide password-input">
						<label class="rt-form__label" for="password"><?php esc_html_e('Password', 'woocommerce'); ?><span class="required">*</span></label>
						<input class="rt-form__input woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password" required />
					</p>

					<?php do_action('woocommerce_login_form'); ?>

					<p class="flex flex-middle flex-between">
						<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
							<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e('Remember me', 'woocommerce'); ?></span>
						</label>
						<a class="lost_password link-text" href="<?php echo esc_url(wp_lostpassword_url()); ?>">
							<i class="rt-icon">
								<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-life-preserver" viewBox="0 0 16 16">
									<path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm6.43-5.228a7.025 7.025 0 0 1-3.658 3.658l-1.115-2.788a4.015 4.015 0 0 0 1.985-1.985l2.788 1.115zM5.228 14.43a7.025 7.025 0 0 1-3.658-3.658l2.788-1.115a4.015 4.015 0 0 0 1.985 1.985L5.228 14.43zm9.202-9.202-2.788 1.115a4.015 4.015 0 0 0-1.985-1.985l1.115-2.788a7.025 7.025 0 0 1 3.658 3.658zm-8.087-.87a4.015 4.015 0 0 0-1.985 1.985L1.57 5.228A7.025 7.025 0 0 1 5.228 1.57l1.115 2.788zM8 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
								</svg>
							</i>
							<?php esc_html_e('Lost your password?', 'woocommerce'); ?>
						</a>
					</p>

					<div class="rt-form__group">
						<?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
						<button type="submit" class="rt-btn rt-btn--primary woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e('Login', 'woocommerce'); ?>"><?php esc_html_e('Login', 'woocommerce'); ?></button>
					</div>

					<?php do_action('woocommerce_login_form_end'); ?>

				</form>

				<?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>
					<p class="rt-account-link-login mt-15"><?php _e('Not a member?', 'saudagarwp') ?>

						<?php if (rt_option('woocommerce_custom_register_link')) : ?>
							<a href="<?php echo esc_url(rt_option('woocommerce_custom_register_link')) ?>"><?php _e('Join now.', 'saudagarwp') ?></a>
						<?php else : ?>
							<a class="js-account-register"><?php _e('Join now.', 'saudagarwp') ?></a>
						<?php endif; ?>
					</p>
				<?php endif ?>
			</div>

			<?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>

				<div class="rt-form-register" style="display: none;">


					<form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action('woocommerce_register_form_tag'); ?>>

						<?php do_action('woocommerce_register_form_start'); ?>

						<?php if ('no' === get_option('woocommerce_registration_generate_username')) : ?>

							<p class="rt-form rt-form--overlay js-form-overlay woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
								<label class="rt-form__label" for="reg_username"><?php esc_html_e('Username', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
								<input type="text" class="rt-form__input woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" required /><?php // @codingStandardsIgnoreLine 																																																																?>
							</p>

						<?php endif; ?>

						<p class="rt-form rt-form--overlay js-form-overlay woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
							<label class="rt-form__label" for="reg_email"><?php esc_html_e('Email address', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
							<input type="email" class="rt-form__input woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" autocomplete="email" value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>" required /><?php // @codingStandardsIgnoreLine 																																																													?>
						</p>

						<?php if ('no' === get_option('woocommerce_registration_generate_password')) : ?>

							<p class="rt-form rt-form--overlay js-form-overlay woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide password-input">
								<label class="rt-form__label" for="reg_password"><?php esc_html_e('Password', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
								<input type="password" class="rt-form__input woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" autocomplete="new-password" required />
							</p>

						<?php endif; ?>

						<?php do_action('woocommerce_register_form'); ?>

						<p class="woocommerce-FormRow form-row">
							<?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
							<button type="submit" class="rt-btn rt-btn--primary woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e('Register', 'woocommerce'); ?>"><?php esc_html_e('Register', 'woocommerce'); ?></button>
						</p>

						<?php do_action('woocommerce_register_form_end'); ?>

					</form>

					<p class="rt-account-link-login"><?php _e('Already a member?', 'saudagarwp') ?> <a href="#" class="js-account-login"><?php _e('Sign in.', 'saudagarwp') ?></a></p>


				</div>

			<?php endif; ?>


			<?php do_action('woocommerce_after_customer_login_form'); ?>


		</div>

	</div>
</div>