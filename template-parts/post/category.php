<?php if (!empty(get_the_category()) && rt_post_option('category')): ?>
<div class="rt-badges">
  <?php foreach (get_the_category() as $term) : ?>
    <a class="<?php echo esc_attr( $term->slug ) ?>" href="<?php echo esc_url(get_tag_link($term->term_id)) ?>">
      <?php echo $term->name ?>
    </a>
  <?php endforeach; ?>
</div>
<?php endif ?>
