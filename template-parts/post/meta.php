<?php
$date_url   = esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')));
$author_url = get_author_posts_url(get_the_author_meta('ID'));
$avatar     = get_avatar(get_the_author_meta('ID'), 30);
$avatar_url = get_the_author_meta('profile_avatar', get_the_author_meta('ID'));

?>
<?php if (rt_option('blog_meta', ['meta-author', 'meta-date', 'meta-comment'])) : ?>
  <div class="rt-post__meta">

    <?php if (rt_post_option('meta-date')) : ?>
      <a class="rt-post__meta-item date" href="<?php echo esc_url($date_url) ?>">
        <i class="rt-icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-calendar3" viewBox="0 0 16 16">
            <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"></path>
            <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
          </svg>
        </i>

        <?php echo get_the_date() ?>
      </a>
    <?php endif; ?>

    <?php if (rt_post_option('meta-author')) : ?>
      <a class="rt-post__meta-item author" href="<?php echo esc_url($author_url) ?>">
        <i class="rt-icon">
          <svg width="1em" height="1em" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/">
            <g transform="matrix(1.02129,0,0,1.18173,-0.168648,-0.696392)">
              <path d="M2.362,13.719C4.968,13.719 11.039,13.719 13.645,13.686C14.101,13.686 14.536,13.493 14.804,13.167C15.073,12.84 15.142,12.422 14.99,12.044C14.99,12.042 14.989,12.041 14.989,12.04C13.904,9.426 11.191,8.205 8,8.205C4.812,8.205 2.1,9.424 0.978,12.024C0.977,12.027 0.975,12.03 0.974,12.033C0.818,12.421 0.889,12.851 1.165,13.185C1.441,13.52 1.887,13.719 2.362,13.719L2.362,13.719ZM2.362,12.872C2.362,12.872 2.362,12.872 2.362,12.872C2.203,12.872 2.055,12.806 1.963,12.695C1.873,12.585 1.848,12.445 1.896,12.318C2.87,10.068 5.243,9.051 8,9.051C10.756,9.051 13.128,10.066 14.066,12.322C14.113,12.442 14.091,12.573 14.006,12.676C13.922,12.779 13.784,12.84 13.638,12.84C13.636,12.84 13.633,12.84 13.631,12.84C11.028,12.872 4.965,12.872 2.362,12.872Z" />
            </g>
            <g transform="matrix(1.03322,0,0,1.03322,-2.12109,-2.68141)">
              <path d="M9.779,2.986C7.757,2.986 6.116,4.628 6.116,6.649C6.116,8.671 7.757,10.312 9.779,10.312C11.8,10.312 13.441,8.671 13.441,6.649C13.441,4.628 11.8,2.986 9.779,2.986ZM9.779,3.954C11.266,3.954 12.473,5.162 12.473,6.649C12.473,8.136 11.266,9.344 9.779,9.344C8.291,9.344 7.084,8.136 7.084,6.649C7.084,5.162 8.291,3.954 9.779,3.954Z" />
            </g>
          </svg>
        </i>
        <?php the_author() ?>
      </a>
    <?php endif; ?>

    <?php if (rt_post_option('meta-comment') && get_comments_number() >= 1) : ?>
      <span class="rt-post__meta-item comment">
        <i class="rt-icon">
          <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-chat" viewBox="0 0 16 16">
            <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z" />
          </svg>
        </i>
        <?php echo get_comments_number() ?>
      </span>
    <?php endif ?>

  </div>

<?php endif ?>