<?php if (rt_post_option('content')): ?>
<div class="rt-post__content">
  <?php rt_the_content(rt_option('blog_archive_excerpt', 18)) ?>
</div>
<?php endif ?>