<?php if (rt_post_option('thumbnail')): ?>
<div class="rt-post__thumbnail">
  <?php rt_the_post_thumbnail('featured_medium');?>
</div>
<?php endif?>