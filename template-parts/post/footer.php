<?php if (rt_post_option('readmore')) : ?>
  <div class="rt-post__footer">
    <a href="<?php the_permalink() ?>" class="rt-post__readmore">
      <?php echo __('Read More', 'saudagarwp') ?>
      <i class="rt-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z" />
        </svg>
      </i>
    </a>
  </div>
<?php endif ?>