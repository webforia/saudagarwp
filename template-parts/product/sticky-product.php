<?php

$product = wc_get_product(get_the_ID());
$product_type_class = "type-product product-type-{$product->get_type()}";
?>

<div class="rt-sticky-product js-sticky-product <?php echo $product_type_class ?>">
    <div class="page-container">
        <div class="rt-sticky-product__item">
            <div class="rt-img rt-img--full main_product_thumbnail">
                <?php the_post_thumbnail('thumbnail'); ?>
            </div>
            <div class="rt-sticky-product__body">
                <h6 class="rt-sticky-product__title product_title"><?php echo $product->get_title(); ?></h6>
                <div class="rt-sticky-product__price product_price"><?php echo $product->get_price_html(); ?></div>
            </div>
        </div>
        <div class="rt-sticky-product__action">

            <?php if (function_exists('wwc_chat_trigger')) {
                wwc_chat_trigger();
            } ?>

            <?php if ($product->is_in_stock() || $product->is_type('variable')) : ?>
                <?php if ($product->is_type('external')) : ?>
                    <a class="rt-btn rt-btn--action button" href="<?php echo esc_url($product->add_to_cart_url()) ?>"><?php echo $product->single_add_to_cart_text() ?></a>
                <?php else : ?>
                    <a class="rt-btn rt-btn--action single_add_to_cart_button button"><?php echo esc_html($product->single_add_to_cart_text()); ?></a>
                <?php endif ?>

                <?php if (function_exists('wwc_product_checkout')) {
                    wwc_product_checkout();
                } ?>
            <?php else : ?>
                <p class="rt-btn rt-btn--second rt-btn--block is-disabled mr-15"><?php echo __('Out of stock', 'woocommerce') ?></p>
            <?php endif ?>
        </div>
    </div>
</div>