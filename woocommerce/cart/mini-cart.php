<?php

/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 9.4.0
 */
if (!defined('ABSPATH')) {
	exit;
} ?>
<div class="rt-mini-cart">

	<?php wc_print_notices(); ?>

	<?php do_action('woocommerce_before_mini_cart'); ?>
	
	<?php if (WC()->cart && ! WC()->cart->is_empty()) : ?>

		<div class="rt-mini-cart__body">

			<?php
			do_action('woocommerce_before_mini_cart_contents');

			get_template_part('woocommerce/cart/mini-cart-item');

			do_action('woocommerce_mini_cart_contents');
			?>
		</div>

		<div class="rt-mini-cart__footer">

			<p class="woocommerce-mini-cart__total total">
				<?php
				/**
				 * Hook: woocommerce_widget_shopping_cart_total.
				 *
				 * @hooked woocommerce_widget_shopping_cart_subtotal - 10
				 */
				do_action('woocommerce_widget_shopping_cart_total');
				?>
			</p>

			<?php do_action('woocommerce_widget_shopping_cart_before_buttons'); ?>

			<p class="woocommerce-mini-cart__buttons buttons"><?php do_action( 'woocommerce_widget_shopping_cart_buttons' ); ?></p>


			<?php do_action('woocommerce_widget_shopping_cart_after_buttons'); ?>

		</div>

	<?php else : ?>

		<div class="rt-mini-cart__empty">
			<i class="rt-icon">
				<svg width="1em" height="1em" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/">
					<path d="M12.005,12.002C10.905,12.002 10.01,12.899 10.01,14.005C10.01,15.111 10.905,16.008 12.005,16.008C13.106,16.008 14.001,15.111 14.001,14.005C14.001,12.899 13.106,12.002 12.005,12.002ZM4.993,12.002C3.892,12.002 2.997,12.899 2.997,14.005C2.997,15.111 3.892,16.008 4.993,16.008C6.093,16.008 6.988,15.111 6.988,14.005C6.988,12.899 6.093,12.002 4.993,12.002ZM12.005,13.002C12.555,13.002 13.001,13.452 13.001,14.005C13.001,14.558 12.555,15.008 12.005,15.008C11.455,15.008 11.01,14.558 11.01,14.005C11.01,13.452 11.455,13.002 12.005,13.002ZM4.993,13.002C5.543,13.002 5.988,13.452 5.988,14.005C5.988,14.558 5.543,15.008 4.993,15.008C4.443,15.008 3.997,14.558 3.997,14.005C3.997,13.452 4.443,13.002 4.993,13.002ZM0,0.785C-0,0.511 0.226,0.285 0.5,0.285L2,0.285C2.229,0.285 2.43,0.442 2.485,0.664L2.89,2.285L14.5,2.285C14.774,2.285 15,2.511 15,2.785C15,2.818 14.996,2.851 14.99,2.883L13.99,7.883C13.945,8.106 13.753,8.272 13.525,8.284L4.128,8.756L4.415,10.285L13,10.285C13.274,10.285 13.5,10.511 13.5,10.785C13.5,11.059 13.274,11.285 13,11.285L4,11.285C3.76,11.285 3.553,11.112 3.509,10.877L2.01,2.892L1.61,1.285L0.5,1.285C0.226,1.285 0,1.059 0,0.785ZM3.102,3.285L3.942,7.764L13.086,7.305L13.89,3.285L3.102,3.285Z"></path>
				</svg>
			</i>
			<p class="woocommerce-mini-cart__empty-message"><?php _e('No products in the cart.', 'woocommerce'); ?></p>
			<a class="rt-btn rt-btn--border" href="<?php echo  get_permalink(wc_get_page_id('shop')) ?>"><?php echo __('Return to shop', 'woocommerce'); ?></a>
		</div>

	<?php endif; ?>

	<input type="hidden" name="cart_count_total" value="<?php echo WC()->cart->get_cart_contents_count() ?>">
	<input type="hidden" name="currency" value="<?php echo get_woocommerce_currency_symbol();  ?>">
	<?php wp_nonce_field('retheme_minicart_nonce', 'retheme_minicart_nonce_field'); ?>

	<?php do_action('woocommerce_after_mini_cart'); ?>
</div>