<?php
foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
    $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
    $product_name = '';
    $product_attribute = '';
    

    if ($_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
        
        // get name
        if($_product->get_parent_id()){
            $product_parent = wc_get_product($_product->get_parent_id());
            $product_name = apply_filters('woocommerce_cart_item_name', rt_limited_string($product_parent->get_name(), 8), $cart_item, $cart_item_key);
            $product_attribute = implode(', ', $_product->get_attributes());

        }else{
            $product_name = apply_filters('woocommerce_cart_item_name', rt_limited_string($_product->get_name(), 8), $cart_item, $cart_item_key);
        }

        $thumbnail         	= apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image('thumbnail'), $cart_item, $cart_item_key );
        $product_price     	= apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
        $product_permalink 	= apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
        $product_subtotal 	= apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
        $product_price_quantity = apply_filters('woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf('%s &times; %s', $cart_item['quantity'], $product_price) . '</span>', $cart_item, $cart_item_key);

        // quantity
        if($_product->is_sold_individually()) {
            $product_quantity = sprintf('<input type="hidden" name="quantity" value="1" />', $cart_item_key);
        } elseif($_product->get_max_purchase_quantity() === 1){
            $product_quantity = wc_get_stock_html($_product);
        }else{
            $product_quantity = woocommerce_quantity_input([
                'input_name' => "quantity",
                'input_value' => $cart_item['quantity'],
                'max_value' => $_product->get_max_purchase_quantity(),
                'min_value' => '1',
                'product_name' => $_product->get_name(),
            ], $_product, false);
        }

        $product_quantity = apply_filters('woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item); // PHPCS: XSS ok.

        ?>

        <div id="cart-item-<?php echo esc_attr($_product->get_id());?>" class="rt-cart-item js-cart-item">
            <div class="rt-cart-item__thumbnail rt-img rt-img-full">
                <?php echo $thumbnail ?>
            </div>
            <div class="rt-cart-item__body">
                
                <h5 class="rt-cart-item__title">
                    <?php if (empty($product_permalink)): ?>
                        <?php echo $product_name; ?>
                    <?php else: ?>
                        <a href="<?php echo esc_url($product_permalink); ?>">
                            <?php echo $product_name; ?>
                        </a>
                    <?php endif;?>
                </h5>
                
               <?php $backorder_notification = ($_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ))? true: false;?>
               
               <?php if($_product->get_parent_id() || wc_get_formatted_cart_item_data( $cart_item ) || $backorder_notification):?>
                <div class="rt-cart-item__meta">
                    <?php 
                    // Show variation
                    echo ($product_attribute)? $product_attribute: '';

                    // Meta data.
                    echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

                    // Backorder notification.
                    if ( $backorder_notification ) {
                        echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
                    }
                    ?>
                </div>
                <?php endif ?>
                

                <?php if(rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') !== 'default'): ?>
                <div class="rt-cart-item__quantity">
                    <?php echo $product_quantity?>
                </div>
                <?php endif ?>


                <div class="rt-cart-item__price"><?php echo $product_price_quantity ?></div>
            </div>
            
            <?php if(rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') !== 'default'): ?>
            <div class="rt-cart-item__remove"> 
                <a class="js-remove-cart-button">&times;</a>
            </div>
            
            <?php endif ?>

            <input type="hidden" name="product_id" value="<?php echo absint($_product->get_id());?>">
            <input type="hidden" name="cart_item_price" value="<?php echo absint($_product->get_price())?>">
            <input type="hidden" name="cart_item_key" value="<?php echo esc_attr($cart_item_key)?>">
        </div>

        <?php
    }
}
