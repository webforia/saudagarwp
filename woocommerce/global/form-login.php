<?php

/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     9.2.0
 * 
 * 
 * - Overridden - Retheme 2.11.0
 * - Added wrapper panel
 * - Added form overlay and button style from theme
 * - Added icon svg
 */
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

if (is_user_logged_in()) {
	return;
}
?>


<form class="rt-panel woocommerce-form woocommerce-form-login login" method="post" <?php echo ($hidden) ? 'style="display:none;"' : ''; ?>>


	<div class="rt-panel__header">
		<h4 class="rt-panel__title"><?php _e('Login', 'woocommerce') ?></h4>
	</div>
	<div class="rt-panel__body">

		<?php do_action('woocommerce_login_form_start'); ?>

		<?php echo ( $message ) ? wpautop( wptexturize( $message ) ) : ''; // @codingStandardsIgnoreLine ?>

		<p class="rt-form rt-form--overlay js-form-overlay form-row">
			<label class="rt-form__label" for="username"><?php esc_html_e('Username or email', 'woocommerce'); ?>&nbsp;<span class="required" aria-hidden="true">*</span><span class="screen-reader-text"><?php esc_html_e( 'Required', 'woocommerce' ); ?></span></label>
			<input type="text" class="rt-form__input woocommerce-Input input-text" name="username" id="username" autocomplete="username" required aria-required="true"/>
		</p>

		<p class="rt-form rt-form--overlay js-form-overlay form-row">
			<label class="rt-form__label" for="password"><?php esc_html_e('Password', 'woocommerce'); ?>&nbsp;<span class="required" aria-hidden="true">*</span><span class="screen-reader-text"><?php esc_html_e( 'Required', 'woocommerce' ); ?></span></label>
			<input class="rt-form__input woocommerce-Input input-text" type="password" name="password" id="password" autocomplete="current-password" required aria-required="true" />
		</p>

		<?php do_action('woocommerce_login_form'); ?>

		<p class="flex flex-middle flex-between">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
				<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e('Remember me', 'woocommerce'); ?></span>
			</label>

			<a class="lost_password link-text" href="<?php echo esc_url(wp_lostpassword_url()); ?>">
				<i class="rt-icon">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-life-preserver" viewBox="0 0 16 16">
						<path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm6.43-5.228a7.025 7.025 0 0 1-3.658 3.658l-1.115-2.788a4.015 4.015 0 0 0 1.985-1.985l2.788 1.115zM5.228 14.43a7.025 7.025 0 0 1-3.658-3.658l2.788-1.115a4.015 4.015 0 0 0 1.985 1.985L5.228 14.43zm9.202-9.202-2.788 1.115a4.015 4.015 0 0 0-1.985-1.985l1.115-2.788a7.025 7.025 0 0 1 3.658 3.658zm-8.087-.87a4.015 4.015 0 0 0-1.985 1.985L1.57 5.228A7.025 7.025 0 0 1 5.228 1.57l1.115 2.788zM8 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6z" />
					</svg>
				</i>
				<?php esc_html_e('Lost your password?', 'woocommerce'); ?>
			</a>
		</p>
	
		<div class="rt-form__group mt-20">
			<?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
			<button type="submit" class="rt-btn rt-btn--primary woocommerce-button button woocommerce-form-login__submit<?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ? ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>" name="login" value="<?php esc_attr_e('Login', 'woocommerce'); ?>"><?php esc_html_e('Login', 'woocommerce'); ?></button>
			<input type="hidden" name="redirect" value="<?php echo esc_url($redirect) ?>" />
		</div>
		

		<?php do_action('woocommerce_login_form_end'); ?>

	</div>

</form>