<?php

/**
 * Single Product Up-Sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/up-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     9.6.0
 * 
 * - Overridden - Retheme 2.11.0
 * - added Slider DOM wrapper
 * - added header woocommerce
 */

if (!defined('ABSPATH')) {
    exit;
}

if (!empty($upsells) && rt_option('woocommerce_single_product_related', true)) {
    
    echo rt_html_open(['class' => 'rt-product-up-sells']);

    echo rt_header_block(array(
        "title" => __("Recommended", 'saudagarwp'),
        "class" => 'rt-header-block--center',
    ));

    echo rt_before_slider(array(
        'id' => 'product-up-sells',
        'class' => 'related products rt-swiper--card mb-30',
        'items-lg' => rt_option('woocommerce_single_related_show', 4),
        'items-md' => rt_option('woocommerce_single_related_show_tablet', 2),
        'items-sm' => rt_option('woocommerce_single_related_show_mobile', 2),
        'sameheight' => true,
    ));

    foreach ($upsells as $upsell) {
        $post_object = get_post($upsell->get_id());

        setup_postdata( $GLOBALS['post'] = $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found
        
        echo rt_html_open(['class' => 'swiper-slide']);
            wc_get_template_part('content', 'product');
        echo rt_html_close();
    }

    echo rt_after_slider();

    wp_reset_postdata();

    echo rt_html_close();
}
