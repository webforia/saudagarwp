<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     9.5.0
 */

defined('ABSPATH') || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if (!function_exists('wc_get_gallery_image_html')) {
	return;
}

global $product;
$post_thumbnail_id = $product->get_image_id();
$image = wp_get_attachment_image($post_thumbnail_id, 'woocommerce_gallery_thumbnail', true, array("class" => "main_thumbnails_product_image woocommerce-product-gallery__image"));
$attachment_ids = $product->get_gallery_image_ids();

// product image layout
$image_layout = (rt_option('woocommerce_single_gallery', 'left-gallery') == 'left-gallery' && rt_option('woocommerce_single_layout', 'normal') == 'normal') ? 'vertical' : 'horizontal';
$image_position = (rt_option('woocommerce_single_gallery', 'left-gallery') == 'left-gallery' && rt_option('woocommerce_single_layout', 'normal') == 'normal') ? false : true;
?>

<?php if ($attachment_ids && $product->get_image_id()) : ?>

	<div class="js-product-thumbnails rt-product-gallery__thumbnails woocommerce-product-gallery__wrapper rt-swiper swiper"
		data-direction="<?php echo esc_attr($image_layout) ?>"
		data-position="<?php echo esc_attr($image_position) ?>"
		style="--slides:5; --gap: 10px; --md--slides: 7; --md--gap: 10px; --lg--slides: 7; --lg--gap: 10px;">

		<div class="swiper-wrapper">

			<?php if (!empty($post_thumbnail_id)) : ?>
				<div class="swiper-slide">
					<div class="rt-product-gallery-thumbnail main_product_thumbnail"><?php echo $image ?></div>
				</div>
			<?php endif ?>

			<?php foreach ($attachment_ids as $key => $attachment_id) : ?>
				<div class="swiper-slide">
					<div class="rt-product-gallery-thumbnail">
						<?php echo apply_filters('woocommerce_single_product_image_thumbnail_html',  wc_get_gallery_image_html( $attachment_id, false, $key ), $attachment_id );// PHPCS:Ignore WordPress.Security.EscapeOutput.OutputNotEscaped
						?>
					</div>
				</div>
			<?php endforeach ?>

		</div>

		<div class="swiper-button-next"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
				<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z" />
			</svg></div>
		<div class="swiper-button-prev"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
				<path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z" />
			</svg></div>

	</div>

<?php endif ?>