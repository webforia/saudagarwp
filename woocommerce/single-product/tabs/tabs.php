<?php

/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 9.6.0
 */

if (!defined('ABSPATH')) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
global $product;

$product_tabs = apply_filters('woocommerce_product_tabs', array());
?>

<?php if (!empty($product_tabs)) : ?>
	<div class="rt-product-tabs">
		<div class="rt-tab rt-tab--accordion js-tab mb-20 woocommerce-tabs wc-tabs-wrapper" data-animatein="fadeIn">

			<div class="rt-tab__nav">
				<?php foreach ($product_tabs as $key => $tab) : ?>
					<li>
						<h5 href="#tab-<?php echo esc_attr($key); ?>" id="tab-title-<?php echo esc_attr($key); ?>" class="rt-tab__title <?php echo ($key === array_key_first($product_tabs)) ? 'is-active' : '' ?>">
							<?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', esc_html($tab['title']), $key); ?>
						</h5>
					</li>
				<?php endforeach ?>
			</div>

			<div class="rt-tab__body">

				<div class="rt-accordion js-accordion" data-toggle="false">

					<?php foreach ($product_tabs as $key => $tab) : ?>
						<div id="accordion-<?php echo esc_attr($key); ?>" class="rt-accordion__item <?php echo ($key === array_key_first($product_tabs)) ? 'is-active' : '' ?>">
							<h6 id="accordion-title--<?php echo esc_attr($key); ?>" class="rt-accordion__title">

								<?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', esc_html($tab['title']), $key); ?>

								<span class="rt-accordion__arrow">
									<i class="rt-icon icon-active">
										<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
											<path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
										</svg>
									</i>
									<i class="rt-icon icon-deactive">
										<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-chevron-up" viewBox="0 0 16 16">
											<path fill-rule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1-.708.708L8 5.707l-5.646 5.647a.5.5 0 0 1-.708-.708l6-6z" />
										</svg>
									</i>
								</span>

							</h6>

							<div id="tab-<?php echo esc_attr($key); ?>" class="rt-accordion__content rt-tab__item <?php echo ($key === array_key_first($product_tabs)) ? 'is-active' : '' ?> <?php echo (esc_attr($key) == 'description') ? 'rt-entry-content' : '' ?>">
								<?php if (isset($tab['callback'])) {
									call_user_func($tab['callback'], $key, $tab);
								} ?>
							</div>
						</div>
					<?php endforeach ?>

				</div>

			</div>
		</div>
	</div>
<?php endif; ?>