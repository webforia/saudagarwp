<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 9.7.0
 * 
 */

defined('ABSPATH') || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if (!function_exists('wc_get_gallery_image_html')) {
	return;
}

global $product;

// product image layout
$image_layout = (rt_option('woocommerce_single_gallery', 'left-gallery') == 'left-gallery' && rt_option('woocommerce_single_layout', 'normal') == 'normal') ? 'vertical' : 'horizontal';

$post_thumbnail_id = $product->get_image_id();
$image = wp_get_attachment_image($post_thumbnail_id, 'shop_single', true, array("class" => "attachment-shop_single size-shop_single wp-post-image"));
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
		'rt-product-gallery',
		"rt-product-gallery--{$image_layout}",
	)
);

$class_container = implode(' ', array_map('sanitize_html_class', $wrapper_classes));
$attachment_ids = $product->get_gallery_image_ids();
$lightbox_src = wc_get_product_attachment_props($post_thumbnail_id);

?>
<div class="<?php echo esc_attr($class_container) ?>">
	<figure class="js-product-image rt-product-gallery__display rt-swiper swiper" style="--slides: 1; --gap: 10px; --md--slides: 1; --md--gap: 10px; --lg--slides: 1; --lg--gap: 10px;">

		<div class="swiper-wrapper">

			<?php if($post_thumbnail_id): ?>
			<div class="js-product-lightbox main_single_product_image swiper-slide" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
				<a title="<?php echo esc_attr($lightbox_src['title']) ?>" href="<?php echo esc_url($lightbox_src['url']) ?>" itemprop="contentUrl" width="<?php echo $lightbox_src['full_src_w'] ?>" height="<?php echo $lightbox_src['full_src_h'] ?>">
					<?php echo $image ?>
				</a>
			</div>
			<?php else:?>
				<div class="main_single_product_image swiper-slide"><?php echo wc_placeholder_img() ?></div>
			<?php endif ?>

			<?php
			if ($attachment_ids) :
				foreach ($attachment_ids as $attachment_id) :
					$thumbnail_image     = wp_get_attachment_image($attachment_id, 'shop_single');
					$lightbox_src 		 = wc_get_product_attachment_props($attachment_id); ?>
					<div class="js-product-lightbox swiper-slide" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a title="<?php echo esc_attr($lightbox_src['title']) ?>" href="<?php echo esc_url($lightbox_src['url']) ?>" itemprop="contentUrl" width="<?php echo $lightbox_src['full_src_w'] ?>" height="<?php echo $lightbox_src['full_src_h'] ?>">
							<?php echo $thumbnail_image ?>
						</a>
					</div>

			<?php endforeach;
			endif; ?>

		</div>

		<div class="swiper-pagination"></div>

	</figure>

	<?php do_action('woocommerce_product_thumbnails'); ?>
</div>