<aside id="page-aside" class="page-aside">
   <?php
   // Check Elementor Pro Location
   if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('sidebar')) {
      if (is_active_sidebar('retheme_woocommerce_sidebar')) {
         dynamic_sidebar('retheme_woocommerce_sidebar');
      } else {

         the_widget('Retheme_Woo_Ordering', array('title' => 'Sort by'), array(
            'before_widget' => '<div class="rt-widget rt-widget--aside widget_retheme_product_sort">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
            'after_title' => '</h4></div>',
         ));

         the_widget('WC_Widget_Product_Categories', array('title' => 'Product Categories'), array(
            'before_widget' => '<div class="rt-widget rt-widget--aside widget_product_categories">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
            'after_title' => '</h4></div>',
         ));
      }
   }
   ?>
</aside>