��    ?                         -        H     V     ^     p     }     �     �     �     �     �     �     �  1   �     *  	   1  
   ;     F     U     Z  	   c     m     z  	   �     �  =   �  |   �     U  
   b     m     r          �     �     �     �     �     �     �  	   �     �     �          !  
   .     9     P     _     e     j     x     �  l   �            l   )  $   �  )   �     �     �     �  �  	     �
  2   �
     �
     �
                (  	   6  &   @     g     p     �     �     �  7   �     �     �               0     8     E     V     f     x     �  K   �  �   �     n     |  
   �     �     �  
   �     �     �     �               %     -     :     F     `     y     �     �     �     �     �     �     �     �  ~        �     �  g   �  ,     ,   D     q     }     �   %1$s at %2$s &quot;%s&quot; has been updated to your cart. 404 Not Found Account Already a member? Back to home Buy now Cart Click here to enter your code Comment Comment Count Comment Page %s Comments are closed. Confirm new password Could not find the comment parent for comment #%d Filter Full Name Go to shop Have a coupon? Home John Doe Join now. Lastest Post Leave a Reply Load More Loading Login failed, please check username & password then try again Lost your password? Please enter your username or email address. You will receive a link to create a new password via email. New password Newer Post Next No %s result Not a member? Out of stock Payment confirmation Previous Previous Post Product Product Categories Qty: Read More Recommended Registration success Return to login Save changes Seach post Search results for: %s Send a Comment Share Shop Shopping Cart Showing %s of %s2 results Sign in. Sorry, "%s" is not in stock. Please edit your cart and try again. We apologize for any inconvenience caused. Website (optional) You also like You cannot add that amount of &quot;%s&quot; to the cart because there is not enough stock (%2$s remaining). Your comment is awaiting moderation. Your email address will not be published. Your rating author john.doe@email.com Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-15 16:24+0700
PO-Revision-Date: 2023-12-11 06:55+0000
Last-Translator: 
Language-Team: Bahasa Indonesia
Language: id_ID
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: .
X-Loco-Version: 2.6.6; wp-6.4.2 %1$s pada %2$s &quot;%s&quot; telah diperbarui ke keranjang Anda. 404 tidak ditemukan Akun Sudah memiliki akun? Kembali ke beranda Beli sekarang Keranjang Klik disini untuk memasukan kode kupon Komentar Jumlah komentar Halaman komentar %s Komentar ditutup. Konfirmasi password baru Tidak dapat menemukan induk komentar untuk komentar #%d Filter Nama lengkap Kembali ke toko Memiliki kode kupon? Beranda Budi Budiman Daftar sekarang. Artikel Terbaru Tuliskan Komentar Tampilkan Lebih Banyak Memuat Login gagal, silakan periksa username & password Anda kemudian coba kembali Lupas password? Silakan masukkan nama pengguna atau alamat email Anda. Kami akan mengirimkan URL untuk membuat password baru melalui email. Password baru Postingan Terbaru Berikutnya Pencarian %s tidak ditemukan Belum memiliki akun? Stok Habis Konfirmasi pembayaran Sebelumnnya Postingan Sebelumnnya Produk Kategori Produk Jumlah: Selengkapnya Rekomendasi Pendaftaran akun berhasil Kembali ke halaman login Simpan perubahan Cari postingan Hasil pencarian untuk: %s Kirim Komentar Bagikan Toko Keranjang belanja Menampilkan %s dari %s2 hasil Masuk. Maaf, “%s” tidak tersedia. Harap edit keranjang Anda dan coba lagi. Kami mohon maaf atas ketidaknyamanan yang ditimbulkan. Website (opsional) Anda mungkin juga suka Anda tidak dapat menambahkan jumlah &quot;%s&quot; ke keranjang karena stok tidak cukup (tersisa %2$s). Komentar dari Anda sedang menunggu moderasi. Alamat email Anda tidak akan dipublikasikan. Rating Anda penulis budi@email.com 