<?php get_header() ?>

<?php do_action('rt_before_wrapper'); ?>

<section id="page-wrapper" class="page-wrapper">

    <div id="page-container" class="page-container">

        <div class="flex flex-row">

            <?php do_action('rt_before_content'); ?>

            <div class="page-content" id="page-content">

                <?php rt_get_template_part('page/page-404'); ?>

            </div>

            <?php do_action('rt_after_content'); ?>

            <?php do_action('rt_sidebar'); ?>

        </div>

    </div>
</section>

<?php do_action('rt_after_wrapper'); ?>

<?php get_footer() ?>