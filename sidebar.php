<aside id="page-aside" class="page-aside">
    <?php
    // Check Elementor Pro Location
    if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('sidebar')) {
        dynamic_sidebar('retheme_sidebar');
    }
    ?>
</aside>