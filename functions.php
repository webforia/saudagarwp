<?php
define('RT_THEME_NAME', 'Saudagar WP');
define('RT_THEME_SLUG', 'saudagarwp');
define('RT_THEME_DOMAIN', 'saudagarwp');
define('RT_THEME_VERSION', wp_get_theme()->get('Version'));
define('RT_THEME_URL', 'https://saudagarwp.com');
define('RT_THEME_DOC', 'https://panduan.saudagarwp.com');

// include all core file
require get_template_directory() . '/core/core.php';

// Disables the block editor from managing widgets in the Gutenberg plugin.
add_filter('gutenberg_use_widgets_block_editor', '__return_false');
// Disables the block editor from managing widgets.
add_filter('use_widgets_block_editor', '__return_false');
// add_filter('block_preview', '__return_true');
add_filter('wpcf7_validate_configuration', '__return_false');