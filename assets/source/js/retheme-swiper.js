/* =================================================
 * GLOBAL SLIDER
 * =================================================== */
function swiperjs() {
  const sliders = document.querySelectorAll(".js-swiper");

  sliders.forEach(function (slider, index) {
    const options = JSON.parse(slider.getAttribute("data-options"));
    const navPrev = slider.querySelector(".swiper-button-prev");
    const navNext = slider.querySelector(".swiper-button-next");

    const swiper = new Swiper(slider, options);

    // remove navigation if set disable
    if(options.navigation == false){
      navPrev.style.display = 'none';
      navNext.style.display = "none";
    }

    // run countdown class
    if (slider.querySelector(".wsb-countdown") != null && swiper) {
      new countdown();
    }
    
  });
}

swiperjs();
