/**
 * Open sidepanel after product add to cart success on product archive or shop page
 */

jQuery(document).on('added_to_cart', (event, fragments, cart_hash, button) => {
   const panelCart = document.querySelector('#panel_minicart');
   const _this = event.currentTarget;

   if (panelCart !== null) {
      // Show sidepanel
      if (!panelCart.classList.contains('is-active')) {
         const slidePanel = new slidepanel();
         slidePanel.load('#panel_minicart');
      }

      // Scrool to element
      if (button && button[0]) {
         var productID = button[0].getAttribute('data-product_id');

         if (productID) {
            var cartItem = panelCart.querySelector(`#cart-item-${productID}`);
            cartItem.scrollIntoView();
         }
      }
   }
});

/**
 * Automatic update cart page after user change quantity
 */
jQuery('.woocommerce-cart').on('click', '.woocommerce-cart-form .js-quantity-change span', () => {
   var update_cart = jQuery("button[name='update_cart']");
   setTimeout(() => {
      jQuery('.woocommerce-message').remove();
      update_cart.prop('disabled', false);
      update_cart.trigger('click');
   }, 500);
});

/**
 * Mini cart ajax
 *
 * @version 2.0.0
 */
class miniCart {
   constructor() {
      this.totalItem = "input[name='cart_count_total']";
      this.totalDisplay = '.js-cart-total';
      this.miniCart = '#panel_minicart';
      this.wrapper = '.widget_shopping_cart, #order_review';
      this.content = '.widget_shopping_cart_content';
      this.loader = 'rt-loading';

      // this.shop();
      this.product();
      this.update();
      this.delete();
      this.formSubmit();
   }

   // Run submit form action if sticky button fired on cart behavior not have ajax action
   formSubmit() {
      const action = document.querySelector('.single-product:not(.js-ajax-add-to-cart) .js-sticky-product .single_add_to_cart_button, .js-sticky-product.product-type-grouped .single_add_to_cart_button');
      const formSubmit = document.querySelector('.single-product form.cart .single_add_to_cart_button');

      if (action == null || formSubmit == null) {
         return false;
      }

      action.addEventListener('click', (event) => {
         formSubmit.click();
      });
   }

   // get total item on cart
   cartTotal() {
      const total = document.querySelector(this.totalItem);
      const carts = document.querySelectorAll(this.totalDisplay);

      if (total == null) {
         return false;
      }

      carts.forEach((cart) => {
         cart.innerHTML = total.value;
      });
   }

   // add html results from ajax
   cartContent(results) {
      const contents = document.querySelectorAll(this.content);
      const panelCart = document.querySelector(this.miniCart);

      // update mini cart items
      contents.forEach((content) => {
         content.innerHTML = results.content;
      });

      // update checkout
      if (document.body.classList.contains('woocommerce-checkout')) {
         jQuery(document.body).trigger('update_checkout');
      }

      // open panel minicart
      if (results.productID && !results.delete && panelCart !== null) {
         const cartItem = panelCart.querySelector(`#cart-item-${results.productID}`);
         cartItem.scrollIntoView();
      }
   }

   // ajax on single product
   product() {
      const products = document.querySelectorAll('.js-ajax-add-to-cart .single_add_to_cart_button');
      const product_type = document.querySelector('.type-product');
      const self = this;

      products.forEach((product) => {
         product.addEventListener('click', (event) => {
            const _this = event.currentTarget;

            /**
             * button needed select variation
             * if click add to cart needed selection variation the windows scroll back to form
             */
            if (_this.classList.contains('wc-variation-selection-needed')) {
               document.querySelector('form.variations_form').scrollIntoView();
            }

            // check type product
            // disable ajax on product external
            // disable ajax on has disable class
            if (product_type.classList.contains('product-type-external') || product_type.classList.contains('product-type-grouped') || _this.classList.contains('disabled')) {
               return false;
            }

            event.preventDefault();

            // Disable button state loading
            if (_this.classList.contains('loading')) {
               return false;
            }

            const getQuantity = document.querySelector('form.cart input[name="quantity"]');
            const getProductID = document.querySelector('form.cart button[name="add-to-cart"]');
            const getVariationID = document.querySelector('form.cart input[name="variation_id"]');

            // check type product
            if (getVariationID !== null) {
               var productID = getVariationID.value;
            } else {
               var productID = getProductID.value;
            }

            // ajax run
            jQuery.ajax({
               url: woocommerce_params.wc_ajax_url.toString().replace('%%endpoint%%', 'add_to_cart'),
               type: 'POST',
               data: {
                  product_id: productID,
                  quantity: getQuantity.value,
               },
               beforeSend: function (response) {
                  _this.classList.add('loading');
               },
               success: function (response) {
                  if (response.error && response.product_url) {
                     window.location = response.product_url;
                     return;
                  } else {
                     var button = jQuery(".js-ajax-add-to-cart .single_add_to_cart_button:not('.loading')");

                     jQuery(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, button]);

                     _this.classList.remove('loading');

                     self.cartContent({
                        content: response.fragments['div.widget_shopping_cart_content'],
                        productID: productID,
                     });
                  }
               },
            });
         });
      });
   }

   updateAction(trigger, duration) {
      const self = this;
      const _this = trigger;
      const item = trigger.closest('.js-cart-item');
      const content = trigger.closest('.widget_shopping_cart_content');
      const quantity = item.querySelector("[name='quantity']");
      const productID = item.querySelector("[name='product_id']");
      const itemKey = item.querySelector("[name='cart_item_key']");
      const checkout = document.querySelector('body').classList.contains('.woocommerce-checkout');
      const maxStock = quantity.getAttribute('max');

      // set new quantity from user click action plus or minus button
      let quantityNew = quantity.value;

      if (_this.classList.contains('plus')) {
         quantityNew = parseInt(quantity.value) + 1;
      }
      if (_this.classList.contains('minus')) {
         quantityNew = parseInt(quantity.value) - 1;
      }

      // Skip ajax if quantity 1
      if (quantityNew < 1) {
         return false;
      }

      // waiting qty change
      setTimeout(() => {
         // ajax run
         jQuery.ajax({
            url: woocommerce_params.wc_ajax_url.toString().replace('%%endpoint%%', 'update_cart'),
            type: 'POST',
            data: {
               product_id: productID.value,
               quantity: quantityNew,
               cart_item_key: itemKey.value,
            },
            beforeSend: function (response) {
               content.classList.add(self.loader);
            },
            success: function (response) {
               if (response.error && response.product_url) {
                  window.location = response.product_url;
                  return;
               } else {
                  content.classList.remove(self.loader);

                  var button = jQuery(".js-ajax-add-to-cart .single_add_to_cart_button:not('.loading')");

                  jQuery(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, button]);

                  self.cartContent({
                     content: response.fragments['div.widget_shopping_cart_content'],
                     productID: productID.value,
                  });
               }
            },
         });
      }, duration);
   }

   // Updae qty
   update() {
      const self = this;
      const bases = document.querySelectorAll(this.wrapper);
      const triggersClick = '.js-cart-item .rt-qty span';
      const triggersKeyUp = '.js-cart-item .rt-qty .input-text';

      bases.forEach((base) => {
         base.addEventListener('click', (event) => {
            let trigger = event.target.closest(triggersClick);

            if (trigger && base.contains(trigger)) {
               event.preventDefault();
               this.updateAction(trigger, 1000);
            }
         });

         base.addEventListener('keyup', (event) => {
            let trigger = event.target.closest(triggersKeyUp);

            if (trigger && base.contains(trigger)) {
               event.preventDefault();
               this.updateAction(trigger, 500);
            }
         });
      });
   }

   // Delete
   delete() {
      const self = this;
      const bases = document.querySelectorAll(this.wrapper);
      const triggers = '.js-remove-cart-button';

      bases.forEach((base) => {
         base.addEventListener('click', (event) => {
            var trigger = event.target.closest(triggers);

            if (trigger && base.contains(trigger)) {
               event.preventDefault();
               const _this = trigger;
               const item = trigger.closest('.js-cart-item');
               const content = trigger.closest('.widget_shopping_cart_content');
               // marked item deleted
               item.classList.add('item-delete');

               // get all item delete
               const itemKey = item.querySelector("[name='cart_item_key']");
               const productID = item.querySelector("[name='product_id']");

               // ajax run
               // waiting qty change
               setTimeout(() => {
                  jQuery.ajax({
                     url: woocommerce_params.wc_ajax_url.toString().replace('%%endpoint%%', 'remove_from_cart'),
                     type: 'POST',
                     data: {
                        cart_item_key: itemKey.value,
                     },
                     beforeSend: function (response) {
                        content.classList.add(self.loader);
                     },
                     success: function (response) {
                        self.cartContent({
                           content: response.fragments['div.widget_shopping_cart_content'],
                           productID: productID.value,
                           delete: true,
                        });

                        self.cartTotal();

                        content.classList.remove(self.loader);
                     },
                  });
               }, 300);
            }
         });
      });
   }
}

new miniCart();