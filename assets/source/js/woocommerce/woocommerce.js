/**
 * Show hidden form login and register on modal login WooCommerce
 * @version 1.6.0
 */
function accountFormLogin() {
   const modalLogin = document.querySelector('.js-account-form');

   if (modalLogin == null) {
      return false;
   }

   const triggerLogin = modalLogin.querySelector('.js-account-login');
   const triggerRegister = modalLogin.querySelector('.js-account-register');
   const loginWrappers = modalLogin.querySelectorAll('.rt-form-login, .account-login-title');
   const registerWrappers = modalLogin.querySelectorAll('.rt-form-register, .account-register-title');

   if (triggerLogin !== null) {
      triggerLogin.addEventListener('click', (event) => {
         loginWrappers.forEach((element) => {
            element.style.display = 'block';
         });
         registerWrappers.forEach((element) => {
            element.style.display = 'none';
         });
      });
   }

   if (triggerRegister !== null) {
      triggerRegister.addEventListener('click', (event) => {
         registerWrappers.forEach((element) => {
            element.style.display = 'block';
         });
         loginWrappers.forEach((element) => {
            element.style.display = 'none';
         });
      });
   }
}

accountFormLogin();

/**
 * Quantity input with plus/minus button UI
 *
 * @version 2.1.1
 */

function quantityCountInput() {
   // Hide quantity field if stock product is only 1
   window.addEventListener('load', (event) => {
      const quantities = document.querySelectorAll('.rt-qty input[type="hidden"]');

      quantities.forEach((quantity) => {
         quantity.parentElement.style.display = 'none';
      });
   });

   // Change quantity value
   document.addEventListener('click', (event) => {
      const trigger = event.target.closest('.rt-qty span');

      if (trigger && document.contains(trigger)) {
         event.preventDefault();

         const qty = trigger.closest('.rt-qty').querySelector('.qty');
         const val = parseInt(qty.value) || 1;
         const max = parseInt(qty.getAttribute('max'));
         const min = parseInt(qty.getAttribute('min'));
         const step = parseInt(qty.getAttribute('step')) || 1;

        
         // Check if the value is at or above max
         if (val >= max) {
            qty.style.setProperty('pointer-events', 'none');

            if (val >= max && trigger.classList.contains('plus')) {
                // Display an alert indicating the maximum allowed value
                // Find text from wp_localize
                const stockMessage = localize.stock;
                alert(stockMessage.replace('%s', max));
             }
         } else {
            qty.style.setProperty('pointer-events', 'auto');
         }

         // Change the value if plus or minus
         if (trigger.classList.contains('plus')) {
            if (max && max <= val) {
               qty.value = max;
            } else if (isNaN(val) || val == null) {
               qty.value = 1;
            } else {
               qty.value = val + step;
            }
         } else {
            if (min && min >= val) {
               qty.value = min;
            } else if (val > min) {
               qty.value = val - step;
            }
         }
      }
   });
}
quantityCountInput();

/**
 * Limit quantity input to a maximum value
 *
 * @version 1.0.0
 */
function quantityLimitMax() {
   // Get the input elements that you want to limit
   const inputElements = document.querySelectorAll('.js-quantity-change input[name="quantity"]');

   inputElements.forEach((inputElement) => {
      // Add an event listener for the keyup event on the input
      inputElement.addEventListener('keyup', (event) => {
         // Convert the input value to an integer
         const quantity = parseInt(event.target.value);
         // Determine the maximum allowed value
         const maxQuantity = parseInt(inputElement.getAttribute('max'));

         // Check if the value exceeds the maximum allowed
         if (!isNaN(maxQuantity) && quantity > maxQuantity) {
            // If yes, set the input value back to the maximum allowed value
            event.target.value = maxQuantity;

            // Display an alert indicating the maximum allowed value
            // Find text from wp_localize
            const stockMessage = localize.stock;
            alert(stockMessage.replace('%s', maxQuantity));
         }
      });
   });
}
quantityLimitMax();

/**
 * Set width  orderby select box on shop page
 *
 * @version 1.0.0
 */
function orderbyWidth() {
   const selectId = document.querySelector('.orderby');

   if (selectId == null) {
      return false;
   }

   const selectedText = selectId.options[selectId.selectedIndex].text;

   // set selected width
   selectId.style.width = selectedText.length * 7 + 40 + 'px';
}
orderbyWidth();
