/**
 * Open sidepanel after product add to cart success on product archive or shop page
 */

jQuery(document).on('added_to_cart', (event, fragments, cart_hash, button) => {
   const panelCart = document.querySelector('#panel_minicart');
   const productID = button && button[0] ? button[0].getAttribute('data-product_id') : null;

   if (panelCart !== null) {
      // Show sidepanel if not active
      if (!panelCart.classList.contains('is-active')) {
         const slidePanel = new slidepanel();
         slidePanel.load('#panel_minicart');
      }

      // Scroll to cart item if product ID is available
      if (productID) {
         var cartItem = panelCart.querySelector(`#cart-item-${productID}`);
         cartItem.scrollIntoView();
      }
   }
});

/**
 *  Automatic update cart page after user changes quantity
 */
jQuery('.woocommerce-cart').on('click', '.woocommerce-cart-form .js-quantity-change span', () => {
   var update_cart = jQuery("button[name='update_cart']");

   setTimeout(() => {
      jQuery('.woocommerce-message').remove();

      update_cart.prop('disabled', false).trigger('click');
   }, 500);
});

/**
 * Mini cart ajax
 *
 * @version 3.0.0
 */
class miniCart {
   constructor() {
      this.productAddToCart();
      this.update();
      this.delete();
      this.noAjaxButton();
   }

   // Get new cart content results from wc_fragment
   cartContent(results) {
      const contents = document.querySelectorAll('.widget_shopping_cart_content');

      // Update mini cart items with the fragment content
      contents.forEach((content) => {
         content.innerHTML = results.content;
      });

      // Update checkout if on the checkout page
      if (document.body.classList.contains('woocommerce-checkout')) {
         jQuery(document.body).trigger('update_checkout');
      }

      // Update total count on cart display
      const total = document.querySelector("input[name='cart_count_total']");
      const carts = document.querySelectorAll('.js-cart-total');

      if (total !== null) {
         carts.forEach((cart) => {
            cart.innerHTML = total.value;
         });
      }
   }

   /**
    * Handle non-AJAX add to cart buttons.
    * Trigger default add to cart button if sticky button does not have AJAX action.
    */
   noAjaxButton() {
      // Select the sticky add to cart button that does not have AJAX action
      const stickyButtonAction = document.querySelector('.single-product:not(.js-ajax-add-to-cart) .js-sticky-product .single_add_to_cart_button, .js-sticky-product.product-type-grouped .single_add_to_cart_button');
      // Select the default add to cart button in the product form
      const addToCartButton = document.querySelector('.single-product form.cart .single_add_to_cart_button');

      // If either button is not found, exit the function
      if (stickyButtonAction == null || addToCartButton == null) {
         return false;
      }

      // Add a click event listener to the sticky button
      // Programmatically click the default add to cart button
      stickyButtonAction.addEventListener('click', (event) => {
         event.preventDefault();
         addToCartButton.click();
      });
   }

   /**
    * Run AJAX on update item in the cart.
    *
    * @param trigger - The element that triggered the update action.
    */
   updateAction(trigger) {
      const fn = this;

      // Find the closest cart item and its content container
      const item = trigger.closest('.js-cart-item');
      const content = trigger.closest('.widget_shopping_cart_content');

      // Select relevant input fields within the cart item
      const quantity = item.querySelector("[name='quantity']");
      const productID = item.querySelector("[name='product_id']");
      const itemKey = item.querySelector("[name='cart_item_key']");

      // Set new quantity based on user action (plus or minus button click)
      let quantityNew = quantity.value;

      if (trigger.classList.contains('plus')) {
         quantityNew = parseInt(quantity.value) + 1;
      }
      if (trigger.classList.contains('minus')) {
         quantityNew = parseInt(quantity.value) - 1;
      }

      // Skip AJAX request if the new quantity is less than 1
      if (quantityNew < 1) {
         return false;
      }

      // Wait for quantity change before running AJAX request
      setTimeout(() => {
         jQuery.ajax({
            url: woocommerce_params.wc_ajax_url.toString().replace('%%endpoint%%', 'update_cart'),
            type: 'POST',
            data: {
               product_id: productID.value,
               quantity: quantityNew,
               cart_item_key: itemKey.value,
            },
            beforeSend: function (response) {
               // Add loading state to the content container
               content.classList.add('rt-loading');
            },
            success: function (response) {
               if (response.error && response.product_url) {
                  // Redirect to product URL if there's an error
                  window.location = response.product_url;
                  return;
               } else {
                  // Update mini cart content
                  fn.cartContent({
                     content: response.fragments['div.widget_shopping_cart_content'],
                     productID: productID,
                  });

                  // Remove loading state from the content container
                  content.classList.remove('rt-loading');
               }
            },
            error: function () {
               // Handle errors (optional)
               console.error('Failed to update item from cart.');
            },
         });
      }, 500);
   }

   /**
    * Update the quantity of items in the cart.
    */
   update() {
      // Select the base containers where the cart items are located
      const bases = document.querySelectorAll('.widget_shopping_cart, #order_review');

      // Selectors for the click and keyup triggers
      const triggersClick = '.js-cart-item .rt-qty span';
      const triggersKeyUp = '.js-cart-item .rt-qty .input-text';

      bases.forEach((base) => {
         // Handle click events on the plus/minus buttons
         base.addEventListener('click', (event) => {
            // Find the closest trigger for the click event
            const trigger = event.target.closest(triggersClick);

            if (trigger && base.contains(trigger)) {
               event.preventDefault();

               // Call the update item action function
               this.updateAction(trigger);
            }
         });

         // Handle keyup events on the quantity input fields
         base.addEventListener('keyup', (event) => {
            // Find the closest trigger for the keyup event
            const trigger = event.target.closest(triggersKeyUp);

            if (trigger && base.contains(trigger)) {
               event.preventDefault();

               // Call the update item action function
               this.updateAction(trigger);
            }
         });
      });
   }

   /**
    * Add to cart on single product
    */
   productAddToCart() {
      const fn = this;

      const products = document.querySelectorAll('.js-ajax-add-to-cart .single_add_to_cart_button');
      const product_type = document.querySelector('.type-product');
      const panelCart = document.querySelector('#panel_minicart');

      products.forEach((product) => {
         product.addEventListener('click', (event) => {
            const _this = event.currentTarget;

            /**
             * button needed select variation
             * if click add to cart needed selection variation the windows scroll back to form
             */
            if (_this.classList.contains('wc-variation-selection-needed')) {
               document.querySelector('form.variations_form').scrollIntoView();
            }

            // Check product type external, grup and disabled state
            if (product_type.classList.contains('product-type-external') || product_type.classList.contains('product-type-grouped') || _this.classList.contains('disabled')) {
               return false;
            }

            // Disable button state loading
            if (_this.classList.contains('loading')) {
               return false;
            }

            const getQuantity = document.querySelector('form.cart input[name="quantity"]');
            const getQuantityMax = getQuantity.getAttribute('max');
            const getProductID = document.querySelector('form.cart button[name="add-to-cart"]');
            const getVariationID = document.querySelector('form.cart input[name="variation_id"]');

            event.preventDefault();

            // Get ID from product type
            if (getVariationID !== null) {
               var productID = getVariationID.value;
            } else {
               var productID = getProductID.value;
            }

            // AJAX request to add product to cart
            jQuery.ajax({
               url: woocommerce_params.wc_ajax_url.toString().replace('%%endpoint%%', 'add_to_cart'),
               type: 'POST',
               data: {
                  product_id: productID,
                  quantity: getQuantity.value,
               },
               beforeSend: function (response) {
                  _this.classList.add('loading');
               },
               success: function (response) {
                  if (response.error && response.product_url) {
                     // redirect current product
                     window.location = response.product_url;
                     return;
                  } else {
                     // Update mini cart content
                     fn.cartContent({
                        content: response.fragments['div.widget_shopping_cart_content'],
                        productID: productID,
                     });

                     if (panelCart !== null) {
                        // Show sidepanel using slidePanel
                        const slidePanel = new slidepanel();
                        slidePanel.load('#panel_minicart');

                        // Find the cart item within the loaded panel
                        // Scroll to the cart item if found
                        const cartItem = panelCart.querySelector(`#cart-item-${productID}`);
                        cartItem.scrollIntoView();
                     }

                     // Remove loading
                     _this.classList.remove('loading');
                  }
               },
               error: function () {
                  // Handle errors (optional)
                  console.error('Failed to add to cart item to cart.');
               },
            });
         });
      });
   }

   /**
    * Function to handle item deletion from the mini cart.
    */
   delete() {
      const fn = this;

      const bases = document.querySelectorAll('.widget_shopping_cart, #order_review');

      // Iterate through each base element
      bases.forEach((base) => {
         base.addEventListener('click', (event) => {
            var trigger = event.target.closest('.js-remove-cart-button');

            if (trigger && base.contains(trigger)) {
               // Prevent the default action of the click event
               event.preventDefault();

               // Find the closest cart item and its content container
               const item = trigger.closest('.js-cart-item');
               const content = trigger.closest('.widget_shopping_cart_content');

               // Mark the item as deleted visually
               item.classList.add('item-delete');

               // Get the item key and product ID from the item
               const itemKey = item.querySelector("[name='cart_item_key']");
               const productID = item.querySelector("[name='product_id']");

               // Execute the AJAX request after a short delay
               jQuery.ajax({
                  url: woocommerce_params.wc_ajax_url.toString().replace('%%endpoint%%', 'remove_from_cart'),
                  type: 'POST',
                  data: {
                     cart_item_key: itemKey.value,
                  },
                  beforeSend: function (response) {
                     // Add loading state to the content
                     content.classList.add('rt-loading');
                  },
                  success: function (response) {
                     // Update mini cart content
                     fn.cartContent({
                        content: response.fragments['div.widget_shopping_cart_content'],
                        productID: productID,
                     });

                     // Remove the loading state from the content
                     content.classList.remove('rt-loading');
                  },
                  error: function () {
                     // Handle errors (optional)
                     console.error('Failed to remove item from cart.');
                  },
               });
            }
         });
      });
   }
}

new miniCart();
