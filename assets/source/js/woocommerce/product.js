/*=================================================;
/* PRODUCT GALLERY SLIDER
/*================================================= */
function productGallery() {
    const product_thumbnails = document.querySelector('.js-product-thumbnails');
 
    if (product_thumbnails == null) {
       return false;
    }
 
    const product_gallery_thumbnails = new Swiper('.js-product-thumbnails', {
       centerInsufficientSlides: product_thumbnails.getAttribute('data-position'),
       direction: 'horizontal',
       spaceBetween: 10,
       navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
       },
       breakpoints: {
          0: {
             direction: 'horizontal',
             slidesPerView: 5,
             centerInsufficientSlides: 'center',
          },
          992: {
             direction: product_thumbnails.getAttribute('data-direction'),
             slidesPerView: product_thumbnails.getAttribute('data-direction') == 'horizontal' ? 7 : 5,
          },
       },
    });
 
    const product_gallery_dislay = new Swiper('.js-product-image', {
       spaceBetween: 10,
       pagination: {
          el: '.swiper-pagination',
          type: 'fraction',
       },
       thumbs: {
          swiper: product_gallery_thumbnails,
       },
    });
 
    jQuery(document).on('found_variation', 'form.variations_form', function (event, variation) {
       // back to first main image
       product_gallery_thumbnails.slideTo(0, 700);
       product_gallery_dislay.slideTo(0, 700);
    });
 }
 productGallery();
 
 /*=================================================;
 /* lIST LIGHTBOX IMAGE
 /*================================================= */
 // create lightbox list from all product image
 // set global product image array
 let product_lightbox_items = [];
 
 function productGalleryItems() {
    const elements = document.querySelectorAll('.js-product-lightbox');
    elements.forEach(function (element, index) {
       const link = element.querySelector('a');
       const items = {
          src: link.getAttribute('href'),
          w: link.getAttribute('width'),
          h: link.getAttribute('height'),
       };
       product_lightbox_items.push(items);
    });
 }
 productGalleryItems();
 
 /*=================================================;
 /* LIGHTBOX ACTION
 /*================================================= */
 // Ligtbox gallery action
 // show ligtbox if user click a image gallery display
 function productLightbox() {
    const trigger = document.querySelectorAll('.js-product-lightbox a');
    trigger.forEach(function (element, index) {
       element.addEventListener('click', function (event) {
          event.preventDefault();
          const _this = event.target;
          // Define object and gallery options
          // arg index,create start lightbox image form image click
          const lightbox = document.querySelectorAll('.pswp')[0];
          const options = {
             index: index,
             bgOpacity: 0.85,
             showHideOpacity: true,
          };
          // Initialize PhotoSwipe
          const gallery = new PhotoSwipe(lightbox, PhotoSwipeUI_Default, product_lightbox_items, options);
          gallery.init();
       });
    });
 }
 productLightbox();
 
 /*=================================================;
 /* VARIABLE SELECT
 /*================================================= */
 /**
  * Change image thumbnails if variation select
  */
 jQuery(document).on('found_variation', 'form.variations_form', function (event, variation) {
    // rebuild array list if change image
    if (product_lightbox_items[0]) {
       product_lightbox_items[0].src = variation.image.full_src;
    }
 });
 
 /*=================================================;
 /* PRODUCT STICKY
 /*================================================= */
 /**
  * Floating sticky product action if windows scroling down
  */
 function productSticky() {
    const body = document.querySelector('body');
    const element = document.querySelector('.js-sticky-product');
    const trigger = document.querySelector('.rt-product-summary form .button');
 
    if (element == null) {
       return false;
    }
 
    let position = 500;
 
    if (trigger !== null) {
       position = trigger.offsetTop + 400;
    }
 
    if (document.body.scrollTop > position || document.documentElement.scrollTop > position) {
       element.classList.add('is-sticky');
       body.classList.add('sticky-product');
    } else {
       element.classList.remove('is-sticky');
       body.classList.remove('sticky-product');
    }
 }
 window.addEventListener('scroll', productSticky);
 