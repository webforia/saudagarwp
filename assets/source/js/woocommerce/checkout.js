/**
 * Move coupon box after product review on checkout page
 */
function checkoutMoveCoupon() {
  const coupon = document.querySelector(".checkout_coupon");
  const trigger = document.querySelector(".js-coupon-trigger");
  const couponContainer = document.querySelector(".coupon_checkout_message");

  if (couponContainer == null) {
    return false;
  }

  // move coupon after review
  couponContainer.before(coupon);

  // Toggle coupon slide
  trigger.addEventListener("click", (event) => {
    event.preventDefault();
    const _this = event.currentTarget;

    if (_this.classList.contains("is-active")) {
      _this.classList.remove("is-active");
      slideUp(coupon);
    } else {
      _this.classList.add("is-active");
      slideDown(coupon);
    }
  });
}

checkoutMoveCoupon();