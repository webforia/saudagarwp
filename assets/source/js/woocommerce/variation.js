/**
 * Handle woocomemrce product varian change
 *
 * @version 4.2.0
 */

/*=================================================;
/* VARIATION - SELECT
/*================================================= */
function retheme_variation_select() {
  var form = jQuery('.variations_form.cart');
  // triggered when displayed variation data is reset
  form.on('hide_variation', (event) => {
     const buttons = document.querySelectorAll('.single_add_to_cart_button, .product_addtocart, .product_to_whatsapp');

     buttons.forEach((button) => {
        button.classList.add('disabled', 'wc-variation-selection-needed');
     });
  });

  // triggered when a variation has been found which matches all attributes
  form.on('show_variation', (event, variation, purchasable) => {
     event.preventDefault();

     const buttons = document.querySelectorAll('.single_add_to_cart_button, .product_addtocart, .product_to_whatsapp');

     if (purchasable) {
        buttons.forEach((button) => {
           button.classList.remove('disabled', 'wc-variation-selection-needed', 'wc-variation-is-unavailable');
        });
     } else {
        buttons.forEach((button) => {
           button.classList.remove('wc-variation-selection-needed');
           button.classList.add('disabled', 'wc-variation-is-unavailable');
        });
     }
  });
}
retheme_variation_select();

/*=================================================;
/* VARIATION - SET DEFAULT
/*================================================= */
// Create product data default
// Save product data to form order
function retheme_variation_default() {
  const form = document.querySelector('form.cart');

  if (form == null) {
     return false;
  }

  const title = document.querySelector('.product_title');
  const price = document.querySelector('.product_price');
  const image = document.querySelector('.main_single_product_image img');

  // Set default product data
  form.setAttribute(
     'data-product',
     JSON.stringify({
        product_title: title !== null ? title.textContent.trim() : '',
        product_price_html: price !== null ? price.innerHTML : '',
        product_image: image !== null ? image.getAttribute('src') : '',
     })
  );
}
retheme_variation_default();

/*=================================================;
/* VARIATION - CHANGE
/*================================================= */
function retheme_variation_title(form, variation) {
  // Create attribute from label
  let attributeLabels = [];

  for (const attribute in variation.attributes) {
     const attributeValue = document.querySelector(`[data-attribute_name="${attribute}"]`);
     attributeLabels.push(attributeValue.options[attributeValue.selectedIndex].innerHTML);
  }

  // get data from html
  let productData = JSON.parse(form.getAttribute('data-product'));

  const title = productData.product_title;
  const titles = document.querySelectorAll('.product_title');
  const attribute = Object.values(attributeLabels).join(', ');
  const variationTitle = `${title} - ${attribute}`;
  const variationTitleHTML = `${title} <span> - ${attribute} </span>`;

  // Set title with new title with attribute
  titles.forEach((title) => {
     title.innerHTML = variationTitleHTML;
  });

  // Update product data with variation title
  form.setAttribute(
     'data-product',
     JSON.stringify(
        Object.assign(productData, {
           variation_title: variationTitle,
           variation_title_html: variationTitleHTML,
        })
     )
  );
}

function retheme_variation_price(form, variation) {
  let productData = JSON.parse(form.getAttribute('data-product'));
  const prices = document.querySelectorAll('.product_price');

  // Update price with price attribute
  form.setAttribute(
     'data-product',
     JSON.stringify(
        Object.assign(productData, {
           variation_price: variation.display_price,
           variation_price_html: variation.price_html,
        })
     )
  );

  // Update all price
  if (variation.price_html) {
     prices.forEach((price) => {
        price.innerHTML = variation.price_html;
     });
  }
}

function retheme_variation_sales(variation) {
  if (variation.display_price) {
     const sales = document.querySelectorAll('.rt-product-price .product_onsale');
     const badges = document.querySelectorAll('.rt-product-price .onsale');
     const salePrice = ((variation.display_regular_price - variation.display_price) / variation.display_regular_price) * 100;

     sales.forEach((sale) => {
        sale.innerHTML = `${Math.round(salePrice)}%`;
     });

     badges.forEach((badge) => {
        if (salePrice >= 1) {
           badge.style.display = 'block';
        } else {
           badge.style.display = 'none';
        }
     });
  }
}

function retheme_variation_stocks(form, variation) {
  // Remove quantity field if stock only 1
  const quantity = form.querySelector('.js-quantity-change');
  if (variation.max_qty == 1) {
     quantity.style.display = 'none';
  } else {
     quantity.style.display = 'block';
  }

  // Disable button
  const buttons = document.querySelectorAll('.product_addtocart');
  if (variation.is_in_stock > 0) {
     buttons.forEach((button) => {
        button.classList.remove('disabled wc-variation-is-unavailable');
     });
  } else {
     buttons.forEach((button) => {
        button.classList.add('disabled wc-variation-is-unavailable');
     });
  }
}

function retheme_variation_images(variation) {
  const images = document.querySelectorAll('.main_product_thumbnail img, .main_single_product_image img');
  const newImageFull = variation.image.full_src;
  const newImagePrimary = variation.image.src;
  const newImageThumbnails = variation.image.gallery_thumbnail_src;

  if (newImagePrimary) {
     images.forEach((image) => {
        image.setAttribute('src', newImagePrimary);
        image.removeAttribute('srcset');
     });
  }
}

function retheme_variation_sku(variation) {
  const sku = document.querySelector('.sku_value');

  if (sku == null) {
     return false;
  }

  sku.textContent = variation.sku;
}

function retheme_variation_change() {
  const form = document.querySelector('.variations_form.cart');

  if (form == null) {
     return false;
  }

  // select variation action
  jQuery(document).on('found_variation', 'form.variations_form', (event, variation) => {
     const _this = event.currentTarget;

     console.log(variation.sku);
     // Title
     retheme_variation_title(form, variation);

     // Price
     retheme_variation_price(form, variation);

     // Sale
     retheme_variation_sales(variation);

     // Stock
     retheme_variation_stocks(form, variation);

     // Image
     retheme_variation_images(variation);

     // SKU
     retheme_variation_sku(variation);
  });
}
retheme_variation_change();

/*=================================================;
  /* VARIATION - RESET
  /*================================================= */

// Reset variation link click
function retheme_variation_reset() {
  const form = document.querySelector('.variations_form.cart');
  const reset = document.querySelector('.reset_variations');

  if (reset == null && form == null) {
     return false;
  }

  reset.addEventListener('click', (event) => {
     event.preventDefault();

     let productData = JSON.parse(form.getAttribute('data-product'));

     // get data form product attribute
     const titles = document.querySelectorAll('.product_title');
     const prices = document.querySelectorAll('.product_price');
     const images = document.querySelectorAll('.main_product_thumbnail img, .main_single_product_image img');

     // reset default data
     titles.forEach((title) => {
        title.innerHTML = productData.product_title;
     });
     prices.forEach((price) => {
        price.innerHTML = productData.product_price_html;
     });
     images.forEach((image) => {
        image.setAttribute('src', productData.product_image);
     });

     // reset defaut data sku
     const sku = document.querySelector('.sku_value');

     if (sku) {
        sku.textContent = sku.getAttribute('data-sku');
     }
  });
}
retheme_variation_reset();

/*=================================================;
/* CLASS
/*================================================= */
class retheme_product_variation {
  /**
   * Get Variation Data
   *
   * Get variation data by id variation
   * @param variation_id,
   * @returns
   */
  get_variation(variation_id) {
     const form = document.querySelector('.variations_form.cart');
     const variations = JSON.parse(form.getAttribute('data-product_variations'));
     return variations.find((variation) => variation.variation_id === parseInt(variation_id));
  }

  /**
   * Alert Selection
   * Show alert if user click add to cart but not select variation
   */
  selection_needed() {
     const buttons = document.querySelectorAll('.single_add_to_cart_button, .product_addtocart, .product_to_whatsapp');
     const form = document.querySelector('.rt-product-summary form.variations_form');

     buttons.forEach((button, index) => {
        button.addEventListener('click', (event) => {
           const _this = event.target;
           let error = '';
           
           // notifikasi saat variasi belum dipilih
           if (_this.classList.contains('wc-variation-selection-needed')) {
              event.preventDefault();
              window.alert(wc_add_to_cart_variation_params.i18n_make_a_selection_text);
              error = true;
           }

           // notifikasi saat stock atau kombinasi tidak sesuai
           if (_this.classList.contains('wc-variation-is-unavailable')) {
              event.preventDefault();
              window.alert(wc_add_to_cart_variation_params.i18n_no_matching_variations_text);
              error = true;
           }

           if(error && form !== null){
               form.scrollIntoView();
           }
        });
     });
  }

  /**
   * Check this product have variation
   */
  is_variation() {
     const variation = document.querySelector('input.variation_id');

     if (variation) {
        return true;
     } else {
        return false;
     }
  }
}
class retheme_product_helper {
  money_format(number, currency) {
     if (number) {
        const numberInt = parseInt(number);
        const getCurrency = currency ? currency : 'Rp';

        return getCurrency + Number(numberInt.toFixed(1)).toLocaleString();
     } else {
        console.log('not format number');
     }
  }
}

var variation = new retheme_product_variation();
variation.selection_needed();
