function shopQuantityField() {
  const bases = document.querySelectorAll(".products");

  bases.forEach((base) => {
    // Trigger Click
    base.addEventListener("click", (event) => {
      const trigger = event.target.closest(".products .quantity span");

      if (trigger && base.contains(trigger)) {
        event.preventDefault();

        setTimeout(() => {
          const _this = event.currentTarget;
          const product = event.target.closest(".product");
          const buttons = product.querySelectorAll(".add_to_cart_button");
          const qty = product.querySelector(".qty");

          // For AJAX add-to-cart actions
          buttons.forEach((button) => {
            button.setAttribute("data-quantity", qty.value);
          });

          // For non-AJAX add-to-cart actions
          buttons.forEach((button) => {
            const productID = button.getAttribute("data-product_id");
            button.setAttribute(
              "href",
              `?add-to-cart=${productID}&quantity=${qty.value}`
            );
          });
        }, 100);
      }
    });

    // Trigger Keyup
    base.addEventListener("keyup", (event) => {
      const trigger = event.target.closest(".products .quantity .qty");

      if (trigger && base.contains(trigger)) {
        event.preventDefault();

        setTimeout(() => {
          const _this = event.currentTarget;
          const product = event.target.closest(".product");
          const buttons = product.querySelectorAll(".add_to_cart_button");
          const qty = product.querySelector(".qty");

          // For AJAX add-to-cart actions
          buttons.forEach((button) => {
            button.setAttribute("data-quantity", qty.value);
          });

          // For non-AJAX add-to-cart actions
          buttons.forEach((button) => {
            const productID = button.getAttribute("data-product_id");
            button.setAttribute(
              "href",
              `?add-to-cart=${productID}&quantity=${qty.value}`
            );
          });
        }, 100);
      }
    });
  });
}
shopQuantityField();