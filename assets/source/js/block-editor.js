/* =================================================
 *  SLIDER
 * =================================================== */
const editorSwiper = () => {
  const sliders = document.querySelectorAll(".js-swiper");

  sliders.forEach(function (slider, index) {
    const options = JSON.parse(slider.getAttribute("data-options"));
    const container = slider.querySelector(".swiper-container");
    const navPrev = slider.querySelector(".swiper-button-prev");
    const navNext = slider.querySelector(".swiper-button-next");
    const swiper = new Swiper(container, options);

    // remove navigation if set disable
    if (options.navigation == false) {
      navPrev.style.display = "none";
      navNext.style.display = "none";
    }
  });
}

/*=================================================;
/* DISABLE LINK URL 
/*================================================= */
const editorLink = () => {

  const links = document.querySelectorAll(".acf-block-preview a");

  links.forEach((link) => {
    link.addEventListener("click", (event) => {
      event.preventDefault();
    });
  });

}

// Initialize dynamic block preview (editor).
if (window.acf) {
  window.acf.addAction("render_block_preview/type=retheme-product", editorSwiper);
  window.acf.addAction("render_block_preview/type=retheme-product-cat",editorSwiper);
  window.acf.addAction("render_block_preview/type=retheme-slider", editorSwiper);
  window.acf.addAction("render_block_preview/type=retheme-post", editorSwiper);

  window.acf.addAction("render_block_preview/type=retheme-product", editorLink);
  window.acf.addAction("render_block_preview/type=retheme-product-cat",editorLink);
  window.acf.addAction("render_block_preview/type=retheme-slider", editorLink);
  window.acf.addAction("render_block_preview/type=retheme-post", editorLink);
}
