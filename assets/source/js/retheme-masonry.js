/* =================================================
 *  MASONRY
 * =================================================== */
function masonry() {
    var grid = document.querySelector(".js-masonry");
  
    if (grid) {
      var masonry = new Masonry(grid, {
        columnWidth: ".post-item",
        itemSelector: ".post-item",
        percentPosition: true,
        horizontalOrder: true,
      });
  
      imagesLoaded(grid).on("progress", function () {
        // layout Masonry after each image loads
        masonry.layout();
      });
    }
  }
  masonry();