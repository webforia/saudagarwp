/**
 * slidepanel
 * @version 2.1.0
 */
class slidepanel {
  constructor() {
    this.trigger = ".js-slidepanel";
    this.component = ".rt-sidepanel";
    this.overlay = ".rt-sidepanel__overlay";
    this.inner = ".rt-sidepanel__inner";
    this.close = ".js-sidepanel-close";

    this.listener();
    this.delete();
  }

  open(el) {
    el.element.style.display = "block";
    el.element.classList.add("is-active");
    animateCSS(el.overlay, "fadeIn", "500");
    animateCSS(el.inner, el.animateIn, "500");

    // add listener to disable scroll
    document.querySelector("html").style.overflowY = "hidden";
  }

  delete(el) {
    const closes = document.querySelectorAll(this.close);

    // close panel
    closes.forEach((close) => {
      close.addEventListener("click", (event) => {

        const target = close.closest(this.component);

        // panel open from point
        if (target.classList.contains("rt-sidepanel--left")) {
          var flowOut = "slideOutLeft";
        } else if (target.classList.contains("rt-sidepanel--bottom")) {
          var flowOut = "slideOutDown";
        } else if (target.classList.contains("rt-sidepanel--top")) {
          var flowOut = "slideOutUp";
        } else {
          var flowOut = "slideOutRight";
        }

        // Close panel
        animateCSS(target.querySelector(this.inner), flowOut, "500");
        animateCSS(target.querySelector(this.overlay), "fadeOut", "500").then(() => {
          target.style.display = "none";
          target.classList.remove("is-active");
        });

        // Remove listener to re-enable scroll
        document.querySelector("html").style.overflowY = "auto";

      });
    });
  }

  listener() {
    const slidepanels = document.querySelectorAll(this.trigger);

    slidepanels.forEach((slidepanel) => {
      const dataTarget = slidepanel.getAttribute("data-target");
      const target = document.querySelector(dataTarget);

      if (target == null) {
        return false;
      }

      // panel open from point
      if (target.classList.contains("rt-sidepanel--left")) {
        var flowIn = "slideInLeft";
      } else if (target.classList.contains("rt-sidepanel--bottom")) {
        var flowIn = "slideInUp";
      } else if (target.classList.contains("rt-sidepanel--top")) {
        var flowIn = "slideInDown";
      } else {
        var flowIn = "slideInRight";
      }

      const el = {
        element: target,
        overlay: target.querySelector(this.overlay),
        inner: target.querySelector(this.inner),
        animateIn: flowIn,
      };

      // open
      slidepanel.addEventListener("click", (event) => {
        event.preventDefault();
        const _this = event.currentTarget;

        if (_this.classList.contains("disabled")) {
          return false;
        }

        // Run open action
        this.open(el);
      });

    });
  }

  /**
   * open automatic if windows load
   * @param {id component panel} id
   */
  load(id) {
    const slidepanels = document.querySelectorAll(id);
    slidepanels.forEach((slidepanel) => {
      // flow panel animation
      // panel open from point
      if (slidepanel.classList.contains("rt-sidepanel--left")) {
        var flowIn = "slideInLeft";
      } else if (slidepanel.classList.contains("rt-sidepanel--bottom")) {
        var flowIn = "slideInUp";
      } else if (slidepanel.classList.contains("rt-sidepanel--top")) {
        var flowIn = "slideInDown";
      } else {
        var flowIn = "slideInRight";
      }

      const el = {
        element: slidepanel,
        overlay: slidepanel.querySelector(this.overlay),
        inner: slidepanel.querySelector(this.inner),
        animateIn: flowIn,
      };

      this.open(el);
    });
  }
}

new slidepanel();
