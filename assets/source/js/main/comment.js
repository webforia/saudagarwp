/**
 * Comment Class
 * @version 2.0.0
 */
class comment {
  constructor() {
    this.comment = ".js-comment";
    this.textarea = "textarea";
    this.input = ".comment-input";

    this.listener();
  }

  listener() {
    const comments = document.querySelectorAll(this.comment);

    comments.forEach((comment) => {
      const textarea = comment.querySelector(this.textarea);
      const input = comment.querySelector(this.input);

      if(input == null){
        return false;
      }

      textarea.addEventListener("click", (event) => {
        event.preventDefault();

        if (!comment.classList.contains("is-active")) {
          comment.classList.add("is-active");

          slideDown(input);
        }
      });
    });
  }
}

// Initialize Comment class
document.addEventListener("DOMContentLoaded", () => {
  new Comment();
});

