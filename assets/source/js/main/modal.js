/**
 * Retheme Modal Pop up
 * @version 2.0.0
 */
class modal {
  constructor() {
    this.trigger = ".js-modal";
    this.close = ".js-modal-close";
    this.component = ".rt-modal";
    this.overlay = ".rt-modal__overlay";
    this.inner = ".rt-modal__inner";
    this.animateIn = "data-animatein";
    this.animateOut = "data-animateOut";
    this.scroll = "data-scroll";

    this.listener();
    this.delete();
  }

  open(el) {
    el.element.style.display = "block";

    animateCSS(el.overlay, "fadeIn", "500");
    animateCSS(el.inner, el.animateIn, "500");

    // add listener to disable scroll
    document.querySelector("html").style.overflowY = "hidden";
  }

  delete(el) {
    const closes = document.querySelectorAll(this.close);

    closes.forEach((close) => {
      close.addEventListener("click", (event) => {
        const target = close.closest(this.component);

        // close modal
        animateCSS(target.querySelector(this.inner),target.getAttribute(this.animateOut),"500");
        animateCSS(target.querySelector(this.overlay),"fadeOut","500").then(() => {
          target.style.display = "none";
        });

        // Remove listener to re-enable scroll
        document.querySelector("html").style.overflowY = "auto";

      });
    });
  }

  listener() {
    const modals = document.querySelectorAll(this.trigger);
    modals.forEach((modal) => {
      const dataTarget = modal.getAttribute("data-target");
      const target = document.querySelector(dataTarget);

      if (target == null) {
        return false;
      }

      const el = {
        element: target,
        overlay: target.querySelector(this.overlay),
        inner: target.querySelector(this.inner),
        animateIn: target.getAttribute(this.animateIn),
        scroll: target.getAttribute(this.scroll),
      };

      modal.addEventListener("click", (event) => {
        event.preventDefault();
        const _this = event.currentTarget;

        if (_this.classList.contains("disabled")) {
          return false;
        }

        this.open(el);

      });
    });
  }

  load(id) {
    const modals = document.querySelectorAll(id);

    modals.forEach((modal) => {
      const el = {
        element: modal,
        overlay: modal.querySelector(this.overlay),
        inner: modal.querySelector(this.inner),
        animateIn: modal.getAttribute(this.animateIn),
      };

      this.open(el);
    });
  }
}

new modal();
