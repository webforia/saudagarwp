/**
 * Accordion
 * @version 2.0.0
 */
class Accordion {
  constructor() {
    this.component = ".js-accordion";
    this.item = ".rt-accordion__item";
    this.title = ".rt-accordion__title";
    this.content = ".rt-accordion__content";
    this.toggle = "data-toggle";

    this.open();
  }

  deleteAll() {
    const components = document.querySelectorAll(this.component);

    components.forEach((component) => {
      const items = component.querySelectorAll(this.item);
      const contents = component.querySelectorAll(this.content);

      // Hide all items and contents
      items.forEach((element) => {
        element.classList.remove("is-active");
      });
      contents.forEach((element) => {
        element.style.display = "none";
      });
    });
  }

  open() {
    const components = document.querySelectorAll(this.component);

    components.forEach((component) => {
      const items = component.querySelectorAll(this.item);
      const toggle = component.getAttribute(this.toggle);

      items.forEach((item) => {
        const title = item.querySelector(this.title);

        if (title) {
          title.addEventListener("click", (event) => {
            if (toggle === "true") {
              this.deleteAll();
            }

            const el = event.currentTarget;
            const item = el.closest(this.item);
            const content = item.querySelector(this.content);

            if (item.classList.contains("is-active")) {
              item.classList.remove("is-active");
              slideUp(content);
            } else {
              item.classList.add("is-active");
              slideDown(content);
            }
          });
        }
      });
    });
  }
}

// Initialize accordion after DOM is fully loaded
document.addEventListener("DOMContentLoaded", () => {
  new Accordion();
});
