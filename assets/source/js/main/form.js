/**
 * Form
 * @version 2.0.0
 */
class form {
  constructor() {
    this.form = ".js-form-overlay";
    this.input = ".js-form-overlay .rt-form__input";

    this.openAll();
    this.open();
  }

  /**
   * Open/close all form if windows load
   * open if form have value
   * close if form not have value
   */
  openAll() {
    const inputs = document.querySelectorAll(this.input);

    inputs.forEach((input) => {
      if (input.value.length > 0) {
        input.closest(this.form).classList.add("is-active");
      } else {
        input.closest(this.form).classList.remove("is-active");
      }
    });
  }

  /**
   * Open form on focus
   * run openall function
   */
  open() {
    const inputs = document.querySelectorAll(this.input);

    inputs.forEach((input) => {

      input.addEventListener("focus", (event) => {
          const item = event.currentTarget;

          // close all form
          this.openAll();

          // active this input
          item.closest(this.form).classList.add("is-active");
      });
    });
  }
}

new form;
