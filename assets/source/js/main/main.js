/**
 * Main
 * @version 4.0.0
 */
// add class if js ready
document.querySelector("html").classList.remove("no-js");
document.querySelector("html").classList.add("js");


/* =================================================
 *  STICKY HEADER
 * =================================================== */
function headerFloatDesktop() {
  const header = document.querySelector(".js-header");

  if (header == null) {
    return false;
  }
  const sticky = header.getAttribute("data-sticky");
  const headerHeight = header.clientHeight;

  if (window.pageYOffset > headerHeight && sticky == true) {
    header.classList.add("is-sticky");
  } else {
    header.classList.remove("is-sticky");
  }
}
window.addEventListener("scroll", headerFloatDesktop);

// header sm sticky
function headerFloatMobile() {
  const header = document.querySelector(".js-header-mobile");

  if (header == null) {
    return false;
  }
  const sticky = header.getAttribute("data-sticky");
  const headerHeight = header.clientHeight;

  if (window.pageYOffset > headerHeight && sticky == true) {
    header.classList.add("is-sticky");
  } else {
    header.classList.remove("is-sticky");
  }
}
window.addEventListener("scroll", headerFloatMobile);

/*=================================================;
/* BACK HISTORY
/*================================================= */
function windowBack() {
  const triggers = document.querySelectorAll(".js-window-back");

  triggers.forEach((trigger) => {
    trigger.addEventListener("click", (event) => {
      event.preventDefault();

      window.history.back();
    });
  });
}
windowBack();

/* =================================================
 *  GOTOP
 * =================================================== */
function gotop() {
  const gotop = document.querySelector(".js-gotop");

  if (gotop == null) {
    return false;
  }

  // show gotop button
  if (window.pageYOffset > 200) {
    gotop.style.display = "block";
  } else {
    gotop.style.display = "none";
  }

  // scroll to top
  gotop.addEventListener("click", () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  });
}
window.addEventListener("scroll", gotop);

/*====================================================
/* RESPONSIVE EMBED
/*====================================================*/
document.addEventListener("DOMContentLoaded", function () {
  var embeds = document.querySelectorAll("iframe");

  embeds.forEach(function (embed) {
    var width = embed.width || embed.clientWidth;
    var height = embed.height || (width * 9) / 16;
    var ratio = height / width;

    embed.style.width = "100%";
    embed.style.height = embed.clientWidth * ratio + "px";
  });

  window.addEventListener("resize", function () {
    embeds.forEach(function (embed) {
      var width = embed.clientWidth;
      var ratio = embed.dataset.ratio || (9 / 16);
      embed.style.height = width * ratio + "px";
    });
  });
});
