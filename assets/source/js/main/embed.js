document.addEventListener("DOMContentLoaded", function () {
    var iframes = document.querySelectorAll(".rt-entry-content iframe:not(.elementor-video)");

    iframes.forEach(function (iframe) {
        var width = iframe.getAttribute("width") || iframe.clientWidth;
        var height = iframe.getAttribute("height") || (width * 9) / 16; // Default 16:9 jika tidak ada height
        var ratio = height / width;

        function adjustSize() {
            if (iframe.clientWidth > width) {
                iframe.style.width = width + "px"; // Kembalikan ke ukuran asli jika lebih kecil dari parent
                iframe.style.height = height + "px";
            } else {
                iframe.style.width = "100%";
                iframe.style.height = iframe.clientWidth * ratio + "px";
            }
        }

        adjustSize();
        window.addEventListener("resize", adjustSize);
    });
});