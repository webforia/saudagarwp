const { src, dest, watch, series, parallel } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const autoprefixer = require("autoprefixer");
const postCss = require("gulp-postcss");
const cleanCss = require("gulp-clean-css");
const mergeMediaQueries = require("gulp-merge-media-queries");
const rename = require("gulp-rename");
const plumber = require("gulp-plumber");
const sassGlob = require("gulp-sass-glob");
const sourcemaps = require("gulp-sourcemaps");
const browserSync = require("browser-sync").create();
const notify = require("gulp-notify");
const uglify = require("gulp-uglify");
const babel = require("gulp-babel");
const concat = require("gulp-concat");

// Compile SCSS to CSS
function buildScss() {
   return src("assets/source/scss/**/*.scss")
      .pipe(plumber())
      .pipe(sassGlob())
      .pipe(sass().on("error", sass.logError))
      .pipe(postCss([autoprefixer()]))
      .pipe(mergeMediaQueries({ log: true }))
      .pipe(cleanCss())
      .pipe(rename({ extname: ".min.css" }))
      .pipe(dest("assets/css"))
      .pipe(notify("SCSS Compiled"))
      .pipe(browserSync.stream());
}

// Compile JS
function buildJs() {
   return src('assets/source/js/**/*.js')
      .pipe(babel())
      .pipe(uglify())
      .pipe(rename({ extname: ".min.js" }))
      .pipe(dest(`assets/js`))
      .pipe(notify("Js Compiled"))
      .pipe(browserSync.stream());
}

// Combine multiple JS to single JS file
function combineJs() {
   return src(`assets/source/js/folder/**/*.js`)
      .pipe(sourcemaps.init())
      .pipe(concat("main.js"))
      .pipe(babel())
      .pipe(uglify())
      .pipe(sourcemaps.write("."))
      .pipe(rename({ extname: ".min.js" }))
      .pipe(dest(`assets/js`));
}

// Initialize BrowserSync
function initBrowserSync() {
   // Local Server
   browserSync.init({
      proxy: "saudagarwp.local",
   });

   watch(`assets/source/scss/*.scss`, buildScss);
   watch(`assets/source/js/*.js`, buildJs);
   watch(`*.php`).on("change", browserSync.reload);
}

// Export the tasks
exports.js = buildJs;
exports.combineJs = combineJs;
exports.scss = buildScss;

// Run default task
exports.default = series(buildScss, initBrowserSync);
