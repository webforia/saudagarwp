<?php
$text = __('Seach post', 'saudagarwp');
$result = 'post';
if (!empty($_GET['post_type']) && $_GET['post_type'] == 'product') {
    $text = rt_option('search_main_text', 'Type Something and enter');
    $result = rt_option('search_main_result', 'product');
}
?>
<div class="rt-search-form">
    <form class="rt-search-form__inner" action="<?php echo esc_url(home_url('/')); ?>" method="get">
        <input class="rt-search-form__input" type="search" placeholder="<?php echo $text ?>" name="s" id="s">
        <input type="hidden" name="post_type" value="<?php echo $result ?>" />
        <button type="submit" class="rt-search-form__btn">
            <i class="rt-icon">
                <svg width="1em" height="1em" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/">
                    <path d="M10.719,11.426L15.126,15.833C15.32,16.027 15.635,16.027 15.829,15.833C15.831,15.831 15.833,15.83 15.834,15.828C16.029,15.634 16.029,15.319 15.834,15.125L11.426,10.716C12.397,9.58 12.984,8.105 12.984,6.494C12.984,2.904 10.069,-0.01 6.479,-0.01C2.89,-0.01 -0.025,2.904 -0.025,6.494C-0.025,10.084 2.89,12.999 6.479,12.999C8.099,12.999 9.58,12.406 10.719,11.426ZM6.479,0.99C9.517,0.99 11.984,3.456 11.984,6.494C11.984,9.532 9.517,11.999 6.479,11.999C3.441,11.999 0.975,9.532 0.975,6.494C0.975,3.456 3.441,0.99 6.479,0.99Z" />
                </svg>
            </i>
        </button>
    </form>
</div>