<?php

if (post_password_required()) {
  return;
}

function rt_comment_callback($comment, $args, $depth) {
  if ('div' === $args['style']) {
    $tag = 'div';
    $add_below = 'comment';
  } else {
    $tag = 'li';
    $add_below = 'div-comment';
  } ?>
  <<?php echo esc_html($tag) ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">

    <?php if ('div' != $args['style']) : ?>
      <div id="div-comment-<?php comment_ID() ?>">
      <?php endif; ?>

      <div class="rt-comment-list__item">
        <div class="rt-img rt-comment-list__avatar">
          <?php echo get_avatar($comment, $args['avatar_size']); ?>
        </div>
        <div class="rt-comment-list__body">

          <?php if ($comment->comment_approved == '0') : ?>
            <p class="rt-comment-list__approved"><?php _e('Your comment is awaiting moderation.', 'saudagarwp'); ?></p>
          <?php endif; ?>

          <h5 class="rt-comment-list__title">
            <a href="<?php echo get_comment_author_url(); ?>"><?php echo get_comment_author() ?></a>
            <?php if (get_the_author() == get_comment_author()) : ?>
              <span class="rt-comment-list__author"><?php _e('author', 'saudagarwp') ?></span>
            <?php endif; ?>
          </h5>

          <div class="rt-comment-list__meta">
            <span href="#" class="comment-list-meta-date">

              <i class="rt-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-calendar3" viewBox="0 0 16 16">
                  <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"></path>
                  <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                </svg>
            </i>

              <?php printf(__('%1$s at %2$s', 'saudagarwp'), get_comment_date(), get_comment_time()); ?>
            </span>
          </div>

          <div class="rt-comment-list__content">
            <?php comment_text(); ?>
          </div>

          <div class="rt-comment-list__reply">
            <i class="rt-icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-reply-fill" viewBox="0 0 16 16">
                <path d="M5.921 11.9 1.353 8.62a.719.719 0 0 1 0-1.238L5.921 4.1A.716.716 0 0 1 7 4.719V6c1.5 0 6 0 7 8-2.5-4.5-7-4-7-4v1.281c0 .56-.606.898-1.079.62z" />
              </svg>
            </i>
            <?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
          </div>

        </div>
      </div>

      <?php if ('div' != $args['style']) : ?>
      </div>
    <?php endif; ?>
  <?php
} ?>
  <div class="rt-comment">

    <div class="rt-header-block">
      <h2 class="rt-header-block__title">
        <?php _e('Leave a Reply', 'saudagarwp') ?>
      </h2>
    </div>


    <?php if (number_format_i18n(get_comments_number()) > 0) : ?>
      <ul class="rt-comment-list">
        <?php

        wp_list_comments(array(
          'style' => 'ul',
          'short_ping' => true,
          'avatar_size' => 70,
          'callback' => 'rt_comment_callback',
        ));
        ?>
      </ul>


      <div class="rt-pagination">
        <?php paginate_comments_links(array(
          'prev_text' => '<i class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/></svg></i>',
          'next_text' => '<i class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/></svg></i>',
        )); ?>
      </div>


    <?php endif; ?>


    <?php if (!comments_open() && get_comments_number() && post_type_supports(get_post_type(), 'comments')) : ?>
      <p class="no-comments rt-alert rt-alert--info"><?php esc_html_e('Comments are closed.', 'saudagarwp'); ?></p>
    <?php endif; ?>



    <?php
    /** COMMENT FORM */
    if (comments_open()) :
      comment_form(array(
        'title_reply' => '',
        'label_submit' => __('Send a Comment', 'saudagarwp'),
        'id_form' => 'commentform',
        'class_form' => 'rt-comment-form js-comment',
        'comment_notes_before' => '<p class="comment_notes">' . __('Your email address will not be published.', 'saudagarwp') . '</p>',
        'cancel_reply_before' => '<small class="comment_cancel-reply">',
        'cancel_reply_after' => '</small>',
      ));

    endif;
    ?>
  </div>