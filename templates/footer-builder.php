<?php
/*
 * Template Name: Builder - Footer
 * Template Post Type: elementor_library
 * Plan: Premium
 */

?>


<?php get_header(); ?>

<style media="screen">.builder-block{min-height: 600px;}.builder-block__item > div{background-color: #f5f5f5;min-height: 20px;margin-bottom: 10px;}.builder-block__container{padding-top: 40px;padding-bottom: 40px;}.builder-block__item{margin-bottom: 30px;}.builder-block__thumbnail{height: 340px;display: flex;justify-content: center;align-items: center;}.builder-block__thumbnail .fa-image{font-size: 6em;color: #ebebebeb;}.builder-block__subtitle{width: 80%;}</style>

<section class="builder-block">
  <div class="page-container builder-block__container">

    <div class="flex flex-row">

      <?php for ($index = 1; $index <= 12; $index++) : ?>
        <div class="flex-md-3">
          <div class="builder-block__item">
            <div class="builder-block__thumbnail" style="height: 170px;"><i class="fa fa-image"></i></div>
            <div class="builder-block__title"></div>
            <div class="builder-block__subtitle"></div>
          </div>
        </div>
      <?php endfor; ?>

    </div>

  </div>
</section>

<?php do_action('rt_before_footer') ?>

<!-- start builder footer -->
<footer id="page-footer" class="rt-footer rt-footer--builder rt-widget--footer">
  <?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

      <?php the_content() ?>

    <?php endwhile; ?>


  <?php else : ?>

    <?php do_action('rt_post_none') ?>

  <?php endif; ?>
</footer>

<?php do_action('rt_after_footer') ?>

</div>

<?php do_action('rt_after_main') ?>
<?php wp_footer(); ?>

</body>

</html>