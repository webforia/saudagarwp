<?php
/* Template Name: Front Page 
 *
 * The template for displaying content without sidebar and header page.
 *
 * This is the template that displays for page builder.
 */


get_header();

if (function_exists('wc_get_notices') && !empty(wc_get_notices())) {
    echo '<div class="rt-global-woocommerce-notices mt-30"><div class="page-container">';
    wc_print_notices();
    echo '</div></div>';
    wc_clear_notices();
}

if (have_posts()) :

    while (have_posts()) : the_post();

        rt_get_template_part('homepage/homepage');

    endwhile;

endif;

get_footer();
