<?php
/* 
 * Template Name: Builder - Header
 * Template Post Type: elementor_library
 * Plan: Premium
 */

?>

<!DOCTYPE html>
<html class="no-js" <?php language_attributes() ?>>

<head>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <title><?php wp_title() ?></title>
  <?php wp_head(); ?>
  <style media="screen">.builder-block {min-height: 600px;}.builder-block__item>div {background-color: #f5f5f5;min-height: 20px;margin-bottom: 10px;}.builder-block__container {padding-top: 40px;padding-bottom: 40px;}.builder-block__item {margin-bottom: 30px;}.builder-block__thumbnail {height: 340px;display: flex;justify-content: center;align-items: center;}.builder-block__thumbnail .fa-image {font-size: 6em;color: #ebebebeb;}.builder-block__subtitle {width: 80%;}</style>

</head>

<body <?php body_class(); ?>>

  <?php do_action('rt_before_main') ?>

  <div id="page-main" class="page-main">

    <div class="rt-header-builder">

      <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>

          <?php the_content() ?>

        <?php endwhile; ?>


      <?php else : ?>

        <?php do_action('rt_post_none') ?>

      <?php endif; ?>

      <?php rt_get_template_part('header/header-search'); ?>

    </div>


    <section class="builder-block">
      <div class="page-container builder-block__container">

        <div class="flex flex-row">

          <div class="flex-md-8">

            <div class="builder-block__item">
              <div class="builder-block__title"></div>
              <div class="builder-block__subtitle"></div>
              <div class="builder-block__thumbnail"><i class="fa fa-image"></i></div>
              <div class="builder-block__content"></div>
              <div class="builder-block__content"></div>
              <div class="builder-block__subtitle"></div>
            </div>

            <div class="mb-40"></div>

            <div class="builder-block__item">
              <div class="builder-block__title"></div>
              <div class="builder-block__subtitle"></div>
              <div class="builder-block__thumbnail"><i class="fa fa-image"></i></div>
              <div class="builder-block__content"></div>
              <div class="builder-block__content"></div>
              <div class="builder-block__subtitle"></div>
            </div>


          </div>

          <div class="flex-md-4">

            <div class="builder-block__item">
              <div class="builder-block__thumbnail" style="height: 170px;"><i class="fa fa-image"></i></div>
              <div class="builder-block__title"></div>
              <div class="builder-block__subtitle"></div>
            </div>

            <div class="builder-block__item">
              <div class="builder-block__thumbnail" style="height: 170px;"><i class="fa fa-image"></i></div>
              <div class="builder-block__title"></div>
              <div class="builder-block__subtitle"></div>
            </div>


          </div>

        </div>

      </div>
    </section>

    <?php get_footer(); ?>