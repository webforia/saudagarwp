<?php
/* Template Name: Full Width 
 * Template Post Type: post, page, campaign
 * The template for displaying content without sidebar and header page.
 *
 * This is the template that displays for page builder.
 */
?>

<?php get_header() ?>

<?php do_action('rt_before_wrapper'); ?>

<section id="page-wrapper" class="page-wrapper">

    <div id="page-container" class="page-container">

        <div class="flex flex-row">

            <?php do_action('rt_before_content'); ?>

            <div class="page-content" id="page-content">

                <?php
                if (have_posts()) :

                    do_action('rt_before_loop');

                    while (have_posts()) : the_post();

                        echo '<div class="rt-entry-content">';

                        the_content();

                        echo '</div>';

                    endwhile;

                    do_action('rt_after_loop');

                else :

                    rt_get_template_part('page/page-none');

                endif;
                ?>

            </div>

            <?php do_action('rt_after_content'); ?>

            <?php do_action('rt_sidebar'); ?>

        </div>

    </div>
</section>

<?php do_action('rt_after_wrapper'); ?>

<?php get_footer() ?>