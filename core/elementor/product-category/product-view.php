<?php
// category
$hide_empty = ($settings['setting_empty_category'] === 'yes')? true: false;

if ($settings['setting_hide_sub_category'] === 'yes') {
    $terms = get_terms([
        'taxonomy' => 'product_cat',
        'hide_empty' => $hide_empty,
        'parent' => 0,
    ]);
} else {
    $terms = get_terms([
        'taxonomy' => 'product_cat',
        'hide_empty' => $hide_empty,
    ]);
}


if ($settings['setting_style'] == 'style-1') {
    echo rt_before_slider([
        'id' => 'home_pruduct_category',
        'items-lg' => !empty($settings['slider_item']) ? $settings['slider_item'] : 4,
        'items-md' => !empty($settings['slider_item_tablet']) ? $settings['slider_item_tablet'] : 3,
        'items-sm' => !empty($settings['slider_item_mobile']) ? $settings['slider_item_mobile'] : 1,
        'loop' => ($settings['slider_loop'] == 'yes') ? true : false,
        'autoplay' => ($settings['slider_auto_play'] == 'yes') ? true : false,
    ]);
}

if ($settings['setting_style'] == 'style-2') {
    echo rt_before_slider([
        'id' => 'home_pruduct_category',
        'class' => 'rt-swiper--pagination-outer',
        'breakpoints' => [
            '960' => [
                'spaceBetween' => 30,
            ],
            '720' => [
                'spaceBetween' => 20,
            ],
            '320' => [
                'spaceBetween' => 15,
            ],
        ],
        'loop' => ($settings['slider_loop'] == 'yes') ? true : false,
        'autoplay' => ($settings['slider_auto_play'] == 'yes') ? true : false,
        'centerInsufficientSlides' => true,
        'navigation' => false,
        'pagination' => [
            'el' => '.swiper-pagination',
            'clickable' => true,
        ],
    ]);
}
?>

<?php if (!empty($terms) && !is_wp_error($terms)) : ?>

    <?php foreach ($terms as $key => $term) : ?>


        <?php if ($term->slug != 'uncategorized') : ?>

            <?php $thumbnail_id = get_term_meta($term->term_id, 'thumbnail_id', true); ?>

            <?php if ($settings['setting_style'] == 'style-1') : ?>

                <div class="swiper-slide">
                    <div class="rt-banner <?php echo "rt-banner-category-{$term->slug}" ?>">
                        <a href="<?php echo get_term_link($term->slug, 'product_cat') ?>">

                            <div class="rt-banner__thumbnail">
                                <?php
                                if ($thumbnail_id) {
                                    echo wp_get_attachment_image($thumbnail_id, 'featured_medium');
                                } else {
                                    echo wc_placeholder_img('featured_medium');
                                }
                                ?>
                            </div>
                            <span class="rt-banner__overlay"></span>
                            <div class="rt-banner__body">
                                <h5 class="rt-banner__title"><?php echo esc_html($term->name); ?></h5>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($settings['setting_style'] == 'style-2') : ?>
                <div class="swiper-slide" style="width: auto">

                    <a href="<?php echo get_term_link($term->slug, 'product_cat') ?>">
                        <div class="rt-img-box mb-0 <?php echo "rt-banner-category-{$term->slug}" ?>">
                            <div class="rt-img-box__thumbnail rt-img rt-img--circle" style="width: 70px">
                                <?php
                                if ($thumbnail_id) {
                                    echo wp_get_attachment_image($thumbnail_id, 'thumbnail');
                                } else {
                                    echo wc_placeholder_img('thumbnail');
                                }
                                ?>
                            </div>

                            <div class="rt-img-box__body">
                                <h5 class="rt-img-box__title"><?php echo $term->name ?></h5>
                                <span class="rt-img-box__meta"><?php echo wp_sprintf(__('%s items', 'saudagarwp'), $term->count) ?></span>
                            </div>
                        </div>
                    </a>

                </div>
            <?php endif ?>


        <?php endif ?>
    <?php endforeach ?>

<?php endif; ?>

<?php echo rt_after_slider(); ?>