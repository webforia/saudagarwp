<?php
namespace Retheme\Elementor;

use Elementor;
use Elementor\Controls_Manager;
use Retheme\Elementor_Base;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Product_Categories extends Elementor_Base
{
    public function get_name()
    {
        return 'retheme-product-category';
    }

    public function get_title()
    {
        return __('Products Categories', 'saudagarwp');
    }

    public function get_icon()
    {
        return 'ate-icon ate-product';

    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function register_controls()
    {
        $this->setting_option();
    }

    protected function setting_option()
    {
        $this->start_controls_section(
            'setting_product',
            [
                'label' => __('Options', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'setting_style',
            [
                'label' => __('Style', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'style-1' => __('Style 1', 'saudagarwp'),
                    'style-2' => __('Style 2', 'saudagarwp'),
                ],
                'default' => 'style-1',
            ]
        );

        $this->add_control(
            'setting_empty_category',
            [
                'label' => __('Hide Empty Category', 'saudagarwp'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'saudagarwp'),
                'label_off' => __('Off', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'setting_hide_sub_category',
            [
                'label' => __('Hide Sub Category', 'saudagarwp'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'saudagarwp'),
                'label_off' => __('Off', 'saudagarwp'),
            ]
        );


        $slides_to_show = range(1, 10);
        $slides_to_show = array_combine($slides_to_show, $slides_to_show);
        $this->add_responsive_control(
            'slider_item',
            [
                'label' => __('Slides Per View', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'options' => $slides_to_show,
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 4,
                'tablet_default' => 3,
                'mobile_default' => 1,
                'condition' => [
                    'setting_style!' => 'style-2',
                ],
            ]
        );

        $this->add_control(
            'slider_loop',
            [
                'label' => __('Infinite Loop', 'saudagarwp'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'saudagarwp'),
                'label_on' => __('On', 'saudagarwp'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'slider_auto_play',
            [
                'label' => __('Auto Play', 'saudagarwp'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'saudagarwp'),
                'label_off' => __('Off', 'saudagarwp'),
                'return_value' => 'yes',
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        include dirname(__FILE__) . '/product-view.php';
    }
}
