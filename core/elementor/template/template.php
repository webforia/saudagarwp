<?php
namespace Retheme\Elementor;
use Retheme\Elementor_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Template_Partial extends Elementor_Base
{

    public function get_name()
    {
        return 'retheme-template';
    }

    public function get_title()
    {
        return __('Template Partial', 'saudagarwp');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post';
    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function register_controls()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Content', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'part',
            [
                'label' => __('Template Location', 'saudagarwp'),
                'type' => Controls_Manager::TEXT,
            ]
        );
        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();

        rt_get_template_part($settings['part']);

    }
    /* end class */
}
