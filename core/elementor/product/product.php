<?php
/**
 * Class Product
 * @version 1.2.0
 */
namespace Retheme\Elementor;

use Elementor;
use Elementor\Controls_Manager;
use Retheme\Elementor_Base;
use Retheme\Helper;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Products extends Elementor_Base
{
    public function get_name()
    {
        return 'retheme-product';
    }

    public function get_title()
    {
        return __('Products', 'saudagarwp');
    }

    public function get_icon()
    {
        return 'ate-icon ate-product';

    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function register_controls()
    {
        $this->setting_query();
        $this->setting_option();
        $this->setting_carousel([
            'gap' => 'no',
        ]);

    }

    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', 'saudagarwp'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'product',
            ]
        );

        $this->add_control(
            'posts_per_page',
            [
                'label' => __('Posts Number', 'saudagarwp'),
                'type' => Controls_Manager::NUMBER,
                'default' => 6,
            ]
        );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'saudagarwp'),
                'type' => Controls_Manager::HEADING,
            ]
        );
        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Latest Products', 'saudagarwp'),
                    'featured' => __('Featured Products', 'saudagarwp'),
                    'category' => __('Categories', 'saudagarwp'),
                    'manually' => __('Hand Picks', 'saudagarwp'),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Category', 'saudagarwp'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'default' => 'lastest',
                'options' => Helper::get_terms('product_cat'),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Product', 'saudagarwp'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts('product', apply_filters('limit_option_get_posts', 30)),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'saudagarwp'),
                    'total_sales' => __('Most Selling', 'saudagarwp'),
                    'wp_post_views_count' => __('Most Viewer', 'saudagarwp'),
                    'comment_count' => __('Most Review', 'saudagarwp'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'saudagarwp'),
                    'DESC' => __('DESC', 'saudagarwp'),
                ],
            ]
        );

        // $this->add_control(
        //     'offset',
        //     [
        //         'label' => __('Offset', 'saudagarwp'),
        //         'type' => Controls_Manager::NUMBER,
        //         'default' => 0,
        //         'condition' => [
        //             'query_by!' => 'manually',
        //         ],
        //         'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'saudagarwp'),
        //     ]
        // );
        $this->end_controls_section();
    }

    protected function setting_option()
    {
        $this->start_controls_section(
            'setting_product',
            [
                'label' => __('Options', 'saudagarwp'),
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                    6 => 6,
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        if (rt_is_premium()) {
            $this->add_control(
                'pagination_style',
                [
                    'label' => __('Pagination Style', 'saudagarwp'),
                    'type' => Controls_Manager::SELECT,
                    'default' => 'no_pagination',
                    'options' => array(
                        'no_pagination' => 'No Pagination',
                        'loadmore' => 'Load More',
                    ),
                ]
            );
        }

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        echo $this->elementor_loop(wp_parse_args([
            'id' => $this->get_id(),
            'class_wrapper' => 'products woocommerce',
            'template_part' => 'core/elementor/product/product-view',
            'layout_masonry' => (rt_option('woocommerce_archive_masonry', false)) ? 'yes' : '',
        ], $settings));
    }
}
