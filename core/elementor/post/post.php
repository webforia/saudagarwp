<?php
namespace Retheme\Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Retheme\Elementor_Base;
use Retheme\Helper;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts extends Elementor_Base
{
    protected $post_type = 'post';
    protected $post_taxonomy = 'category';

    public function get_name()
    {
        return 'retheme-post';
    }

    public function get_title()
    {
        return __('Posts Grid', 'saudagarwp');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post';
    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function register_controls()
    {
        $this->setting_query();
        $this->setting_options(); //protected

        $this->style_title();
        $this->style_meta();
        $this->style_excerpt();
        $this->style_readmore();
        $this->style_loadmore();
        $this->setting_carousel([
            'gap' => 'no',
        ]);

    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query($args = array())
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'saudagarwp'),
            ]
        );

        /**
         *  Number perpage not show if widget smart-tiles
         */
        if (empty($args['limit_perpage'])) {
            $this->add_control(
                'posts_per_page',
                [
                    'label' => __('Posts Number', 'saudagarwp'),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 6,
                ]
            );
            $this->add_control(
                'advanced',
                [
                    'label' => __('Advanced', 'saudagarwp'),
                    'type' => Controls_Manager::HEADING,
                ]
            );
        } else {
            $this->add_control(
                'advanced',
                [
                    'label' => __('Advanced', 'saudagarwp'),
                    'type' => Controls_Manager::HEADING,
                    'separator' => 'before',
                ]
            );
        }

        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'saudagarwp'),
                    'category' => __('Categories', 'saudagarwp'),
                    'manually' => __('Hand Picks', 'saudagarwp'),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Categories', 'saudagarwp'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_terms($this->post_taxonomy),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Post', 'saudagarwp'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts($this->post_type, apply_filters('limit_option_get_posts', 30)),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => __('Post Id', 'saudagarwp'),
                    'author' => __('Post Author', 'saudagarwp'),
                    'title' => __('Title', 'saudagarwp'),
                    'date' => __('Date', 'saudagarwp'),
                    'modified' => __('Last Modified Date', 'saudagarwp'),
                    'parent' => __('Parent Id', 'saudagarwp'),
                    'rand' => __('Random', 'saudagarwp'),
                    'comment_count' => __('Comment Count', 'saudagarwp'),
                    'menu_order' => __('Menu Order', 'saudagarwp'),
                    'wp_post_views_count' => __('Most Viewer', 'saudagarwp'),
                    'comment_count' => __('Most Review', 'saudagarwp'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'saudagarwp'),
                    'DESC' => __('DESC', 'saudagarwp'),
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'saudagarwp'),
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                    6 => 6,
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'excerpt',
            [
                'label' => __('Excerpt Number', 'saudagarwp'),
                'type' => Controls_Manager::NUMBER,
                'default' => __('18', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'readmore',
            [
                'label' => __('Read More', 'saudagarwp'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Read More', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'saudagarwp'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'saudagarwp'),
                'label_off' => __('Off', 'saudagarwp'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'saudagarwp'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'saudagarwp'),
                'label_off' => __('Off', 'saudagarwp'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'saudagarwp'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'saudagarwp'),
                'label_off' => __('Off', 'saudagarwp'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'saudagarwp'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'saudagarwp'),
                'label_off' => __('Off', 'saudagarwp'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );

        $this->end_controls_section();
    }

    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'saudagarwp'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', 'saudagarwp'),
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title a' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', 'saudagarwp'),
            ]
        );
        $this->add_control(
            'title_color_hover',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title a:hover' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .rt-post__title',
            ]
        );

        $this->end_controls_section();
    }

    public function style_meta()
    {
        $this->start_controls_section(
            'style_meta',
            [
                'label' => __('Meta', 'saudagarwp'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('meta_tabs');
        $this->start_controls_tab(
            'meta_normal',
            [
                'label' => __('Normal', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'meta_color',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post .rt-post__meta,
                    {{WRAPPER}} .rt-post .rt-post__meta a' => 'color: {{VALUE}}!important;',
                    '{{WRAPPER}} .rt-post .rt-post__meta-item::before' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'meta_hover',
            [
                'label' => __('Hover', 'saudagarwp'),
            ]
        );
        $this->add_control(
            'meta_color_hover',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */
        $this->end_controls_section();
    }

    public function style_excerpt()
    {
        $this->start_controls_section(
            'style_excerpt',
            [
                'label' => __('Excerpt', 'saudagarwp'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'excerpt_color',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'excerpt_typography',
                'selector' => '{{WRAPPER}} .rt-post__content',
            ]
        );
        $this->end_controls_section();
    }

    public function style_readmore()
    {
        $this->start_controls_section(
            'style_readmore',
            [
                'label' => __('Read More', 'saudagarwp'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('readmore_tabs');
        $this->start_controls_tab(
            'readmore_normal',
            [
                'label' => __('Normal', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'readmore_color',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__readmore' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'readmore_border',
            [
                'label' => __('Border Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__readmore' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'readmore_hover',
            [
                'label' => __('Hover', 'saudagarwp'),
            ]
        );
        $this->add_control(
            'readmore_color_hover',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__readmore:hover' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'readmore_border_hover',
            [
                'label' => __('Border Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__readmore:hover' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */
        $this->end_controls_section();
    }

    public function style_loadmore()
    {
        $this->start_controls_section(
            'style_loadmore',
            [
                'label' => __('Load More', 'saudagarwp'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('loadmore_tabs');
        $this->start_controls_tab(
            'loadmore_normal',
            [
                'label' => __('Normal', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'loadmore_color',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-pagination__button' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'loadmore_border',
            [
                'label' => __('Border Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-pagination__button' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'loadmore_hover',
            [
                'label' => __('Hover', 'saudagarwp'),
            ]
        );
        $this->add_control(
            'loadmore_color_hover',
            [
                'label' => __('Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-pagination__button:hover' => 'color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->add_control(
            'loadmore_border_hover',
            [
                'label' => __('Border Color', 'saudagarwp'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-pagination__button:hover' => 'border-color: {{VALUE}}!important;',
                ],
            ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */
        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        $args = array(
            'post_type' => 'post',
            'class' => 'rt-swiper--card',
            'id' => $this->get_id(),
            'template_part' => 'core/elementor/post/post-view',
            'layout_masonry' => (rt_option('blog_archive_masonry', false)) ? 'yes' : '',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
    /* end class */
}
