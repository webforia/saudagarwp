/*=================================================;
/* ELEMENTOR INIT
/*================================================= */
jQuery(window).on("elementor/frontend/init", function () {
  elementorFrontend.hooks.addAction("frontend/element_ready/retheme-product.default",swiperjs);
  elementorFrontend.hooks.addAction("frontend/element_ready/retheme-product-category.default",swiperjs);
  elementorFrontend.hooks.addAction("frontend/element_ready/retheme-post.default", swiperjs);
});

