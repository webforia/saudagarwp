<?php 
$link_target = $slider['link']['is_external'] ? ' target="_blank"' : '';
$link_nofollow = $slider['link']['nofollow'] ? ' rel="nofollow"' : '';
?>
<div class="rt-img rt-img--full rt-img--rounded">
    <?php if (!empty($slider['link']['url'])): ?>
    <a href="<?php echo esc_url($slider['link']['url']) ?>" <?php echo esc_attr( $link_target ) . esc_attr( $link_nofollow ) ?>>
    <?php endif?>

            <?php if ($slider['image']['id']): ?>
                <?php echo wp_get_attachment_image($slider['image']['id'], $settings['image_size']); ?>
            <?php else: ?>
                <img src="<?php echo esc_url($slider['image']['url']) ?>">
            <?php endif?>

    <?php if (!empty($slider['link']['url'])): ?>
    </a>
    <?php endif?>
</div>