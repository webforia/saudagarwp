<?php
namespace Retheme\Elementor;

use Elementor\Controls_Manager;
use Retheme\Elementor_Base;
use Retheme\Helper;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Carousel extends Elementor_Base
{
    public function get_name()
    {
        return 'retheme-carousel';
    }

    public function get_title()
    {
        return __('Image Carousel', 'saudagarwp');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-tiles';

    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function register_controls()
    {
        $this->setting_options(); //protected
        $this->setting_content();

        // extend retheme base
        $this->setting_carousel(array(
            'carousel' => 'no',
            'navigation' => 'no',
            'gap' => 'no',
        ));
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'saudagarwp'),
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'saudagarwp'),
                'type' => Controls_Manager::SELECT,
                'default' => 'large',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->end_controls_section();
    }

    public function setting_content()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'saudagarwp'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'image',
            [
                'label' => __('Choose Image', 'saudagarwp'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $repeater->add_control(
            'title', [
                'label' => __('Title', 'saudagarwp'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Title', 'saudagarwp'),
            ]
        );

        $repeater->add_control(
            'link',
            [
                'label' => __('url', 'saudagarwp'),
                'type' => Controls_Manager::URL,
                'placeholder' => __('https://your-link.com', 'saudagarwp'),
                'show_external' => true,
                'label_block' => false,
                'default' => [
                    'url' => '',
                    'is_external' => false,
                    'nofollow' => true,
                ],
            ]
        );

        $this->add_control(
            'sliders',
            [
                'label' => __('Item', 'saudagarwp'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'title' => __('Slide #1', 'saudagarwp'),
                    ],
                ],
                'title_field' => '{{{ title }}}',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();
        $classes = array();

        // Pagination
        $slider_pagination = array();

        if ($settings['slider_pagination'] == 'yes') {
            $slider_pagination = array(
                'pagination' => [
                    'el' => '.swiper-pagination',
                    'clickable' => true,
                ],
            );
        }

        echo rt_before_slider(wp_parse_args($slider_pagination, [
            'id' => "rt-swiper-{$this->get_id()}",
            'class' => implode(' ', $classes),
            'items-lg' => !empty($settings['slider_item'])? $settings['slider_item']: 3,
            'items-md' => !empty($settings['slider_item_tablet'])? $settings['slider_item_tablet']: 3,
            'items-sm' => !empty($settings['slider_item_mobile'])? $settings['slider_item_mobile']: 3,
            'loop' => ($settings['slider_loop'] == 'yes') ? true : false,
            'autoplay' => ($settings['slider_auto_play'] == 'yes') ? true : false,
            'sameheight' => true,
        ]));

        foreach ($settings['sliders'] as $key => $slider) {
            echo rt_html_open(['class' => 'swiper-slide']);
            include dirname(__FILE__) . '/carousel-view.php';
            echo rt_html_close();
        }

        echo rt_after_slider();
    }
}
