<?php
namespace Retheme;

use Elementor;

if (!defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly

/**
 * Main Plugin Class
 *
 * Register new elementor widget.
 *
 * @since 1.0.0
 */

class Retheme_Elementor
{

    /**
     * Constructor
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function __construct()
    {
        add_action('elementor/widgets/widgets_registered', [$this, 'on_widgets_registered']);
        add_action('elementor/init', array($this, 'register_category'));
        add_action('wp_enqueue_scripts', [$this, 'register_frontend_scripts']);
    }

    /**
     * On Widgets Registered
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function on_widgets_registered()
    {
        $this->includes_part();
        $this->register_widget();
        $this->register_editor_style();
    }

    public function register_category()
    {
        Elementor\Plugin::instance()->elements_manager->add_category(
            'retheme-elements',
            [
                'title' => 'Theme Elements',
                'icon' => 'font',
            ],
            1
        );
    }

    /**
     * Includes
     *
     * @since 1.0.0
     *
     * @access private
     */
    private function includes_part()
    {
        require __DIR__ . '/post/post.php';
        require __DIR__ . '/carousel/carousel.php';
        require __DIR__ . '/template/template.php';

        if (rt_is_woocommerce()) {
            require __DIR__ . '/product/product.php';
            require __DIR__ . '/product-category/product.php';
        }

    }

    public function register_frontend_scripts()
    {   
        wp_enqueue_script('retheme-elementor', get_template_directory_uri() . '/core/elementor/assets/js/retheme-elementor.min.js', array('jquery'), '1.3.0', true);
        wp_enqueue_script('retheme-elementor-masonry', get_template_directory_uri() . '/core/elementor/assets/js/retheme-elementor-masonry.min.js', array('jquery', 'retheme-masonry'), '1.2.0', true);
    }

    public function register_editor_style()
    {
        /**
         * Add CSS on editor Elementor
         */
        add_action('elementor/editor/after_enqueue_scripts', function () {
            wp_enqueue_style('elementor_editor_style', get_template_directory_uri() . '/core/elementor/assets/css/elementor-editor.css', false, '1.2.0');
        });
    }

    /**
     * Register Widget
     *
     * @since 1.0.0
     *
     * @access private
     */
    private function register_widget()
    {
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Retheme\Elementor\Posts());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Retheme\Elementor\Carousel());


        if (rt_is_woocommerce()) {
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Retheme\Elementor\Products());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \Retheme\Elementor\Product_Categories());
        }
    }

    // end class
}

new Retheme_Elementor();
