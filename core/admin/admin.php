<?php
/*====================================================
/* REGISTER SCRIPTS ADMIN
/*====================================================*/
function rt_admin_register_scripts() {
    wp_enqueue_style('retheme-admin', get_template_directory_uri() . '/core/admin/assets/css/admin-style.min.css', '', '3.0.0');
    wp_enqueue_script('retheme-admin', get_template_directory_uri() . '/core/admin/assets/js/admin-script.js', ['jquery'], '2.0.0', true);
}
add_action('admin_enqueue_scripts', 'rt_admin_register_scripts', 999);

/*====================================================
/* ADD PANEL MENU
/*====================================================*/
function rt_admin_add_menu_panel() {

    add_menu_page('Theme Panel', 'Theme Panel', 'manage_options', 'theme-panel', false, '', 999);

    add_submenu_page('theme-panel', 'Dashboard', 'Dashboard', 'manage_options', 'theme-panel', function () {
        get_template_part("core/admin/views/dashboard");
    }, 1);

    add_submenu_page('theme-panel', 'Theme Setup', 'Theme Setup', 'manage_options', 'theme-setup', function () {
        $url = get_admin_url() . 'themes.php?page=merlin';
        echo "<script>location.href = '{$url}'</script>";
    }, 10);

    add_submenu_page('theme-panel', 'License', 'License', 'manage_options', 'theme-license', function () {
        get_template_part("core/admin/views/license");
    }, 99);
}
add_action('admin_menu', 'rt_admin_add_menu_panel');


/*====================================================
/* NOTICE ADMIN
/*====================================================*/
function rt_admin_license_key_notice() {
    $product_name = RT_THEME_NAME;
    $product_slug = RT_THEME_SLUG;
    $product_key = get_option("{$product_slug}_key");
    $product_domain_register = get_option("{$product_slug}_domain_register", true);
    $site_url = esc_html(preg_replace('#^https?://(?:www\.)?#', '', site_url()));
    $license_page_url = esc_url(admin_url('admin.php?page=theme-license'));
    $member_area_url = esc_url('https://webforia.id/akun');
    $license_page = (!empty($_GET['page']) && $_GET['page'] === 'theme-license');
    $license = new \Retheme\Activation();

    if (empty($product_key) && !$license->is_local() && !$license_page) {
        $html  = "<div class='notice notice-warning is-dismissible'>";
        $html .= sprintf('<p>' . esc_html__('Anda belum memasukkan kunci lisensi untuk tema %s. Tanpa kunci lisensi, Anda tidak dapat mengakses fitur premium, pembaruan, dan dukungan teknis.', 'opini-text') . '</p>', $product_name);
        $html .= "<p><a class='button is-secondary' href='" . esc_attr($license_page_url) . "'>" . esc_html__('Masukkan kunci lisensi sekarang', 'opini-text') . "</a></p>";
        $html .= "</div>";

        echo wp_kses_post($html);
    }

    if ($license->is_premium() && empty($product_domain_register) && !$license->is_local()) {
        $html  = "<div class='notice notice-error is-dismissible'>";
        $html .= sprintf('<p>' . esc_html__('Anda sedang menggunakan tema %s, namun domain %s tidak terdaftar dalam sistem kami. Silakan reset lisensi, kemudian lakukan aktivasi ulang menggunakan kode lisensi yang Anda miliki.', 'opini-text') . '</p>', $product_name, $site_url);
        if (!$license_page) {
            $html .= "<p><a class='button is-secondary' href='" . esc_attr($license_page_url) . "'>" . esc_html__('Reset lisensi sekarang', 'opini-text') . "</a></p>";
        }
        $html .= "</div>";

        echo wp_kses_post($html);
    }

    if ($license->is_local() && !$license_page) {
        $html  = "<div class='notice notice-warning'>";
        $html .= sprintf('<p>' . esc_html__('Anda sedang menggunakan %s di localhost server, semua fitur Premium akan aktif pada server lokal. Anda wajib memasukan lisensi ketika di hosting atau server online', 'a') . '</p>', $product_name);
        $html .= "</div>";

        echo wp_kses_post($html);
    }
}
add_action('admin_notices', 'rt_admin_license_key_notice', 99);
