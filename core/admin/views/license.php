<?php
// Product attribute
$product_name = RT_THEME_NAME;
$product_slug = RT_THEME_SLUG;
$product_url = RT_THEME_URL;
$product_doc = RT_THEME_DOC;
$product_version = RT_THEME_VERSION;

// Form value
$license_key = !empty($_POST['product_license_key']) ? sanitize_text_field($_POST['product_license_key']) : '';
$license_action = !empty($_POST['product_license_key']) ? sanitize_text_field($_POST['product_license_submit']) : '';

// API Manager
$license = new Retheme\Activation([
    'product_key' => $license_key,
]);

$manager = array('result' => '');

if ($license_action) {
    $manager = ($license_action === 'activate') ? $license->activate() : $license->deactivate();
}

// Get license data attribute
$product_key = get_option("{$product_slug}_key");
$get_status = $license->get_status();
$expired_date = $license->get_expired_date(true);
$get_days_until_expiry = $license->get_expired_date_duration();
$is_local = $license->is_local();
$is_premium = $license->is_premium();
?>

<div class="rta theme-panel" style="margin-top: 40px">
    <section class="rta-site__header">
        <h1 class="rta-site__title"><?php echo sprintf('%s Lisensi', $product_name); ?></h1>
        <strong><?php echo sprintf('Version %s', $product_version); ?></strong>
    </section>

    <div class="rta-site__body">

        <div class="grids grids-md-2">
            <?php if (!empty($manager['result'])) : ?>
                <?php if ($manager['result'] == 'error') : ?>
                    <div class="rta-alert rta-alert--warning"><?php echo $manager['message'] ?><a class="js-alert-close rta-alert__close"><span class="rta-icon"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                </svg></span></a></div>
                <?php else : ?>
                    <div class="rta-alert rta-alert--success"><?php echo $manager['message'] ?><a class="js-alert-close rta-alert__close"><span class="rta-icon"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                    <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                                </svg></span></a></div>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <div class="grids grids-md-2">

            <div class="rta-panel">

                <div class="rta-panel__header">
                    <h4 class="rta-panel__title">Lisensi

                        <?php if ($is_local) : ?>
                            <span class="rta-badges rta-badges--success" style="float: right">Local Lisensi</span>
                        <?php elseif ($get_status === 'active') : ?>
                            <span class="rta-badges rta-badges--success" style="float: right">Active</span>
                        <?php elseif ($get_status === 'expired') : ?>
                            <span class="rta-badges rta-badges--warning" style="float: right">Expired</span>
                        <?php else : ?>
                            <span class="rta-badges rta-badges--warning" style="float: right"><?php echo ucwords($get_status) ?></span>
                        <?php endif ?>

                    </h4>

                </div>

                <div class="rta-panel__body">
                    <div class="rta-form-wrapper">
                        <div class="content mb-5">

                            <?php if ($is_local) : ?>
                                <p><?php echo sprintf('Anda sedang menggunakan %s di localhost server, semua fitur <strong>Premium</strong> akan aktif pada server lokal. Anda wajib memasukan lisensi ketika di hosting atau server online.', $product_name); ?></p>
                            <?php elseif ($get_status === 'active') : ?>
                                <p><?php echo sprintf('Tema Anda berhasil diaktifkan, silakan cek <a href="%s" target="_blank">di sini</a> untuk melihat panduan tema.', esc_url($product_doc)); ?></p>
                            <?php elseif ($get_status === 'trial') : ?>
                                <p><?php echo sprintf('Saat ini Anda menggunakan tema %s versi uji coba (Trial).', $product_name); ?></p>
                            <?php elseif ($get_status === 'expired') : ?>
                                <p><?php echo sprintf('Lisensi tema Anda berakhir pada tanggal <strong>%s</strong>, silakan lakukan perpanjangan lisensi untuk tetap mendapatkan update dan support. Perpanjangan dapat dilakukan melalui member area. Klik <a href="https://webforia.id/akun/wlm-license/" target="_blank">di sini</a> untuk memperpanjang.', $expired_date); ?></p>
                            <?php else : ?>
                                <p><?php echo sprintf('Masukkan kunci lisensi Anda untuk dapat menggunakan semua fitur premium, update, dan support. <a target="_blank" href="%s">Dapatkan lisensi di sini</a>.', esc_url($product_url)); ?></p>
                            <?php endif ?>

                            <?php if ($is_premium && !in_array($get_status, ['unregistered', 'expired']) && !$is_local && $product_key) : ?>
                                <p><?php echo sprintf('Aktif Hingga: %s (%d hari)', $expired_date, $get_days_until_expiry); ?></p>
                            <?php endif ?>

                        </div>

                        <?php if (!$is_local): ?>
                            <form action="#" method="post">
                                <div class="rta-form">
                                    <label class="rta-form__label">Lisensi Key</label>

                                    <?php if ($is_premium) : ?>
                                        <input class="rta-form__input" type="password" value="<?php echo $product_key; ?>" disabled />
                                        <input class="rta-form__input" id="product_license_key" name="product_license_key" type="hidden" value="<?php echo $product_key; ?>" />
                                    <?php else : ?>
                                        <input class="rta-form__input" id="product_license_key" name="product_license_key" type="text" value="<?php echo $product_key; ?>" required />
                                    <?php endif ?>

                                </div>
                                <div class="rta-form">

                                    <?php if ($is_premium) : ?>
                                        <button class="rta-btn" name="product_license_submit" type="submit" value="reset">Reset License</button>
                                    <?php else : ?>
                                        <button class="rta-btn rta-btn" name="product_license_submit" type="submit" value="activate">Active License</button>
                                    <?php endif ?>

                                </div>
                            </form>
                        <?php endif ?>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>