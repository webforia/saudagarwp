<div class="rta theme-panel" style="margin-top: 40px">
    <section class="rta-site__header">
        <h1 class="rta-site__title"><?php echo RT_THEME_NAME ?> Dashboard </h1>
        <strong>Version <?php echo RT_THEME_VERSION ?></strong>
    </section>
    <section class="rta-site__body">
        <div class="grids grids-md-3">
            <div class="rta-panel">
                <div class="rta-panel__header">
                    <h4 class="rta-panel__title">Panduan</h4>
                </div>
                <div class="rta-panel__body">
                    <p>Pelajari lebih lanjut cara menggunakan Tema</p>
                </div>
                <div class="rta-panel__footer">
                    <a href="<?php echo RT_THEME_DOC ?>" target="_blank" class="rta-btn"> Pelajari Sekarang </a>
                </div>
            </div>

        </div>
    </section>
</div>