document.addEventListener("DOMContentLoaded", () => {
  (document.querySelectorAll(".js-alert-close") || []).forEach(
    ($delete) => {
      const $notification = $delete.parentNode;

      $delete.addEventListener("click", () => {
        $notification.parentNode.removeChild($notification);
      });
    }
  );

  (document.querySelectorAll(".js-modal-close") || []).forEach(
    ($delete) => {
      const $modal = $delete.closest('.rta-modal');

      $delete.addEventListener("click", () => {
        $modal.style.display = 'none';
      });
    }
  );
});
