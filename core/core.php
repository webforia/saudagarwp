<?php
include_once dirname(__FILE__) . '/vendor/vendor.php';
include_once dirname(__FILE__) . '/classes/class.php';
include_once dirname(__FILE__) . '/admin/admin.php';
include_once dirname(__FILE__) . '/widget/widget.php';


// Skip on wc ajax request
$ajax_request = (!empty($_GET['wc-ajax']) || strpos( $_SERVER['REQUEST_URI'], 'wp-json'))? true: false;

if (!$ajax_request) {
    include_once dirname(__FILE__) . '/customizer/customizer.php';
}

include_once dirname(__FILE__) . '/includes/include.php';

if (rt_is_woocommerce()) {
    include_once dirname(__FILE__) . '/woocommerce/woocommerce.php';
}

if (did_action('elementor/loaded') && rt_is_premium()) {
    include_once dirname(__FILE__) . '/elementor/elementor.php';
}
