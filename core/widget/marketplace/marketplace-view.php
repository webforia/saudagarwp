<?php 
if($style == 'full'){
    $blibli_img = get_template_directory_uri() . '/assets/img/marketplace-blibli-full.webp';
    $bukalapak_img = get_template_directory_uri() . '/assets/img/marketplace-bukalapak-full.webp';
    $tokopedia_img = get_template_directory_uri() . '/assets/img/marketplace-tokopedia-full.webp';
    $lazada_img = get_template_directory_uri() . '/assets/img/marketplace-lazada-full.webp';
    $shopee_img = get_template_directory_uri() . '/assets/img/marketplace-shopee-full.webp';
    $tiktok_img = get_template_directory_uri() . '/assets/img/marketplace-tiktok-full.webp';
    $width = '100';
    $height = '38';
}else{
    $blibli_img = get_template_directory_uri() . '/assets/img/marketplace-blibli.webp';
    $bukalapak_img = get_template_directory_uri() . '/assets/img/marketplace-bukalapak.webp';
    $tokopedia_img = get_template_directory_uri() . '/assets/img/marketplace-tokopedia.webp';
    $lazada_img = get_template_directory_uri() . '/assets/img/marketplace-lazada.webp';
    $shopee_img = get_template_directory_uri() . '/assets/img/marketplace-shopee.webp';
    $tiktok_img = get_template_directory_uri() . '/assets/img/marketplace-tiktok.webp';
    $width = '50';
    $height = '50';
}
?>
<div class="<?php echo "rt-img-list"?>">
    <?php if($blibli): ?>
     <a href="<?php echo esc_url_raw($blibli)?>" class="rt-img-list__item blibli" target="_blank"><img src="<?php echo $blibli_img ?>" alt="blibli" width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height)?>"></a>
     <?php endif ?>

     <?php if ($bukalapak): ?>
     <a href="<?php echo esc_url_raw($bukalapak)?>" class="rt-img-list__item bukalapak" target="_blank"><img src="<?php echo $bukalapak_img ?>" alt="bukalapak" width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height)?>"></a>
     <?php endif?>

     <?php if($tokopedia): ?>
     <a href="<?php echo esc_url_raw($tokopedia)?>" class="rt-img-list__item tokopedia" target="_blank"><img src="<?php echo $tokopedia_img ?>" alt="tokopedia" width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height)?>"></a>
     <?php endif ?>

     <?php if($lazada): ?>
     <a href="<?php echo esc_url_raw($lazada)?>" class="rt-img-list__item lazada" target="_blank"><img src="<?php echo $lazada_img ?>" alt="lazada" width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height)?>"></a>
     <?php endif ?>
     
     <?php if ($shopee): ?>
     <a href="<?php echo esc_url_raw($shopee)?>" class="rt-img-list__item shopee" target="_blank"><img src="<?php echo $shopee_img ?>" alt="shopee" width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height)?>"></a>
     <?php endif?>

     <?php if ($tiktok): ?>
     <a href="<?php echo esc_url_raw($tiktok)?>" class="rt-img-list__item tiktok" target="_blank"><img src="<?php echo $tiktok_img ?>" alt="shopee" width="<?php echo esc_attr($width)?>" height="<?php echo esc_attr($height)?>"></a>
     <?php endif?>
</div>