<?php
// Creating the widget
class Retheme_Marketplace extends WP_Widget
{
    public function __construct()
    {
        parent::__construct('retheme_marketplace', __("Saudagar WP - Marketplace", 'saudagarwp'), array(
            'customize_selective_refresh' => true,
        ));
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget($widget, $instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : __('Marketplace');
        $style = !empty($instance['style']) ? $instance['style'] : 'icon';
        $blibli = rt_option('marketplace_blibli', 'https://www.blibli.com');
        $bukalapak = rt_option('marketplace_bukalapak', 'https://www.bukalapak.com');
        $tokopedia = rt_option('marketplace_tokopedia', 'https://www.tokopedia.com');
        $lazada = rt_option('marketplace_lazada', 'https://www.lazada.co.id');
        $shopee = rt_option('marketplace_shopee', 'https://shopee.co.id');
        $tiktok = rt_option('marketplace_tiktok', 'https://www.tiktok.com');


        if (array_key_exists('before_widget', $widget)) {
            echo $widget['before_widget'];
            if (!empty($title)) {
                echo $widget['before_title'] . $title . $widget['after_title'];
            }

        }

        include dirname(__FILE__) . '/marketplace-view.php';
     
        if (array_key_exists('after_widget', $widget)) {
            echo $widget['after_widget'];
        }
    }

    // Widget Backend
    public function form($instance)
    {
        $title = !empty($instance['title']) ? $instance['title'] : __('Marketplace');
        $style = !empty($instance['style']) ? $instance['style'] : 'icon';

        ?>
         <p><?php echo sprintf(__('Visit the <a href="%s">marketplace options</a> to add and edit shop url on marketplace', 'saudagarwp'), "javascript:wp.customize.section( 'marketplace_section' ).focus();");?></p>
        <p>
            <label ><?php _e('Title:');?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

         <p>
          <label><?php _e('Style:');?></label>
           <select class="widefat" id="<?php echo $this->get_field_id('style'); ?>" name="<?php echo $this->get_field_name('style'); ?>">
                <option value="icon" <?php selected( $style, 'icon' ); ?>>Icon</option>
                <option value="full" <?php selected( $style, 'full' ); ?>>Full</option>
            </select>
        </p>


        <?php
}

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        
        $instance = array();
        $instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : __('Marketplace');
        $instance['style'] = !empty($new_instance['style']) ? strip_tags($new_instance['style']) : 'icon';

        return $instance;
    }
} // Class wpb_widget ends here
