<?php
// Creating the widget
class Retheme_Woo_Ordering extends WP_Widget {
    public function __construct() {

        $args = array(
            'description' => __('Output the product sorting options',  'saudagarwp'),
            'customize_selective_refresh' => true,
        );
        parent::__construct('retheme_product_sort', __("Saudagar WP - WooCommerce Sorting", 'saudagarwp'), $args);
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget($widget, $instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('Sort by', 'saudagarwp');

        if (array_key_exists('before_widget', $widget)) {
            echo $widget['before_widget'];
            if (!empty($title)) {
                echo $widget['before_title'] . $title . $widget['after_title'];
            }
        }

        $catalog_orderby = apply_filters('woocommerce_catalog_orderby', array(
            'menu_order' => __('Default sorting', 'woocommerce'),

            'popularity' => __('Sort by popularity', 'woocommerce'),

            'rating' => __('Sort by average rating', 'woocommerce'),

            'date' => __('Sort by latest', 'woocommerce'),

            'price' => __('Sort by price: low to high', 'woocommerce'),

            'price-desc' => __('Sort by price: high to low', 'woocommerce'),
        ));

        if (get_option('woocommerce_enable_review_rating') == 'no') {
            unset($catalog_orderby['rating']);
        }

        // Get current order
        $current = !empty($_GET['orderby']) ? wc_clean($_GET['orderby']) : apply_filters('woocommerce_default_catalog_orderby', get_option('woocommerce_default_catalog_orderby'));

        echo "<ul>";
        foreach ($catalog_orderby as $id => $name) {

            // Set active link
            $active = ($current == $id) ? "class=is-active" : "";

            // Current page
            $url_var = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';

            $orderby = add_query_arg('orderby', wc_clean($id), $url_var);

            echo '<li ' . esc_attr($active) . '><a href="' . $orderby . '" >' . esc_attr($name) . '</a></li>';
        }
        echo "</ul>";

        if (array_key_exists('after_widget', $widget)) {
            echo $widget['after_widget'];
        }
    }

    // Widget Backend
    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('Sort by');

?>

        <p>
            <label><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

<?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';

        return $instance;
    }
} // Class wpb_widget ends here
