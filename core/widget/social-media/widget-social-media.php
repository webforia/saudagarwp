<?php
// Creating the widget
class Retheme_Social_Widget extends WP_Widget {
    public function __construct() {

        parent::__construct('retheme_social', __("Saudagar WP - Social Media", 'saudagarwp'), array(
            'description' => __('Widget for sosial media link'),
            'customize_selective_refresh' => true,
        ));
    }


    // Creating widget front-end
    // This is where the action happens
    public function widget($widget, $instance) {
        $title = !empty($instance['title']) ? $instance['title'] : '';
        $style = !empty($instance['style']) ? $instance['style'] : 'simple';
        $size = !empty($instance['size']) ? $instance['size'] : 'sm';

        if (array_key_exists('before_widget', $widget)) {
            echo $widget['before_widget'];
            if (!empty($title)) {
                echo $widget['before_title'] . $title . $widget['after_title'];
            }
        }


        if ($size === 'large') {
            $size =  'lg';
        }

        if ($size === 'medium') {
            $size =  'md';
        }

        if ($size === 'small') {
            $size =  'sm';
        }



        $classes = "rt-socmed--{$style} rt-socmed--{$size}";

        rt_social_media(['class' => $classes]);

        if (array_key_exists('after_widget', $widget)) {
            echo $widget['after_widget'];
        }
    }

    // Widget Backend
    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : '';
        $style = !empty($instance['style']) ? $instance['style'] : '';
        $size = !empty($instance['size']) ? $instance['size'] : '';

        // Option
        $style_lists = array('simple', 'brand');
        $size_lists = array('small', 'medium', 'large');

        ?>
        <p><?php echo sprintf(__('Visit the <a href="%s">social media options</a> to add and edit social media account', 'saudagarwp'), "javascript:wp.customize.section( 'social_section' ).focus();");?></p>
        <p>
            <label><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <!-- style social media -->
        <p>
            <label><?php _e('Style:'); ?></label>

            <select class="widefat" id="<?php echo $this->get_field_id('style'); ?>" name="<?php echo $this->get_field_name('style'); ?>">
                <?php foreach ($style_lists as $key => $style_list) : ?>
                    <option <?php selected($style == $style_list) ?> value="<?php echo $style_list ?>"><?php echo ucwords($style_list) ?></option>
                <?php endforeach ?>
            </select>
        </p>

        <p>
            <label><?php _e('Size:'); ?></label>

            <select class="widefat" id="<?php echo $this->get_field_id('size'); ?>" name="<?php echo $this->get_field_name('size'); ?>">
                <?php foreach ($size_lists as $key => $size_list) : ?>
                    <option <?php selected($size == $size_list) ?> value="<?php echo $size_list ?>"><?php echo ucwords($size_list) ?></option>
                <?php endforeach ?>
            </select>
        </p>

<?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : '';
        $instance['style'] = !empty($new_instance['style']) ? strip_tags($new_instance['style']) : '';
        $instance['size'] = !empty($new_instance['size']) ? strip_tags($new_instance['size']) : '';

        return $instance;
    }
} // Class wpb_widget ends here
