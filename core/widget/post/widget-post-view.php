<?php $date_url   = esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d'))); ?>

<div class="rt-post rt-post--list-small">
    <div class="rt-post__thumbnail rt-img rt-im--full">
        <?php rt_the_post_thumbnail('thumbnail'); ?>
    </div>
    <div class="rt-post__body">
        <h6 class="rt-post__title">
            <a href="<?php the_permalink() ?>"><?php echo get_the_title() ?></a>
        </h6>

        <?php if ($meta_date) : ?>
            <div class="rt-post__meta">
                <a class="rt-post__meta-item date" href="<?php echo esc_url($date_url) ?>">
                    <i class="rt-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-calendar3" viewBox="0 0 16 16">
                            <path d="M14 0H2a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"></path>
                            <path d="M6.5 7a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm-9 3a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm3 0a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path>
                        </svg>
                    </i>
                    <?php echo get_the_date() ?>
                </a>
            </div>
        <?php endif ?>

    </div>
</div>