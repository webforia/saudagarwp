<?php
include dirname(__FILE__) . '/menu/widget-nav-menu.php';
include dirname(__FILE__) . '/social-media/widget-social-media.php';
include dirname(__FILE__) . '/post/widget-post.php';
include dirname(__FILE__) . '/woo-ordering/woo-ordering.php';
include dirname(__FILE__) . '/marketplace/marketplace.php';

// Register and load the widget
function rt_custom_widget()
{
    if (rt_is_premium()) {
        register_widget('Retheme_Nav_Menu_Widget');
        register_widget('Retheme_social_widget');
        register_widget('Retheme_Post_Widget');
        register_widget('Retheme_Marketplace');
    }
    register_widget('Retheme_Woo_Ordering');

}
add_action('widgets_init', 'rt_custom_widget');
