<?php

namespace Retheme\Customizer;

use Retheme\Customizer_Base;
use Retheme\Helper;

class Header extends Customizer_Base {
    public function __construct() {
        $this->set_panel();
        $this->set_section();

        $this->add_header_builder();

        /** desktop header */
        $this->add_options_topbar();
        $this->add_options_middle();
        $this->add_options_main();
        $this->add_options_sticky();
        $this->add_options_overlay();

        /** Mobile header */
        $this->add_mobile_header();

        /** Main menu */
        $this->add_menu_primary();
        $this->add_menu_secondary();
        $this->add_menu_tertiary();
        $this->add_menu_off_canvas();
        $this->add_menu_mobile();

        /** Header Element */
        $this->add_element_social();

        $this->add_element_search_box();
        $this->add_element_search_box_mobile();

        $this->add_element_account();

        $this->add_element_button();
        $this->add_element_html();
    }

    public function set_panel() {
        $this->add_panel('header_panel', [
            'title' => __('Header', 'saudagarwp'),
        ]);
    }

    public function set_section() {

        $menu_link = sprintf(__('Visit the <a href="%s">Menus Area</a> to create menus and add items', 'saudagarwp'), "javascript:wp.customize.panel( 'nav_menus' ).focus();");
        $social_media_link = sprintf(__('Visit the <a href="%s">Social Media Options</a> to add and change items', 'saudagarwp'), "javascript:wp.customize.section( 'social_section' ).focus();");
        $logo_overlay = sprintf(__('Visit the <a href="%s">Logo options</a> to add and change logo on transparent header', 'saudagarwp'), "javascript:wp.customize.control( 'brand_logo_overlay' ).focus();");

        $this->add_section('header_panel', [
            'header_builder' => [__('Builder', 'saudagarwp')],

            'header_topbar' => [__('Header Top Bar', 'saudagarwp')],
            'header_middle' => [__('Header Middle', 'saudagarwp')],
            'header_main' => [__('Header Main', 'saudagarwp')],
            'header_mobile' => [__('Header Mobile', 'saudagarwp')],
            'header_overlay' => [__('Header Overlay', 'saudagarwp'), $logo_overlay],

            'header_menu_primary' => [__('Menu Primary', 'saudagarwp'), $menu_link],
            'header_menu_secondary' => [__('Menu Secondary', 'saudagarwp'), $menu_link],
            'header_menu_tertiary' => [__('Menu Tertiary', 'saudagarwp'), $menu_link],
            'header_menu_off_canvas' => [__('Menu Off Canvas', 'saudagarwp'), $menu_link],
            'header_menu_mobile' => [__('Menu Mobile', 'saudagarwp'), $menu_link],

            'header_search' => [__('Search Box', 'saudagarwp')],
            'header_search_mobile' => [__('Search Box Mobile', 'saudagarwp')],
            'header_socmed' => [__('Social Media', 'saudagarwp'), $social_media_link],

            'header_account' => [__('Account', 'saudagarwp')],

            'header_button_1' => [__('Button 1', 'saudagarwp')],
            'header_button_2' => [__('Button 2', 'saudagarwp')],
            'header_button_3' => [__('Button 3', 'saudagarwp')],

            'header_html_1' => [__('HTML 1', 'saudagarwp')],
            'header_html_2' => [__('HTML 2', 'saudagarwp')],
            'header_html_3' => [__('HTML 3', 'saudagarwp')],
            'header_html_4' => [__('HTML 4', 'saudagarwp')],
            'header_html_5' => [__('HTML 5', 'saudagarwp')],

        ]);
    }

    public function add_header_builder() {
        if (rt_is_premium()) {
            $elements = [
                'logo' => 'Logo',
                'menu-primary' => 'Primary Menu',
                'menu-secondary' => 'Secondary Menu',
                'menu-tertiary' => 'Tertiary Menu',
                'menu-off-canvas' => 'Off Canvas Menu',
                'cart-icon' => 'Cart Icon',
                'user-icon' => 'User Icon',
                'search-icon' => 'Search Icon',
                'search-form' => 'Search Box',
                'social' => 'Social Media',
                'divender-1' => '|',
                'divender-2' => '|',
                'divender-3' => '|',
                'divender-4' => '|',
                'divender-5' => '|',
                'button-1' => 'Button 1',
                'button-2' => 'Button 2',
                'button-3' => 'Button 3',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
                'html-3' => 'HTML 3',
                'html-4' => 'HTML 4',
                'html-5' => 'HTML 5',
            ];

            $mobile_elements = [
                'logo-mobile' => 'Logo',
                'toggle-menu' => 'Toggle Menu',
                'cart-icon' => 'Cart Icon',
                'user-icon' => 'User Icon',
                'search-icon-mobile' => 'Search Icon',
                'search-form' => 'Search Box',
                'divender-1' => '|',
                'divender-2' => '|',
                'divender-3' => '|',
                'divender-4' => '|',
                'divender-5' => '|',
                'button-1' => 'Button 1',
                'button-2' => 'Button 2',
                'button-3' => 'Button 3',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
                'html-3' => 'HTML 3',
                'html-4' => 'HTML 4',
                'html-5' => 'HTML 5',
            ];

            $drawer_elements = [
                'menu-mobile' => 'Menu Mobile',
                'search-form' => 'Search Box',
                'social' => 'Social Media',
                'button-1' => 'Button 1',
                'button-2' => 'Button 2',
                'button-3' => 'Button 3',
                'html-1' => 'HTML 1',
                'html-2' => 'HTML 2',
                'html-3' => 'HTML 3',
                'html-4' => 'HTML 4',
                'html-5' => 'HTML 5',
            ];
        } else {
            $elements = [
                'logo' => 'Logo',
                'menu-primary' => 'Menu Primary',
                'cart-icon' => 'Cart Icon',
                'user-icon' => 'User Icon',
                'search-icon' => 'Search Icon',
                'social' => 'Social Media',
                'divender-1' => '|',
                'divender-2' => '|',
                'divender-3' => '|',
                'divender-4' => '|',
                'divender-5' => '|',
                'button-1' => 'Button 1',
                'html-1' => 'HTML 1',
            ];

            $mobile_elements = [
                'logo-mobile' => 'Logo',
                'toggle-menu' => 'Toggle Menu',
                'cart-icon' => 'Cart Icon',
                'user-icon' => 'User Icon',
                'search-icon-mobile' => 'Search Icon',
                'divender-1' => '|',
                'divender-2' => '|',
                'divender-3' => '|',
                'divender-4' => '|',
                'divender-5' => '|',
                'button-1' => 'Button 1',
                'html-1' => 'HTML 1',

            ];

            $drawer_elements = [
                'menu-mobile' => 'Menu Mobile',
                'search-form' => 'Search Box',
                'button-1' => 'Button 1',
                'html-1' => 'HTML 1',

            ];
        }

        $this->add_field([
            'label' => __('Header Builder', 'saudagarwp'),
            'type' => 'builder',
            'settings' => 'header_builder_option',
            'section' => 'header_builder_section',
            'default' => rt_get_theme('header_builder_option'),
            'choices' => [
                'normal_elements' => $elements,
                'sticky_elements' => $elements,
                'mobile_elements' => $mobile_elements,
                'drawer_elements' => $drawer_elements,
            ],
            'partial_refresh' => [
                'builder' => [
                    'selector' => '.rt-header-customizer',
                    'render_callback' => function () {
                        ob_start();
                        rt_get_template_part('header/header');
                        $header_parts = ob_get_clean();
                        return $header_parts;
                    },
                ],
            ],
        ]);
    }

    public function add_options_topbar() {
        if (rt_is_premium()) {
            $section = 'header_topbar_section';

            $this->add_field([
                'type' => 'toggle',
                'settings' => 'header_topbar',
                'label' => __('Enable Top Header', 'saudagarwp'),
                'section' => $section,
                'default' => false,
            ]);

            $this->add_field([
                'settings' => 'header_topbar_height',
                'type' => 'slider',
                'label' => __('Height', 'saudagarwp'),
                'section' => $section,
                'default' => '37',
                'choices' => [
                    'min' => '15',
                    'max' => '150',
                    'step' => '1',
                ],
                'output' => [
                    [
                        'element' => '.rt-header__topbar',
                        'property' => '--theme-header-height',
                        'units' => 'px',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field_color([
                'settings' => 'header_topbar_color',
                'section' => $section,
                'pseudo' => 'hover',
                'default' => [
                    'normal' => $this->get_default_mod('global_font_color', 'secondary'),
                    'hover' => $this->get_default_mod('global_color_link', 'hover'),
                ],
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.rt-header__topbar',
                        'property' => '--theme-font-color-secondary',
                    ],
                    [
                        'choice' => 'normal',
                        'element' => '.rt-header__topbar',
                        'property' => '--theme-color-link',
                    ],
                    [
                        'choice' => 'hover',
                        'element' => '.rt-header__topbar',
                        'property' => '--theme-color-link-active',
                    ],
                ],
            ]);

            $this->add_field_background([
                'settings' => 'header_topbar_background',
                'section' => $section,
                'default' => $this->get_default_mod('global_background_scheme', 'secondary'),
                'output' => [
                    [
                        'element' => '.rt-header__topbar',
                        'property' => '--theme-background-secondary',
                    ],
                ],
            ]);

            $this->add_field_border_color([
                'settings' => 'header_topbar_border_color',
                'section' => $section,
                'default' => $this->get_default_mod('global_border_color'),
                'output' => [
                    [
                        'element' => '.rt-header__topbar',
                        'property' => '--theme-border-color',
                    ],
                ],
            ]);
        }
    }

    public function add_options_middle() {
        if (rt_is_premium()) {
            $section = 'header_middle_section';

            $this->add_field([
                'type' => 'toggle',
                'settings' => 'header_middle',
                'label' => __('Enable Middle Header', 'saudagarwp'),
                'section' => $section,
                'default' => false,
            ]);

            $this->add_field([
                'settings' => 'header_middle_height',
                'type' => 'slider',
                'label' => __('Height', 'saudagarwp'),
                'section' => $section,
                'default' => '120',
                'choices' => [
                    'min' => '15',
                    'max' => '200',
                    'step' => '1',
                ],
                'output' => [
                    [
                        'element' => '.rt-header__middle',
                        'property' => '--theme-header-height',
                        'units' => 'px',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field_color([
                'settings' => 'header_middle_color',
                'section' => $section,
                'pseudo' => 'hover',
                'default' => [
                    'normal' => $this->get_default_mod('global_font_color', 'secondary'),
                    'hover' => $this->get_default_mod('global_color_link', 'hover'),
                ],
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.rt-header__middle',
                        'property' => '--theme-font-color-secondary',
                    ],
                    [
                        'choice' => 'normal',
                        'element' => '.rt-header__middle',
                        'property' => '--theme-color-link',
                    ],
                    [
                        'choice' => 'hover',
                        'element' => '.rt-header__middle',
                        'property' => '--theme-color-link-active',
                    ],
                ],
            ]);

            $this->add_field_background([
                'settings' => 'header_middle_background',
                'section' => $section,
                'default' => $this->get_default_mod('global_background_scheme', 'primary'),
                'output' => [
                    [
                        'element' => '.rt-header__middle',
                        'property' => '--theme-background-primary',
                    ],
                ],
            ]);

            $this->add_field_border_color([
                'settings' => 'header_middle_border_color',
                'section' => $section,
                'default' => $this->get_default_mod('global_border_color'),
                'element' => '.rt-header__middle',
                'output' => [
                    [
                        'element' => '.rt-header__middle',
                        'property' => '--theme-border-color',
                    ],
                ],
            ]);
        }
    }

    public function add_options_main() {
        $section = 'header_main_section';

        $this->add_header([
            'label' => __('Main Bar', 'saudagarwp'),
            'settings' => 'header_main',
            'section' => $section,
        ]);

        $this->add_field([
            'settings' => 'header_main_height',
            'type' => 'slider',
            'label' => __('Height', 'saudagarwp'),
            'section' => $section,
            'default' => '70',
            'choices' => [
                'min' => '15',
                'max' => '150',
                'step' => '1',
            ],
            'output' => [
                [
                    'element' => '.rt-header__main, .rt-header__sticky',
                    'property' => '--theme-header-height',
                    'units' => 'px',
                ],
                [
                    'element' => '.rt-header.is-overlay:not(.is-sticky) .rt-search',
                    'property' => 'height',
                    'units' => 'px',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field_color([
            'settings' => 'header_main_color',
            'section' => $section,
            'pseudo' => 'hover',
            'default' => [
                'normal' => $this->get_default_mod('global_font_color', 'secondary'),
                'hover' => $this->get_default_mod('global_color_link', 'hover'),
            ],
            'output' => [
                [
                    'choice' => 'normal',
                    'element' => '.rt-header__main, .rt-header__sticky',
                    'property' => '--theme-font-color-secondary',
                ],
                [
                    'choice' => 'normal',
                    'element' => '.rt-header__main, .rt-header__sticky',
                    'property' => '--theme-color-link',
                ],
                [
                    'choice' => 'hover',
                    'element' => '.rt-header__main, .rt-header__sticky',
                    'property' => '--theme-color-link-active',
                ],
            ],
        ]);

        $this->add_field_background([
            'settings' => 'header_main_background',
            'section' => $section,
            'default' => $this->get_default_mod('global_background_scheme', 'primary'),
            'output' => [
                [
                    'element' => '.rt-header__main, .rt-header__sticky',
                    'property' => '--theme-background-primary',
                ],
            ],
        ]);

        $this->add_field_border_color([
            'settings' => 'header_main_border_color',
            'section' => $section,
            'default' => $this->get_default_mod('global_border_color'),
            'output' => [
                [
                    'element' => '.rt-header__main, .rt-header__sticky',
                    'property' => '--theme-border-color',
                ],
            ],
        ]);
    }

    public function add_options_sticky() {
        if (rt_is_premium()) {
            $section = 'header_main_section';

            $this->add_header([
                'label' => __('Sticky Bar', 'saudagarwp'),
                'settings' => 'header_sticky',
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'toggle',
                'settings' => 'header_sticky',
                'label' => __('Enable Sticky Header', 'saudagarwp'),
                'description' => __('Hold down the header at the top of your web page while the user scrolls the page.', 'saudagarwp'),
                'section' => $section,
                'default' => true,
            ]);

            $this->add_field([
                'settings' => 'header_sticky_height',
                'type' => 'slider',
                'label' => __('Height', 'saudagarwp'),
                'section' => $section,
                'default' => '70',
                'choices' => [
                    'min' => '15',
                    'max' => '150',
                    'step' => '1',
                ],
                'output' => [
                    [
                        'element' => '.rt-header__sticky',
                        'property' => '--theme-header-height',
                        'units' => 'px',
                    ],
                ],
                'transport' => 'auto',
                'active_callback' => [
                    [
                        'setting' => 'header_sticky',
                        'operator' => '==',
                        'value' => true,
                    ],
                ],
            ]);

            $this->add_field_color([
                'settings' => 'header_sticky_color',
                'section' => $section,
                'pseudo' => 'hover',
                'default' => [
                    'normal' => $this->get_default_mod('global_font_color', 'secondary'),
                    'hover' => $this->get_default_mod('global_color_link', 'hover'),
                ],
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.rt-header__sticky',
                        'property' => '--theme-font-color-secondary',
                    ],
                    [
                        'choice' => 'normal',
                        'element' => '.rt-header__sticky',
                        'property' => '--theme-color-link',
                    ],
                    [
                        'choice' => 'hover',
                        'element' => '.rt-header__sticky',
                        'property' => '--theme-color-link-active',
                    ],
                ],
                'active_callback' => [
                    [
                        'setting' => 'header_sticky',
                        'operator' => '==',
                        'value' => true,
                    ],

                ],
            ]);

            $this->add_field_background([
                'settings' => 'header_sticky_background',
                'section' => $section,
                'default' => $this->get_default_mod('global_background_scheme', 'primary'),
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.rt-header__sticky',
                        'property' => '--theme-background-primary',
                    ],
                ],
                'active_callback' => [
                    [
                        'setting' => 'header_sticky',
                        'operator' => '==',
                        'value' => true,
                    ],
                ],
            ]);

            $this->add_field_border_color([
                'settings' => 'header_sticky_border_color',
                'section' => $section,
                'default' => $this->get_default_mod('global_border_color'),
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.rt-header__sticky',
                        'property' => '--theme-border-color',
                    ],
                ],
                'active_callback' => [
                    [
                        'setting' => 'header_sticky',
                        'operator' => '==',
                        'value' => true,
                    ],
                ],
            ]);
        }
    }

    public function add_options_overlay() {
        if (rt_is_premium()) {
            $section = 'header_overlay_section';

            $this->add_field([
                'type' => 'toggle',
                'settings' => 'header_overlay',
                'label' => __('Enable Overlay Header', 'saudagarwp'),
                'description' => __('Header will get a transparent background and will use the set of alternative logos. This option only work on home page', 'saudagarwp'),
                'section' => $section,
                'default' => false,
            ]);

            $this->add_field_border_color([
                'settings' => 'header_overlay_border_color',
                'section' => $section,
                'default' => 'rgba(255, 255, 255, 0.1)',
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.is-overlay:not(.is-sticky) .rt-header-mobile__main, 
                                        .is-overlay:not(.is-sticky) .rt-header__main',
                        'property' => '--theme-border-color',
                    ],
                ],
                'active_callback' => [
                    [
                        'setting' => 'header_overlay',
                        'operator' => '==',
                        'value' => true,
                    ],
                ],
            ]);
        }
    }



    public function add_layout_prebuilder() {

        /**
         * get list from elementor library
         */
        $library = \Retheme\Helper::get_posts('elementor_library', 50);
        array_unshift($library, 'Select Header Library');

        $this->add_field([
            'type' => 'select',
            'section' => 'header_layout_section',
            'settings' => 'header_layout_builder_elementor',
            'label' => __('Elementor Library', 'saudagarwp'),
            'multiple' => 1,
            'choices' => $library,
            'active_callback' => [
                [
                    'setting' => 'header_layout_builder',
                    'operator' => '==',
                    'value' => ['header-builder'],
                ],
            ],

        ]);
    }

    public function add_menu_primary() {
        $section = 'header_menu_primary_section';
        if (rt_is_premium()) {
            $this->add_header([
                'label' => __('Menu', 'saudagarwp'),
                'settings' => 'header_menu_primary',
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'typography',
                'settings' => 'header_menu_primary_typography',
                'description' => __('Use CSS Unit px or %', 'saudagarwp'),
                'label' => __('Typography', 'saudagarwp'),
                'section' => $section,
                'default' => [
                    'font-family' => 'inherit',
                    'variant' => '',
                    'font-size' => '',
                    'text-transform' => 'none',
                ],
                'output' => [
                    [
                        'element' => '.rt-header .menu-primary a:not(.rt-menu__submenu a)',
                        'suffix' => '!important',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field([
                'settings' => 'header_menu_primary_background',
                'type' => 'color',
                'choices' => ['alpha' => true,],
                'label' => __('Background Effect', 'saudagarwp'),
                'section' => $section,
                'default' => $this->get_default_mod('global_brand_color', 'primary'),
                'output' => [
                    [
                        'element' => '.menu-primary',
                        'property' => '--theme-color-brand',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field([
                'settings' => 'header_menu_primary_spacing',
                'type' => 'slider',
                'label' => __('Menu Items Spacing', 'saudagarwp'),
                'section' => $section,
                'choices' => [
                    'min' => '5',
                    'max' => '30',
                    'step' => '1',
                ],
                'default' => 15,
                'output' => [
                    [
                        'element' => '.menu-primary',
                        'property' => '--theme-menubar-spacing',
                        'units' => 'px',
                    ],
                ],
                'transport' => 'auto',
            ]);

            // submenu
            $this->add_header([
                'label' => __('Sub Menu', 'saudagarwp'),
                'settings' => 'header_menu_primary_submenu',
                'section' => $section,
            ]);

            $this->add_field_background([
                'settings' => 'header_menu_primary_submenu_background',
                'section' => $section,
                'default' => rt_get_theme('global_background_dark'),
                'output' => [
                    [
                        'element' => '.menu-primary .rt-menu__submenu',
                        'property' => '--theme-background-primary',
                    ],
                ],
            ]);

            $this->add_field_color([
                'settings' => 'header_menu_primary_submenu_color',
                'section' => $section,
                'default' => [
                    'normal' => rt_get_theme('global_font_light_color')['secondary'],
                    'hover' => rt_get_theme('global_font_light_color')['secondary'],
                ],
                'output' => [
                    [
                        'choose' => 'normal',
                        'element' => '.menu-primary .rt-menu__submenu',
                        'property' => '--theme-font-color-secondary',
                    ],
                    [
                        'choose' => 'hover',
                        'element' => '.menu-primary .rt-menu__submenu',
                        'property' => '--theme-color-link-active',
                    ],
                ],
            ]);

            $this->add_field_animation([
                'settings' => 'header_menu_primary_submenu_animation',
                'section' => $section,
            ]);
        } else {
            $this->upgrade_pro([
                'settings' => 'header_menu_primary',
                'section' => $section,
            ]);
        }
    }

    public function add_menu_secondary() {
        $section = 'header_menu_secondary_section';
        if (rt_is_premium()) {
            $this->add_header([
                'label' => __('Menu', 'saudagarwp'),
                'settings' => 'header_menu_secondary',
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'typography',
                'settings' => 'header_menu_secondary_typography',
                'description' => __('Use CSS Unit px or %', 'saudagarwp'),
                'label' => __('Typography', 'saudagarwp'),
                'section' => $section,
                'default' => [
                    'font-family' => 'inherit',
                    'variant' => '',
                    'font-size' => '',
                    'text-transform' => 'none',
                ],
                'output' => [
                    [
                        'element' => '.rt-header .menu-secondary a:not(.rt-menu__submenu a)',
                        'suffix' => '!important',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field([
                'settings' => 'header_menu_secondary_background',
                'type' => 'color',
                'choices' => [
                    'alpha' => true,
                ],
                'label' => __('Background Effect', 'saudagarwp'),
                'section' => $section,
                'default' => $this->get_default_mod('global_brand_color', 'primary'),
                'output' => [
                    [
                        'element' => '.menu-secondary',
                        'property' => '--theme-color-brand',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field([
                'settings' => 'header_menu_secondary_spacing',
                'type' => 'slider',
                'label' => __('Menu Items Spacing', 'saudagarwp'),
                'section' => $section,
                'choices' => [
                    'min' => '5',
                    'max' => '30',
                    'step' => '1',
                ],
                'default' => 15,
                'output' => [
                    [
                        'element' => '.menu-secondary',
                        'property' => '--theme-menubar-spacing',
                        'units' => 'px',
                    ],
                ],
                'transport' => 'auto',
            ]);

            // submenu
            $this->add_header([
                'label' => __('Sub Menu', 'saudagarwp'),
                'settings' => 'header_menu_secondary_submenu',
                'section' => $section,
            ]);

            $this->add_field_background([
                'settings' => 'header_menu_secondary_submenu_background',
                'default' => rt_get_theme('background_dark'),
                'section' => $section,
                'output' => [
                    [
                        'element' => '.menu-secondary .rt-menu__submenu',
                        'property' => '--theme-background-primary',
                    ],
                ],
            ]);

            $this->add_field_color([
                'settings' => 'header_menu_secondary_submenu_color',
                'section' => $section,
                'default' => [
                    'normal' => rt_get_theme('global_font_light_color')['secondary'],
                    'hover' => rt_get_theme('global_font_light_color')['secondary'],
                ],
                'output' => [
                    [
                        'choose' => 'normal',
                        'element' => '.menu-secondary .rt-menu__submenu',
                        'property' => '--theme-font-color-secondary',
                    ],
                    [
                        'choose' => 'hover',
                        'element' => '.menu-secondary .rt-menu__submenu',
                        'property' => '--theme-color-link-active',
                    ],
                ],
            ]);

            $this->add_field_animation([
                'settings' => 'header_menu_secondary_submenu_animation',
                'section' => $section,
            ]);
        }
    }

    public function add_menu_tertiary() {
        $section = 'header_menu_tertiary_section';
        if (rt_is_premium()) {
            $this->add_header([
                'label' => __('Menu', 'saudagarwp'),
                'settings' => 'header_menu_tertiary',
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'typography',
                'settings' => 'header_menu_tertiary_typography',
                'description' => __('Use CSS Unit px or %', 'saudagarwp'),
                'label' => __('Typography', 'saudagarwp'),
                'section' => $section,
                'default' => [
                    'font-family' => 'inherit',
                    'variant' => '',
                    'font-size' => '',
                    'text-transform' => 'none',
                ],
                'output' => [
                    [
                        'element' => '.rt-header .menu-tertiary a:not(.rt-menu__submenu a)',
                        'suffix' => '!important',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field([
                'settings' => 'header_menu_tertiary_background',
                'type' => 'color',
                'choices' => [
                    'alpha' => true,
                ],
                'label' => __('Background Effect', 'saudagarwp'),
                'section' => $section,
                'default' => $this->get_default_mod('global_brand_color', 'primary'),
                'output' => [
                    [
                        'element' => '.menu-tertiary',
                        'property' => '--theme-color-brand',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field([
                'settings' => 'header_menu_tertiary_spacing',
                'type' => 'slider',
                'label' => __('Menu Items Spacing', 'saudagarwp'),
                'section' => $section,
                'choices' => [
                    'min' => '5',
                    'max' => '30',
                    'step' => '1',
                ],
                'default' => 15,
                'output' => [
                    [
                        'element' => '.menu-tertiary',
                        'property' => '--theme-menubar-spacing',
                        'units' => 'px',
                    ],
                ],
                'transport' => 'auto',
            ]);

            // submenu
            $this->add_header([
                'label' => __('Sub Menu', 'saudagarwp'),
                'settings' => 'header_menu_tertiary_submenu',
                'section' => $section,
            ]);

            $this->add_field_background([
                'settings' => 'header_menu_tertiary_submenu_background',
                'default' => rt_get_theme('global_font_light_color')['secondary'],
                'section' => $section,
                'default' => rt_get_theme('global_font_light_color')['secondary'],
                'output' => [
                    [
                        'element' => '.menu-tertiary .rt-menu__submenu',
                        'property' => '--theme-background-primary',
                    ],
                ],
            ]);

            $this->add_field_color([
                'settings' => 'header_menu_tertiary_submenu_color',
                'default' => [
                    'normal' => rt_get_theme('global_font_light_color')['secondary'],
                    'hover' => rt_get_theme('global_font_light_color')['secondary'],
                ],
                'section' => $section,
                'output' => [
                    [
                        'choose' => 'normal',
                        'element' => '.menu-tertiary .rt-menu__submenu',
                        'property' => '--theme-font-color-secondary',
                    ],
                    [
                        'choose' => 'hover',
                        'element' => '.menu-tertiary .rt-menu__submenu',
                        'property' => '--theme-color-link-active',
                    ],
                ],
            ]);

            $this->add_field_animation([
                'settings' => 'header_menu_tertiary_submenu_animation',
                'section' => $section,
            ]);
        }
    }

    public function add_mobile_header() {
        $section = 'header_mobile_section';

        $this->add_header([
            'label' => __('Header', 'saudagarwp'),
            'settings' => 'header_mobile_menu',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'header_mobile_sticky',
            'label' => __('Enable Sticky Header', 'saudagarwp'),
            'description' => __('Hold down the mobile header at the top of your web page while the user scrolls the page.', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'multicolor',
            'settings' => 'header_mobile_color',
            'label' => __('Color', 'saudagarwp'),
            'section' => $section,
            'choices' => [
                'normal' => __('Normal', 'saudagarwp'),
                'hover' => __('Hover', 'saudagarwp'),
            ],
            'default' => [
                'normal' => $this->get_default_mod('global_font_color', 'secondary'),
                'hover' => $this->get_default_mod('global_color_link', 'normal'),
            ],
            'transport' => 'auto',
            'output' => [
                [
                    'choice' => 'normal',
                    'element' => '.rt-header-mobile__main',
                    'property' => '--theme-font-color-primary',
                ],
                [
                    'choice' => 'normal',
                    'element' => '.rt-header-mobile__main',
                    'property' => '--theme-font-color-secondary',
                ],
                [
                    'choice' => 'normal',
                    'element' => '.rt-header-mobile__main',
                    'property' => '--theme-color-link',
                ],
                [
                    'choice' => 'hover',
                    'element' => '.rt-header-mobile__main',
                    'property' => '--theme-color-link-active',
                ],
            ],
        ]);

        $this->add_field_background([
            'settings' => 'header_mobile_background',
            'section' => $section,
            'default' => $this->get_default_mod('global_background_scheme', 'primary'),
            'output' => [
                [
                    'element' => '.rt-header-mobile__main',
                    'property' => '--theme-background-primary',
                ],
            ],
        ]);

        $this->add_field_border_color([
            'settings' => 'header_mobile_border_color',
            'section' => $section,
            'default' => $this->get_default_mod('global_border_color'),
            'output' => [
                [
                    'element' => '.rt-header-mobile__main',
                    'property' => '--theme-border-color',
                ],
            ],
        ]);
    }

    public function add_menu_mobile() {
        $section = 'header_menu_mobile_section';

        if (rt_is_premium()) {

            $this->add_field([
                'type' => 'select',
                'settings' => 'header_drawer_menu_style',
                'section' => $section,
                'tooltip' => 'Atur jenis menu yang ditampilkan',
                'label' => __('Style', 'admin_domain'),
                'default' => 'dropdown',
                'choices' => [
                    'dropdown' => 'Dropdown',
                    'sidepanel' => 'Side Panel',
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'settings' => 'header_drawer_menu_schema',
                'section' => $section,
                'label' => __('Color Schema', 'admin_domain'),
                'default' => 'dark',
                'choices' => [
                    'dark' => 'Dark',
                    'light' => 'Light',
                    'custom' => 'Custom',
                ],
            ]);

            $this->add_header([
                'label' => __('Menu', 'saudagarwp'),
                'settings' => 'header_drawer_menu_typography',
                'section' => $section,
                'active_callback' => [
                    [
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ],
                ],
            ]);

            $this->add_field_color([
                'settings' => 'header_drawer_menu_color',
                'default' => rt_get_theme('global_font_light_color')['secondary'],
                'section' => $section,
                'output' => [
                    [
                        'element' => '.rt-main-canvas-menu',
                        'property' => '--theme-font-color-primary',
                    ],
                    [
                        'element' => '.rt-main-canvas-menu',
                        'property' => '--theme-font-color-secondary',
                    ],
                ],
                'active_callback' => [
                    [
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ],
                ],
            ]);

            $this->add_field_background([
                'settings' => 'header_drawer_menu_background',
                'default' => rt_get_theme('background_dark'),
                'section' => $section,
                'output' => [
                    [
                        'element' => '.rt-main-canvas-menu',
                        'property' => '--theme-background-primary',
                    ],
                ],
                'active_callback' => [
                    [
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ],
                ],
            ]);

            $this->add_field_color([
                'label' => __('Border Color', 'saudagarwp'),
                'settings' => 'header_drawer_menu_border_color',
                'section' => $section,
                'output' => [
                    [
                        'element' => '.rt-main-canvas-menu',
                        'property' => '--theme-border-color',
                    ],
                ],

                'active_callback' => [
                    [
                        'setting' => 'header_drawer_menu_schema',
                        'operator' => '==',
                        'value' => 'custom',
                    ],
                ],
            ]);
        } else {
            $this->upgrade_pro([
                'settings' => 'header_drawer_menu',
                'section' => $section,
            ]);
        }
    }

    public function add_menu_off_canvas() {
        $section = 'header_menu_off_canvas_section';

        $this->add_field([
            'type' => 'select',
            'settings' => 'header_menu_off_canvas_style',
            'section' => $section,
            'label' => __('Style', 'admin_domain'),
            'default' => 'simple',
            'choices' => [
                'simple' => 'Simple',
                'frame' => 'Frame',
            ],
        ]);

        $this->add_field([
            'type' => 'text',
            'section' => $section,
            'settings' => 'header_menu_off_canvas_trigger_label',
            'label' => __('Menu Label', 'saudagarwp'),
            'default' => __('Menu', 'saudagarwp'),
        ]);

        $this->add_field([
            'type' => 'text',
            'section' => $section,
            'settings' => 'header_menu_off_canvas_panel_label',
            'label' => __('Panel Label', 'saudagarwp'),
            'default' => __('Menu', 'saudagarwp'),
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => 'header_menu_off_canvas_menu',
            'section' => $section,
            'default' => 'none',
            'label' => __('Select Menu', 'saudagarwp'),
            'choices' => Helper::get_menu_list(),
        ]);


        $this->add_field_color([
            'settings' => 'header_menu_off_canvas_color',
            'default' => rt_get_theme('global_font_color')['secondary'],
            'section' => $section,
            'output' => [
                [
                    'element' => '.rt-menu-off-canvas',
                    'property' => '--theme-font-color-primary',
                ],
                [
                    'element' => '.rt-menu-off-canvas',
                    'property' => '--theme-font-color-secondary',
                ],
            ],
        ]);

        $this->add_field_color([
            'settings' => 'header_menu_off_canvas_color_accent',
            'label' => __('Color Accent', 'admin_domain'),
            'default' => rt_get_theme('global_color_link')['hover'],
            'section' => $section,
            'output' => [
                [
                    'element' => '.rt-menu-off-canvas',
                    'property' => '--theme-link-active',
                ],
            ],
        ]);

        $this->add_field_background([
            'settings' => 'header_menu_off_canvas_background',
            'default' => rt_get_theme('global_background_scheme')['primary'],
            'section' => $section,
            'output' => [
                [
                    'element' => '.rt-menu-off-canvas',
                    'property' => '--theme-background-primary',
                ],
            ],
        ]);

        $this->add_field_color([
            'label' => __('Border Color', 'admin_domain'),
            'settings' => 'header_menu_off_canvas_border_color',
            'default' => rt_get_theme('global_border_color'),
            'section' => $section,
            'output' => [
                [
                    'element' => '.rt-menu-off-canvas',
                    'property' => '--theme-border-color',
                ],
            ]
        ]);
    }

    public function add_element_search_box_mobile() {
        $section = 'header_search_mobile_section';

        if (rt_is_premium()) {

            $element = '.rt-header-mobile__main .rt-header-search-form, .rt-main-canvas-menu .rt-header-search-form';

            $this->add_field_color([
                'settings' => 'header_search_form_mobile_color',
                'section' => $section,
                'default' => $this->get_default_mod('global_font_color', 'secondary'),
                'output' => [
                    [
                        'element' => $element,
                        'property' => '--theme-font-color-secondary',
                    ],
                ],
            ]);

            $this->add_field_background([
                'settings' => 'header_search_form_mobile_background',
                'section' => $section,
                'default' => $this->get_default_mod('global_background_scheme', 'primary'),
                'output' => [
                    [
                        'element' => $element,
                        'property' => '--theme-background-primary',
                    ],
                ],
            ]);

            $this->add_field_border_color([
                'settings' => 'header_search_form_mobile_border_color',
                'default' => $this->get_default_mod('global_border_color'),
                'section' => $section,
                'output' => [
                    [
                        'element' => $element,
                        'property' => '--theme-border-color',
                    ],
                ],
            ]);

            $this->add_field([
                'settings' => 'header_search_form_mobile_border_radius',
                'type' => 'slider',
                'label' => __('Border Radius', 'saudagarwp'),
                'section' => $section,
                'default' => 7,
                'choices' => [
                    'min' => '0',
                    'max' => '50',
                    'step' => '1',
                ],
                'output' => [
                    [
                        'element' => $element,
                        'property' => '--theme-border-radius',
                        'units' => 'px',
                    ],
                ],
                'transport' => 'auto',
            ]);
        } else {
            $this->upgrade_pro([
                'settings' => 'header_search_form_mobile',
                'section' => $section,
            ]);
        }
    }

    public function add_element_social() {
        $section = 'header_socmed_section';

        if (rt_is_premium()) {

            $settings = 'header_social';

            $this->add_field([
                'type' => 'select',
                'settings' => "header_social_style",
                'label' => __('Style', 'saudagarwp'),
                'section' => $section,
                'default' => 'simple',
                'multiple' => 1,
                'choices' => [
                    'simple' => 'Simple',
                    'brand' => 'Brand',
                    'custom' => 'Custom',
                ],
            ]);

            $this->add_field_color([
                'label' => __('Color', 'saudagarwp'),
                'settings' => "header_social_color",
                'section' => $section,
                'pseudo' => 'hover',
                'output' => [
                    [
                        'element' => '.rt-header-social .rt-socmed .rt-socmed__item',
                        'property' => 'color',
                        'suffix' => '!important',
                        'choice' => 'normal',
                    ],
                    [
                        'element' => '.rt-header-social .rt-socmed .rt-socmed__item:hover, .rt-header-social .rt-socmed .rt-socmed__item:active',
                        'property' => 'color',
                        'suffix' => '!important',
                        'choice' => 'hover',
                    ],
                ],
                'active_callback' => [
                    [
                        'setting' => "header_social_style",
                        'operator' => '==',
                        'value' => 'custom',
                    ],
                ],
            ]);

            $this->add_field_background([
                'label' => __('Background', 'saudagarwp'),
                'settings' => "header_social_background",
                'section' => $section,
                'pseudo' => 'hover',
                'output' => [
                    [
                        'element' => '.rt-header-social .rt-socmed .rt-socmed__item',
                        'property' => 'background-color',
                        'suffix' => '!important',
                        'choice' => 'normal',
                    ],
                    [
                        'element' => '.rt-header-social .rt-socmed .rt-socmed__item',
                        'property' => 'border-color',
                        'suffix' => '!important',
                        'choice' => 'normal',
                    ],
                    [
                        'element' => '.rt-header-social .rt-socmed .rt-socmed__item:hover, .rt-header-social .rt-socmed .rt-socmed__item:active',
                        'property' => 'background-color',
                        'suffix' => '!important',
                        'choice' => 'hover',
                    ],
                    [
                        'element' => '.rt-header-social .rt-socmed .rt-socmed__item:hover, .rt-header-social .rt-socmed .rt-socmed__item:active',
                        'property' => 'border-color',
                        'suffix' => '!important',
                        'choice' => 'hover',
                    ],
                ],
                'active_callback' => [
                    [
                        'setting' => "header_social_style",
                        'operator' => '==',
                        'value' => 'custom',
                    ],
                ],
            ]);
        }
    }

    public function add_element_button() {
        $total = (rt_is_premium()) ? 3 : 1;

        for ($index = 1; $index <= $total; $index++) {

            $section = "header_button_{$index}_section";

            $this->add_field([
                'type' => 'text',
                'settings' => "header_button_{$index}_text",
                'label' => __('Button Text', 'saudagarwp'),
                'section' => $section,
                'default' => __('Your text', 'saudagarwp'),
                'transport' => 'postMessage',
            ]);

            $this->add_field([
                'type' => 'radio-buttonset',
                'settings' => "header_button_{$index}_size",
                'label' => __('Button Size', 'saudagarwp'),
                'section' => $section,
                'default' => 'md',
                'choices' => [
                    'sm' => __('Small', 'saudagarwp'),
                    'md' => __('Medium', 'saudagarwp'),
                    'lg' => __('Large', 'saudagarwp'),
                ],
            ]);

            $this->add_field([
                'type' => 'text',
                'settings' => "header_button_{$index}_link",
                'label' => __('Button Link', 'saudagarwp'),
                'section' => $section,
                'default' => __('#', 'saudagarwp'),
            ]);

            $this->add_field([
                'type' => 'select',
                'settings' => "header_button_{$index}_link_target",
                'label' => __('Target', 'saudagarwp'),
                'section' => $section,
                'default' => 'blank',
                'choices' => [
                    'blank' => 'Open New Tab',
                    'self' => 'Keep on Page',
                ],
            ]);

            // $this->add_field([
            //     'type' => 'text',
            //     'settings' => "header_button_{$index}_icon",
            //     'label' => __('Icon Class', 'saudagarwp'),
            //     'section' => $section,
            // ]);

            $this->add_field_color([
                'settings' => "header_button_{$index}_color",
                'section' => $section,
                'pseudo' => 'hover',
                'default' => [
                    'normal' => '#ffffff',
                    'hover' => '#ffffff',
                ],
                'element' => ".rt-btn.rt-btn--{$index}",
                'suffix' => '!important',
            ]);

            $this->add_field_background([
                'settings' => "header_button_{$index}_background",
                'section' => $section,
                'pseudo' => 'hover',
                'default' => [
                    'normal' => $this->get_default_mod('global_brand_color', 'primary'),
                    'hover' => $this->get_default_mod('global_brand_color', 'secondary'),
                ],
                'element' => ".rt-btn.rt-btn--{$index}",

            ]);

            $this->add_field_responsive([
                'type' => 'dimensions',
                'settings' => "header_btn_{$index}_margin",
                'label' => __('Spacing', 'saudagarwp'),
                'section' => $section,
                'default' => [
                    'left' => '0',
                    'right' => '5px',
                ],
                'output' => [
                    [
                        'element' => ".rt-btn.rt-btn--{$index}",
                        'property' => 'margin',
                    ],

                ],

                'transport' => 'auto',
            ]);
        }
    }

    public function add_element_search_box() {
        if (rt_is_premium()) {
            $section = 'header_search_section';

            $this->add_field([
                'type' => 'select',
                'settings' => 'header_search_form_style',
                'label' => __('Style', 'saudagarwp'),
                'section' => $section,
                'default' => 'default',
                'multiple' => 1,
                'choices' => [
                    'default' => 'Simple',
                    'btn' => 'Form + Button',
                ],
            ]);


            $this->add_field_background([
                'settings' => 'header_search_form_background',
                'section' => $section,
                'default' => $this->get_default_mod('global_background_scheme', 'primary'),
                'output' => [
                    [
                        'element' => '.rt-header .rt-header-search-form',
                        'property' => '--theme-background-primary',
                    ],
                ],

            ]);

            $this->add_field_color([
                'settings' => 'header_search_form_color',
                'section' => $section,
                'default' => $this->get_default_mod('global_font_color', 'secondary'),
                'output' => [
                    [
                        'element' => '.rt-header .rt-header-search-form',
                        'property' => '--theme-font-color-secondary',
                    ],
                ],
            ]);

            $this->add_field_border_color([
                'settings' => 'header_search_form_border_color',
                'section' => $section,
                'default' => $this->get_default_mod('global_border_color'),
                'output' => [
                    [
                        'element' => '.rt-header .rt-header-search-form',
                        'property' => '--theme-border-color',
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'dimension',
                'settings' => 'header_search_form_width',
                'label' => __('Width', 'saudagarwp'),
                'description' => __('Use CSS Unit px or %', 'saudagarwp'),
                'section' => $section,
                'default' => '100%',
                'output' => [
                    [
                        'element' => '.rt-header .rt-header-search-form',
                        'property' => '--header-search-form-width',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field([
                'settings' => 'header_search_form_border_radius',
                'type' => 'slider',
                'label' => __('Border Radius', 'saudagarwp'),
                'section' => $section,
                'default' => 7,
                'choices' => [
                    'min' => '0',
                    'max' => '50',
                    'step' => '1',
                ],
                'output' => [
                    [
                        'element' => '.rt-header .rt-header-search-form',
                        'property' => '--theme-border-radius',
                        'units' => 'px',
                    ],
                ],
                'transport' => 'auto',
            ]);
        }
    }

    public function add_element_html() {
        $total = (rt_is_premium()) ? 5 : 1;

        for ($index = 1; $index <= $total; $index++) {

            $section = "header_html_{$index}_section";

            $this->add_field([
                'label' => __('Input Your Text/HTML/Shortcode', 'saudagarwp'),
                'settings' => "header_html_{$index}",
                'section' => $section,
                'type' => 'textarea',
            ]);
        }
    }

    public function add_element_account() {

        $section = "header_account_section";

        $this->add_field([
            'type' => 'select',
            'settings' => 'woocommerce_user_login',
            'section' => $section,
            'label' => __('User Account', 'saudagarwp'),
            'default' => 'modal',
            'choices' => [
                'modal' => __('Modal Login/Register', 'saudagarwp'),
                'account-page' => __('Redirect Account Page', 'saudagarwp'),
            ],
        ]);
    }
    // end class
}

new Header;
