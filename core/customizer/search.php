<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Search extends Customizer_Base
{

    public function __construct()
    {

        $this->set_section();
        $this->add_search_option();
    }

    public function set_section()
    {
        $this->add_section('', [
            'search_option' => [__('Search ', 'saudagarwp')],
        ]);
    }

    public function add_search_option()
    {
        $section = 'search_option_section';
        $settings = 'search_options';

        $this->add_field([
            'type' => 'select',
            'section' => $section,
            'settings' => 'search_main_result',
            'label' => __('Post Types Result', 'saudagarwp'),
            'description' => __('Display your type posts search results', 'saudagarwp'),
            'default' => 'product',
            'multiple' => 1,
            'choices' => [
                'post' => 'Post',
                'product' => 'Product',
            ],
            'partial_refresh'    => [
                'search_main' => [
                    'selector'        => ".rt-header .rt-search .page-container",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
            
        ]);

        $this->add_field([
            'type' => 'text',
            'section' => $section,
            'settings' => 'search_main_text',
            'label' => __('Placeholder', 'saudagarwp'),
            'default' => __('Type Something and enter', 'saudagarwp'),
        ]);

        if (rt_is_premium()) {
            $this->add_field_color([
                'settings' => 'search_main_color',
                'section' => $section,
                'default' => 'rgba(255,255,255,.65)',
                'output' => [
                    [
                        'element' => '.rt-search',
                        'property' => '--theme-font-color-secondary',
                    ],
                ],
            ]);

            $this->add_field_background([
                'settings' => 'search_main_background',
                'section' => $section,
                'default' => '#1c1c1f',
                'output' => [
                    [
                        'element' => '.rt-search',
                        'property' => '--theme-background-primary',
                    ],
                ],
            ]);
        }

    }

// end class
}

new Search;
