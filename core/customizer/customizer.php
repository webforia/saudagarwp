<?php

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Customizer extends Customizer_Base {
    public function __construct() {
        \Kirki::add_config('retheme_customizer', array(
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ));

        add_action('customize_controls_enqueue_scripts', [$this, 'register_customizer_panel'], 999);
        add_action('customize_preview_init', [$this, 'register_customizer_preview']);
        add_action('customize_register', [$this, 'remove_default_control']);

        add_action('init', [$this, 'include_part']);

        // add_action('customize_save_after', [$this, 'pre_migration_color']);
    }

    public function include_part() {
        /**
         * Filter customizer section
         */
        $customizers = array(
            'general',
            'header',
            'blog',
            'footer',
            'brand',
            'page',
            'search',
            'connect',
            'homepage',
            'tweak',
        );

        /** custom control */
        include dirname(__FILE__) . '/control/builder/builder.php';

        foreach ($customizers as $key => $customizer) {
            include dirname(__FILE__) . "/{$customizer}.php";
        }

        if (rt_is_woocommerce()) {
            include dirname(__FILE__) . '/woocommerce.php';
        }
    }

    public function register_customizer_panel() {
        // Font Awesome
        wp_enqueue_style('fontawesome', get_template_directory_uri() . '/assets/fontawesome/css/all.min.css', false, '5.14.0');
        wp_enqueue_style('retheme-customizer-panel', get_template_directory_uri() . '/core/customizer/assets/css/customizer-panel.css', false, '2.1.0');

        wp_enqueue_script('retheme-customizer-control', get_template_directory_uri() . '/core/customizer/assets/js/customizer-control.js', array('jquery'), '2.0.0', true);
        wp_enqueue_script('retheme-header-control', get_template_directory_uri() . '/core/customizer/assets/js/header-control.js', array('jquery'), '2.0.0', true);
    }

    public function register_customizer_preview() {
        wp_enqueue_style('retheme-customizer-preview', get_template_directory_uri() . '/core/customizer/assets/css/customizer-preview.css', false, '2.0.0');
        wp_enqueue_script('retheme-customizer-preview', get_template_directory_uri() . '/core/customizer/assets/js/customizer-preview.js', array('jquery', 'animate', 'retheme'), '2.0.0', true);
    }

    /**
     * Disable default control costumizer
     */
    public function remove_default_control($wp_customize) {
        $wp_customize->remove_section('static_front_page');
    }

    public function pre_migration_color() {
        // Backup Theme Mods
        if (get_option('_backup_saudagarwp_theme_mods_backup_before_migration_color')) {
            
            update_option('_backup_saudagarwp_theme_mods_backup_before_migration_color', get_theme_mods());

            $colors = array(
                // General - Colors
                'global_brand_color--primary',
                'global_brand_color--secondary',
                'global_font_color--primary',
                'global_font_color--secondary',
                'global_font_color--tertiary',
                'global_color_link--normal',
                'global_color_link--hover',
                'global_background_scheme--primary',
                'global_background_scheme--secondary',

                // General - Button Primary
                'button_primary_color--normal',
                'button_primary_color--hover',

                // General - Button Action
                'button_action_color--normal',
                'button_action_color--hover',

                // Header - Topbar
                'header_topbar_color--normal',
                'header_topbar_color--hover',

                // Header - Middle
                'header_middle_color--normal',
                'header_middle_color--hover',

                // Header - Main
                'header_main_color--normal',
                'header_main_color--hover',
                'header_sticky_color--normal',
                'header_sticky_color--hover',

                // Header - Mobile
                'header_mobile_color--normal',
                'header_mobile_color--hover',

                // Header - Button
                'header_button_1_color--normal',
                'header_button_1_color--hover',
                'header_button_1_background--normal',
                'header_button_1_background--hover',

                'header_button_2_color--normal',
                'header_button_2_color--hover',
                'header_button_2_background--normal',
                'header_button_2_background--hover',

                'header_button_3_color--normal',
                'header_button_3_color--hover',
                'header_button_3_background--normal',
                'header_button_3_background--hover',

                // Footer - Widget
                'footer_widget_color--normal',
                'footer_widget_color--hover',
            );

            foreach ($colors as $key => $color) {
                $result = explode("--", $color);
                $name = $result[0];
                $variation = $result[1];

                $get_theme_mod = get_theme_mod($name);

                if (isset($get_theme_mod[$variation])) {
                    set_theme_mod("{$name}_{$variation}", $get_theme_mod[$variation]);
                }
            }
        }
    }

    public function pre_migration_responsive() {
        $responsives = array(
            'blog_archive_column' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'single_related_show' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],

            'header_btn_1_margin' => [
                'default' => [
                    'left' => '0',
                    'right' => '5px',
                ],
                'default_tablet' => [
                    'left' => '0',
                    'right' => '5px',
                ],
                'default_mobile' => [
                    'left' => '0',
                    'right' => '5px',
                ],
            ],
            'header_btn_2_margin' => [
                'default' => [
                    'left' => '0',
                    'right' => '5px',
                ],
                'default_tablet' => [
                    'left' => '0',
                    'right' => '5px',
                ],
                'default_mobile' => [
                    'left' => '0',
                    'right' => '5px',
                ],
            ],
            'header_btn_3_margin' => [
                'default' => [
                    'left' => '0',
                    'right' => '5px',
                ],
                'default_tablet' => [
                    'left' => '0',
                    'right' => '5px',
                ],
                'default_mobile' => [
                    'left' => '0',
                    'right' => '5px',
                ],
            ],

            'homebuilder_text_content_background_spacing' => [
                'default' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_tablet' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_mobile' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
            ],
            'homebuilder_image_padding' => [
                'default' => [
                    'left' => '0px',
                    'top' => '0px',
                    'right' => '0px',
                    'bottom' => '0px',
                ],
                'default_tablet' => [
                    'left' => '0px',
                    'top' => '0px',
                    'right' => '0px',
                    'bottom' => '0px',
                ],
                'default_mobile' => [
                    'left' => '0px',
                    'top' => '0px',
                    'right' => '0px',
                    'bottom' => '0px',
                ],
            ],
            'homebuilder_image_content_background_spacing' => [
                'default' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_tablet' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_mobile' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
            ],
            'homebuilder_slider_background_spacing' => [
                'default' => [
                    'top' => '30px',
                    'bottom' => '30px',
                ],
                'default_tablet' => [
                    'top' => '30px',
                    'bottom' => '30px',
                ],
                'default_mobile' => [
                    'top' => '30px',
                    'bottom' => '30px',
                ],
            ],

            'homebuilder_banner_column' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_banner_slider_show' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_banner_background_spacing' => [

                'default' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_tablet' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_mobile' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
            ],

            'homebuilder_infobox_column' => [
                'default' => 3,
                'default_tablet' => 1,
                'default_mobile' => 1,
            ],
            'homebuilder_infobox_slider_show' => [
                'default' => 3,
                'default_tablet' => 1,
                'default_mobile' => 1,
            ],
            'homebuilder_infobox_background_spacing' => [
                'top' => '0',
                'bottom' => '30px',
            ],

            'homebuilder_product_1_column' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_product_1_slider_show' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_product_1_background_spacing' => [
                'top' => '0',
                'bottom' => '30px',
            ],

            'homebuilder_product_2_column' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_product_2_slider_show' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_product_2_background_spacing' => [
                'default' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_tablet' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_mobile' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
            ],

            'homebuilder_product_3_column' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_product_3_slider_show' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_product_3_background_spacing' => [

                'default' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_tablet' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_mobile' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
            ],

            'homebuilder_product_category_column' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_product_category_slider_show' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_product_category_background_spacing' => [

                'default' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_tablet' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_mobile' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
            ],

            'homebuilder_blog_1_column' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_blog_1_slider_show' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'homebuilder_blog_1_background_spacing' => [

                'default' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_tablet' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'default_mobile' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
            ],

            'homebuilder_welcome_typography_title',
            'homebuilder_welcome_typography_content',
            'homebuilder_welcome_background_spacing'  => [


                'default' => [
                    'top' => '90px',
                    'bottom' => '90px',
                ],
                'default_tablet' => [
                    'top' => '90px',
                    'bottom' => '90px',
                ],
                'default_mobile' => [
                    'top' => '90px',
                    'bottom' => '90px',
                ],
            ],

            'page_header_padding' => [

                'default' => [
                    'top' => '',
                    'bottom' => '',
                ],
                'default_tablet' => [
                    'top' => '',
                    'bottom' => '',
                ],
                'default_mobile' => [
                    'top' => '',
                    'bottom' => '',
                ],
            ],

            'woocommerce_archive_options_column' => [
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
            'woocommerce_archive_options_post_per_page' => [
                'default' => 9,
                'default_tablet' => 9,
                'default_mobile' => 9,
            ],
            'woocommerce_single_related_show' => [
                'default' => 4,
                'default_tablet' => 2,
                'default_mobile' => 2,
            ],
        );

        $responsives_typo = array(
            'footer_widget_heading_typography' => [
                'margin-bottom' => '',
                'font-size' => '',
                'line-height' => '',
                'text-transform' => 'none',
            ],
            'homebuilder_welcome_typography_title' => [
                'font-family' => 'Inter',
                'variant' => '600',
                'font-size' => '30px',
                'line-height' => '1.2',
            ],
            'homebuilder_welcome_typography_content' => [
                'font-size' => '17px',
                'line-height' => '1.75',
            ],
        );
    }

    // end class
}

new Customizer;
