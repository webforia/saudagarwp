<?php

/**
 * @author : Reret
 */

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Blog extends Customizer_Base
{
    public function __construct()
    {
        $this->set_panel();
        $this->set_section();

        // archive section
        $this->add_blog_options();
        $this->add_blog_layout();

        // single section
        $this->add_single_options();
        $this->add_single_related();
        $this->add_single_layout();
    }

    public function set_panel()
    {
        $this->add_panel('blog_panel', [
            'title' => __('Blog', 'saudagarwp'),
        ]);
    }

    public function set_section()
    {
        $this->add_section('blog_panel', [
            'blog_archive' => [__('Blog Archive', 'saudagarwp')],
            'blog_single' => [__('Blog Single', 'saudagarwp')],
        ]);
    }

    public function add_blog_options()
    {
        $section = 'blog_archive_section';

        $this->add_header([
            'label' => __('Options', 'saudagarwp'),
            'settings' => 'blog_archive',
            'section' => $section,
            'partial_refresh'    => [
                'blog_archive' => [
                    'selector'        => ".rt-post-archive",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);
        
        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'radio-image',
                'settings' => 'blog_archive_style',
                'label' => __('Style', 'saudagarwp'),
                'section' => $section,
                'default' => 'grid',
                'choices' => [
                    'grid' => get_template_directory_uri() . '/core/customizer/assets/img/post-grid.png',
                    'list' => get_template_directory_uri() . '/core/customizer/assets/img/post-list.png',
                ],
            ]);

            $this->add_field([
                'type' => 'toggle',
                'settings' => 'blog_archive_masonry',
                'label' => __('Masonry', 'saudagarwp'),
                'description' => __('Grid layout with different height', 'saudagarwp'),
                'section' => $section,
                'default' => false,
                'active_callback' => [
                    [
                        'setting' => 'blog_archive_style',
                        'operator' => '==',
                        'value' => 'grid',
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'toggle',
                'settings' => 'blog_archive_card',
                'label' => __('Blog Card frame', 'saudagarwp'),
                'description' => __('When enabled, show a border around Product Cards.', 'saudagarwp'),
                'section' => $section,
                'default' => false,
                'active_callback' => [
                    [
                        'setting' => 'blog_archive_style',
                        'operator' => '==',
                        'value' => 'grid',
                    ],
                ],
            ]);

            $this->add_field_responsive([
                'type' => 'slider',
                'section' => $section,
                'settings' => 'blog_archive_column',
                'label' => __('Column', 'saudagarwp'),
                'description' => __('Number of Column Per Row', 'saudagarwp'),
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
                'choices' => [
                    'min' => 1,
                    'max' => 6,
                ],
                'active_callback' => [
                    [
                        'setting' => 'blog_archive_style',
                        'operator' => '==',
                        'value' => 'grid',
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'number',
                'section' => $section,
                'settings' => 'blog_archive_excerpt',
                'label' => __('Excerpt', 'saudagarwp'),
                'description' => __('Set word length of excerpt on post', 'saudagarwp'),
                'default' => 18,
                'choices' => [
                    'min' => 0,
                    'max' => 50,
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'settings' => 'blog_archive_pagination',
                'label' => __('Pagination', 'saudagarwp'),
                'section' => $section,
                'default' => 'number',
                'multiple' => 1,
                'choices' => [
                    'none' => __('None', 'saudagarwp'),
                    'number' => __('Number', 'saudagarwp'),
                    'loadmore' => __('Load More', 'saudagarwp'),
                ],
            ]);
        }

        $this->add_field([
            'type' => 'multicheck',
            'settings' => 'blog_element',
            'label' => __('Blog Elements', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'category', 'thumbnail', 'content', 'readmore',
            ],
            'multiple' => 999,
            'choices' => [
                'category' => __('Category', 'saudagarwp'),
                'thumbnail' => __('Featured Image', 'saudagarwp'),
                'content' => __('Content', 'saudagarwp'),
                'readmore' => __('Read More', 'saudagarwp'),
            ],
        ]);

        $this->add_field([
            'type' => 'multicheck',
            'settings' => 'blog_meta',
            'label' => __('Blog Meta', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'meta-author', 'meta-date', 'meta-comment',
            ],
            'multiple' => 999,
            'choices' => [
                'meta-author' => __('Author', 'saudagarwp'),
                'meta-date' => __('Date', 'saudagarwp'),
                'meta-comment' => __('Comment', 'saudagarwp'),
            ],
        ]);
    }

    public function add_blog_layout()
    {
        $section = 'blog_archive_section';

        $this->add_header([
            'label' => __('Blog Archive Layout', 'saudagarwp'),
            'settings' => 'blog_archive_layout',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'blog_archive_layout',
            'label' => __('Layout', 'saudagarwp'),
            'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a sidebar.', 'saudagarwp'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
            'section' => $section,
            'default' => 'sidebar-right',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/layout-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/layout-compact.png',
                'sidebar-left' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-left.png',
                'sidebar-right' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-right.png',
            ],
        ]);
    }

    public function add_single_options()
    {
        $section = 'blog_single_section';

        $this->add_header([
            'label' => __('Options', 'saudagarwp'),
            'settings' => 'blog_single_title_align',
            'section' => $section,
            'partial_refresh'    => [
                'blog_single' => [
                    'selector'        => ".rt-single__title, #rt-swiper-post_related",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);
        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'radio-buttonset',
                'settings' => 'blog_single_title_align',
                'label' => __('Post Title Align', 'saudagarwp'),
                'section' => $section,
                'default' => 'center',
                'choices' => [
                    'left' => '<i class="fa-solid fa-align-left"></i>',
                    'center' => '<i class="fa-solid fa-align-center"></i>',
                    'right' => '<i class="fa-solid fa-align-right"></i>',
                ],
                'transport' => 'auto',
                'output' => [
                    [
                        'element' => '.rt-single__title, .rt-single__meta',
                        'property' => 'text-align',
                        'value_pattern' => '$',
                    ],
                    [
                        'choice' => 'center',
                        'element' => '.single-post .rt-breadcrumbs, .single-post .rt-share',
                        'property' => 'display',
                        'value_pattern' => 'flex',
                    ],
                    [
                        'choice' => 'center',
                        'element' => '.single-post .rt-breadcrumbs, .single-post .rt-share',
                        'property' => 'justify-content',
                        'value_pattern' => '$',
                    ],
                ],
            ]);
        }

        $this->add_field([
            'type' => 'multicheck',
            'settings' => 'single_element',
            'label' => __('Single Elements', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'thumbnail', 'tag', 'authorbox', 'navigation', 'comment', 'related',
            ],
            'multiple' => 999,
            'choices' => [
                'thumbnail' => __('Featured Image', 'saudagarwp'),
                'tag' => __('Tags', 'saudagarwp'),
                'authorbox' => __('Author Box', 'saudagarwp'),
                'navigation' => __('Navigation', 'saudagarwp'),
                'comment' => __('Comment', 'saudagarwp'),
                'related' => __('Related Posts', 'saudagarwp'),
            ],
        ]);

        $this->add_field([
            'type' => 'multicheck',
            'settings' => 'single_meta',
            'label' => __('Single Meta', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'meta-author', 'meta-date', 'meta-category',
            ],
            'multiple' => 999,
            'choices' => [
                'meta-author' => __('Author', 'saudagarwp'),
                'meta-date' => __('Date', 'saudagarwp'),
                'meta-category' => __('Category', 'saudagarwp'),
            ],
        ]);
    }

    public function add_single_related()
    {

        $section = 'blog_single_section';

        $this->add_header([
            'label' => __('Related', 'saudagarwp'),
            'settings' => 'single_related',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'number',
            'settings' => 'single_related_count',
            'label' => __('Total Number of Related Posts', 'saudagarwp'),
            'section' => $section,
            'default' => 6,
        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'settings' => 'single_related_show',
            'label' => __('Slides Per View', 'saudagarwp'),
            'section' => $section,
            'default' => 3,
            'default_tablet' => 2,
            'default_mobile' => 2,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
        ]);
    }

    public function add_single_layout()
    {
        $section = 'blog_single_section';

        $this->add_header([
            'label' => __('Blog Single Layout', 'saudagarwp'),
            'settings' => 'blog_single_layout',
            'section' => $section,

        ]);
        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'blog_single_layout',
            'label' => __('Layout', 'saudagarwp'),
            'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a sidebar.', 'saudagarwp'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
            'section' => $section,
            'default' => 'sidebar-right',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/layout-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/layout-compact.png',
                'sidebar-left' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-left.png',
                'sidebar-right' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-right.png',
            ],
        ]);
    }
    // end class
}

new Blog;
