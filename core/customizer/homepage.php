<?php

/**
 * @author : Reret
 */

namespace Retheme\Customizer;

use Retheme\Customizer_Base;
use Retheme\Helper;

class Homebuilder extends Customizer_Base {

    public function __construct() {
        $this->set_panel();
        $this->set_section();

        $this->add_layout();
        $this->add_slider();
        $this->add_product_archive();
        $this->add_product_loop();
        $this->add_blog_loop();

        if (rt_is_premium()) {
            $this->add_welcome_area();
            $this->add_product_category();
            $this->add_banner();
            $this->add_image();
            $this->add_image_content();
            $this->add_text_content();
            $this->add_infobox();
        }
    }

    public function set_panel() {
        $this->add_panel('homebuilder_panel', [
            'title' => __('Front Page Builder', 'saudagarwp'),
            'priority' => 0,
        ]);
    }

    public function set_section() {

        $layout = sprintf(__('Visit the <a href="%s">Layout options</a> to add and reorder front page element', 'saudagarwp'), "javascript:wp.customize.control( 'homebuilder_layout' ).focus();");

        $this->add_section('homebuilder_panel', [
            'homepage_layout' => [__('Layout', 'saudagarwp')],
            'homepage_welcome' => [__('Welcome Area', 'saudagarwp'), $layout],
            'homepage_slider' => [__('Images Slider', 'saudagarwp'), $layout],
            'homepage_product_archive' => [__('Latest Products', 'saudagarwp'), $layout],
            'homepage_product_1' => [__('Product Catalog 1', 'saudagarwp'), $layout],
            'homepage_product_2' => [__('Product Catalog 2', 'saudagarwp'), $layout],
            'homepage_product_3' => [__('Product Catalog 3', 'saudagarwp'), $layout],
            'homepage_product_cat' => [__('Product Categories', 'saudagarwp'), $layout],
            'homepage_blog_1' => [__('Blog/News', 'saudagarwp'), $layout],
            'homepage_banner' => [__('Banner', 'saudagarwp'), $layout],
            'homepage_image' => [__('Image', 'saudagarwp'), $layout],
            'homepage_image_content' => [__('Image Content', 'saudagarwp'), $layout],
            'homepage_text_content' => [__('Text Content', 'saudagarwp'), $layout],
            'homepage_infobox' => [__('Infobox', 'saudagarwp'), $layout],
        ]);
    }

    public function add_layout() {
        $section = 'homepage_layout_section';

        if (rt_is_premium()) {
            $chooses = [
                'slider' => __('Images Slider', 'saudagarwp'),
                'welcome-area' => __('Welcome Area', 'saudagarwp'),
                'product-archive' => __('Latest Products', 'saudagarwp'),
                'product-1' => __('Product Catalog 1', 'saudagarwp'),
                'product-2' => __('Product Catalog 2', 'saudagarwp'),
                'product-3' => __('Product Catalog 3', 'saudagarwp'),
                'product-category' => __('Product Categories', 'saudagarwp'),
                'blog-1' => __('Blog/News', 'saudagarwp'),
                'banner' => __('Banner', 'saudagarwp'),
                'image' => __('Image', 'saudagarwp'),
                'image-content' => __('Image Content', 'saudagarwp'),
                'text-content' => __('Text Content', 'saudagarwp'),
                'infobox' => __('Infobox', 'saudagarwp'),
            ];
        } else {
            $chooses = [
                'slider' => __('Images Slider', 'saudagarwp'),
                'product-archive' => __('Product Archives', 'saudagarwp'),
                'product-1' => __('Product Catalog 1', 'saudagarwp'),
            ];
        }

        $this->add_field([
            'type' => 'sortable',
            'settings' => 'homebuilder_layout',
            'label' => __('Sections'),
            'section' => $section,
            'description' => __('Select (and re-order) front page sections that you want to display', 'saudagarwp'),
            'default' => [
                'product-1',
            ],
            'choices' => $chooses,
            'partial_refresh' => [
                'homebuilder_layout' => [
                    'selector' => '#homebuilder_layout_wrapper',
                    'render_callback' => function () {
                        rt_get_template_part("homepage/homepage-content");
                    },
                ],
            ],
        ]);

        $this->add_header([
            'label' => __('Homepage Layout', 'saudagarwp'),
            'settings' => 'homebuilder_content_layout',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'homebuilder_content_layout',
            'label' => __('Layout', 'saudagarwp'),
            'section' => $section,
            'default' => 'normal',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/layout-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/layout-compact.png',
            ],
        ]);
    }

    public function get_homepage_content() {
        return [
            'homebuilder_layout' => [
                'selector' => '#homebuilder_layout_wrapper',
                'render_callback' => function () {
                    rt_get_template_part("homepage/homepage-content");
                },
            ],
        ];
    }

    public function add_text_content() {
        $section = 'homepage_text_content_section';

        $this->add_field([
            'type' => 'editor',
            'settings' => "homebuilder_text_content_text",
            'label' => __('Content', 'saudagarwp'),
            'tooltip' => __('Use the rich text editor to add in text into the section', 'saudagarwp'),
            'section' => $section,
            'partial_refresh'    => [
                'homebuilder_text_content' => [
                    'selector'        => "#homepage-text-content .page-container",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => "homebuilder_text_content_text_background_type",
            'section' => $section,
            'label' => __('Background', 'saudagarwp'),
            'default' => 'background-primary',
            'choices' => [
                'background-primary' => 'Primary',
                'background-secondary' => 'Secondary',
            ],
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => "homebuilder_text_content_background_spacing",
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'top' => '0',
                'bottom' => '30px',
            ],
            'output' => [
                [
                    'element' => "#homepage-text-content",
                    'property' => 'padding',
                ],
            ],

            'transport' => 'auto',
        ]);
    }

    public function add_image() {
        $section = 'homepage_image_section';

        $this->add_field([
            'type' => 'image',
            'settings' => 'homebuilder_image',
            'label' => __('Image', 'saudagarwp'),
            'description' => __('Upload an image to be used for the section. For best results, the image should be at least 1200px wide and it looks best if the image is taller than the overall text you will be adding.', 'saudagarwp'),
            'section' => $section,
            'default' => '',
            'choices' => [
                'save_as' => 'id',
            ],
        ]);

        $this->add_field([
            'label' => __('Expand full width', 'saudagarwp'),
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_image_full_width",
            'default' => false,
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'homebuilder_image_link_url',
            'label' => __('Link Url', 'saudagarwp'),
            'default' => '#',
            'section' => $section,
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => 'homebuilder_image_padding',
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'left' => '0px',
                'top' => '0px',
                'right' => '0px',
                'bottom' => '0px',
            ],
            'choices' => [
                'labels' => [
                    'left' => __('Left', 'saudagarwp'),
                    'top' => __('Top', 'saudagarwp'),
                    'right' => __('Right', 'saudagarwp'),
                    'bottom' => __('Bottom', 'saudagarwp'),
                ],
            ],
            'output' => [
                [
                    'element' => '#homepage-image',
                    'property' => 'padding',
                ],
            ],
            'transport' => 'auto',
        ]);
    }

    public function add_image_content() {
        $section = 'homepage_image_content_section';

        $this->add_field([
            'type' => 'image',
            'settings' => 'homebuilder_image_content_item',
            'label' => __('Image', 'saudagarwp'),
            'tooltip' => __('Upload an image to be used for the section. For best results, the image should be at least 600px wide and it looks best if the image is taller than the overall text you will be adding.', 'saudagarwp'),
            'section' => $section,
            'default' => '',
            'choices' => [
                'save_as' => 'id',
            ],

            'partial_refresh'    => [
                'homebuilder_image_content' => [
                    'selector'        => "#homepage-image-content .rt-image-content__title",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_image_content_title",
            'label' => __('Title', 'saudagarwp'),
            'default' => __('Title Image Content', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'editor',
            'settings' => "homebuilder_image_content_content",
            'label' => __('Content', 'saudagarwp'),
            'default' => __('Add rich text to the section. Ideally the text should be short and to the point in order to prevent if from being taller than the image.', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_image_content_link_text",
            'label' => __('Link Text', 'saudagarwp'),
            'default' => __('Read More', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'homebuilder_image_content_link_url',
            'label' => __('Link Url', 'saudagarwp'),
            'section' => $section,
            'default' => get_site_url(),
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => 'homebuilder_image_content_link_style',
            'section' => $section,
            'label' => __('Button Style', 'admin_domain'),
            'default' => 'button-link',
            'choices' => array(
                'button-link' => 'Link',
                'button-primary' => 'Button Primary',
                'button-action' => 'Button Action',
            ),
        ]);

        $this->add_field([
            'type' => 'radio-buttonset',
            'settings' => "homebuilder_image_content_text_align",
            'label' => __('Text align', 'saudagarwp'),
            'section' => $section,
            'default' => 'left',
            'choices' => [
                'left' => '<i class="fa-solid fa-align-left"></i>',
                'center' => '<i class="fa-solid fa-align-center"></i>',
                'right' => '<i class="fa-solid fa-align-right"></i>',
            ],
            'output' => [
                [
                    'element' => '#homepage-image-content',
                    'property' => 'text-align',
                ],
            ],
            'transport' => 'auto',
        ]);

        // section
        $this->add_header([
            'label' => 'Section',
            'settings' => "homebuilder_image_content",
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => "homebuilder_image_content_background_type",
            'section' => $section,
            'label' => __('Background', 'saudagarwp'),
            'default' => 'background-primary',
            'choices' => [
                'background-primary' => 'Primary',
                'background-secondary' => 'Secondary',
            ],
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => "homebuilder_image_content_background_spacing",
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'top' => '0',
                'bottom' => '30px',
            ],
            'output' => [
                [
                    'element' => '#homepage-image-content',
                    'property' => 'padding',
                ],
            ],
            'transport' => 'auto',
        ]);
    }

    public function add_slider() {
        $section = 'homepage_slider_section';

        $this->add_header([
            'label' => __('Options', 'saudagarwp'),
            'settings' => "homebuilder_slider_layout",
            'section' => $section,
            'partial_refresh'    => [
                'homebuilder_image_slider' => [
                    'selector'        => "#homepage-slider .swiper-pagination",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'repeater',
            'label' => __('Image Slides', 'saudagarwp'),
            'tooltip' => __('With each image slide you have the option to upload an image and set the links.  For best results, all of your slides should use images that are identical in size and ratio. One recommended size that works well for most shops is 1500 x 600px.', 'saudagarwp'),
            'section' => $section,
            'row_label' => [
                'type' => 'field',
                'value' => __('Slider Item', 'saudagarwp'),
                'field' => 'title',
            ],
            'button_label' => __("Add New Slider", 'saudagarwp'),
            'settings' => 'homebuilder_slider',
            'fields' => [
                'image' => [
                    'type' => 'image',
                    'label' => __('Image', 'saudagarwp'),
                    'choices' => [
                        'save_as' => 'id',
                    ],
                ],
                'link_url' => [
                    'type' => 'link',
                    'label' => __('Link URL', 'saudagarwp'),
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => 'homebuilder_slider_image_size',
            'label' => __('Image Size', 'saudagarwp'),
            'section' => $section,
            'multiple' => 1,
            'default' => 'full',
            'choices' => Helper::get_image_size(),
        ]);

        $this->add_field([
            'label' => __('Expand full width', 'saudagarwp'),
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_slider_full_width",
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_slider_loop",
            'label' => __('Infinity Loop', 'saudagarwp'),
        ]);

        $this->add_field([
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_slider_autoplay",
            'label' => __('Auto Play', 'saudagarwp'),
        ]);

        $this->add_field([
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_slider_nav",
            'label' => __('Navigation', 'saudagarwp'),
            'default' => true,
        ]);

        $this->add_header([
            'label' => 'Section',
            'settings' => "homebuilder_slider_section",
            'section' => $section,
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => "homebuilder_slider_background_spacing",
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'top' => '30px',
                'bottom' => '30px',
            ],
            'output' => [
                [
                    'element' => "#homepage-slider",
                    'property' => 'padding',
                ],
            ],
            'transport' => 'auto',
        ]);
    }

    public function add_banner() {
        $section = 'homepage_banner_section';

        $this->add_header([
            'label' => __('Options', 'saudagarwp'),
            'settings' => "homebuilder_banner",
            'section' => $section,
            'partial_refresh'    => [
                'homebuilder_banner' => [
                    'selector'        => "#homepage-banner .rt-header-block__title",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'repeater',
            'label' => __('Content', 'saudagarwp'),
            'section' => $section,
            'row_label' => [
                'type' => 'field',
                'value' => __('Banner Item', 'saudagarwp'),
                'field' => 'title',
            ],
            'button_label' => __('Add New Banner', 'saudagarwp'),
            'settings' => 'homebuilder_banner',
            'fields' => [
                'image' => [
                    'type' => 'image',
                    'label' => __('Image', 'saudagarwp'),
                    'choices' => [
                        'save_as' => 'id',
                    ],
                ],
                'link' => [
                    'type' => 'link',
                    'label' => __('Link Url', 'saudagarwp'),
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_banner_carousel",
            'label' => __('Carousel', 'saudagarwp'),
        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'section' => $section,
            'settings' => "homebuilder_banner_column",
            'label' => __('Column', 'saudagarwp'),
            'description' => __('Number of Column Per Row', 'saudagarwp'),
            'default' => 3,
            'default_tablet' => 2,
            'default_mobile' => 2,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_banner_carousel",
                    'operator' => '!==',
                    'value' => true,
                ],
            ],

        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'settings' => "homebuilder_banner_slider_show",
            'section' => $section,
            'label' => __('Slides Per View', 'saudagarwp'),
            'default' => 3,
            'default_tablet' => 2,
            'default_mobile' => 2,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_banner_carousel",
                    'operator' => '==',
                    'value' => true,
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => 'homebuilder_banner_image_size',
            'label' => __('Image Size', 'saudagarwp'),
            'section' => $section,
            'multiple' => 1,
            'default' => 'featured_medium',
            'choices' => Helper::get_image_size(),
        ]);

        // section
        $this->add_header([
            'label' => 'Section',
            'settings' => "homebuilder_banner_section",
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_banner_title",
            'label' => __('Title', 'saudagarwp'),
            'default' => __('Banner Title', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'textarea',
            'settings' => "homebuilder_banner_desc",
            'label' => __('Description', 'saudagarwp'),
            'default' => __('Banner Description', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => "homebuilder_banner_background_type",
            'section' => $section,
            'label' => __('Background', 'saudagarwp'),
            'default' => 'background-primary',
            'choices' => [
                'background-primary' => 'Primary',
                'background-secondary' => 'Secondary',
            ],
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => "homebuilder_banner_background_spacing",
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'top' => '0',
                'bottom' => '30px',
            ],
            'output' => [
                [
                    'element' => "#homepage-banner",
                    'property' => 'padding',
                ],
            ],

            'transport' => 'auto',
        ]);
    }

    public function add_infobox() {
        $section = 'homepage_infobox_section';

        $this->add_header([
            'label' => __('Options', 'saudagarwp'),
            'settings' => "homebuilder_infobox",
            'section' => $section,
            'partial_refresh'    => [
                'homebuilder_infobox' => [
                    'selector'        => "#homepage-infobox .rt-header-block__title",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'repeater',
            'label' => __('Info Box', 'saudagarwp'),
            'section' => $section,
            'row_label' => [
                'type' => 'field',
                'value' => __('Info Item', 'saudagarwp'),
                'field' => 'title',
            ],
            'button_label' => __('Add New Info Box', 'saudagarwp'),
            'settings' => 'homebuilder_infobox',
            'fields' => [
                'image' => [
                    'type' => 'image',
                    'label' => __('Image', 'saudagarwp'),
                    'choices' => [
                        'save_as' => 'id',
                    ],
                ],
                'title' => [
                    'type' => 'text',
                    'label' => __('Title', 'saudagarwp'),
                ],
                'content' => [
                    'type' => 'textarea',
                    'label' => __('Content', 'saudagarwp'),
                ],
                'link_url' => [
                    'type' => 'link',
                    'label' => __('Link Url', 'saudagarwp'),
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => "homebuilder_infobox_style",
            'section' => $section,
            'label' => __('Style', 'saudagarwp'),
            'default' => 'stack',
            'choices' => [
                'stack' => 'Stack Left',
                'center' => 'Stack Center',
                'left' => 'Inline Left',
            ],
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'homebuilder_infobox_card',
            'label' => __('Infobox Card Frame', 'saudagarwp'),
            'description' => __('When enabled, show a border around Infobox Cards.', 'saudagarwp'),
            'section' => $section,
            'default' => false,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_infobox_carousel",
            'label' => __('Carousel', 'saudagarwp'),
        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'section' => $section,
            'settings' => "homebuilder_infobox_column",
            'label' => __('Column', 'saudagarwp'),
            'description' => __('Number of Column Per Row', 'saudagarwp'),
            'default' => 3,
            'default_tablet' => 1,
            'default_mobile' => 1,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_infobox_carousel",
                    'operator' => '!==',
                    'value' => true,
                ],
            ],

        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'settings' => "homebuilder_infobox_slider_show",
            'section' => $section,
            'label' => __('Slides Per View', 'saudagarwp'),
            'default' => 3,
            'default_tablet' => 1,
            'default_mobile' => 1,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_infobox_carousel",
                    'operator' => '==',
                    'value' => true,
                ],
            ],
        ]);

        // section
        $this->add_header([
            'label' => 'Section',
            'settings' => "homebuilder_infobox_section",
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_infobox_title",
            'label' => __('Title', 'saudagarwp'),
            'default' => __('Info Box Title', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'textarea',
            'settings' => "homebuilder_infobox_desc",
            'label' => __('Description', 'saudagarwp'),
            'default' => __('Info Box Description', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => "homebuilder_infobox_background_type",
            'section' => $section,
            'label' => __('Background', 'saudagarwp'),
            'default' => 'background-primary',
            'choices' => [
                'background-primary' => 'Primary',
                'background-secondary' => 'Secondary',
            ],
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => "homebuilder_infobox_background_spacing",
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'top' => '0',
                'bottom' => '30px',
            ],
            'output' => [
                [
                    'element' => "#homepage-infobox",
                    'property' => 'padding',
                ],
            ],

            'transport' => 'auto',
        ]);
    }

    public function add_product_archive() {
        $section = "homepage_product_archive_section";

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_product_archive_title",
            'label' => __('Title', 'saudagarwp'),
            'default' => __('Shop', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'textarea',
            'settings' => "homebuilder_product_archive_desc",
            'label' => __('Description', 'saudagarwp'),
            'default' => __('Lastest Product', 'saudagarwp'),
            'section' => $section,
        ]);
    }

    public function add_product_loop() {

        // before product loop
        $total = (rt_is_premium()) ? 3 : 1;

        for ($index = 1; $index <= $total; $index++) {

            $section = "homepage_product_{$index}_section";
            $setting = "homebuilder_product_{$index}";

            $this->add_header([
                'label' => __('Options', 'saudagarwp'),
                'settings' => "homebuilder_product_{$index}",
                'section' => $section,
                'partial_refresh'    => [
                    "homebuilder_product_{$index}" => [
                        'selector'        => "#homepage-product-{$index} .rt-header-block__title",
                        'render_callback' => function () {
                            return false;
                        },
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'toggle',
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_carousel",
                'label' => __('Carousel', 'saudagarwp'),
            ]);

            $this->add_field_responsive([
                'type' => 'slider',
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_column",
                'label' => __('Column', 'saudagarwp'),
                'description' => __('Number of Column Per Row', 'saudagarwp'),
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
                'choices' => [
                    'min' => 1,
                    'max' => 6,
                ],
                'active_callback' => [
                    [
                        'setting' => "homebuilder_product_{$index}_carousel",
                        'operator' => '!==',
                        'value' => true,
                    ],
                ],
            ]);

            $this->add_field_responsive([
                'type' => 'slider',
                'settings' => "homebuilder_product_{$index}_slider_show",
                'section' => $section,
                'label' => __('Slides Per View', 'saudagarwp'),
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
                'choices' => [
                    'min' => 1,
                    'max' => 6,
                ],
                'active_callback' => [
                    [
                        'setting' => "{$setting}_carousel",
                        'operator' => '==',
                        'value' => true,
                    ],
                ],

            ]);

            $this->add_field([
                'type' => 'toggle',
                'label' => __('Link Action', 'saudagarwp'),
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_slider_link",
                'active_callback' => [
                    [
                        'setting' => "homebuilder_product_{$index}_carousel",
                        'operator' => '==',
                        'value' => true,
                    ],
                ],

            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_pagination",
                'label' => __('Pagination Style', 'saudagarwp'),
                'default' => 'none',
                'multiple' => 1,
                'choices' => [
                    'none' => __('None', 'saudagarwp'),
                    'loadmore' => __('Load More', 'saudagarwp'),
                    'link' => __('Link Url', 'saudagarwp'),
                ],
                'active_callback' => [
                    [
                        'setting' => "homebuilder_product_{$index}_carousel",
                        'operator' => '!=',
                        'value' => true,
                    ],
                ],

            ]);

            $this->add_field([
                'type' => 'text',
                'settings' => "homebuilder_product_{$index}_pagination_link_text",
                'label' => __('Link Text', 'saudagarwp'),
                'section' => $section,
                'default' => __('View More', 'saudagarwp'),
                'active_callback' => [
                    [
                        [
                            'setting' => "homebuilder_product_{$index}_slider_link",
                            'operator' => '==',
                            'value' => true,
                        ],
                        [
                            'setting' => "homebuilder_product_{$index}_pagination",
                            'operator' => '==',
                            'value' => 'link',
                        ],
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'link',
                'settings' => "homebuilder_product_{$index}_pagination_link_url",
                'label' => __('Link Url', 'saudagarwp'),
                'section' => $section,
                'active_callback' => [
                    [
                        [
                            'setting' => "homebuilder_product_{$index}_slider_link",
                            'operator' => '==',
                            'value' => true,
                        ],
                        [
                            'setting' => "homebuilder_product_{$index}_pagination",
                            'operator' => '==',
                            'value' => 'link',
                        ],
                    ],
                ],
            ]);

            $this->add_header([
                'label' => __('Query', 'saudagarwp'),
                'settings' => "homebuilder_product_{$index}_query",
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_query_by",
                'label' => __('Query Products by', 'saudagarwp'),
                'default' => 'lastest',
                'multiple' => 1,
                'choices' => [
                    'lastest' => __('Latest Products', 'saudagarwp'),
                    'featured' => __('Featured Products', 'saudagarwp'),
                    'category' => __('Categories', 'saudagarwp'),
                    'manually' => __('Hand Picks', 'saudagarwp'),
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_category",
                'label' => __('Category', 'saudagarwp'),
                'multiple' => 99,
                'choices' => Helper::get_terms('product_cat'),
                'active_callback' => [
                    [
                        'setting' => "homebuilder_product_{$index}_query_by",
                        'operator' => '==',
                        'value' => 'category',
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_id",
                'label' => __('Select Product', 'saudagarwp'),
                'description' => __('Limit 30 products newest', 'saudagarwp'),
                'multiple' => 99,
                'choices' => \Kirki_Helper::get_posts([
                    'posts_per_page' => apply_filters('limit_option_get_posts', 30),
                    'post_type'      => 'product'
                ]),
                'active_callback' => [
                    [
                        'setting' => "homebuilder_product_{$index}_query_by",
                        'operator' => '==',
                        'value' => 'manually',
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_orderby",
                'label' => __('Order By', 'saudagarwp'),
                'default' => 'date',
                'multiple' => 1,
                'choices' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'total_sales' => __('Most Selling'),
                    'wp_post_views_count' => __('Most Viewer'),
                    'comment_count' => __('Most Review'),
                ],

            ]);

            $this->add_field([
                'type' => 'number',
                'section' => $section,
                'settings' => "homebuilder_product_{$index}_posts_per_page",
                'label' => __('Posts Per Page', 'saudagarwp'),
                'default' => 9,
            ]);

            // section
            $this->add_header([
                'label' => 'Section',
                'settings' => "homebuilder_product_{$index}_section",
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'text',
                'settings' => "homebuilder_product_{$index}_title",
                'label' => __('Title', 'saudagarwp'),
                'default' => __('Product Title', 'saudagarwp'),
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'textarea',
                'settings' => "homebuilder_product_{$index}_desc",
                'label' => __('Description', 'saudagarwp'),
                'default' => __('Product Description', 'saudagarwp'),
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'select',
                'settings' => "homebuilder_product_{$index}_background_type",
                'section' => $section,
                'label' => __('Background', 'saudagarwp'),
                'default' => 'background-primary',
                'choices' => [
                    'background-primary' => 'Primary',
                    'background-secondary' => 'Secondary',
                ],
            ]);

            $this->add_field_responsive([
                'type' => 'dimensions',
                'settings' => "homebuilder_product_{$index}_background_spacing",
                'label' => __('Spacing', 'saudagarwp'),
                'description' => __('Use CSS Unit px or %', 'saudagarwp'),
                'section' => $section,
                'default' => [
                    'top' => '0',
                    'bottom' => '30px',
                ],
                'output' => [
                    [
                        'element' => "#homepage-product-{$index}",
                        'property' => 'padding',
                    ],
                ],
                'transport' => 'auto',
            ]);
        }
    }

    public function add_product_category() {

        $section = 'homepage_product_cat_section';

        $this->add_header([
            'label' => __('General', 'saudagarwp'),
            'settings' => "homebuilder_product_category",
            'section' => $section,
            'partial_refresh'    => [
                'homebuilder_product_category' => [
                    'selector'        => "#homepage-product-category .rt-header-block__title",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => 'homebuilder_product_category_style',
            'label' => __('Style', 'saudagarwp'),
            'default' => 'style-1',
            'section' => $section,
            'choices' => [
                'style-1' => __('Style 1'),
                'style-2' => __('Style 2'),
            ],

        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'homebuilder_product_category_hide_empty',
            'label' => __('Hide Empty Category', 'saudagarwp'),
            'default' => false,
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'homebuilder_product_category_hide_child',
            'label' => __('Hide Sub Category', 'saudagarwp'),
            'default' => false,
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_product_category_carousel",
            'default' => true,
            'label' => __('Carousel', 'saudagarwp'),
        ]);


        $this->add_field_responsive([
            'type' => 'slider',
            'section' => $section,
            'settings' => "homebuilder_product_category_column",
            'label' => __('Column', 'saudagarwp'),
            'description' => __('Number of Column Per Row', 'saudagarwp'),
            'default' => 3,
            'default_tablet' => 2,
            'default_mobile' => 2,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_product_category_carousel",
                    'operator' => '!==',
                    'value' => true,
                ],
            ],

        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'settings' => "homebuilder_product_category_slider_show",
            'section' => $section,
            'label' => __('Slides Per View', 'saudagarwp'),
            'default' => 3,
            'default_tablet' => 2,
            'default_mobile' => 2,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_product_category_style",
                    'operator' => '==',
                    'value' => 'style-1',
                ],
            ],

        ]);


        // section
        $this->add_header([
            'label' => 'Section',
            'settings' => "homebuilder_product_category_section",
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_product_category_title",
            'label' => __('Title', 'saudagarwp'),
            'default' => __('Categories Title', 'saudagarwp'),
            'section' => $section,

        ]);

        $this->add_field([
            'type' => 'textarea',
            'settings' => "homebuilder_product_category_desc",
            'label' => __('Description', 'saudagarwp'),
            'default' => __('Categories Description', 'saudagarwp'),
            'section' => $section,

        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => "homebuilder_product_category_background_type",
            'section' => $section,
            'label' => __('Background', 'saudagarwp'),
            'default' => 'background-primary',
            'choices' => [
                'background-primary' => 'Primary',
                'background-secondary' => 'Secondary',
            ],
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => "homebuilder_product_category_background_spacing",
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'top' => '0',
                'bottom' => '30px',
            ],
            'output' => [
                [
                    'element' => "#homepage-product-category",
                    'property' => 'padding',
                ],
            ],
            'transport' => 'auto',
        ]);
    }

    public function add_blog_loop() {

        $section = "homepage_blog_1_section";

        $this->add_header([
            'label' => __('Options', 'saudagarwp'),
            'settings' => "homebuilder_blog_1",
            'section' => $section,
            'partial_refresh'    => [
                'homebuilder_blog_1' => [
                    'selector'        => "#homepage-blog-1 .rt-header-block__title",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'toggle',
            'section' => $section,
            'settings' => "homebuilder_blog_1_carousel",
            'label' => __('Carousel', 'saudagarwp'),
        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'section' => $section,
            'settings' => "homebuilder_blog_1_column",
            'label' => __('Column', 'saudagarwp'),
            'description' => __('Number of Column Per Row', 'saudagarwp'),
            'default' => 3,
            'default_tablet' => 2,
            'default_mobile' => 2,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_blog_1_carousel",
                    'operator' => '!==',
                    'value' => true,
                ],
            ],

        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'settings' => "homebuilder_blog_1_slider_show",
            'section' => $section,
            'label' => __('Slides Per View', 'saudagarwp'),
            'default' => 3,
            'default_tablet' => 2,
            'default_mobile' => 2,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_blog_1_carousel",
                    'operator' => '==',
                    'value' => true,
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'toggle',
            'label' => __('Link Action', 'saudagarwp'),
            'section' => $section,
            'settings' => "homebuilder_blog_1_slider_link",
            'active_callback' => [
                [
                    'setting' => "homebuilder_blog_1_carousel",
                    'operator' => '==',
                    'value' => true,
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'select',
            'section' => $section,
            'settings' => "homebuilder_blog_1_pagination",
            'label' => __('Pagination Style', 'saudagarwp'),
            'default' => 'none',
            'multiple' => 1,
            'choices' => [
                'none' => __('None', 'saudagarwp'),
                'loadmore' => __('Load More', 'saudagarwp'),
                'link' => __('Link Url', 'saudagarwp'),
            ],
            'active_callback' => [
                [
                    'setting' => "homebuilder_blog_1_carousel",
                    'operator' => '!==',
                    'value' => true,
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_blog_1_pagination_link_text",
            'label' => __('Link Text', 'saudagarwp'),
            'section' => $section,
            'default' => __('View More', 'saudagarwp'),
            'active_callback' => [
                [
                    [
                        'setting' => "homebuilder_blog_1_slider_link",
                        'operator' => '==',
                        'value' => true,
                    ],
                    [
                        'setting' => "homebuilder_blog_1_pagination",
                        'operator' => '==',
                        'value' => 'link',
                    ],
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => "homebuilder_blog_1_pagination_link_url",
            'label' => __('Link Url', 'saudagarwp'),
            'section' => $section,
            'active_callback' => [
                [
                    [
                        'setting' => "homebuilder_blog_1_slider_link",
                        'operator' => '==',
                        'value' => true,
                    ],
                    [
                        'setting' => "homebuilder_blog_1_pagination",
                        'operator' => '==',
                        'value' => 'link',
                    ],
                ],
            ],
        ]);

        $this->add_header([
            'label' => __('Query', 'saudagarwp'),
            'settings' => "homebuilder_blog_1_query",
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'select',
            'section' => $section,
            'settings' => "homebuilder_blog_1_query_by",
            'label' => __('Query posts by', 'saudagarwp'),
            'default' => 'lastest',
            'multiple' => 1,
            'choices' => [
                'lastest' => __('Latest Posts', 'saudagarwp'),
                'category' => __('Categories', 'saudagarwp'),
                'manually' => __('Hand Picks', 'saudagarwp'),
            ],

        ]);

        $this->add_field([
            'type' => 'select',
            'section' => $section,
            'settings' => "homebuilder_blog_1_category",
            'label' => __('Category', 'saudagarwp'),
            'multiple' => 99,
            'choices' => Helper::get_terms('category'),
            'active_callback' => [
                [
                    'setting' => "homebuilder_blog_1_query_by",
                    'operator' => '==',
                    'value' => 'category',
                ],
            ],

        ]);

        $this->add_field([
            'type' => 'select',
            'section' => $section,
            'settings' => "homebuilder_blog_1_id",
            'label' => __('Select Post', 'saudagarwp'),
            'description' => __('Limit 30 posts newest', 'saudagarwp'),
            'multiple' => 99,
            'choices' => \Kirki_Helper::get_posts([
                'posts_per_page' => apply_filters('limit_option_get_posts', 30),
                'post_type'      => 'post'
            ]),

            'active_callback' => [
                [
                    'setting' => "homebuilder_blog_1_query_by",
                    'operator' => '==',
                    'value' => 'manually',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'select',
            'section' => $section,
            'settings' => "homebuilder_blog_1_orderby",
            'label' => __('Order By', 'saudagarwp'),
            'default' => 'date',
            'multiple' => 1,
            'choices' => [
                'ID' => __('Post Id', 'saudagarwp'),
                'author' => __('Post Author', 'saudagarwp'),
                'title' => __('Title', 'saudagarwp'),
                'date' => __('Date', 'saudagarwp'),
                'modified' => __('Last Modified Date', 'saudagarwp'),
                'comment_count' => __('Most Comment', 'saudagarwp'),
            ],

        ]);

        $this->add_field([
            'type' => 'number',
            'section' => $section,
            'settings' => "homebuilder_blog_1_posts_per_page",
            'label' => __('Posts Per Page', 'saudagarwp'),
            'default' => 9,
        ]);

        // section
        $this->add_header([
            'label' => 'Section',
            'settings' => "homebuilder_blog_1_section",
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_blog_1_title",
            'label' => __('Title', 'saudagarwp'),
            'default' => __('Post Title', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'textarea',
            'settings' => "homebuilder_blog_1_desc",
            'label' => __('Description', 'saudagarwp'),
            'default' => __('Post Description', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'select',
            'settings' => "homebuilder_blog_1_background_type",
            'section' => $section,
            'label' => __('Background', 'saudagarwp'),
            'default' => 'background-primary',
            'choices' => [
                'background-primary' => 'Primary',
                'background-secondary' => 'Secondary',
            ],
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => "homebuilder_blog_1_background_spacing",
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'top' => '0',
                'bottom' => '30px',
            ],
            'output' => [
                [
                    'element' => "#homepage-blog-1",
                    'property' => 'padding',
                ],
            ],

            'transport' => 'auto',
        ]);
    }

    public function add_welcome_area() {
        $section = "homepage_welcome_section";

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'homebuilder_welcome_style',
            'section' => $section,
            'label' => __('Style', 'admin_domain'),
            'default' => 'style-1',
            'choices' => [
                'style-1' => get_template_directory_uri() . '/core/customizer/assets/img/welcome-area-1.png',
                'style-2' => get_template_directory_uri() . '/core/customizer/assets/img/welcome-area-2.png',
                'style-3' => get_template_directory_uri() . '/core/customizer/assets/img/welcome-area-3.png',
            ],
            'partial_refresh'    => [
                'homebuilder_welcome_area' => [
                    'selector'        => "#homepage-welcome-area .rt-welcome-area__title",
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);


        $this->add_field([
            'type' => 'image',
            'settings' => 'homebuilder_welcome_image',
            'label' => __('Image', 'saudagarwp'),
            'section' => $section,
            'choices' => [
                'save_as' => 'id',
            ],
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_welcome_text_intro",
            'label' => __('Sub Title', 'saudagarwp'),
            'default' => __('Sub Title', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_welcome_title",
            'label' => __('Title', 'saudagarwp'),
            'default' => __('Title', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'editor',
            'settings' => "homebuilder_welcome_content",
            'label' => __('Content', 'saudagarwp'),
            'default' => __('Content', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'text',
            'settings' => "homebuilder_welcome_link_label",
            'label' => __('Button label', 'saudagarwp'),
            'default' => __('Read More', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'homebuilder_welcome_link_url',
            'label' => __('Button Url', 'saudagarwp'),
            'section' => $section,
            'default' => '#',
        ]);


        $this->add_header([
            'label' => __('Advanced Styles', 'saudagarwp'),
            'settings' => 'homebuilder_welcome_style',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'homebuilder_welcome_background_overlay',
            'label' => __('Background Overlay', 'saudagarwp'),
            'section' => $section,
            'default' => false,
        ]);

        $this->add_field([
            'type' => 'background',
            'settings'    => 'homebuilder_welcome_background',
            'label' => __('Background', 'saudagarwp'),
            'section' => $section,
            'default'     => [
                'background-color'      => rt_get_theme('global_background_scheme')['secondary'],
                'background-image'      => '',
                'background-repeat'     => 'no-repeat',
                'background-position'   => 'center center',
                'background-size'       => 'cover',
                'background-attachment' => 'scroll',
            ],
            'transport'   => 'auto',
            'output'      => [
                [
                    'element' => '.rt-welcome-area__background',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'settings' => 'homebuilder_welcome_sub_title_color',
            'label' => __('Sub Title Color', 'saudagarwp'),
            'default' => rt_get_theme('global_font_color')['tertiary'],
            'choices' => ['alpha' => true,],
            'section' => $section,
            'transport'   => 'auto',
            'output'      => [
                [
                    'element' => '.rt-welcome-area',
                    'property' => '--theme-font-color-tertiary',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'settings' => 'homebuilder_welcome_title_color',
            'label' => __('Title Color', 'saudagarwp'),
            'default' => rt_get_theme('global_font_color')['primary'],
            'choices' => ['alpha' => true,],
            'section' => $section,
            'transport'   => 'auto',
            'output'      => [
                [
                    'element' => '.rt-welcome-area__title',
                    'property' => '--theme-font-color-primary',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'settings' => 'homebuilder_welcome_body_color',
            'label' => __('Text Color', 'saudagarwp'),
            'default' => rt_get_theme('global_font_color')['secondary'],
            'choices' => ['alpha' => true,],
            'section' => $section,
            'transport'   => 'auto',
            'output'      => [
                [
                    'element' => '.rt-welcome-area__content',
                    'property' => '--theme-font-color-secondary',
                ],
            ],
        ]);


        $this->add_field_responsive([
            'type' => 'typography',
            'settings' => 'homebuilder_welcome_typography_title',
            'label' => __('Title Typography', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'font-family' => 'Inter',
                'variant' => '600',
                'font-size' => '30px',
                'line-height' => '1.2',
            ],
            'output' => [
                [
                    'element' => '.rt-welcome-area__title',
                ],
            ],
            'transport' => 'auto',
        ]);


        $this->add_field_responsive([
            'type' => 'typography',
            'settings' => 'homebuilder_welcome_typography_content',
            'label' => __('Content Typography', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'font-size' => '17px',
                'line-height' => '1.75',
            ],
            'output' => [
                [
                    'element' => '.rt-welcome-area__content',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'type' => 'dimension',
            'settings' => 'homebuilder_welcome_img_width',
            'label' => __('Image Width', 'saudagarwp'),
            'section' => $section,
            'default' => '50%',
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-welcome-area-image-width',
                ],
            ],
            'transport' => 'auto',
        ]);


        // section
        $this->add_header([
            'label' => 'Section',
            'settings' => "homebuilder_welcome_section",
            'section' => $section,
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'settings' => "homebuilder_welcome_background_spacing",
            'label' => __('Spacing', 'saudagarwp'),
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => $section,
            'default' => [
                'top' => '90px',
                'bottom' => '90px',
            ],
            'output' => [
                [
                    'element' => "#homepage-welcome-area",
                    'property' => 'padding',
                ],
            ],
            'transport' => 'auto',
        ]);
    }

    // end class
}

new Homebuilder;
