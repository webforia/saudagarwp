<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Sidebar extends Customizer_Base
{
    public function __construct()
    {
        $this->set_section();

        $this->add_option();
    }

    public function set_section()
    {
        $this->add_section('', [
            'sidebar_option' => [__('Sidebar', 'saudagarwp')],
        ]);
    }

    public function add_option()
    {
        $section ='sidebar_option_section';

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'toggle',
                'settings' => 'sidebar_sticky',
                'label' => __('Enable Sticky', 'saudagarwp'),
                'section' => $section,
                'default' => true,
            ]);
        }

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'sidebar_style',
            'label' => __('Sidebar Style', 'saudagarwp'),
            'section' => $section,
            'default' => 'style-1',
            'choices' => [
                'style-1' => get_template_directory_uri() . '/core/customizer/assets/img/sidebar-style-1.png',
                'style-2' => get_template_directory_uri() . '/core/customizer/assets/img/sidebar-style-2.png',
                'style-3' => get_template_directory_uri() . '/core/customizer/assets/img/sidebar-style-3.png',
                'style-4' => get_template_directory_uri() . '/core/customizer/assets/img/sidebar-style-4.png',
            ],
        ]);
    }

    // end class
}

new Sidebar;
