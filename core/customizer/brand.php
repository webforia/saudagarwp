<?php

/**
 * @author : Reret
 */

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Brand extends Customizer_Base
{

    public function __construct()
    {

        $this->add_brand_logo();
    }

    public function add_brand_logo()
    {
        $section = 'title_tagline';

        $this->add_header([
            'label' => __('Desktop Logo', 'saudagarwp'),
            'settings' => 'brand_logo',
            'section' => $section,
            'partial_refresh'    => [
                'brand_logo' => [
                    'selector'        => '.rt-header .rt-logo',
                    'render_callback' => function() {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'image',
            'settings' => 'brand_logo_primary',
            'label' => __('Default Logo', 'saudagarwp'),
            'description' => __('This logo will be used as your default logo on desktop device', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'slider',
            'settings' => 'brand_logo_primary_size',
            'label' => __('Default Logo Width', 'saudagarwp'),
            'section' => $section,
            'choices' => [
                'min' => '10',
                'max' => '300',
                'step' => '1',
            ],
            'default' => '180',
            'output' => [
                [
                    'element' => '.rt-header__main .rt-logo,
                                 .rt-header__middle .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ],

            ],
            'transport' => 'auto',
        ]);

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'image',
                'settings' => 'brand_logo_sticky',
                'label' => __('Sticky Logo', 'saudagarwp'),
                'description' => __('This logo will be used when the header is in a sticky state.', 'saudagarwp'),
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'slider',
                'settings' => 'brand_logo_sticky_size',
                'label' => __('Sticky Logo Width', 'saudagarwp'),
                'section' => $section,
                'choices' => [
                    'min' => '10',
                    'max' => '300',
                    'step' => '1',
                ],
                'default' => '180',
                'output' => [
                    [
                        'element' => '.rt-header__sticky .rt-logo',
                        'property' => 'width',
                        'units' => 'px',
                    ],

                ],
                'transport' => 'auto',
            ]);
        }

        $this->add_header([
            'label' => __('Mobile Logo', 'saudagarwp'),
            'settings' => 'brand_logo_mobile',
            'section' => $section,
            'partial_refresh'    => [
                'brand_logo_mobile' => [
                    'selector'        => '.rt-header-mobile .rt-logo',
                    'render_callback' => function() {
                        return false;
                    },
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'image',
            'settings' => 'brand_logo_mobile',
            'label' => __('Mobile Logo', 'saudagarwp'),
            'description' => __('This logo will be used on mobile devices', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'slider',
            'settings' => 'brand_logo_mobile_size',
            'label' => __('Mobile Logo Width', 'saudagarwp'),
            'section' => $section,
            'choices' => [
                'min' => '10',
                'max' => '200',
                'step' => '1',
            ],
            'default' => '180',
            'output' => [
                [
                    'element' => '.rt-header-mobile__main .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ],

            ],
            'transport' => 'auto',
        ]);

        if (rt_is_premium()) {
            $this->add_header([
                'label' => __('Checkout Logo', 'saudagarwp'),
                'settings' => 'focus_logo',
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'image',
                'settings' => 'focus_logo_primary',
                'label' => __('Checkout Logo', 'saudagarwp'),
                'description' => __('This logo will be used on checkout template', 'saudagarwp'),
                'section' => $section,
            ]);

            $this->add_field([
                'type' => 'slider',
                'settings' => 'focus_logo_primary_size',
                'label' => __('Checkout Logo Width', 'saudagarwp'),
                'section' => $section,
                'choices' => [
                    'min' => '10',
                    'max' => '300',
                    'step' => '1',
                ],
                'default' => '180',
                'output' => [
                    [
                        'element' => '.ptf-header .rt-logo',
                        'property' => 'width',
                        'units' => 'px',
                    ],

                ],
                'transport' => 'auto',
            ]);
        }

        $this->add_header([
            'label' => __('Alternative Desktop Logo', 'saudagarwp'),
            'settings' => 'brand_logo_overlay',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'image',
            'settings' => 'brand_logo_overlay',
            'label' => __('Alternative Logo', 'saudagarwp'),
            'description' => __('This logo will be used as your default logo on transparent header', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'slider',
            'settings' => 'brand_logo_overlay_size',
            'label' => __('Alternative Desktop Logo Width', 'saudagarwp'),
            'section' => $section,
            'choices' => [
                'min' => '10',
                'max' => '300',
                'step' => '1',
            ],
            'default' => '180',
            'output' => [
                [
                    'element' => '.is-overlay .rt-header__main .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ],

            ],
            'transport' => 'auto',
        ]);

        $this->add_header([
            'label' => __('Alternative Mobile Logo', 'saudagarwp'),
            'settings' => 'brand_logo_overlay_mobile',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'image',
            'settings' => 'brand_logo_mobile_overlay',
            'label' => __('Alternative Mobile Logo', 'saudagarwp'),
            'description' => __('This logo will be used as your mobile logo on transparent header', 'saudagarwp'),
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'slider',
            'settings' => 'brand_logo_mobile_overlay_size',
            'label' => __('Alternative Mobile Logo Width', 'saudagarwp'),
            'section' => $section,
            'choices' => [
                'min' => '10',
                'max' => '300',
                'step' => '1',
            ],
            'default' => '180',
            'output' => [
                [
                    'element' => '.is-overlay .rt-header-mobile__main .rt-logo',
                    'property' => 'width',
                    'units' => 'px',
                ],

            ],
            'transport' => 'auto',
        ]);

        $this->add_header([
            'label' => __('Color Browser', 'saudagarwp'),
            'settings' => 'brand_color_browser',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'color',
            'label' => __('Color Browser', 'saudagarwp'),
            'settings' => 'brand_color_browser',
            'description' => __('Change the color of address bar in mobile browser', 'saudagarwp'),
            'section' => $section,
            'default' => $this->get_default_mod('global_brand_color', 'primary'),
        ]);

        $this->add_header([
            'label' => __('Pavicon', 'saudagarwp'),
            'settings' => 'brand_pavicon',
            'section' => $section,
        ]);
    }

    // end class
}

new Brand;
