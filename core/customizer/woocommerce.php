<?php

/**
 * @author : Reret
 */

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class WooCommerce extends Customizer_Base {

    public function __construct() {

        add_action('customize_register', [$this, 'remove_default_field']);

        $this->set_section();

        $this->add_archive_option();
        $this->shop_navbar();
        $this->add_archive_layout();

        $this->add_single_option();
        $this->add_single_related();
        $this->add_single_layout();

        $this->thank_page_action();

        $this->add_woocommerce_store_notice();

        if (rt_is_premium()) {
            $this->add_sale_badge();
            $this->add_cart();
        }
    }

    public function set_section() {
        $this->add_section('woocommerce', [
            'woocommerce_single' => [__('Product Single', 'saudagarwp')],
            'woocommerce_badge' => [__('Badge Sale', 'saudagarwp')],
            'woocommerce_cart' => [__('Cart', 'saudagarwp')],
            'woocommerce_checkout' => [__('Checkout', 'saudagarwp')],
            'woocommerce_navbar' => [__('Navbar Shop', 'saudagarwp')],
        ]);
    }


    /**
     * Remove default customizer woocommerce
     *
     * @param [type] $wp_customize customizer control
     * @return void
     */
    public function remove_default_field($wp_customize) {
        $wp_customize->add_control('woocommerce_catalog_columns')->theme_supports = false;
        $wp_customize->add_control('woocommerce_catalog_rows')->theme_supports = false;
    }

    public function shop_navbar() {

        $section = 'woocommerce_navbar_section';

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'multicheck',
                'settings' => 'woocommerce_shop_navbar_bottom',
                'label' => __('Navbar Bottom', 'saudagarwp'),
                'section' => $section,
                'default' => [
                    'shop', 'filter', 'user', 'cart',
                ],
                'multiple' => 999,
                'choices' => [
                    'shop' => __('Shop', 'saudagarwp'),
                    'filter' => __('Filters', 'saudagarwp'),
                    'user' => __('User', 'saudagarwp'),
                    'cart' => __('Cart', 'saudagarwp'),
                ],
                'partial_refresh' => [
                    'woocommerce_shop_navbar' => [
                        'selector' => '.rt-shop-navbar__item:first-child .rt-shop-navbar__icon',
                        'render_callback' => function () {
                            return false;
                        },
                    ],
                ],
            ]);
        }
    }

    public function add_archive_option() {
        $section = 'woocommerce_product_catalog';
        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'radio-image',
                'settings' => 'woocommerce_archive_style',
                'label' => __('Style', 'saudagarwp'),
                'section' => $section,
                'default' => 'style-1',
                'choices' => [
                    'style-1' => get_template_directory_uri() . '/core/customizer/assets/img/product-style-1.png',
                    'style-2' => get_template_directory_uri() . '/core/customizer/assets/img/product-style-2.png',
                    'style-3' => get_template_directory_uri() . '/core/customizer/assets/img/product-style-3.png',
                ],
                'partial_refresh'    => [
                    'woocommerce_product_catalog' => [
                        'selector'        => ".rt-product-archive, #home_product_archive",
                        'render_callback' => function () {
                            return false;
                        },
                    ],
                ],
            ]);

            $this->add_field([
                'type' => 'toggle',
                'settings' => 'woocommerce_archive_masonry',
                'description' => __('Grid layout with different height', 'saudagarwp'),
                'label' => __('Masonry', 'saudagarwp'),
                'section' => $section,
                'default' => false,
            ]);

            $this->add_field([
                'type' => 'toggle',
                'settings' => 'woocommerce_archive_card',
                'label' => __('Product Card frame', 'saudagarwp'),
                'description' => __('When enabled, show a border around Product Cards.', 'saudagarwp'),
                'section' => $section,
                'default' => false,
            ]);

            $this->add_field([
                'type' => 'radio-buttonset',
                'settings' => 'woocommerce_archive_text_align',
                'label' => __('Content Position', 'saudagarwp'),
                'section' => $section,
                'default' => 'center',
                'choices' => [
                    'left' => 'Left',
                    'center' => 'Center',
                    'right' => 'Right',
                ],
                'transport' => 'auto',
                'output' => [
                    [
                        'exclude' => ['center', 'right'],
                        'element' => '.rt-product__body',
                        'property' => 'text-align',
                        'value_pattern' => 'left',
                    ],
                    [
                        'exclude' => ['left', 'right'],
                        'element' => '.rt-product__body',
                        'property' => 'text-align',
                        'value_pattern' => 'center',
                    ],
                    [
                        'exclude' => ['left', 'center'],
                        'element' => '.rt-product__body',
                        'property' => 'text-align',
                        'value_pattern' => 'right',
                    ],
                    [
                        'exclude' => ['center', 'right'],
                        'element' => '.rt-product .wsb-countdown',
                        'property' => 'justify-content',
                        'value_pattern' => 'left',
                    ],
                    [
                        'exclude' => ['left', 'right'],
                        'element' => '.rt-product__body',
                        'property' => 'justify-content',
                        'value_pattern' => 'center',
                    ],
                    [
                        'exclude' => ['left', 'center'],
                        'element' => '.rt-product__body',
                        'property' => 'justify-content',
                        'value_pattern' => 'right',
                    ],
                ],
            ]);

            $this->add_field_responsive([
                'type' => 'slider',
                'section' => $section,
                'settings' => 'woocommerce_archive_options_column',
                'label' => __('Column', 'saudagarwp'),
                'description' => __('Number of Column Per Row', 'saudagarwp'),
                'default' => 3,
                'default_tablet' => 2,
                'default_mobile' => 2,
                'choices' => [
                    'min' => 1,
                    'max' => 6,
                ],

            ]);

            $this->add_field_responsive([
                'type' => 'number',
                'section' => $section,
                'settings' => 'woocommerce_archive_options_post_per_page',
                'label' => __('Products Per Page', 'saudagarwp'),
                'default' => 9,
                'choices' => [
                    'min' => 1,
                    'max' => 20,
                ],
            ]);

            $this->add_field([
                'type' => 'select',
                'settings' => 'woocommerce_archive_pagination',
                'label' => __('Pagination', 'saudagarwp'),
                'section' => $section,
                'default' => 'number',
                'multiple' => 1,
                'choices' => [
                    'none' => __('None', 'saudagarwp'),
                    'number' => __('Number', 'saudagarwp'),
                    'loadmore' => __('Load More', 'saudagarwp'),
                ],
            ]);
        }
    }

    public function add_archive_layout() {
        $section = 'woocommerce_product_catalog';

        $this->add_header([
            'label' => __('Product Archive Layout', 'saudagarwp'),
            'settings' => 'woocommerce_archive_layout',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'woocommerce_archive_layout',
            'label' => __('Layout', 'saudagarwp'),
            'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a sidebar shop or filter.', 'saudagarwp'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
            'section' => $section,
            'default' => 'sidebar-right',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/layout-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/layout-compact.png',
                'sidebar-left' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-left.png',
                'sidebar-right' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-right.png',
            ],
        ]);
    }

    public function add_single_option() {
        $section = 'woocommerce_single_section';

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'radio-image',
                'settings' => 'woocommerce_single_gallery',
                'label' => __('Image Gallery thumbnails', 'saudagarwp'),
                'description' => __('Choose the layout for thumbnails of images uploaded into the products', 'saudagarwp'),
                'section' => $section,
                'default' => 'left-gallery',
                'choices' => [
                    'left-gallery' => get_template_directory_uri() . '/core/customizer/assets/img/gallery-left.png',
                    'bottom-gallery' => get_template_directory_uri() . '/core/customizer/assets/img/gallery-bottom.png',
                ],
                'active_callback' => [
                    [
                        'setting' => 'woocommerce_single_layout',
                        'operator' => '==',
                        'value' => 'normal',
                    ],

                ],
                'partial_refresh' => [
                    'woocommerce_single' => [
                        'selector' => '.rt-product-summary .product_title, .rt-product-related .rt-header-block__title, .rt-product-up-sells .rt-header-block__title',
                        'render_callback' => function () {
                            return false;
                        },
                    ],
                ],

            ]);

            $this->add_field([
                'type' => 'toggle',
                'label' => __('Product Meta', 'saudagarwp'),
                'settings' => 'woocommerce_single_meta',
                'section' => $section,
                'default' => true,
            ]);

            $this->add_field([
                'type' => 'toggle',
                'label' => __('Share Button', 'saudagarwp'),
                'settings' => 'woocommerce_single_share',
                'section' => $section,
                'default' => true,
            ]);

            $this->add_field([
                'type' => 'toggle',
                'label' => __('Related Product', 'saudagarwp'),
                'settings' => 'woocommerce_single_product_related',
                'section' => $section,
                'default' => true,
            ]);

            $this->add_field([
                'type' => 'toggle',
                'label' => __('Unique Header On Mobile', 'saudagarwp'),
                'description' => __('Header like native mobile app', 'saudagarwp'),
                'settings' => 'header_product',
                'section' => $section,
                'default' => true,
            ]);
        }
    }

    public function add_single_related() {
        $section = 'woocommerce_single_section';

        $this->add_header([
            'label' => 'Related Product',
            'settings' => 'woocommerce_single_related',
            'section' => $section,
            'active_callback' => [
                [
                    'setting' => 'woocommerce_single_product_related',
                    'operator' => '==',
                    'value' => true,
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'number',
            'settings' => 'woocommerce_single_related_count',
            'label' => __('Total Number of Related Products', 'saudagarwp'),
            'section' => $section,
            'default' => 6,
            'active_callback' => [
                [
                    'setting' => 'woocommerce_single_product_related',
                    'operator' => '==',
                    'value' => true,
                ],
            ],
        ]);

        $this->add_field_responsive([
            'type' => 'slider',
            'section' => $section,
            'settings' => 'woocommerce_single_related_show',
            'label' => __('Slides Per View', 'saudagarwp'),
            'default' => 4,
            'default_tablet' => 2,
            'default_mobile' => 2,
            'choices' => [
                'min' => 1,
                'max' => 6,
            ],
            'active_callback' => [
                [
                    'setting' => 'woocommerce_single_product_related',
                    'operator' => '==',
                    'value' => true,
                ],
            ],

        ]);
    }

    public function add_single_layout() {
        $section = 'woocommerce_single_section';

        $this->add_header([
            'label' => 'Product Single Layout',
            'settings' => 'woocommerce_single_layout',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'woocommerce_single_layout',
            'label' => __('Layout', 'saudagarwp'),
            'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a sidebar shop or filter.', 'saudagarwp'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
            'section' => $section,
            'default' => 'normal',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/layout-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/layout-compact.png',
                'sidebar-left' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-left.png',
                'sidebar-right' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-right.png',
            ],
        ]);
    }

    /**
     * sale badge in image thumbnail
     *
     * @return void
     */
    public function add_sale_badge() {
        if (rt_is_premium()) {
            $this->add_field_color([
                'settings' => 'woocommerce_sale',
                'section' => 'woocommerce_badge_section',
                'element' => '.woocommerce span.onsale',
            ]);

            $this->add_field_background([
                'settings' => 'woocommerce_sale_background',
                'section' => 'woocommerce_badge_section',
                'element' => '.woocommerce span.onsale',
            ]);

            $this->add_field_border_color([
                'settings' => 'woocommerce_sale_border_color',
                'section' => 'woocommerce_badge_section',
                'element' => '.woocommerce span.onsale',
            ]);
        }
    }

    public function add_cart() {
        $section = 'woocommerce_cart_section';

        $this->add_field([
            'type' => 'select',
            'settings' => 'woocommerce_cart_behavior',
            'section' => $section,
            'label' => __('Add to cart behaviour', 'saudagarwp'),
            'default' => 'ajax_addtocart_panel',
            'priority' => 1,
            'choices' => [
                'default' => 'Default WooCommerce',
                'ajax_addtocart_panel' => 'Ajax Add To Cart',
                'keep_on_page' => 'Keep On Page',
                'redirect_checkout' => 'Redirect to checkout Page',
                'catalog' => 'Catalog (without button)',
            ],
            'partial_refresh' => [
                'woocommerce_cart' => [
                    'selector' => '.rt-product-summary .add-to-cart',
                    'render_callback' => function () {
                        return false;
                    },
                ],
            ],
        ]);
    }
    
    public function thank_page_action() {
        $section = 'woocommerce_checkout';

        $this->add_field([
            'type' => 'select',
            'settings' => 'woocommerce_checkout_confirmation_page',
            'section' => $section,
            'label' => __('Confirmation Page', 'saudagarwp'),
            'description' => __('Limit 30 pages newest', 'saudagarwp'),
            'priority' => 99,
            'choices' => \Kirki_Helper::get_posts([
                'posts_per_page' => apply_filters('limit_option_get_posts', 30),
                'post_type'      => 'page'
            ]),
        ]);
    }

    public function add_woocommerce_store_notice() {

        $section = 'woocommerce_store_notice';

        $this->add_field([
            'type' => 'color',
            'label' => __('Color', 'saudagarwp'),
            'settings' => 'woocommerce_store_notice_color',
            'section' => $section,
            'default' => '#fff',
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.woocommerce-store-notice, p.demo_store, .woocommerce-store-notice a, p.demo_store a',
                    'property' => 'color',
                ],
            ],
        ]);
        $this->add_field([
            'type' => 'color',
            'label' => __('Background', 'saudagarwp'),
            'settings' => 'woocommerce_store_notice_background',
            'section' => $section,
            'default' => $this->get_default_mod('global_brand_color', 'primary'),
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.woocommerce-store-notice, p.demo_store',
                    'property' => 'background',
                ],
            ],
        ]);
    }

    // end class
}

new WooCommerce;
