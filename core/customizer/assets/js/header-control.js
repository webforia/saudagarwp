/**
 * Header builder focus element
 *
 * this function for focus to element header if element in header builder click event
 * @version 2.0.0
 */

jQuery(document).ready(($) => {
  
  function panel_focus(element, panel) {
    $(element).on("click", () => {
      wp.customize.panel(panel).focus();
    });
  }

  function section_focus(element, section) {
    $(element).on("click", () => {
      wp.customize.section(section).focus();
    });
  }

  function control_focus(element, control) {
    $(element).on("click", () => {
      wp.customize.control(control).focus();
      // control active move to top panel
      var position = $("#customize-control-" + control).position();

      $(".wp-full-overlay-sidebar-content").animate({
        scrollTop: position.top,
      });
    });
  }

  // Tabs Header
  control_focus(".ui-tabs-nav #ui-id-1", "control_header_header_main");
  control_focus(".ui-tabs-nav #ui-id-2", "control_header_header_sticky");
  control_focus(".ui-tabs-nav #ui-id-3", "control_header_header_mobile_menu");
  section_focus(".ui-tabs-nav #ui-id-4", "header_menu_mobile_section");

  // Element - Logo
  control_focus("#header-tab-normal .logo", "brand_logo_primary");
  control_focus("#header-tab-sticky .logo", "brand_logo_sticky");
  control_focus("#header-tab-mobile .logo-mobile", "brand_logo_mobile");

  // Element - Menu
  section_focus(".menu-primary", "header_menu_primary_section");
  section_focus(".menu-secondary", "header_menu_secondary_section");
  section_focus(".menu-tertiary", "header_menu_tertiary_section");
  section_focus(".menu-off-canvas", "header_menu_off_canvas_section");
  section_focus(".menu-mobile", "header_menu_mobile_section");

  // Element - Search
  section_focus(
    "#header-tab-normal .search-form, #header-tab-sticky .search-form",
    "header_search_section"
  );
  section_focus(
    "#header-tab-mobile .search-form, #header-tab-drawer .search-form",
    "header_search_mobile_section"
  );

  // Element - Social Media
  section_focus(".social", "header_socmed_section");

  // Element - Button
  section_focus(".button-1", "header_button_1_section");
  section_focus(".button-2", "header_button_2_section");
  section_focus(".button-3", "header_button_3_section");

  // Element - HTML
  section_focus(".html-1", "header_html_1_section");
  section_focus(".html-2", "header_html_2_section");
  section_focus(".html-3", "header_html_3_section");
  section_focus(".html-4", "header_html_4_section");
  section_focus(".html-5", "header_html_5_section");

  // Element - Account
  section_focus(".user-icon", "header_account_section");

  // Open Header
  panel_focus(".js-header-builder-trigger", "header_panel");
});
