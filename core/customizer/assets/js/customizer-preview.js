/**
 * Assign all js function if selective resfresh load
 *
 * @version 2.0.1
 */
jQuery(document).ready(($) => {

  /** Reload header js */
  wp.customize.selectiveRefresh.bind("partial-content-rendered", () => {
    new menu();
    new modal();
    new slidepanel();
    swiperjs();
  });

  /**
   * Typography heading
   *
   * Set new value from customizer to root body class
   */
  // general
  const retheme = document.querySelector(".retheme-root");
  const headings = document.querySelectorAll("h1, h2, h3, h4, h5, h6");

  wp.customize("typography_heading", (setting) => {
    setting.bind((value) => {

      retheme.style.setProperty("--theme-font-family-primary",value["font-family"]);

      headings.forEach((heading) => {
        heading.style.setProperty("font-family", value["font-family"]);
        heading.style.setProperty("font-weight", value["font-weight"]);
        heading.style.setProperty("text-transform", value["text-transform"]);
        heading.style.setProperty("font-style", value["font-style"]);
        heading.style.setProperty("font-weight", value["font-weight"]);
      });

    });
  });

  // general
  wp.customize("typography_regular", (setting) => {
    setting.bind((value) => {
      
      retheme.style.setProperty("--theme-font-family-secondary", value["font-family"]);

      document.querySelectorAll('.rt-entry-content').forEach(content => {
        content.style.setProperty("font-size", value["font-size"]);
        content.style.setProperty("line-height", value["line-height"]);
      });
      
    });
  });

  // end document
});
