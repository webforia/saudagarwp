<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;
use Retheme\Helper;

class Connect extends Customizer_Base
{

    public function __construct()
    {
        $this->set_panel();
        $this->set_section();

        $this->add_socmed();
        $this->add_share();
        $this->add_marketplace();

    }

    public function set_panel()
    {
        $this->add_panel('connect_panel', [
            'title' => __('Connect', 'saudagarwp'),
        ]);
    }

    public function set_section()
    {
        $this->add_section('connect_panel', [
            'social' => [__('Social Media', 'saudagarwp')],
            'share' => [__('Share', 'saudagarwp')],
            'marketplace' => [__('Market Place', 'saudagarwp')],
        ]);
    }

    public function add_socmed()
    {
        $section = 'social_section';

        $this->add_field([
            'type' => 'repeater',
            'settings' => 'social_item',
            'label' => __('Social Media', 'saudagarwp'),
            'section' => $section,
            'row_label' => [
                'type' => 'field',
                'value' => __('Your Social Media', 'saudagarwp'),
                'field' => 'link_text',
            ],

            'fields' => [
                'link_text' => [
                    'type' => 'select',
                    'label' => __('Social Media', 'saudagarwp'),
                    'default' => 'facebook',
                    'choices' => Helper::get_social_media(),
                ],
                'link_url' => [
                    'type' => 'text',
                    'label' => __('Link URL', 'saudagarwp'),
                ],
            ],
        ]);
    }

    public function add_marketplace()
    {
        $section = 'marketplace_section';

        $this->add_field([
            'type' => 'link',
            'settings' => 'marketplace_blibli',
            'label' => __('Blibli', 'saudagarwp'),
            'section' => $section,
            'default' => 'https://www.blibli.com',
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'marketplace_bukalapak',
            'label' => __('Bukalapak', 'saudagarwp'),
            'section' => $section,
            'default' => 'https://www.bukalapak.com',
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'marketplace_tokopedia',
            'label' => __('Tokopedia', 'saudagarwp'),
            'section' => $section,
            'default' => 'https://www.tokopedia.com',
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'marketplace_lazada',
            'label' => __('Lazada', 'saudagarwp'),
            'section' => $section,
            'default' => 'https://www.lazada.co.id',
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'marketplace_shopee',
            'label' => __('Shopee', 'saudagarwp'),
            'section' => $section,
            'default' => 'https://shopee.co.id',
        ]);

        $this->add_field([
            'type' => 'link',
            'settings' => 'marketplace_tiktok',
            'label' => __('Tiktok Shop', 'saudagarwp'),
            'section' => $section,
            'default' => 'https://www.tiktok.com',
        ]);

    }

    public function add_share()
    {
        $section = 'share_section';

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'connect_share_facebook',
            'label' => __('Facebook', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);
        $this->add_field([
            'type' => 'toggle',
            'settings' => 'connect_share_twitter',
            'label' => __('Twitter', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'connect_share_pinterest',
            'label' => __('Pinterest', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'connect_share_email',
            'label' => __('Email', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

    }

// end class
}

new Connect;
