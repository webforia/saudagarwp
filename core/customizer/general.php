<?php

/**
 * @author : Reret
 */

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class General extends Customizer_Base {

    public function __construct() {
        $this->set_panel();

        $this->set_section();

        $this->add_typography();
        $this->add_color();

        $this->add_topup();

        $this->add_button_primary();
        $this->add_button_action();

        $this->add_badges();

        $this->add_pagination_number();
    }

    public function set_panel() {
        $this->add_panel('general_panel', [
            'title' => __('Global', 'saudagarwp'),
        ]);
    }

    public function set_section() {
        $this->add_section('general_panel', [
            'general_typography' => [__('Typography', 'saudagarwp')],
            'general_color' => [__('Colors & Styles', 'saudagarwp')],
            'general_topup' => [__('Scroll To Top', 'saudagarwp')],
            'general_pagination' => [__('Pagination', 'saudagarwp')],
            'general_button' => [__('Buttons', 'saudagarwp')],
            'general_badges' => [__('Badges', 'saudagarwp')],
            'general_form' => [__('Forms', 'saudagarwp')],
            'general_slider' => [__('Slider', 'saudagarwp')],
        ]);
    }

    public function add_typography() {
        $section = 'general_typography_section';

        $this->add_field([
            'label' => __('All Heading', 'saudagarwp'),
            'type' => 'typography',
            'settings' => 'typography_heading',
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'section' => 'general_typography_section',
            'default' => rt_get_theme('typography_heading'),
            'output' => [
                [
                    'choice' => 'font-family',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-family-primary',
                ],
                [
                    'choice' => 'line-height',
                    'element' => 'h1, h2, h3, h4, h5, h6',
                    'property' => 'line-height',
                ],
                [
                    'choice' => 'font-weight',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-weight',
                ],
                [
                    'choice' => 'text-transform',
                    'element' => 'h1, h2, h3, h4, h5, h6',
                    'property' => 'text-transform',
                ],
                [
                    'choice' => 'font-style',
                    'element' => 'h1, h2, h3, h4, h5, h6',
                    'property' => 'font-style',
                ],

                [
                    'choice' => 'font-family',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-font-family-primary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'line-height',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-line-height-primary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'text-transform',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-transform-primary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'font-style',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-style-primary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'font-weight',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-font-weight',
                    'context' => ['editor'],
                ],

            ],
            'transport' => 'postMessage',
        ]);

        $this->add_field([
            'type' => 'typography',
            'settings' => 'typography_regular',
            'label' => __('Body', 'saudagarwp'),
            'section' => 'general_typography_section',
            'default' => rt_get_theme('typography_regular'),
            'output' => [
                [
                    'choice' => 'font-family',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-family-secondary',
                ],
                [
                    'choice' => 'font-size',
                    'element' => '.rt-entry-content',
                    'property' => 'font-size',
                ],
                [
                    'choice' => 'line-height',
                    'element' => '.rt-entry-content',
                    'property' => 'line-height',
                ],
                [
                    'choice' => 'font-family',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-font-family-secondary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'font-size',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-font-size-secondary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'line-height',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-line-height-secondary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'text-transform',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-text-transform-secondary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'font-style',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-font-style-secondary',
                    'context' => ['editor'],
                ],

            ],
            'transport' => 'postMessage',
        ]);
    }

    public function add_color() {
        $section = 'general_color_section';

        $this->add_field([
            'label' => __('Brand Color', 'saudagarwp'),
            'settings' => 'global_brand_color',
            'type' => 'multicolor',
            'section' => $section,
            'choices' => [
                'primary' => __('Primary', 'saudagarwp'),
                'secondary' => __('Secondary', 'saudagarwp'),
            ],
            'default' => rt_get_theme('global_brand_color'),
            'output' => [
                [
                    'choice' => 'primary',
                    'element' => '.retheme-root',
                    'property' => '--theme-color-brand',
                ],
                [
                    'choice' => 'secondary',
                    'element' => '.retheme-root',
                    'property' => '--theme-color-brand-secondary',
                ],
                [
                    'choice' => 'primary',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-color-brand',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'secondary',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-color-brand-secondary',
                    'context' => ['editor'],
                ],

            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'label' => __('Font Color', 'saudagarwp'),
            'settings' => 'global_font_color',
            'type' => 'multicolor',
            'section' => $section,
            'choices' => [
                'primary' => __('Primary', 'saudagarwp'),
                'secondary' => __('Secondary', 'saudagarwp'),
                'tertiary' => __('Tertiary', 'saudagarwp'),
            ],
            'default' => rt_get_theme('global_font_color'),
            'output' => [
                [
                    'choice' => 'primary',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-color-primary',
                ],
                [
                    'choice' => 'secondary',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-color-secondary',
                ],
                [
                    'choice' => 'tertiary',
                    'element' => '.retheme-root',
                    'property' => '--theme-font-color-tertiary',
                ],

                [
                    'choice' => 'primary',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-font-color-primary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'secondary',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-font-color-secondary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'tertiary',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-font-color-tertiary',
                    'context' => ['editor'],
                ],

            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'label' => __('Link Color', 'saudagarwp'),
            'settings' => 'global_color_link',
            'type' => 'multicolor',
            'section' => $section,
            'choices' => [
                'normal' => __('Normal', 'saudagarwp'),
                'hover' => __('Hover', 'saudagarwp'),

            ],
            'default' => rt_get_theme('global_color_link'),
            'output' => [
                [
                    'choice' => 'normal',
                    'element' => '.retheme-root',
                    'property' => '--theme-color-link',
                ],
                [
                    'choice' => 'hover',
                    'element' => '.retheme-root',
                    'property' => '--theme-color-link-active',
                ],
                [
                    'choice' => 'normal',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-color-link',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'hover',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-color-link-active',
                    'context' => ['editor'],
                ],
            ],
            'transport' => 'auto',

        ]);

        $this->add_field([
            'label' => __('Background Scheme', 'saudagarwp'),
            'settings' => 'global_background_scheme',
            'section' => $section,
            'type' => 'multicolor',
            'choices' => [
                'primary' => __('Primary', 'saudagarwp'),
                'secondary' => __('Secondary', 'saudagarwp'),
            ],
            'default' => rt_get_theme('global_background_scheme'),
            'output' => [
                // css var
                [
                    'choice' => 'primary',
                    'element' => '.retheme-root',
                    'property' => '--theme-background-primary',
                ],
                [
                    'choice' => 'secondary',
                    'element' => '.retheme-root',
                    'property' => '--theme-background-secondary',

                ],
                [
                    'choice' => 'primary',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-background-primary',
                    'context' => ['editor'],
                ],
                [
                    'choice' => 'secondary',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-background-secondary',
                    'context' => ['editor'],
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'label' => __('Border Color', 'saudagarwp'),
            'settings' => 'global_border_color',
            'section' => $section,
            'type' => 'color',
            'default' => rt_get_theme('global_border_color'),
            'output' => [
                [
                    'element' => '.retheme-root',
                    'property' => '--theme-border-color',
                ],
            ],
            'transport' => 'auto',
        ]);

        $this->add_field([
            'label' => __('Highlight Color', 'saudagarwp'),
            'settings' => 'global_highlight_color',
            'section' => $section,
            'type' => 'color',
            'default' => rt_get_theme('global_highlight_color'),
            'output' => [
                [
                    'choice' => 'primary',
                    'element' => '.retheme-root',
                    'property' => '--theme-color-highlight',
                ],
                [
                    'choice' => 'primary',
                    'element' => '.editor-styles-wrapper',
                    'property' => '--theme-color-highlight',
                    'context' => ['editor'],
                ],
            ],
            'transport' => 'auto',
        ]);
    }

    public function add_topup() {

        $section = 'general_topup_section';

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'nav_gotop',
            'label' => __('Enable Scroll To Top', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);
    }

    public function add_pagination_number() {

        $section = 'general_pagination_section';

        $this->add_field([
            'type' => 'radio-buttonset',
            'settings' => 'pagination_number_align',
            'label' => __('Pagination Align', 'saudagarwp'),
            'section' => $section,
            'default' => 'center',
            'choices' => [
                'left' => '<i class="fa-solid fa-align-left"></i>',
                'center' => '<i class="fa-solid fa-align-center"></i>',
                'right' => '<i class="fa-solid fa-align-right"></i>',
            ],
            'transport' => 'auto',
            'output' => [
                [
                    'element' => '.rt-pagination',
                    'property' => 'justify-content',
                    'value_pattern' => '$',
                ],
            ],
        ]);
    }

    /**
     * style button for theme button, ninja form, and contact form 7
     * @return void
     */
    public function add_button_primary() {
        $this->add_header([
            'label' => __('Button Primary', 'saudagarwp'),
            'settings' => 'button_primary',
            'section' => 'general_button_section',
        ]);

        $section = 'general_button_section';

        $this->add_field_color([
            'settings' => "button_primary_color",
            'section' => $section,
            'pseudo' => 'hover',
            'default' => [
                'normal' => '#ffffff',
                'hover' => '#ffffff',
            ],
            'output' => [
                [
                    'choice' => 'normal',
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-color',
                ],
                [
                    'choice' => 'hover',
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-color-active',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'multicolor',
            'label' => __('Background', 'saudagarwp'),
            'settings' => 'button_primary_background',
            'section' => $section,
            'choices' => [
                'normal' => 'Normal',
                'hover' => 'Hover',
            ],
            'default' => [
                'normal' => '#141414',
                'hover' => '#353535',
            ],
            'output' => [
                [
                    'choice' => 'normal',
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-background',
                ],
                [
                    'choice' => 'hover',
                    'element' => '.retheme-root',
                    'property' => '--theme-btn-primary-background-active',
                ],
            ],
            'transport' => 'auto',
        ]);
    }

    public function add_button_action() {
        if (rt_is_premium() && class_exists('woocommerce')) {

            $this->add_header([
                'label' => __('Button Action', 'saudagarwp'),
                'settings' => 'button_action',
                'section' => 'general_button_section',
            ]);

            $section = 'general_button_section';

            $this->add_field_color([
                'settings' => "button_action_color",
                'section' => $section,
                'pseudo' => 'hover',
                'default' => [
                    'normal' => '#ffffff',
                    'hover' => '#ffffff',
                ],
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.retheme-root',
                        'property' => '--theme-btn-action-color',
                    ],
                    [
                        'choice' => 'hover',
                        'element' => '.retheme-root',
                        'property' => '--theme-btn-action-color-active',
                    ],
                ],
            ]);

            $this->add_field_color([
                'label' => __('Background', 'saudagarwp'),
                'settings' => 'button_action_background',
                'section' => $section,
                'pseudo' => 'hover',
                'default' => [
                    'normal' => rt_get_theme('global_color_action'),
                    'hover' => '#0ab70c',
                ],
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.retheme-root',
                        'property' => '--theme-btn-action-background',
                    ],
                    [
                        'choice' => 'hover',
                        'element' => '.retheme-root',
                        'property' => '--theme-btn-action-background-active',
                    ],
                ],
            ]);
        }
    }

    public function add_badges() {
        if (rt_is_premium()) {

            $section = 'general_badges_section';

            $this->add_field_color([
                'settings' => "badges_color",
                'section' => $section,
                'default' => '#ffffff',
                'output' => [
                    [
                        'element' => '.retheme-root',
                        'property' => '--theme-badges-color',
                    ],
                ],
            ]);

            $this->add_field_color([
                'label' => __('Background', 'saudagarwp'),
                'settings' => 'badges_background',
                'section' => $section,
                'default' => rt_get_theme('global_brand_color')['primary'],
                'output' => [
                    [
                        'element' => '.retheme-root',
                        'property' => '--theme-badges-background',
                    ],
                ],
            ]);
        }
    }

    // end class
}

new General;
