 <!-- Drawer -->
 <div id="header-tab-drawer" class="rt-builder__tab">

     <div id="header-drawer" class="rt-builder__row">

         <div class="rt-builder__row-title js-builder-control-focus" data-control="header_drawer_menu_style">
             <i class="fas fa-cog"></i> Drawer Menu
         </div>

         <div class="rt-builder__column rt-builder__column--drawer-source rt-builder__column--drawer ">
             <div id="header-drawer-drop" class="sortable-wrapper js-builder-connect js-builder-source">
                 <?php foreach ($drawer_element_exits as $key => $element) : ?>
                     <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                 <?php endforeach; ?>
             </div>
         </div>

         <div class="rt-builder__column rt-builder__column--drawer">
             <div id="drawer_element" class="sortable-wrapper js-builder-connect">
                 <?php foreach ($drawer as $key => $element) : ?>
                     <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                 <?php endforeach; ?>
             </div>
         </div>

     </div>

 </div>