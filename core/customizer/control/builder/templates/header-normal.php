<!-- Normal Header -->
<div id="header-tab-normal" class="rt-builder__tab">
    <!-- topbar -->
    <?php if (rt_is_premium()) : ?>
        <div id="header-topbar" class="rt-builder__row <?php echo !(rt_option('header_topbar')) ? 'disable' : '' ?>">

            <div class="rt-builder__row-title js-builder-control-focus" data-control="header_topbar">
                <i class="fas fa-cog"></i> Topbar Header <?php echo !(rt_option('header_topbar')) ? '<span class="disable"> - Disable </span>' : '' ?>
            </div>

            <div class="rt-builder__column left" data-alignment="<?php esc_attr_e($args['topbar_left_alignment']) ?>" data-display="<?php esc_attr_e($args['topbar_left_display']) ?>">
                <div id="header-topbar-left" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($topbar_left as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php echo esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
            <div class="rt-builder__column center" data-alignment="<?php echo  esc_attr($args['topbar_center_alignment']) ?>" data-display="<?php esc_attr_e($args['topbar_center_display']) ?>">
                <div id="header-topbar-center" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($topbar_center as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
            <div class="rt-builder__column right" data-alignment="<?php echo  esc_attr($args['topbar_right_alignment']) ?>" data-display="<?php esc_attr_e($args['topbar_right_display']) ?>">
                <div id="header-topbar-right" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($topbar_right as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
        </div>
    <?php endif ?>

    <!-- middle -->
    <?php if (rt_is_premium()) : ?>
        <div id="header-middle" class="rt-builder__row  <?php echo !(rt_option('header_middle')) ? 'disable' : '' ?>">

            <div class="rt-builder__row-title js-builder-control-focus" data-control="header_middle">
                <i class="fas fa-cog"></i> Middle Header <?php echo !(rt_option('header_middle')) ? '<span class="disable"> - Disable </span>' : '' ?>
            </div>

            <div class="rt-builder__column left" data-alignment="<?php esc_attr_e($args['middle_left_alignment']) ?>" data-display="<?php esc_attr_e($args['middle_left_display']) ?>">
                <div id="header-middle-left" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($middle_left as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
            <div class="rt-builder__column center" data-alignment="<?php esc_attr_e($args['middle_center_alignment']) ?>" data-display="<?php esc_attr_e($args['middle_center_display']) ?>">
                <div id="header-middle-center" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($middle_center as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
            <div class="rt-builder__column right" data-alignment="<?php esc_attr_e($args['middle_right_alignment']) ?>" data-display="<?php esc_attr_e($args['middle_right_display']) ?>">
                <div id="header-middle-right" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($middle_right as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
        </div>
    <?php endif ?>

    <!-- main -->
    <div id="header-main" class="rt-builder__row">

        <div class="rt-builder__row-title js-builder-control-focus" data-control="control_header_header_main">
            <i class="fas fa-cog"></i> Main Header
        </div>

        <div class="rt-builder__column left" data-alignment="<?php esc_attr_e($args['main_left_alignment']) ?>" data-display="<?php esc_attr_e($args['main_left_display']) ?>">
            <div id="header-main-left" class="sortable-wrapper js-builder-connect">
                <?php foreach ($main_left as $key => $element) : ?>
                    <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                <?php endforeach; ?>
            </div>
            <span class="setting js-builder-column-setting-trigger">
                <i class="fas fa-cog"></i>
            </span>
        </div>
        <div class="rt-builder__column center" data-alignment="<?php esc_attr_e($args['main_center_alignment']) ?>" data-display="<?php esc_attr_e($args['main_center_display']) ?>">
            <div id="header-main-center" class="sortable-wrapper js-builder-connect">
                <?php foreach ($main_center as $key => $element) : ?>
                    <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                <?php endforeach; ?>
            </div>
            <span class="setting js-builder-column-setting-trigger">
                <i class="fas fa-cog"></i>
            </span>
        </div>
        <div class="rt-builder__column right" data-alignment="<?php esc_attr_e($args['main_right_alignment']) ?>" data-display="<?php esc_attr_e($args['main_right_display']) ?>">
            <div id="header-main-right" class="sortable-wrapper js-builder-connect">
                <?php foreach ($main_right as $key => $element) : ?>
                    <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                <?php endforeach; ?>
            </div>
            <span class="setting js-builder-column-setting-trigger">
                <i class="fas fa-cog"></i>
            </span>
        </div>
    </div>

    <!-- element -->
    <div class="rt-builder__row rt-builder__row--source">

        <div class="rt-builder__column">
            <div class="sortable-wrapper js-builder-connect js-builder-source">
                <?php foreach ($header_normal_element_exits as $key => $element) : ?>
                    <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                <?php endforeach; ?>
            </div>

        </div>

    </div>
</div>