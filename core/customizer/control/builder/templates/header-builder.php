<div class="rt-builder js-builder">

    <div class="rt-builder__header">

        <h3 class="rt-builder__title"><i class="fa fa-cubes"></i> Header Builder</h3>

        <div class="rt-builder__display js-builder-preview">

            <a href="#" id="header-builder-close" class="js-header-builder-close">
                <i class="fa fa-times-circle"></i> Close
            </a>
            <a href="#" id="header-builder-open" class="js-header-builder-trigger">
                <i class="fa fa-cubes"></i> Header Builder
            </a>

        </div>

    </div>

    <div id="tabs">

        <ul class="rt-builder__nav">
            <li>
                <a href="#header-tab-normal" class="js-preview-devices" data-device="desktop">Normal</a>
            </li>

            <?php if (rt_is_premium()) : ?>
                <li>
                    <a href="#header-tab-sticky" class="js-preview-devices" data-device="desktop">Sticky Header</a>
                </li>
            <?php endif ?>

            <li>
                <a href="#header-tab-mobile" class="js-preview-devices" data-device="mobile">Mobile Header</a>
            </li>

            <li>
                <a href="#header-tab-drawer" class="js-preview-devices" data-device="mobile">Drawer</a>
            </li>
            
        </ul>

        <div class="rt-builder__body">

            <?php 
            include dirname(__FILE__) . '/header-normal.php';
            include dirname(__FILE__) . '/header-sticky.php'; 
            include dirname(__FILE__) . '/header-mobile.php'; 
            include dirname(__FILE__) . '/header-drawer.php';  
            ?>

        </div>

    </div>

</div>