<!-- Sticky Header -->
<?php if (rt_is_premium()) : ?>
    <div id="header-tab-sticky" class="rt-builder__tab">

        <div id="header-sticky" class="rt-builder__row <?php echo !(rt_option('header_sticky')) ? 'disable' : '' ?>">

            <div class="rt-builder__row-title js-builder-control-focus" data-control="control_header_sticky">
                <i class="fas fa-cog"></i> Sticky Header <?php echo !(rt_option('header_sticky')) ? '<span class="disable"> - Disable </span>' : '' ?>
            </div>

            <div class="rt-builder__column left" data-alignment="<?php esc_attr_e($args['sticky_left_alignment']) ?>" data-display="<?php esc_attr_e($args['sticky_left_display']) ?>">
                <div id="header-sticky-left" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($sticky_left as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
            <div class="rt-builder__column center" data-alignment="<?php esc_attr_e($args['sticky_center_alignment']) ?>" data-display="<?php esc_attr_e($args['sticky_center_display']) ?>">
                <div id="header-sticky-center" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($sticky_center as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
            <div class="rt-builder__column right" data-alignment="<?php esc_attr_e($args['sticky_right_alignment']) ?>" data-display="<?php esc_attr_e($args['sticky_right_display']) ?>">
                <div id="header-sticky-right" class="sortable-wrapper js-builder-connect">
                    <?php foreach ($sticky_right as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>
                <span class="setting js-builder-column-setting-trigger">
                    <i class="fas fa-cog"></i>
                </span>
            </div>
        </div>

        <!-- element -->
        <div class="rt-builder__row rt-builder__row--source">

            <div class="rt-builder__column">
                <div class="sortable-wrapper js-builder-connect js-builder-source">
                    <?php foreach ($header_sticky_element_exits as $key => $element) : ?>
                        <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                    <?php endforeach; ?>
                </div>

            </div>

        </div>

    </div>
<?php endif ?>