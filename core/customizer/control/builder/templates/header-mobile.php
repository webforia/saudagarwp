 <!-- Mobile Header -->
 <div id="header-tab-mobile" class="rt-builder__tab">

     <div id="header-mobile" class="rt-builder__row">

         <div class="rt-builder__row-title js-builder-control-focus" data-control="control_header_header_mobile_menu">
             <i class="fas fa-cog"></i> Mobile Header
         </div>

         <div class="rt-builder__column left" data-alignment="<?php esc_attr_e($args['mobile_left_alignment']) ?>" data-display="<?php esc_attr_e($args['mobile_left_display']) ?>">
             <div id="header-mobile-left" class="sortable-wrapper js-builder-connect">
                 <?php foreach ($mobile_left as $key => $element) : ?>
                     <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                 <?php endforeach; ?>
             </div>
             <span class="setting js-builder-column-setting-trigger">
                 <i class="fas fa-cog"></i>
             </span>
         </div>

         <div class="rt-builder__column center" data-alignment="<?php esc_attr_e($args['mobile_center_alignment']) ?>" data-display="<?php esc_attr_e($args['mobile_center_display']) ?>">
             <div id="header-mobile-center" class="sortable-wrapper js-builder-connect">
                 <?php foreach ($mobile_center as $key => $element) : ?>
                     <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                 <?php endforeach; ?>
             </div>
             <span class="setting js-builder-column-setting-trigger">
                 <i class="fas fa-cog"></i>
             </span>
         </div>

         <div class="rt-builder__column right" data-alignment="<?php esc_attr_e($args['mobile_right_alignment']) ?>" data-display="<?php esc_attr_e($args['mobile_right_display']) ?>">
             <div id="header-mobile-right" class="sortable-wrapper js-builder-connect">
                 <?php foreach ($mobile_right as $key => $element) : ?>
                     <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                 <?php endforeach; ?>
             </div>
             <span class="setting js-builder-column-setting-trigger">
                 <i class="fas fa-cog"></i>
             </span>
         </div>
         
     </div>

     <div class="rt-builder__row rt-builder__row--source">

         <div class="rt-builder__column">
             <div class="sortable-wrapper js-builder-connect js-builder-source">
                 <?php foreach ($header_mobile_element_exits as $key => $element) : ?>
                     <div id="<?php esc_attr_e($key) ?>" class="rt-builder-element <?php esc_attr_e($key) ?>"><?php esc_html_e($element) ?> <i class="fas fa-cog"></i><i class="fa fa-times p-close js-builder-element-close"></i></div>
                 <?php endforeach; ?>
             </div>

         </div>

     </div>

 </div>