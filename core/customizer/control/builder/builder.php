<?php

/*=================================================
 * KIRKI CUSTOM CONTROL
/*================================================= */
add_action('customize_register', function ($wp_customize) {
    /**
     * The custom control class
     */
    class Retheme_Header_Control extends Kirki_Control_Base {
        /**
         * @access public
         * @var string
         */
        public $type = 'builder';
        /**
         * @access public
         * @var array
         */
        public $builder;

        public function enqueue() {
            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-sortable');
            wp_enqueue_script('jquery-ui-draggable');
            wp_enqueue_script('jquery-ui-tabs');

            wp_enqueue_style('retheme-style-builder', get_template_directory_uri() . '/core/customizer/control/builder/assets/builder.css', false, '1.5.0', false);
            wp_enqueue_script('retheme-scripts-builder', get_template_directory_uri() . '/core/customizer/control/builder/assets/builder.js', array('jquery'), '1.4.3', true);
        }

        public function to_json() {
            parent::to_json();
            $this->json['builder'] = $this->builder;
        }

        /**
         * Array value to key
         *
         * @param string $choices
         * @param array $elements
         * @return void
         */
        public function filter_values($choices = '', $elements = array()) {
            $filter_values = array();

            if (($elements)) {
                foreach ($elements as $key => $element) {
                    if (($this->choices[$choices][$element])) {
                        $filter_values[$element] = $this->choices[$choices][$element];
                    }
                }
            }

            return $filter_values;
        }

        public function render_content() {
            $values = $this->value();
            $defaults = $this->setting->default;
            $choices = $this->choices;
            $args = wp_parse_args($values, $defaults);

            /* Topbar */
            $topbar_left = $this->filter_values('normal_elements', $args['topbar_left_element']);
            $topbar_center = $this->filter_values('normal_elements', $args['topbar_center_element']);
            $topbar_right = $this->filter_values('normal_elements', $args['topbar_right_element']);
            $topbar = array_merge($topbar_left, $topbar_center, $topbar_right);

            /** Middle */
            $middle_left = $this->filter_values('normal_elements', $args['middle_left_element']);
            $middle_center = $this->filter_values('normal_elements', $args['middle_center_element']);
            $middle_right = $this->filter_values('normal_elements', $args['middle_right_element']);
            $middle = array_merge($middle_left, $middle_center, $middle_right);

            /** Main */
            $main_left = $this->filter_values('normal_elements', $args['main_left_element']);
            $main_center = $this->filter_values('normal_elements', $args['main_center_element']);
            $main_right = $this->filter_values('normal_elements', $args['main_right_element']);
            $main = array_merge($main_left, $main_center, $main_right);

            /** Merge all Element in normal header */
            $header_normal_element_exits = array_diff($choices['normal_elements'], array_merge($topbar, $middle, $main));

            /** Sticky */
            $sticky_left = $this->filter_values('sticky_elements', $args['sticky_left_element']);
            $sticky_center = $this->filter_values('sticky_elements', $args['sticky_center_element']);
            $sticky_right = $this->filter_values('sticky_elements', $args['sticky_right_element']);

            $header_sticky_element_exits = array_diff($choices['sticky_elements'], array_merge($sticky_left, $sticky_center, $sticky_right));

            /** Mobile */
            $mobile_left = $this->filter_values('mobile_elements', $args['mobile_left_element']);
            $mobile_center = $this->filter_values('mobile_elements', $args['mobile_center_element']);
            $mobile_right = $this->filter_values('mobile_elements', $args['mobile_right_element']);
            $header_mobile_element_exits = array_diff($choices['mobile_elements'], array_merge($mobile_left, $mobile_center, $mobile_right));

            /** Drawer */
            $drawer = $this->filter_values('drawer_elements', $args['drawer_element']);
            $drawer_element_exits = array_diff($choices['drawer_elements'], $drawer);

            // Header builder template
            include dirname(__FILE__) . '/templates/header-builder.php';

        }
    }

    // Register our custom control with Kirki
    add_filter('kirki_control_types', function ($controls) {
        $controls['builder'] = 'Retheme_Header_Control';
        return $controls;
    });
});
