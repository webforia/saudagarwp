<?php

/**
 * @author : Reret
 */

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Page extends Customizer_Base
{

    public function __construct()
    {

        $this->set_section();
        $this->add_page_header_options();
        $this->add_page_layout();
    }

    public function set_section()
    {
        $this->add_section('', [
            'page' => [__('Page', 'saudagarwp')],
        ]);
    }

    public function add_page_header_options()
    {
        $section = 'page_section';

        $this->add_header([
            'label' => __('Options', 'saudagarwp'),
            'settings' => 'page_header',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'page_header',
            'label' => __('Enable Page Header', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'page_header_layout',
            'label' => __('Layout', 'saudagarwp'),
            'section' => $section,
            'default' => 'in-content',
            'choices' => [
                'above' => get_template_directory_uri() . '/core/customizer/assets/img/page-title-above.png',
                'in-content' => get_template_directory_uri() . '/core/customizer/assets/img/page-title-content.png',
            ],
        ]);

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'radio-buttonset',
                'settings' => 'page_header_layout_align',
                'label' => __('Page Title Align', 'saudagarwp'),
                'section' => $section,
                'default' => 'center',
                'choices' => [
                    'left' => '<i class="fa-solid fa-align-left"></i>',
                    'center' => '<i class="fa-solid fa-align-center"></i>',
                    'right' => '<i class="fa-solid fa-align-right"></i>',
                ],
                'transport' => 'auto',
                'output' => [
                    [
                        'element' => '.page-header',
                        'property' => 'text-align',
                        'value_pattern' => '$',
                    ],
                    [
                        'choice' => 'center',
                        'element' => '.page-header .rt-breadcrumbs',
                        'property' => 'display',
                        'value_pattern' => 'flex',
                    ],
                    [
                        'choice' => 'center',
                        'element' => '.page-header .rt-breadcrumbs',
                        'property' => 'justify-content',
                        'value_pattern' => '$',
                    ],
                ],
            ]);
        }

        $this->add_field([
            'type' => 'color',
            'label' => __('Title Color ', 'saudagarwp'),
            'settings' => 'page_header_heading_color',
            'section' => $section,
            'default' => $this->get_default_mod('global_font_color', 'primary'),
            'output' => [
                [
                    'choice' => 'normal',
                    'element' => '.page-header--above',
                    'property' => '--theme-font-color-primary',
                ],
            ],
            'transport' => 'auto',
            'active_callback' => [
                [
                    'setting' => 'page_header_layout',
                    'operator' => '==',
                    'value' => 'above',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'color',
            'label' => __('Content Color ', 'saudagarwp'),
            'settings' => 'page_header_content_color',
            'section' => $section,
            'default' => $this->get_default_mod('global_font_color', 'secondary'),
            'output' => [
                [
                    'choice' => 'normal',
                    'element' => '.page-header--above',
                    'property' => '--theme-font-color-secondary',
                ],
            ],
            'transport' => 'auto',
            'active_callback' => [
                [
                    'setting' => 'page_header_layout',
                    'operator' => '==',
                    'value' => 'above',
                ],
            ],
        ]);

        $this->add_field([
            'label' => __('Link Color', 'saudagarwp'),
            'settings' => 'page_header_link_color',
            'type' => 'multicolor',
            'section' => $section,
            'choices' => [
                'normal' => __('Normal', 'saudagarwp'),
                'hover' => __('Hover', 'saudagarwp'),
            ],
            'default' => [
                'normal' => $this->get_default_mod('global_font_color', 'tertiary'),
                'hover' => $this->get_default_mod('global_color_link', 'hover'),
            ],
            'output' => [
                [
                    'choice' => 'normal',
                    'element' => '.page-header--above',
                    'property' => '--theme-font-color-tertiary',
                ],

                [
                    'choice' => 'hover',
                    'element' => '.page-header--above',
                    'property' => '--theme-color-link-active',
                ],

            ],
            'transport' => 'auto',
            'active_callback' => [
                [
                    'setting' => 'page_header_layout',
                    'operator' => '==',
                    'value' => 'above',
                ],
            ],
        ]);

        $this->add_field([
            'type' => 'background',
            'settings' => 'page_header_background',
            'section' => $section,
            'label' => __('Background', 'saudagarwp'),
            'default' => [
                'background-color' => $this->get_default_mod('global_background_scheme', 'secondary'),
                'background-image' => '',
                'background-repeat' => 'repeat',
                'background-position' => 'center center',
                'background-size' => 'cover',
                'background-attachment' => 'scroll',
            ],
            'output' => [
                [
                    'element' => '.page-header--above',
                ],
            ],
            'transport' => 'auto',
            'active_callback' => [
                [
                    'setting' => 'page_header_layout',
                    'operator' => '==',
                    'value' => 'above',
                ],
            ],
        ]);

        $this->add_field_border_color([
            'settings' => 'page_header_border_color',
            'section' => $section,
            'default' => $this->get_default_mod('global_border_color'),
            'element' => '.page-header--above',
            'active_callback' => [
                [
                    'setting' => 'page_header_layout',
                    'operator' => '==',
                    'value' => 'above',
                ],
            ],
        ]);

        $this->add_field_responsive([
            'type' => 'dimensions',
            'label' => __('Spacing', 'saudagarwp'),
            'settings' => 'page_header_padding',
            'section' => $section,
            'description' => __('Use CSS Unit px or %', 'saudagarwp'),
            'default' => [
                'top' => '',
                'bottom' => '',
            ],
            'output' => [
                [
                    'element' => '.page-header',
                    'property' => 'padding',
                ],
            ],
            'transport' => 'auto',
            'active_callback' => [
                [
                    'setting' => 'page_header_layout',
                    'operator' => '==',
                    'value' => 'above',
                ],
            ],
        ]);
    }

    public function add_page_layout()
    {
        $section = 'page_section';

        $this->add_header([
            'label' => __('Page Layout', 'saudagarwp'),
            'settings' => 'page_layout',
            'section' => $section,
        ]);

        $this->add_field([
            'type' => 'radio-image',
            'settings' => 'page_layout',
            'label' => __('Layout', 'saudagarwp'),
            'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a sidebar.', 'saudagarwp'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
            'section' => $section,
            'default' => 'sidebar-right',
            'choices' => [
                'normal' => get_template_directory_uri() . '/core/customizer/assets/img/layout-normal.png',
                'compact' => get_template_directory_uri() . '/core/customizer/assets/img/layout-compact.png',
                'sidebar-left' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-left.png',
                'sidebar-right' => get_template_directory_uri() . '/core/customizer/assets/img/layout-sidebar-right.png',
            ],
        ]);
    }

    // end class
}

new Page;
