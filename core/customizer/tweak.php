<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Tweak extends Customizer_Base
{

    public function __construct()
    {   
        $this->set_panel();
        $this->tweak();
    }

    public function set_panel()
    {
        $this->add_section('', [
            'tweak' => [__('Tweaks ', 'saudagarwp')],
        ]);
    }

    public function tweak()
    {
        $section = 'tweak_section';

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'move_jquery_to_footer',
            'label' => __('Move jQuery to Footer', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'remove_wp_emoji',
            'label' => __('Remove Emoji', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'tweak_responsive_embed',
            'label' => __('Responsive Embed', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'remove_block_style',
            'label' => __('Remove Block Style', 'saudagarwp'),
            'section' => $section,
            'default' => true,
        ]);

        if (rt_is_woocommerce()) {
            $this->add_field([
                'type' => 'toggle',
                'settings' => 'remove_wc_block_style',
                'label' => __('Remove WooCommerce Block Style', 'saudagarwp'),
                'section' => $section,
                'default' => true,
            ]);
        }
    }

}

new Tweak;
