<?php

/**
 * @author : Reret
 */

namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Footer extends Customizer_Base {

    public function __construct() {
        $this->set_panel();
        $this->set_section();
        $this->add_option_widget();
        $this->add_option_bottom();
    }

    public function set_panel() {
        $this->add_panel('footer_panel', [
            'title' => __('Footer', 'saudagarwp'),
        ]);
    }

    public function set_section() {
        $this->add_section('footer_panel', [
            'footer_widget' => [__('Widget Footer', 'saudagarwp')],
            'footer_bottom' => [__('Bottom Footer', 'saudagarwp')],
        ]);
    }

    public function add_option_widget() {

        $section = 'footer_widget_section';

        $this->add_field([
            'type' => 'toggle',
            'settings' => 'footer_widget',
            'label' => __('Enable Footer Widgets', 'saudagarwp'),
            'section' => $section,
            'default' => false,
        ]);

        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'radio-image',
                'section' => $section,
                'settings' => 'footer_widget_layout',
                'label' => __('Layout', 'saudagarwp'),
                'description' => sprintf(__('Visit the <a href="%s">Widgets panel</a> and add widget to display a element in a footer.', 'saudagarwp'), "javascript:wp.customize.panel( 'widgets' ).focus();"),
                'default' => 'footer-1',
                'choices' => [
                    'footer-1' => get_template_directory_uri() . '/core/customizer/assets/img/footer-1.png',
                    'footer-2' => get_template_directory_uri() . '/core/customizer/assets/img/footer-2.png',
                    'footer-3' => get_template_directory_uri() . '/core/customizer/assets/img/footer-6.png',
                    'footer-4' => get_template_directory_uri() . '/core/customizer/assets/img/footer-7.png',
                    'footer-5' => get_template_directory_uri() . '/core/customizer/assets/img/footer-3.png',
                ],
            ]);

            $this->add_field_responsive([
                'type' => 'typography',
                'settings' => 'footer_widget_heading_typography',
                'label' => __('Heading Typography', 'saudagarwp'),
                'section' => $section,
                'default' => [
                    'margin-bottom' => '',
                    'font-size' => '',
                    'line-height' => '',
                    'text-transform' => 'none',
                ],
                'output' => [
                    [
                        'element' => '.rt-widget--footer .rt-widget__title',
                        'suffix' => '!important',
                    ],
                ],
                'transport' => 'auto',
            ]);

            $this->add_field_color([
                'settings' => 'footer_widget_heading_color',
                'label' => __('Heading Color', 'saudagarwp'),
                'section' => $section,
                'default' => rt_get_theme('global_font_light_color')['primary'],
                'element' => '.rt-widget--footer .rt-widget__title',
                'output' => [
                    [
                        'element' => '.page-footer__widget',
                        'property' => '--theme-font-color-primary',
                    ],
                ],

            ]);

            $this->add_field_color([
                'settings' => 'footer_widget_background',
                'label' => __('Background Color', 'saudagarwp'),
                'default' => rt_get_theme('global_background_dark'),
                'section' => $section,
                'output' => [
                    [
                        'element' => '.page-footer__widget',
                        'property' => '--theme-background-primary',
                    ],
                    [
                        'element' => '.page-footer__widget',
                        'property' => '--theme-border-color',
                    ],
                ],
            ]);

            $this->add_field_color([
                'settings' => 'footer_widget_color',
                'label' => __('Content Color', 'saudagarwp'),
                'section' => $section,
                'pseudo' => 'hover',
                'default' => [
                    'normal' => rt_get_theme('global_font_light_color')['secondary'],
                    'hover' => '#ffffff',
                ],
                'output' => [
                    [
                        'choice' => 'normal',
                        'element' => '.page-footer__widget',
                        'property' => '--theme-font-color-secondary',
                    ],
                    [
                        'choice' => 'normal',
                        'element' => '.page-footer__widget',
                        'property' => '--theme-color-link',
                    ],
                    [
                        'choice' => 'hover',
                        'element' => '.page-footer__widget',
                        'property' => '--theme-color-link-active',
                    ],
                ],

            ]);
        } else {
            $this->add_field([
                'type' => 'radio-image',
                'section' => $section,
                'settings' => 'footer_widget_layout',
                'label' => __('Layout', 'saudagarwp'),
                'default' => 'footer-1',
                'choices' => [
                    'footer-1' => get_template_directory_uri() . '/core/customizer/assets/img/footer-1.png',
                    'footer-2' => get_template_directory_uri() . '/core/customizer/assets/img/footer-2.png',
                ],
            ]);
        }
    }

    public function add_option_bottom() {
        $section = 'footer_bottom_section';
        if (rt_is_premium()) {
            $this->add_field([
                'type' => 'toggle',
                'settings' => 'footer_bottom',
                'label' => __('Enable Footer Bottom', 'saudagarwp'),
                'section' => $section,
                'default' => true,
            ]);

            $this->add_field([
                'type' => 'sortable',
                'settings' => 'footer_bottom_element',
                'label' => __('Element', 'saudagarwp'),
                'section' => $section,
                'default' => [
                    'html-1',
                ],
                'choices' => [
                    'social' => __('Social Media', 'saudagarwp'),
                    'spacer-1' => __('Spacer 1', 'saudagarwp'),
                    'spacer-2' => __('Spacer 2', 'saudagarwp'),
                    'html-1' => __('Copyright', 'saudagarwp'),
                    'image-1' => __('Image', 'saudagarwp'),
                ],

            ]);

            $this->add_field([
                'type' => 'radio-buttonset',
                'settings' => 'footer_bottom_position',
                'label' => __('Position', 'saudagarwp'),
                'section' => $section,
                'default' => 'space-between',
                'choices' => [
                    'left' => '<i class="fa-solid fa-align-left"></i>',
                    'center' => '<i class="fa-solid fa-align-center"></i>',
                    'right' => '<i class="fa-solid fa-align-right"></i>',
                    'space-between' => '<i class="fa-solid fa-align-justify"></i>',
                ],
                'transport' => 'auto',
                'default' => 'left',
                'output' => [
                    [
                        'element' => '.page-footer__bottom .page-container',
                        'property' => 'justify-content',
                    ],
                ],

            ]);

            $this->add_field([
                'label' => __('Copyright (Support HTML tag)', 'saudagarwp'),
                'settings' => "footer_bottom_text",
                'description' => __('You can use text map {{site_name}} {{year}}', 'saudagarwp'),
                'section' => $section,
                "default" => "@Copyright {{site_name}}. All Rights Reserved",
                'type' => 'textarea',
            ]);

            $this->add_field([
                'type' => 'image',
                'settings' => "footer_bottom_img_1",
                'section' => $section,
                'label' => __('Image', 'saudagarwp'),
                'description' => __('You can upload image like payment logo, secure logo etc', 'saudagarwp'),
            ]);

            $this->add_field_color([
                'settings' => 'footer_option_bottom_color',
                'section' => $section,
                'default' => rt_get_theme('global_font_light_color')['secondary'],
                'output' => [
                    [
                        'choose' => 'normal',
                        'element' => '.page-footer__bottom',
                        'property' => '--theme-font-color-secondary',
                    ],
                    [
                        'choose' => 'normal',
                        'element' => '.page-footer__bottom',
                        'property' => '--theme-color-link',
                    ],
                    [
                        'choose' => 'hover',
                        'element' => '.page-footer__bottom',
                        'property' => '--theme-color-link-active',
                    ],
                ],
            ]);

            $this->add_field_background([
                'settings' => 'footer_option_bottom_background',
                'default' => rt_get_theme('global_background_dark'),
                'section' => $section,
                'output' => [
                    [
                        'element' => '.page-footer__bottom',
                        'property' => '--theme-background-primary',
                    ],
                ],
            ]);

            $this->add_field_border_color([
                'settings' => 'footer_option_bottom_border_color',
                'default' => 'rgba(255,255,255,0.06)',
                'section' => $section,
                'output' => [
                    [
                        'element' => '.page-footer__bottom',
                        'property' => '--theme-border-color',
                    ],
                ],
            ]);
        }
    }
    // end class
}

new Footer;
