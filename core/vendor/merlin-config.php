<?php
if (!class_exists('Merlin')) {
    return;
}
/**
 * Set directory locations, text strings, and settings.
 */
class Retheme_Merlin extends Merlin {
    /**
     * Activate the EDD license.
     *
     * This code was taken from the EDD licensing addon theme example code
     * (`activate_license` method of the `EDD_Theme_Updater_Admin` class).
     *
     * @param string $license The license key.
     *
     * @return array
     */
    protected function edd_activate_license($key) {
        $success = false;

        // Strings passed in from the config file.
        $strings = $this->strings;

        // Theme Name.
        $theme = ucfirst($this->theme?? '');

        // Remove "Child" from the current theme name, if it's installed.
        $theme = str_replace(' Child', '', $theme);

        // Text strings.
        $success_message = $strings['license-json-success%s'];

        // API Manager
        $license = new \Retheme\Activation(array(
            'product_extends' => array(
                'webforia-shop-booster',
                'webforia-whatsapp-chat',
                'webforia-wa-chat',
                'webforia-user-price',
                'webforia-checkout-field',
                'astro-element',
            ),
            'product_key' => $key,
        ));

        $status = $license->activate();
        $message = sprintf(esc_html($status['message']), $theme);

        if ($status['result'] == 'success') {
            $success = true;
        } else {
            $success = false;
        }

        return compact('success', 'message');
    }
}

// set config theme wizard
new Retheme_Merlin(
    $config = array(
        'directory' => 'core/vendor/merlin', // Location / directory where Merlin WP is placed in your theme.
        'merlin_url' => 'merlin', // The wp-admin page slug where Merlin WP loads.
        'parent_slug' => 'themes.php', // The wp-admin parent page slug for the admin menu item.
        'capability' => 'manage_options', // The capability required for this menu to be displayed to the user.
        'child_action_btn_url' => 'https://codex.wordpress.org/child_themes', // URL for the 'child-action-link'.
        'dev_mode' => true, // Enable development mode for testing.
        'license_step' => true, // EDD license activation step.
        'license_required' => false, // Require the license activation step.
        'license_help_url' => '', // URL for the 'license-tooltip'.
        'edd_remote_api_url' => '', // EDD_Theme_Updater_Admin remote_api_url.
        'edd_item_name' => RT_THEME_NAME, // EDD_Theme_Updater_Admin item_name.
        'edd_theme_slug' => RT_THEME_SLUG, // EDD_Theme_Updater_Admin item_slug.
        'ready_big_button_url' => site_url(), // Link for the big button on the ready step.
    ),

    $strings = array(
        'admin-menu' => esc_html__('Theme Setup', 'saudagarwp'),
        /* translators: 1: Title Tag 2: Theme Name 3: Closing Title Tag */
        'title%s%s%s%s' => esc_html__('%1$s%2$s Themes &lsaquo; Theme Setup: %3$s%4$s', 'saudagarwp'),
        'return-to-dashboard' => esc_html__('Kembali kehalaman admin', 'saudagarwp'),
        'ignore' => esc_html__('Matikan fitur wizard', 'saudagarwp'),
        'btn-skip' => esc_html__('Lewati', 'saudagarwp'),
        'btn-next' => esc_html__('Selanjutnya', 'saudagarwp'),
        'btn-start' => esc_html__('Mulai', 'saudagarwp'),
        'btn-no' => esc_html__('Batal', 'saudagarwp'),
        'btn-plugins-install' => esc_html__('Instal', 'saudagarwp'),
        'btn-child-install' => esc_html__('Instal', 'saudagarwp'),
        'btn-content-install' => esc_html__('Instal', 'saudagarwp'),
        'btn-import' => esc_html__('Impor', 'saudagarwp'),
        'btn-license-activate' => esc_html__('Aktifkan', 'saudagarwp'),
        'btn-license-skip' => esc_html__('Nanti', 'saudagarwp'),
        /* translators: Theme Name */
        'license-header%s' => esc_html__('Aktifkan %s', 'saudagarwp'),
        /* translators: Theme Name */
        'license-header-success%s' => esc_html__('%s sudah diaktifkan', 'saudagarwp'),
        /* translators: Theme Name */
        'license%s' => esc_html__('Masukkan kunci lisensi Anda untuk mendapatkan akses penuh ke semua fitur premium, pembaruan, dan dukungan dari Webforia Studio.', 'saudagarwp'),
        'license-label' => esc_html__('Kunci lisensi', 'saudagarwp'),
        'license-success%s' => esc_html__('Tema ini sudah terdaftar. Silakan lanjut ke langkah berikutnya.', 'saudagarwp'),
        'license-json-success%s' => esc_html__('Tema Anda telah diaktifkan! Kini, Anda dapat menikmati fitur premium, pembaruan, dan dukungan penuh.', 'saudagarwp'),
        'license-tooltip' => esc_html__('Butuh bantuan?', 'saudagarwp'),
        /* translators: Theme Name */
        'welcome-header%s' => esc_html__('Selamat datang di %s', 'saudagarwp'),
        'welcome-header-success%s' => esc_html__('Hi. Selamat datang kembali', 'saudagarwp'),
        'welcome%s' => esc_html__('Fitur Theme Setup ini akan membantu Anda melakukan pengaturan tema, pemasangan plugin, serta mengimpor konten dengan lebih mudah dan cepat.', 'saudagarwp'),
        'welcome-success%s' => esc_html__('Anda mungkin sudah menjalankan fitur Wizard ini sebelumnnya, Jika Anda ingin melanjutkannya silakan klik tombol "Mulai" di bawah ini', 'saudagarwp'),
        'child-header' => esc_html__('Install tema anak', 'saudagarwp'),
        'child-header-success' => esc_html__('You\'re good to go!', 'saudagarwp'),
        'child' => esc_html__('Aktifkan tema anak sehingga Anda dapat dengan mudah melakukan perubahan tanpa merusak tema utama.', 'saudagarwp'),
        'child-success%s' => esc_html__('Tema anak Anda berhasil di install dan diaktifkan', 'saudagarwp'),
        'child-action-link' => esc_html__('Pelajari lebih lanjut tentang tema anak', 'saudagarwp'),
        'child-json-success%s' => esc_html__('Tema anak Anda sudah diinstall dan diaktifkan', 'saudagarwp'),
        'child-json-already%s' => esc_html__('Tema anak Anda sudah berhasil dibuat dan sekarang sudah diaktifkan', 'saudagarwp'),
        'plugins-header' => esc_html__('Install Plugins', 'saudagarwp'),
        'plugins-header-success' => esc_html__('Mengagumkan', 'saudagarwp'),
        'plugins' => esc_html__('Ayo kita instal beberapa plugin WordPress yang diperlukan untuk membangun situs Anda.', 'saudagarwp'),
        'plugins-success%s' => esc_html__('Semua plugin WordPress yang diperlukan sudah terpasang dan diperbarui. Klik "Selanjutnya" untuk melanjutkan ke pengaturan.', 'saudagarwp'),
        'plugins-action-link' => esc_html__('Tingkat lanjut', 'saudagarwp'),
        'import-header' => esc_html__('Impor Konten', 'saudagarwp'),
        'import' => esc_html__('Pilih desain template yang ingin Anda gunakan', 'saudagarwp'),
        'import-action-link' => esc_html__('Tingkat lanjut', 'saudagarwp'),
        'ready-header' => esc_html__('Proses installasi selesai. Selamat bersenang-senang!', 'saudagarwp'),
        /* translators: Theme Author */
        'ready%s' => esc_html__('Tema Anda telah berhasil diatur. Nikmati tampilan baru yang dirancang khusus oleh %s.', 'saudagarwp'),
        'ready-action-link' => esc_html__('Link penting', 'saudagarwp'),
        'ready-big-button' => esc_html__('Kunjungi website', 'saudagarwp'),
        'ready-link-1' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', 'https://panduan.saudagarwp.com/tutorial/get-starter/', esc_html__('Tutorial Saudagar WP', 'saudagarwp')),
        'ready-link-2' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', admin_url('customize.php'), esc_html__('Mulai melakukan kustomisasi', 'saudagarwp')),
        'ready-link-3' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', 'https://webforia.id/support', esc_html__('Hubungi tim teknis', 'saudagarwp')),

    )
);

// check license
function merlin_check_license() {
    return (rt_is_premium()) ? true : false;
}
add_filter('merlin_is_theme_registered', 'merlin_check_license');
add_filter('wp_ajax_merlin_child_theme', '__return_false'); // disable generate child theme
