<?php
/*=================================================;
/* CART - SET AJAX WOOCOMMERCE SETTINGS
/*=================================================
 * Set new value ajax add to cart on WooCommerce Setting
 * Follow cart behaviour from Checkout setting form panel customizer
 */
function rt_cart_setting_behaviour()
{
    $cart = rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel');

    if ($cart == 'ajax_addtocart_panel') {
        update_option('woocommerce_enable_ajax_add_to_cart', 'yes');
    }
    
    if($cart == 'redirect_checkout'){
        update_option('woocommerce_enable_ajax_add_to_cart', '');
    }

}
add_action('init', 'rt_cart_setting_behaviour');

/*=================================================;
/* CART - ADDED NEW BODY CLASS
/*================================================= 
* Add new body class if ajax add to cart enable
* The class use js if add to cart on single product clicked
* The class added if ajax add to cart option enable
*/
function rt_cart_assign_custom_class($classes)
{
    $cart = rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel');
    $redirect_cart_page = (get_option('woocommerce_cart_redirect_after_add') == 'yes')? true : false;
    
    if ($cart == 'ajax_addtocart_panel' && $redirect_cart_page == false) {
        $classes[] = 'js-ajax-add-to-cart';
    }

    return $classes;
}
add_filter('body_class', 'rt_cart_assign_custom_class');

/*=================================================;
/* CART - AJAX UPDATE
/*=================================================
 * Set the quantity for an item in the cart using it's key.
 * Return result new cart content
 */
function rt_cart_update()
{

    if (!isset($_POST['retheme_minicart_nonce_field']) || !wp_verify_nonce($_POST['retheme_minicart_nonce_field'], 'retheme_minicart_nonce')) {

        $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
        $product = wc_get_product($product_id);
        $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount(wp_unslash($_POST['quantity']));
        $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
        $product_status = get_post_status($product_id);
        $cart_item_key = sanitize_key($_POST['cart_item_key']);

        $error_message = false;

        if ($quantity < 1 || !$product->is_in_stock()) {
            wc_add_notice(sprintf(__('Sorry, "%s" is not in stock. Please edit your cart and try again. We apologize for any inconvenience caused.', 'woocommerce'), $product->get_name()), 'error');
            $error_message = true;
        }

        // Check stock product
        if (!$product->has_enough_stock($quantity)) {
            wc_add_notice(sprintf(__('You cannot add that amount of &quot;%s&quot; to the cart because there is not enough stock (%2$s remaining).', 'woocommerce'), $product->get_name(), wc_format_stock_quantity_for_display($product->get_stock_quantity(), $product)), 'error');
            $error_message = true;
        }

        if ($error_message === false && $product_status === 'publish') {
            WC()->cart->set_quantity($cart_item_key, $quantity);
            wc_add_notice(sprintf(__('&quot;%s&quot; has been updated to your cart.', 'saudagarwp'), $product->get_name()));
        }

        $minicart = new WC_AJAX;
        $minicart::get_refreshed_fragments();

    }

}
add_action('wc_ajax_update_cart', 'rt_cart_update');

/*=================================================;
/* MOVE CROSS SELLING
/*================================================= */
remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
add_action('woocommerce_after_cart_table', 'woocommerce_cross_sell_display');

/*=================================================
 * CART - SLIDE PANEL
/*=================================================
 * Added slide mini cart on global footer
 * @hook rt_footer
 */
function rt_cart_side_panel()
{
    if (rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') !== 'catalog') {
        rt_get_template_part('shop/panel-cart');
    }
}
add_action('rt_footer', 'rt_cart_side_panel');

/*=================================================
 *  DISABLE ADD TO CART
/*================================================= */
/**
 * Remove add to cart button on product
 * Make WooCommerce show product without ecommerce system
 *
 * @hooked filter woocommerce_is_purchasable
 */
function rt_cart_catalog_action()
{
    if (rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') == 'catalog') {
        add_filter('woocommerce_is_purchasable', '__return_false');
    }
}
add_action('init', 'rt_cart_catalog_action');

/*=================================================;
/* CART - REDIRECT TO CHECKOUT
/*================================================= */
/**
 * Skip mini cart after click add to cart
 * Redirect checkout page
 * Use option add to cart behavior redirect to checkout page
 *
 * @hooked filter woocommerce_add_to_cart_redirect
 */

function rt_cart_redirect_checkout($url)
{
    if (rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') == 'redirect_checkout') {
        return wc_get_checkout_url();
    }
}
add_filter('woocommerce_add_to_cart_redirect', 'rt_cart_redirect_checkout');

/*=================================================;
/* CART - HANDLE PRODUCT READY ON CART
/*================================================= */
/**
 * remove error message if product already to cart
 * use option add to cart behavior redirect to checkout page
 *
 * @hooked filter woocommerce_product_add_to_cart_url
 */
function rt_cart_individual_products($add_to_cart_url, $product)
{

    if (function_exists('find_product_in_cart') && $product->get_sold_individually() // if individual product
         && WC()->cart->find_product_in_cart(WC()->cart->generate_cart_id($product->id)) // if in the cart
         && $product->is_purchasable() // we also need these two conditions
         && $product->is_in_stock()
        && rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') == 'redirect_checkout') {
        $add_to_cart_url = wc_get_checkout_url();
    }

    return $add_to_cart_url;

}
add_filter('woocommerce_product_add_to_cart_url', 'rt_cart_individual_products', 10, 2);

/*=================================================;
/* CART - REMOVE SUCCESS MESSAGE
/*=================================================
/**
 * remove message after success add to cart
 * use option add to cart behavior redirect to checkout page
 * @param [type] $message
 * @return HTML
 */
function rt_cart_remove_message($message)
{
    $add_to_cart = rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel');
    
    // Remove message
    if ($add_to_cart == 'redirect_checkout') {
        $message = '';
    }

    // Replace cart url to checkout
    if ($add_to_cart == 'keep_on_page') {
        $message = str_replace(get_permalink(wc_get_page_id('cart')), get_permalink(wc_get_page_id('checkout')), $message);
    }
    return $message;
}
add_filter('wc_add_to_cart_message_html', 'rt_cart_remove_message');


/*=================================================;
/* CART COUNT ITEMS
/*================================================= */
function rt_get_cart_contents_count()
{   
    $current_count = 0;

    if (WC()->cart && method_exists(WC()->cart, 'get_cart_contents_count')) {
        $current_count = WC()->cart->get_cart_contents_count();
    }
    return absint($current_count);
}

/*=================================================;
/* ADDED COUNT ITEM CART TO FRAGMENT
/*================================================= 
* @version 1.0.0
*/
function rt_count_add_to_cart_fragment( $fragments ) {
    $count = rt_get_cart_contents_count();
	$fragments[ '.js-cart-total' ] = '<span class="rt-cart-item-count js-cart-total">'.$count.'</span>';
 	return $fragments;
 }

 add_filter( 'woocommerce_add_to_cart_fragments', 'rt_count_add_to_cart_fragment' );

/*=================================================;
/* CART - RENAME SHOP BUTTON TEXT
/*================================================= */
// Change button text on Product Archives
function rt_shop_add_to_cart_text($add_to_cart_html)
{
    if (rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') == 'redirect_checkout') {
        return str_replace('Add to cart', __('Buy now', 'saudagarwp'), $add_to_cart_html);
    }

    return $add_to_cart_html;

}
add_filter('woocommerce_loop_add_to_cart_link', 'rt_shop_add_to_cart_text');

/*=================================================;
/* CART - RENAME PRODUCT BUTTON TEXT
/*================================================= */
// Change button text on product pages
function rt_product_add_to_cart_text($button)
{
    if (rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') == 'redirect_checkout') {
        return __('Buy now', 'saudagarwp');
    }

    return $button;
}
add_filter('woocommerce_product_single_add_to_cart_text', 'rt_product_add_to_cart_text');

/*=================================================;
/* CART - REMOVE LINK TO CART
/*================================================= */
/**
 * Remove button cart page on mini cart
 *
 * @return void
 */
function rt_cart_widget_shopping_cart_button_view_cart()
{
    if (rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') !== 'default') {
        remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10);
    }
}
add_action('init', 'rt_cart_widget_shopping_cart_button_view_cart');
