<?php
/*=================================================
 *  WOO COMMERCE SUPPORT
/*================================================= */
function rt_woocommerce_support() {
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-lightbox');
}
add_action('after_setup_theme', 'rt_woocommerce_support');

/*=================================================;
/* SCRIPTS WOOCOMMERCE
/*================================================= */
function rt_woocommerce_scripts() {
    // disable default scripts woocommerce
    wp_dequeue_style('woocommerce-general');
    wp_dequeue_style('woocommerce-layout');

    // password strength meter for popup register
    wp_enqueue_script('wc-password-strength-meter');

    // Function to enqueue WooCommerce cart fragments script anywere
    wp_enqueue_script('wc-cart-fragments');

    // add new scripts woocommerce
    wp_enqueue_style('retheme-woo', get_template_directory_uri() . '/assets/css/woocommerce/woocommerce.min.css', false, '1.17.0');
    wp_enqueue_script('retheme-woo', get_template_directory_uri() . '/assets/js/woocommerce/woocommerce.min.js', false, '1.8.0', true);
    wp_localize_script('retheme-woo', 'localize', [
        'stock' => __('Only %s left in stock', 'woocommerce')
    ]);


    // Quantity field
    if (rt_option('woocommerce_archive_style', 'style-1') == 'style-3') {
        wp_enqueue_script('retheme-quantity-field', get_template_directory_uri() . '/assets/js/woocommerce/quantity-field.min.js', ['retheme-woo'], '2.0.0', true);
    }

    // Ajax add-to-cart script for premium users
    if (rt_is_premium()) {
        wp_enqueue_script('retheme-cart', get_template_directory_uri() . '/assets/js/woocommerce/add-to-cart.min.js', ['jquery'], '3.0.0', true);
    }

    if (rt_is_woocommerce('product') || get_option('yith-wcqv-enable') == 'yes' || get_option('yith-wcqv-enable-mobile') == 'yes') {
        wp_enqueue_style('retheme-product', get_template_directory_uri() . '/assets/css/woocommerce/product.min.css', false, '1.9.0');
        wp_enqueue_script('retheme-product', get_template_directory_uri() . '/assets/js/woocommerce/product.min.js', ['jquery', 'swiper'], '1.10.0', true);
        wp_enqueue_script('retheme-variation', get_template_directory_uri() . '/assets/js/woocommerce/variation.min.js', ['jquery'], '4.3.0', true);
    }

    if (rt_is_woocommerce('checkout') || rt_is_woocommerce('cart')) {
        wp_enqueue_style('retheme-checkout', get_template_directory_uri() . '/assets/css/woocommerce/checkout.min.css', false, '1.4.0');
        wp_enqueue_script('retheme-checkout', get_template_directory_uri() . '/assets/js/woocommerce/checkout.min.js', false, '1.3.0', true);
    }

    if (rt_is_woocommerce('account')) {
        wp_enqueue_style('retheme-account', get_template_directory_uri() . '/assets/css/woocommerce/account.min.css', false, '1.3.0');
    }
}
add_action('wp_enqueue_scripts', 'rt_woocommerce_scripts', 99);

/*=================================================;
/* REGISTER WIDGET LOCATION
/*================================================= */
function rt_woocommerce_register_widget_location() {

    register_sidebar(array(
        'name' => __('WooCommerce Sidebar', 'saudagarwp'),
        'id' => 'retheme_woocommerce_sidebar',
        'before_widget' => '<div id="%1$s" class="rt-widget rt-widget--aside %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
        'after_title' => '</h3></div>',
    ));

    register_sidebar(array(
        'name' => __('WooCommerce Filter', 'saudagarwp'),
        'id' => 'retheme_woocommerce_filter',
        'before_widget' => '<div id="%1$s" class="rt-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ));
}
add_action('widgets_init', 'rt_woocommerce_register_widget_location');

/*====================================================
/* PRINT MESSAGE EXCLUDE WOOCOMMERCE PAGE
/*====================================================*/
function rt_woocommerce_print_notices() {

    // Ensure it's not a WooCommerce page, cart, or checkout
    if (!rt_is_woocommerce('pages') && !rt_is_woocommerce('cart') && !rt_is_woocommerce('checkout') && !rt_is_woocommerce('account')) {
        // Check if there are any notices to print
        if (!empty(wc_get_notices())) {
            wc_print_notices();

            // Clear the notices after printing to prevent duplicate output
            wc_clear_notices();
        }
    }
}
add_action('rt_before_loop', 'rt_woocommerce_print_notices', 10);

/*=================================================;
/* CATEGORIES COUNT
/*================================================= */
function rt_woocommerce_subcategory_count_html($html, $category) {
    $count = wp_sprintf(__('%s products', 'saudagarwp'), absint($category->count));
    $html = "<div class='count'>{$count}</div>";
    return $html;
}
add_filter('woocommerce_subcategory_count_html', 'rt_woocommerce_subcategory_count_html', 10, 2);

/*=================================================
 * PRICE CLASS
=================================================== */
function rt_product_price_class($classes) {
    $classes = "product_price {$classes}";

    return $classes;
}
add_filter('woocommerce_product_price_class', 'rt_product_price_class');

/*=================================================;
/* QTY FIELD
/*=================================================
 * Added plus/min icon on product quantity field
 */
function rt_woocommerce_add_before_quantity() {

    if (rt_is_premium()) {
        echo '<div class="rt-qty js-quantity-change">';
        echo '<span class="rt-qty__min minus">-</span>';
    }
}
add_action('woocommerce_before_quantity_input_field', 'rt_woocommerce_add_before_quantity');

function rt_woocommerce_add_after_quantity() {
    if (rt_is_premium()) {
        echo '<span class="rt-qty__max plus">+</span>';
        echo '</div>';
    }
}
add_action('woocommerce_after_quantity_input_field', 'rt_woocommerce_add_after_quantity');

/*=================================================;
/* MOVE STORE NOTIFICATION
/*=================================================
 * move default store notification form footer to header top
 */
remove_action('wp_footer', 'woocommerce_demo_store');
add_action('rt_header', 'woocommerce_demo_store', 1);

/*=================================================;
/* INCLUDE FILE
/*================================================= */
function rt_woocommerce_init() {
    include_once dirname(__FILE__) . '/shop.php';
    include_once dirname(__FILE__) . '/product.php';
    include_once dirname(__FILE__) . '/cart.php';
    include_once dirname(__FILE__) . '/checkout.php';
    include_once dirname(__FILE__) . '/user.php';
}
add_action('woocommerce_init', 'rt_woocommerce_init');
