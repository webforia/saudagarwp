<?php
/*=================================================;
/* SHOP - ADD BODY CLASS
/*================================================= */
function rt_shop_body_class($classes) {
    $navbars = rt_option('woocommerce_shop_navbar_bottom', array('filter', 'shop', 'user', 'cart', 'chat', 'gotop'));

    if ((rt_is_woocommerce('shop') || is_front_page()) && count($navbars) > 0) {
        $classes[] = 'shop-navbar';
    }
    return $classes;
}
add_filter('body_class', 'rt_shop_body_class', 5);

/*=================================================;
/* SHOP - REMOVE DEFAULT LINK HTML
/*================================================= */
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

/*=================================================;
/* SHOP - SET SHOP COLUMNS
/*================================================= */
function rt_woocommerce_loop_columns() {
    return rt_option('woocommerce_archive_options_column', 3);
}
add_filter('loop_shop_columns', 'rt_woocommerce_loop_columns', 999);

/*=================================================;
/* SHOP - ADD CLASS WRAPPER LOOP
/*================================================= */
function rt_woocommerce_loop_class($loop_prop) {
    // added new array class
    $classes[] = 'rt-product-archive products grids';

    /* Column */
    $column = $loop_prop;
    $column_tablet = rt_option('woocommerce_archive_options_column_tablet', 3);
    $column_mobile = rt_option('woocommerce_archive_options_column_mobile', 2);

    $classes[] = "grids-md-{$column}";
    $classes[] = "grids-sm-{$column_tablet}";
    $classes[] = "grids-xs-{$column_mobile}";

    if (rt_option('woocommerce_archive_masonry', false)) {
        $classes[] = 'grid-masonry js-masonry';
    }

    // Separates classes with a single space, collates classes for post DIV.
    echo 'class="' . esc_attr(implode(' ', apply_filters('rt_woocommerce_loop_class', $classes))) . '"';
}

/*=================================================
 *  SHOP -  CHANGE LOOP PERPAGE
/*================================================= */
function rt_woocommerce_per_page() {
    $post_per_page = rt_option('woocommerce_archive_options_post_per_page', 9);

    if (rt_is_only_tablet()) {
        $post_per_page = rt_option('woocommerce_archive_options_post_per_page_tablet', 8);
    }

    if (rt_is_only_mobile()) {
        $post_per_page = rt_option('woocommerce_archive_options_post_per_page_mobile', 8);
    }
    return $post_per_page;
};
add_filter('loop_shop_per_page', 'rt_woocommerce_per_page', 10, 1);

/*=================================================;
/* SHOP -  AJAX QUERY
/*================================================= */
function rt_woocommerce_query_loop($loop) {

    if (rt_is_woocommerce('shop')) {
        $attributes = WC()->query->get_layered_nav_chosen_attributes();
        $min_price = isset($_GET['min_price']) ? wc_clean(wp_unslash($_GET['min_price'])) : 0; // WPCS: input var ok, CSRF ok.
        $max_price = isset($_GET['max_price']) ? wc_clean(wp_unslash($_GET['max_price'])) : 0; // WPCS: input var ok, CSRF ok.
        $rating = isset($_GET['rating_filter']) ? min(array_filter(array_map('absint', explode(',', wp_unslash($_GET['rating_filter']))))) : array(); // WPCS: sanitization ok, input var ok, CSRF ok.
        $orderby = isset($_GET['orderby']) ? wc_clean($_GET['orderby']) : apply_filters('woocommerce_default_catalog_orderby', get_option('woocommerce_default_catalog_orderby'));

        // Filter Price
        if ($min_price || $max_price) {
            $query_args['meta_query'][] = [
                'key' => '_price',
                'value' => array($min_price, $max_price),
                'compare' => 'BETWEEN',
                'type' => 'NUMERIC',
            ];
        }

        // Filter by Rating
        if ($rating) {
            $query_args['meta_query'][] = array(
                'key' => '_wc_average_rating',
                'value' => 2,
                'compare' => '>=',
            );
        }
        // Get select current attribute
        if ($attributes) {
            foreach ($attributes as $taxonomy => $data) {
                $query_args['tax_query'][] = array(
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => $data['terms'],
                    'operator' => 'and' === $data['query_type'] ? 'AND' : 'IN',
                    'include_children' => false,
                );
            }
        }

        // Order by
        switch ($orderby) {
            case 'menu_order':
                $query_args['orderby'] = 'title menu_order';
                $query_args['order'] = 'ASC';
                break;
            case 'date':
                $query_args['orderby'] = 'date id';
                break;
            case 'popularity':
                $query_args['meta_key'] = 'total_sales';
                $query_args['orderby'] = 'meta_value_num';
                $query_args['order'] = 'ASC';
                break;
            case 'price':
                $query_args['meta_key'] = '_price';
                $query_args['orderby'] = 'meta_value_num';
                $query_args['order'] = 'ASC';
                break;
            case 'price-desc':
                $query_args['meta_key'] = '_price';
                $query_args['orderby'] = 'meta_value_num';
                $query_args['order'] = 'DESC';
                break;
            default:
                # code...
                break;
        }

        // Main query
        $query_args['post_type'] = 'product';
        $query_args['posts_per_page'] = rt_woocommerce_per_page();
        $query_args['template_part'] = 'woocommerce/content-product';
        $query_args['pagination_style'] = rt_option('woocommerce_archive_pagination', 'number');
        $loop = wp_parse_args($query_args, $loop);
    }

    return $loop;
};
add_filter('rt_loop_query', 'rt_woocommerce_query_loop');

/*=================================================;
/* SHOP - CATEGORIES LIST
/*=================================================
 * Show all product categories on product archive
 * show categories before archive list
 */
function rt_shop_categories_list() {
    if (rt_option('woocommerce_archive_product_cat_list', false)) {
        rt_get_template_part('shop/product-cat-list');
    }
}
// add_action('woocommerce_before_shop_loop', 'rt_shop_categories_list');

/*=================================================;
/* CUSTOM PRODUCT ARCIVE BY RECENTLY VIEWED
/*=================================================
 *
 * @param [type] $query
 * @see https://developer.wordpress.org/reference/hooks/pre_get_posts/
 * @return void
 */
function rt_shop_recently_viewed($query) {

    // Get orderby
    $query_by = isset($_GET['query_by']) ? sanitize_text_field(wp_unslash($_GET['query_by'])) : 'default';

    // We only want to affect the main query.
    if (is_admin() && !$query->is_main_query()) {
        return;
    }

    // return default if not shop page
    if (rt_is_woocommerce('shop') && $query_by == 'recently-viewed') {

        // Get product array form cookie
        $viewed_products = !empty($_COOKIE['woocommerce_recently_viewed']) ? (array) explode('|', wp_unslash($_COOKIE['woocommerce_recently_viewed'])) : array(); // @codingStandardsIgnoreLine
        $viewed_products = array_reverse(array_filter(array_map('absint', $viewed_products)));
        $query->set('post_type', 'product');
        $query->set('posts_per_page', rt_woocommerce_per_page());
        $query->set('post__in', $viewed_products);
    }

    return $query;
}
// add_action('pre_get_posts', 'rt_shop_recently_viewed');

/*=================================================
 *   REMOVE TITLE SHOP
/*================================================= */
add_filter('woocommerce_show_page_title', '__return_false');

/*=================================================
 * SHOP - REMOVE WOO PAGINATION
/*================================================= */
remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10); // remove default pagination
add_action('woocommerce_after_shop_loop', 'rt_pagination_archive', 10); // add global pagination to woocommerce pagination

/*=================================================;
/* SHOP - REMOVE WRAPPER GENERAL POST
/*=================================================
 * Remove theme DOM wraper loop <div id="post_archive"></div>
 *
 * @version 1.0.0
 */
function rt_woocommerce_wrapper_post() {
    if (rt_is_woocommerce('shop')) {
        remove_action('rt_before_loop', 'rt_before_loop', 25);
        remove_action('rt_after_loop', 'rt_after_loop', 25);
        remove_action('rt_after_loop', 'rt_pagination_archive', 30);
    }
}
add_action('wp', 'rt_woocommerce_wrapper_post');

/*=================================================;
/* SHOP - FILTER
/*=================================================*/
function rt_shop_result_count() {
    echo '<div class="rt-shop-filter-wrapper">';
}
add_action('woocommerce_before_shop_loop', 'rt_shop_result_count', 19);

function rt_shop_catalog_ordering() {
    echo '</div>';
}
add_action('woocommerce_before_shop_loop', 'rt_shop_catalog_ordering', 31);

/*=================================================
 * SHOP - HOOK ARCHIVE
/*=================================================
 * remove add to cart button on archive product
 */
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

/*=================================================;
/* SHOP - REMOVE DESCRIPTION
/*================================================= */
remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description', 10);

/*=================================================;
/* SHOP - ADD TO CART BUTTON ON ARCHIVE
/*================================================= */
function rt_add_to_cart_loop_button() {
    // Yith Label
    $label = get_option('yith-wcqv-button-label');
    $label = call_user_func('__', $label, 'yith-woocommerce-quick-view');
    $label = apply_filters('yith_wcqv_button_label', esc_html($label));
    $product = wc_get_product(get_the_ID());

    if (rt_option('woocommerce_cart_behavior', 'ajax_addtocart_panel') != 'catalog') {

        // compatible with yith quick view
        if ((get_option('yith-wcqv-enable') == 'yes' || get_option('yith-wcqv-enable-mobile') == 'yes') && rt_option('woocommerce_archive_style', 'style-1') == 'style-1') {
            echo '<a href="#" class="button yith-wcqv-button rt-product__btn" data-product_id="' . get_the_ID() . '">' . $label . '</a>';
        } else {
            if ($product->is_in_stock() || $product->is_type('variable')) {
                woocommerce_template_loop_add_to_cart();
            } else {
                echo '<a href="' . get_permalink(get_the_ID()) . '" class="button rt-product__btn">' . __('Out of stock', 'woocommerce') . '</a>';
            }
        }
    }
}
// add button inside thumbnail
add_action('woocommerce_before_shop_loop_item_title', 'rt_add_to_cart_loop_button', 10);
// add button inside body
add_action('woocommerce_after_shop_loop_item_title', 'rt_add_to_cart_loop_button', 20);

/*=================================================;
/* SHOP - REMOVE PREFIX PAGE TITLE
/*================================================= */
function rt_shop_remove_prefix_title($title) {
    if (rt_is_woocommerce('shop')) {
        $title = single_cat_title('', false);
    }
    return $title;
}
add_filter('get_the_archive_title', 'rt_shop_remove_prefix_title');

/*=================================================;
/* SHOP - PRODUCT STYLE
/*================================================= */
function rt_shop_product_class($product) {

    $style = rt_option('woocommerce_archive_style', 'style-1');
    $align = rt_option('woocommerce_archive_text_align', 'center');
    $card = rt_option('woocommerce_archive_card', false);

    $classes[] = "rt-product rt-product--{$align}";

    // style product
    if ($style == 'style-1') {
        $classes[] = 'rt-product--overlay-btn';
    }

    if ($style == 'style-2') {
        $classes[] = 'rt-product--button';
    }

    if ($style == 'style-3') {
        $classes[] = 'rt-product--quantity-field';
    }

    // frame box
    if ($card) {
        $classes[] = "rt-product--card";
    }

    wc_product_class($classes, $product);
}

/*=================================================;
/* SHOP - ADD PANEL
/*================================================= */
function rt_shop_panel_filter() {
    if (rt_is_woocommerce('shop')) {
        rt_get_template_part('shop/panel-filter');
    }
}
add_action('rt_footer', 'rt_shop_panel_filter');

/*=================================================;
/* SHOP - NAVBAR BOTTOM FOR MOBILE
/*================================================= */
function rt_shop_navbar_bottom() {
    if (rt_is_woocommerce('shop') || is_front_page()) {
        rt_get_template_part('shop/shop-navbar');
    }
}
add_action('rt_footer', 'rt_shop_navbar_bottom');

/*=================================================;
/* SHOP - PRODUCT QUANTITY
/*=================================================
 *
 * Add quantity field on shop page
 * @version 1.0.0
 * @hook action woocommerce_after_shop_loop_item
 * @return html
 */
function rt_shop_add_quantity() {
    if (rt_option('woocommerce_archive_style', 'style-1') === 'style-3') {
        /** @var WC_Product $product */
        $product = wc_get_product(get_the_ID());
        if (!$product->is_sold_individually() && 'variable' != $product->get_type() && $product->is_purchasable() && $product->is_in_stock()) {
            woocommerce_quantity_input(array('min_value' => 1, 'max_value' => $product->backorders_allowed() ? '' : $product->get_stock_quantity()));
        }
    }
}
add_action('woocommerce_after_shop_loop_item_title', 'rt_shop_add_quantity', 15);
