<?php
/*=================================================;
/* PRODUCT -  LAYOUT
/*=================================================
 ** Added class product body */
function rt_product_layout($classes) {
    if (rt_is_woocommerce('product')) {
        $layout = rt_option('woocommerce_single_layout', 'normal');
        $classes[] = "product-template--{$layout}";
    }

    if (rt_option('product_sticky_action', true)) {
        $classes[] = 'product-sticky-action';
    }

    return $classes;
}
add_filter('body_class', 'rt_product_layout');

/*=================================================;
/* PRODUCT - STICKY ACTION
/*=================================================
 * Replace global header with product header with action button
 * @since 2.0.0
 * @hook action rt_footer
 * @return void
 */
function rt_product_sticky_action() {
    if (rt_is_woocommerce('product') && rt_option('product_sticky_action', true)) {
        rt_get_template_part('product/sticky-product');
    }
}
add_action('woocommerce_after_single_product', 'rt_product_sticky_action');

/*=================================================;
/* PRODUCT - FLASH SALE BADGES
/*================================================= 
* Remove flash sale badges from product gallery images
* @hook active woocommerce_before_single_product_summary
*/
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

/*=================================================;
/* PRODUCT - REVIEW FIELD
/*=================================================
 * Replace custom html on form review
 * @hook filter woocommerce_product_review_comment_form_args
 * @return html
 */
function rt_product_review_comment_form($review_form) {
    $commenter = wp_get_current_commenter();

    $review_form = array(
        'fields' => array(
            'author' => '<div class="comment-wrapper-personal"><p class="comment-form-author rt-form rt-form--overlay js-form-overlay">' . '<label class="rt-form__label" for="author">' . esc_html__('Name', 'woocommerce') . '&nbsp;<span class="required">*</span></label> ' .
                '<input id="author" class="rt-form__input" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" required /></p>',
            'email' => '<p class="comment-form-email rt-form rt-form--overlay js-form-overlay"><label class="rt-form__label" for="email">' . esc_html__('Email', 'woocommerce') . '&nbsp;<span class="required">*</span></label> ' .
                '<input id="email" class="rt-form__input" name="email" type="email" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" required /></p></div>',
        ),

    );

    $review_form['comment_field'] = '';

    if (wc_review_ratings_enabled()) {
        $review_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . esc_html__('Your rating', 'woocommerce') . '</label><select name="rating" id="rating" required>
						<option value="">' . esc_html__('Rate&hellip;', 'woocommerce') . '</option>
						<option value="5">' . esc_html__('Perfect', 'woocommerce') . '</option>
						<option value="4">' . esc_html__('Good', 'woocommerce') . '</option>
						<option value="3">' . esc_html__('Average', 'woocommerce') . '</option>
						<option value="2">' . esc_html__('Not that bad', 'woocommerce') . '</option>
						<option value="1">' . esc_html__('Very poor', 'woocommerce') . '</option>
					</select></div>';
    }

    $review_form['comment_field'] .= '<p class="comment-form-comment rt-form rt-form--textarea rt-form--overlay js-form-overlay"><label class="rt-form__label" for="comment">' . esc_html__('Your review', 'woocommerce') . '&nbsp;<span class="required">*</span></label><textarea id="comment" class="rt-form__input" name="comment" cols="45" rows="8" required></textarea></p>';

    return $review_form;
}
add_filter('woocommerce_product_review_comment_form_args', 'rt_product_review_comment_form');

/*=================================================;
/* PRODUCT - REMOVE HEADER
/*=================================================
 * Remove header on product page
 * @see rt_page_header()
 */
function rt_product_page_header($args) {
    if (rt_is_woocommerce('product')) {
        return 'no';
    }

    return $args;
}
add_filter('page_header_layout', 'rt_product_page_header');

/*=================================================;
/* PRODUCT - MOVE BREADCRUMBS
/*=================================================
 * added breadcrumbs before title product
 * @see rt_breadcrumbs()
 */
add_action('woocommerce_single_product_summary', 'rt_breadcrumb', 1);

/*=================================================;
/* PRODUCT - REMOVE TITLE TAB
/*=================================================*/
add_filter('woocommerce_product_description_heading', '__return_false');
add_filter('woocommerce_product_additional_information_heading', '__return_false');


/*=================================================;
/* REMOVE DESC TABS
/*=================================================
 * remove default tabs woocommerce
 *
 * @param [type] $tabs additional_information, description, reviews
 * @hook action woocommerce_product_tabs
 * @return array tabs list
 * @see https: //docs.woocommerce.com/document/editing-product-data-tabs/
 */
function rt_product_tabs($tabs) {
    // Return default tabs
    if (rt_option('product_default_attributes', false)) {
        return $tabs;
    }

    // Custom tabs
    unset($tabs['additional_information']);

    if (rt_option('product_detail', true)) {
        $tabs['description'] = array(
            'title' => __('Description', 'woocommerce'),
            'priority' => 5,
            'callback' => function () {
                global $product;

                do_action('woocommerce_product_additional_information', $product);

                the_content();
            },
        );
    }

    return $tabs;
}
add_filter('woocommerce_product_tabs', 'rt_product_tabs');

/*=================================================;
/* REMOVE PRODUCT META
/*=================================================
 * @hook woocommerce_single_product_summary
 */
function rt_product_meta() {
    if (rt_option('product_default_meta', false) && rt_option('woocommerce_single_meta', true)) {
        return;
    }

    // Remove hook
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
}

add_action('init', 'rt_product_meta');

/*=================================================;
/* PRODUCT - ADDITIONAL CATEGORY
/*=================================================
 *
 * added list and value to product attribute table
 * move product meta sku, category, and tag on attribute
 * @param $product_attributes array attribute list weight, demention dll
 * @hook filter woocommerce_display_product_attributes
 * @return all attributes with custom attribute
 */
function rt_product_attributes($product_attributes) {
    global $product;

    // Return default tabs
    if (rt_option('product_default_meta', false)) {
        return $product_attributes;
    }

    if (rt_option('woocommerce_single_meta', true)) {
        // Custom tabs
        if (wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))) {

            $sku = ($sku = $product->get_sku()) ? $sku : esc_html__('N/A', 'woocommerce');

            $product_attributes['attribute_product_sku'] = array(
                'label' => __('SKU', 'woocommerce'),
                'value' => '<span class="sku_value" data-sku="'.$sku.'">'.$sku.'</span>',
            );
        }

        if (count($product->get_category_ids()) >= 1) {
            $product_attributes['attribute_product_categories'] = array(
                'label' => __('Category', 'woocommerce'),
                'value' => wc_get_product_category_list($product->get_id(), ', '),
            );
        }

        if (count($product->get_tag_ids()) >= 1) {
            $product_attributes['attribute_product_tags'] = array(
                'label' => __('Tag', 'woocommerce'),
                'value' => wc_get_product_tag_list($product->get_id(), ', '),
            );
        }
    }

    return $product_attributes;
}
add_filter('woocommerce_display_product_attributes', 'rt_product_attributes');

/*=================================================
 * SHARE
/*================================================= */
/** add share button woocommerce single */
function rt_product_share() {
    if (rt_option('woocommerce_single_share', true) && rt_is_premium()) {
        rt_get_template_part('product/product-share');
    }
}

add_action('woocommerce_share', 'rt_product_share');

/*=================================================;
/* REMOVE STICKY HEADER
/*================================================= */
function rt_product_remove_sticky_header($args) {
    if (rt_is_woocommerce('product') && rt_option('product_sticky_action', true)) {
        $args = false;
    }

    return $args;
}

add_filter('header_sticky', 'rt_product_remove_sticky_header');

/*=================================================;
/* THUMBNAIL IMAGE - REMOVE IMAGE LINK
/*================================================= */
/**
 * remove image wrapper
 * remove image link
 * this needed for image swipper work on product gallery
 * 
 * @hook filter woocommerce_single_product_image_thumbnail_html
 */

function rt_product_remove_image_link($html, $post_id) {
    return preg_replace("!<(div|/div|a|/a).*?>!", '', $html);
}
add_filter('woocommerce_single_product_image_thumbnail_html', 'rt_product_remove_image_link', 10, 2);

/*=================================================;
 * PRODUCT - RELATED
 *=================================================
 *
 * Change number product link per page
 * @version 1.0.0
 * @hook filter woocommerce_output_related_products_args
 */
function rt_product_output_related($args) {
    $args['posts_per_page'] = rt_option('woocommerce_single_related_count', 6);

    return $args;
}
add_filter('woocommerce_output_related_products_args', 'rt_product_output_related');
