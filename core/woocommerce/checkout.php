<?php
/*=================================================;
/* CHECKOUT - MOVE COUPON
/*================================================= */
// add link to open checkout on review order
function rt_checkout_modal_coupon() {
    if (get_option('woocommerce_enable_coupons') === 'yes') {
        echo '<div class="rt-coupon-checkout-trigger coupon_checkout_message">';
        echo apply_filters('woocommerce_checkout_coupon_message', esc_html__('Have a coupon?', 'woocommerce') . ' <a href="#" class="js-coupon-trigger">' . esc_html__('Click here to enter your code', 'woocommerce') . '</a>');
        echo '</div>';
    }
}
add_action('woocommerce_review_order_before_payment', 'rt_checkout_modal_coupon');

/*====================================================
/* THANK PAGE BACK TO HOME
/*====================================================*/
function rt_checkout_thankyou_page_link_home() {

    $page_confirmation = rt_option('woocommerce_checkout_confirmation_page');

    echo '<div class="rt-thankpage-action">';
    if ($page_confirmation) :
        echo '<a href="' . get_permalink($page_confirmation) . '" class="rt-btn rt-btn--border"><i class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-credit-card" viewBox="0 0 16 16"><path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v1h14V4a1 1 0 0 0-1-1H2zm13 4H1v5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V7z"/><path d="M2 10a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1z"/></svg></i>' . __('Payment confirmation', 'saudagarwp') . '</a>';
    endif;
    echo '<a href="' . get_home_url() . '" class="rt-btn rt-btn--primary"><i class="rt-icon"><svg width="1em" height="1em" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><path d="M1.725,7.51L1.049,8.166C0.951,8.261 0.818,8.315 0.679,8.315C0.392,8.315 0.155,8.085 0.155,7.806C0.155,7.671 0.211,7.542 0.309,7.447L7.261,0.695C7.666,0.301 8.334,0.301 8.739,0.695L15.691,7.447C15.789,7.542 15.845,7.671 15.845,7.806C15.845,8.085 15.608,8.315 15.321,8.315C15.182,8.315 15.049,8.261 14.951,8.166L14.275,7.51L14.275,13.859C14.275,14.695 13.567,15.383 12.706,15.383L3.294,15.383C2.433,15.383 1.725,14.695 1.725,13.859L1.725,7.51ZM2.771,6.493L8,1.413L13.229,6.493L13.229,13.859C13.229,14.138 12.993,14.367 12.706,14.367L10.909,14.367L10.909,10.447C10.909,9.605 10.207,8.923 9.34,8.923C8.545,8.923 7.455,8.923 6.66,8.923C5.793,8.923 5.091,9.605 5.091,10.447L5.091,14.367L3.294,14.367C3.007,14.367 2.771,14.138 2.771,13.859L2.771,6.493ZM9.863,14.367L6.137,14.367L6.137,10.447C6.137,10.166 6.371,9.939 6.66,9.939L9.34,9.939C9.629,9.939 9.863,10.166 9.863,10.447L9.863,14.367Z"></path></svg></i>' . __('Back to home', 'saudagarwp') . '</a>';
    echo '</div>';
}
add_action('woocommerce_thankyou', 'rt_checkout_thankyou_page_link_home', 999);
