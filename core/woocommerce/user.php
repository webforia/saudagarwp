<?php
/*=================================================;
/* SHOP - FAILED LOGIN REDIRECT
/*================================================= */
// handle user failed login on modal login
// redirect to default login page
function rt_user_woocommerce_login_failed() {
    $referrer = $_SERVER['HTTP_REFERER'];
    $account_page = wc_get_page_permalink('myaccount');
    $checkout_page = wc_get_page_permalink('checkout');

    if (
        !empty($referrer) &&
        !strstr($referrer, 'wp-login') &&
        !strstr($referrer, 'wp-admin') &&
        !strstr($referrer, $account_page) &&
        !strstr($referrer, $checkout_page)
    ) {
        wp_redirect(wc_get_page_permalink('myaccount') . '?login="failed"');
        exit;
    }
}
add_action('woocommerce_login_failed', 'rt_user_woocommerce_login_failed');

/*=================================================;
/* WOOCOMMERCE - FAILED LOGIN MESSAGE
/*================================================= */
// Send message error if user fail login from modal
// send message on default page login
function rt_user_woocommerce_login_failed_message() {
    if (!empty($_GET['login']) && $_GET['login'] == 'failed' && !is_user_logged_in()) {
        wc_add_notice(__('Login failed, please check username & password then try again', 'saudagarwp'), 'error');
    }
}
add_action('wp', 'rt_user_woocommerce_login_failed_message');

/*====================================================
/* USER - REDIRECT AFTER LOGIN
/*====================================================*/
function rt_user_redirect_after_login($redirect_url) {
    $referrer = $_SERVER['HTTP_REFERER'];
    $my_account_page = wc_get_page_permalink('myaccount');
    $checkout_page = wc_get_page_permalink('checkout');

    if (!strstr($referrer, $my_account_page) && !strstr($referrer, $checkout_page)) {
        $redirect_url = get_permalink(wc_get_page_id('myaccount'));
    }

    return $redirect_url;
}
add_filter('woocommerce_login_redirect', 'rt_user_redirect_after_login');

/*=================================================;
/* USER - MODAL LOGIN
/*================================================= */
function rt_user_modal() {
    if (!is_user_logged_in() && !rt_is_woocommerce('account')) {
        rt_get_template_part('shop/form-login-modal');
    }
}
add_action('rt_footer', 'rt_user_modal');

/*====================================================
/* REDIRECT AFTER REGISTER
/*====================================================*/
function rt_user_redirect_after_registration($redirect_url) {
    $referrer = $_SERVER['HTTP_REFERER'];
    $my_account_page = wc_get_page_permalink('myaccount');
    $checkout_page = wc_get_page_permalink('checkout');

    if (!strstr($referrer, $my_account_page) && !strstr($referrer, $checkout_page)) {
        $redirect_url = get_permalink(wc_get_page_id('myaccount')) . '?registration="success"';
    }

    return $redirect_url;
}
add_filter('woocommerce_registration_redirect', 'rt_user_redirect_after_registration');

/*====================================================
/* USER - SEND MESSAGE SUCCESS AFTER REGISTRATION
/*====================================================*/
function rt_user_woocommerce_registration_success_message() {
    if (!empty($_GET['registration']) && $_GET['registration'] == 'success' && is_user_logged_in() && is_account_page()) {
        wc_add_notice(__('Registration success', 'saudagarwp'), 'success');
    }
}
add_action('wp', 'rt_user_woocommerce_registration_success_message');

/*====================================================
/* USER - REPLACE TEXT REGISTER EMAIL EXISTS
/*====================================================*/
function rt_user_registration_error_email_exists($error) {
    // Get the URL of the 'my account' page
    $myaccount_url = wc_get_page_permalink('myaccount');

    // Create a custom error message using wp_sprintf
    $custom_message = str_replace('#', $myaccount_url, $error);

    // Return the new custom error message
    return $custom_message;
}
add_filter('woocommerce_registration_error_email_exists', 'rt_user_registration_error_email_exists');

/*====================================================
/* RENAME EDIT ADDRESS TAB MENU @ MY ACCOUNT
/*====================================================*/
function rt_user_rename_address_my_account($items) {

    $items['edit-address'] = __('Addresses', 'woocommerce');

    return $items;
}
add_filter('woocommerce_account_menu_items', 'rt_user_rename_address_my_account', 999);
