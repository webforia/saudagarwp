<?php

/*=================================================;
/* HTML - BEFORE
/*================================================= */
function rt_html_open($args = '', $tag = '') {
    return Retheme\HTML::open($args, $tag);
}

/*=================================================;
/* HTML - AFTER
/*================================================= */
function rt_html_close() {
    return Retheme\HTML::close();
}

/*=================================================;
/* HTML - SCRIPTS
/*================================================= */
function rt_html_script($args) {
    return Retheme\HTML::script($args);
}

/*=================================================;
/* HEADER BLOCK
/*================================================= */
function rt_header_block($args) {
    return Retheme\HTML::header_block($args);
}

/*=================================================;
/* SLIDER - BEFORE
/*================================================= */
function rt_before_slider($args = array()) {
    return Retheme\HTML::before_slider($args);
}

/*=================================================;
/* SLIDER - AFTER
/*================================================= */
function rt_after_slider() {
    return Retheme\HTML::after_slider();
}

/*=================================================;
/* PAGINATION
/*================================================= */
function rt_pagination($args = array()) {
    return Retheme\HTML::pagination($args);
}

/*=================================================
 *  LIMIT TITLE
/*=================================================
 * @since 1.0.0
 * @param  number for limit string $length
 * @return string title limit
 */
function rt_the_title($limit = '') {
    global $post;

    if (empty($limit)) {
        $limit = '99';
    }
    $title = get_the_title($post->ID);
    if (strlen($title) > $limit) {
        $title = substr($title, 0, $limit) . '...';
    }

    echo $title;
}

/*=================================================
 *  LOGO
/*================================================= */
function rt_logo($type = '') {
    $logo_image = '';

    $logo_desktop_primary = (rt_option('header_overlay', false) && is_front_page()) ? rt_option('brand_logo_overlay') : rt_option('brand_logo_primary');
    $logo_desktop_sticky = (rt_option('brand_logo_sticky')) ? rt_option('brand_logo_sticky') : rt_option('brand_logo_primary');

    $logo_mobile_primary = (rt_option('brand_logo_mobile')) ? rt_option('brand_logo_mobile') : rt_option('brand_logo_primary');
    $logo_mobile_overlay = (rt_option('brand_logo_mobile_overlay')) ?  rt_option('brand_logo_mobile_overlay') : $logo_mobile_primary;

    /* logo url by device */
    if ($type == 'desktop') {
        $logo_image = $logo_desktop_primary;
    }

    if ($type == 'desktop-sticky') {
        $logo_image = $logo_desktop_sticky;
    }

    if ($type == 'mobile') {
        $logo_image = $logo_mobile_primary;
    }

    /* Print logo */
    $output = '<a class="rt-logo" href="' . esc_url(home_url()) . '">';

    if ($logo_image) {

        $output .= '<img class="rt-logo__primary site-image-logo" src="' . esc_url($logo_image) . '" alt="' . get_bloginfo('name') . '">';

        // Mobile logo
        if (rt_option('header_overlay', false) && is_front_page() && $type == 'mobile') {
            $output .= '<img class="rt-logo__alternative site-image-logo" src="' . esc_url($logo_mobile_overlay) . '" alt="' . get_bloginfo('name') . '">';
        }
    } else {
        $output .= '<span class="rt-logo__sitename">' . get_bloginfo('name') . '</span>';
    }

    $output .= '</a>';

    do_action('rt_before_brand');

    echo $output;

    do_action('rt_after_brand');
}

/*=================================================
 *  LIMIT CONTENT
/*================================================= */
// limited string on the content
function rt_the_content($length = 18) {
    global $post;

    // Check for custom excerpt
    if (has_excerpt($post->ID)) {
        $output = $post->post_excerpt;
    } else {
        // No custom excerpt
        // Check for more tag and return content if it exists
        if (strpos($post->post_content, '<!--more-->')) {
            $output = apply_filters('the_content', get_the_content());
        } else {
            // No more tag defined
            $output = wp_trim_words(strip_shortcodes($post->post_content), $length);
        }
    }

    echo $output;
}

/*=================================================;
/* SOCIAL MEDIA
/*================================================= */
function rt_social_media($args = array()) {
    rt_get_template_part('global/social-media', $args);
}

/*=================================================
 * IMAGE THUMBNAIL
/*================================================= */
function rt_the_post_thumbnail($size = '') {

    if (has_post_thumbnail()) {
        the_post_thumbnail($size);
    }
}

/*=================================================
 *  WOOCOMMERCE CONDITION TAGS
/*=================================================
 * Check woocommerce plugin active
 * Check woocommerce page
 *
 * @version 1.0.0
 */

function rt_is_woocommerce($page = '') {
    if (class_exists('WooCommerce')) {
        // Rertuns true on WooCommerce Plugins active
        if (empty($page)) {
            return true;
        }

        // all template WooCommerce but cart and checkout not include
        if ($page == 'pages' && is_woocommerce()) {
            return true;
        }

        // Returns true when on the product archive page (shop).
        if ($page == 'shop' && is_shop() || $page == 'shop' && is_tax(get_object_taxonomies('product'))) {
            return true;
        }

        // Return true when on shop without taxonomy
        if ($page == 'catalog' && is_shop() ){
            return true;
        }

        // Return true on single product page
        if ($page == 'product' && is_product()) {
            return true;
        }

        // Returns true on the customer’s account pages.
        if ($page == 'account' && is_account_page()) {
            return true;
        }

        // Returns true on the cart page.
        if ($page == 'cart' && is_cart()) {
            return true;
        }

        // Returns true on the checkout page.
        if ($page == 'checkout' && is_checkout()) {
            return true;
        }

        // Returns true when viewing a WooCommerce endpoint
        if ($page == 'endpoint_url' && is_wc_endpoint_url()) {
            return true;
        }

        if ($page == 'category' && is_product_category()) {
            return true;
        }

        if ($page == 'tag' && is_product_tag()) {
            return true;
        }
    } else {
        return false;
    }
}

/*=================================================
/* BREADCRUMBS
/*================================================= */
function rt_breadcrumb() {
    if (rt_option('breadcrumb', true)) {
        echo "<div class='rt-breadcrumbs'>";

        if (function_exists('bcn_display')) {
            bcn_display();
        } elseif (function_exists('yoast_breadcrumb') && !empty(get_option('wpseo_titles')['breadcrumbs-enable'])) {
            yoast_breadcrumb();
        } elseif (function_exists('rank_math_the_breadcrumbs') && rank_math_get_breadcrumbs()) {
            rank_math_the_breadcrumbs();
        } elseif (function_exists('breadcrumb_trail')) {
            breadcrumb_trail([
                'show_title' => false,
                'show_on_front' => false,
                'show_browse' => false,
                'labels' => [
                    'browse' => __('Browse:', 'saudagarwp'),
                    'aria_label' => esc_attr_x('Breadcrumbs', 'breadcrumbs aria label', 'saudagarwp'),
                    'home' => __('Home', 'saudagarwp'),
                    'error_404' => __('404 Not Found', 'saudagarwp'),
                    'archives' => __('Archives', 'saudagarwp'),
                    'search' => __('Search results for: %s', 'saudagarwp'),
                    'paged' => __('Page %s', 'saudagarwp'),
                    'paged_comments' => __('Comment Page %s', 'saudagarwp'),
                    'archive_minute' => __('Minute %s', 'saudagarwp'),
                    'archive_week' => __('Week %s', 'saudagarwp'),
                ],
            ]);
        }

        echo "</div>";
    }
}

/*=================================================;
/* TEMPLATE LOOP
/*================================================= */
/**
 * Show loop contents for grid or carousel layout
 * Added pagination number or load more to CPT
 * Show dynamic content from wp query to carousel or grid layout
 * Show static contnet to carousel or grid layout
 *
 */

function rt_template_loop($args = array()) {

    // custom query
    $the_query = new \WP_Query(\Retheme\Helper::query($args));

    // set default setting
    // replace default setting with new setting from loop query
    $settings = wp_parse_args($args, [
        'id' => 'loop_archive',
        'post_type' => 'post',
        'post_status' => 'publish',
        'content' => '', // use for static content or array
        'posts_per_page' => 9,
        'class' => '',
        'template_part' => 'template-part/content',
        'setting_column' => 4,
        'setting_column_tablet' => 2,
        'setting_column_mobile' => 2,
        'pagination_style' => 'number',
        'carousel' => false,
        'slider_item' => 4,
        'slider_item_tablet' => 2,
        'slider_item_mobile' => 1,
        'slider_gap' => 30,
        'slider_gap_tablet' => 20,
        'slider_gap_mobile' => 15,
        'slider_loop' => false,
        'slider_auto_play' => false,
        'slider_pagination' => false,
        'slider_nav' => [
            'nextEl' => '.swiper-button-next',
            'prevEl' => '.swiper-button-prev',
        ],
        'slider_same_height' => true,
        'slider_link' => false,
        'link_text' => '',
        'link_url' => '',

    ]);

    // class wrapper
    $classes[] = !empty($settings['class']) ? $settings['class'] : '';
    $classes[] = !empty($settings['masonry']) ? 'grid-masonry js-masonry' : '';

    // loop from wp query
    if ($settings['post_type'] !== 'content') {
        if ($the_query->have_posts()) :

            // layout column
            if (!$settings['carousel']) {

                // column setting css class
                $classes[] = 'grids';
                $classes[] = 'grids-md-' . $settings['setting_column'];
                $classes[] = 'grids-sm-' . $settings['setting_column_tablet'];
                $classes[] = 'grids-xs-' . $settings['setting_column_mobile'];

                echo rt_html_open([
                    'id' => $settings['id'],
                    'class' => $classes,
                ]);

                while ($the_query->have_posts()) : $the_query->the_post();

                    get_template_part($settings['template_part']);

                endwhile;

                echo rt_html_close();

                // add pagination
                echo rt_pagination(wp_parse_args($args, [
                    'target' => $settings['id'],
                    'pagination_style' => 'number',
                    'post_type' => 'post',
                    'posts_per_page' => 9,
                    'total' => $the_query->max_num_pages,
                    'post_total' => $the_query->found_posts,
                    'format' => '?paged=%#%',
                    'current' => max(1, get_query_var('paged')),
                ]));
            }

            // layout carousel
            if ($settings['carousel']) {

                echo rt_before_slider([
                    'id' => "rt-swiper-{$settings['id']}",
                    'class' => implode(' ', $classes),
                    'items-lg' => $settings['slider_item'],
                    'items-md' => $settings['slider_item_tablet'],
                    'items-sm' => $settings['slider_item_mobile'],
                    'gap-lg' => $settings['slider_gap'],
                    'gap-md' => $settings['slider_gap_tablet'],
                    'gap-sm' => $settings['slider_gap_mobile'],
                    'loop' => $settings['slider_loop'],
                    'autoplay' => $settings['slider_auto_play'],
                    'sameheight' => $settings['slider_same_height'],
                    'navigation' => $settings['slider_nav'],
                ]);

                while ($the_query->have_posts()) : $the_query->the_post();
                    echo rt_html_open(['class' => 'swiper-slide']);
                    get_template_part($settings['template_part']);
                    echo rt_html_close();
                endwhile;

                // Link slider
                if ($settings['slider_link']) {
                    $link_url = esc_url_raw($settings['link_url']);
                    $link_text = $settings['link_text'];

                    echo rt_html_open(['class' => 'swiper-slide']);
                    echo "<a href='{$link_url}' class='swiper-link'>";
                    echo '<i class="rt-icon"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/></svg></i>';
                    echo "<span>{$link_text}</span>";
                    echo "</a>";
                    echo rt_html_close();
                }

                echo rt_after_slider();
            }

            wp_reset_postdata();
        else :
            do_action('rt_post_none');
        endif;
    }

    // loop form array or static content
    if ($settings['post_type'] === 'content') {
        if ($settings['content']) {

            // layout column
            if (!$settings['carousel']) {

                // class wrapper
                $classes[] = 'grids';
                $classes[] = 'grids-md-' . $settings['setting_column'];
                $classes[] = 'grids-sm-' . $settings['setting_column_tablet'];
                $classes[] = 'grids-xs-' . $settings['setting_column_mobile'];

                echo rt_html_open([
                    'id' => $settings['id'],
                    'class' => $classes,
                ]);

                foreach ($settings['content'] as $key => $content) {
                    rt_get_template_part($settings['template_part'], $content);
                }

                echo rt_html_close();
            }

            // layout carousel
            if ($settings['carousel']) {

                echo rt_before_slider([
                    'id' => "rt-swiper-{$settings['id']}",
                    'class' => implode(' ', $classes),
                    'items-lg' => $settings['slider_item'],
                    'items-md' => $settings['slider_item_tablet'],
                    'items-sm' => $settings['slider_item_mobile'],
                    'spaceBetween' => $settings['slider_gap'],
                    'loop' => $settings['slider_loop'],
                    'autoplay' => $settings['slider_auto_play'],
                    'sameheight' => $settings['slider_same_height'],
                    'navigation' => $settings['slider_nav'],
                ]);

                foreach ($settings['content'] as $key => $content) {
                    echo rt_html_open(['class' => 'swiper-slide']);
                    rt_get_template_part($settings['template_part'], $content);
                    echo rt_html_close();
                }
                echo rt_after_slider();
            }
        } else {
            do_action('rt_post_none');
        }
    }
}

/*=================================================
/* MAIN TEMPLATE
/*================================================= */
/**
 * this function get global template. get template part form template-parts folder
 *
 * @param [type] $content template location
 * @return void
 */
function rt_get_template($content, $element = array()) {

    $layout = wp_parse_args($element, [
        'wrapper' => true,
        'container' => true,
        'content' => true,
        'sidebar' => true,
        'loop' => true,
    ]);

    get_header();

    // Before wrapper
    if ($layout['wrapper']) {

        do_action('rt_before_wrapper');

        echo rt_html_open(['id' => 'page-wrapper', 'class' => 'page-wrapper'], 'section');
    }

    // Before container
    if ($layout['container']) {

        echo rt_html_open(['id' => 'page-container', 'class' => 'page-container']);

        echo rt_html_open(['class' => 'flex flex-row']);
    }

    // Before content
    if ($layout['content']) {

        do_action('rt_before_content');

        echo rt_html_open(['id' => 'page-content', 'class' => 'page-content']);
    }

    if ($layout['loop']) {

        if (have_posts()) :

            do_action('rt_before_loop');

            if ($content !== 'woocommerce') : // load file if not WooCommerce page

                while (have_posts()) : the_post();

                    rt_get_template_part($content);

                endwhile;

            else : // load file if WooCommerce  page

                while (woocommerce_content()) : the_post();

                    woocommerce_content();

                endwhile;

            endif;

            do_action('rt_after_loop');

        else :

            rt_get_template_part('page/page-none');

        endif;
    } else {
        rt_get_template_part('page/page-404');
    }

    // after content
    if ($layout['content']) {

        echo rt_html_close();

        do_action('rt_after_content');
    }

    // sidebar
    if ($layout['sidebar']) {
        do_action('rt_sidebar');
    }

    // after container
    if ($layout['container']) {

        echo rt_html_close();

        echo rt_html_close('section');
    }

    // after wrapper
    if ($layout['wrapper']) {

        echo rt_html_close();

        do_action('rt_after_wrapper');
    }

    get_footer();
}
