<?php
/*=================================================;
/* REMOVE DEFAULT WIDGETS
/*=================================================
 * Add your widget area to unset the default widgets from.
 * If your theme's first widget area is "sidebar-1", you don't need this.
 *
 * @source https://stackoverflow.com/questions/11757461/how-to-populate-widgets-on-sidebar-on-theme-activation
 *
 * @param  array $widget_areas Arguments for the sidebars_widgets widget areas.
 * @return array of arguments to update the sidebars_widgets option.
 */
function rt_impot_merlin_unset_default_widgets_args($widget_areas) {

    $widget_areas = array(
        'retheme_sidebar' => array(),
        'retheme_woocommerce_sidebar' => array(),
        'retheme_woocommerce_filter' => array(),
        'retheme_footer_1' => array(),
        'retheme_footer_2' => array(),
        'retheme_footer_3' => array(),
        'retheme_footer_4' => array(),
    );

    return $widget_areas;
}
add_filter('merlin_unset_default_widgets_args', 'rt_impot_merlin_unset_default_widgets_args');

/*=================================================;
/* DEMO IMPORT
/*================================================= */
function rt_import_files($import) {
    $key = uniqid();
    $templates = [];

    $default = [
        [
            'import_file_name' => 'Default',
            'categories' => ['WooCommerce'],
            'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/default/saudagar-demo-default.xml?key={$key}",
            'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/default/saudagar-demo-default.wie?key={$key}",
            'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/default/saudagar-demo-default.dat?key={$key}",
            'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/default/saudagar-demo-default.png?key={$key}",
            'preview_url' => "https://demo.saudagarwp.com?key={$key}",
        ],
    ];

    if (rt_is_premium()) {
        $templates = [
            [
                'import_file_name' => 'Cosmetic',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/cosmetic/saudagar-demo-cosmetic.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/cosmetic/saudagar-demo-cosmetic.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/cosmetic/saudagar-demo-cosmetic.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/cosmetic/saudagar-demo-cosmetic.png?key={$key}",
                'preview_url' => "https://cantik.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Gadget',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/gadget/saudagar-demo-gadget.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/gadget/saudagar-demo-gadget.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/gadget/saudagar-demo-gadget.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/gadget/saudagar-demo-gadget.png?key={$key}",
                'preview_url' => "https://elektra.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Organic',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/organic/saudagar-demo-organic.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/organic/saudagar-demo-organic.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/organic/saudagar-demo-organic.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/organic/saudagar-demo-organic.png?key={$key}",
                'preview_url' => "https://organic.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Grocery',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/grocery/saudagar-demo-grocery.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/grocery/saudagar-demo-grocery.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/grocery/saudagar-demo-grocery.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/grocery/saudagar-demo-grocery.png?key={$key}",
                'preview_url' => "https://grocery.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Luxury',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/luxury/saudagar-demo-luxury.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/luxury/saudagar-demo-luxury.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/luxury/saudagar-demo-luxury.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/luxury/saudagar-demo-luxury.png?key={$key}",
                'preview_url' => "https://luxury.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Book',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/book/saudagar-demo-book.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/book/saudagar-demo-book.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/book/saudagar-demo-book.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/book/saudagar-demo-book.png?key={$key}",
                'preview_url' => "https://book.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Furniture',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/furniture/saudagar-demo-furniture.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/furniture/saudagar-demo-furniture.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/furniture/saudagar-demo-furniture.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/furniture/saudagar-demo-furniture.png?key={$key}",
                'preview_url' => "https://furniture.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Digital',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/digital/saudagar-demo-digital.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/digital/saudagar-demo-digital.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/digital/saudagar-demo-digital.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/digital/saudagar-demo-digital.png?key={$key}",
                'preview_url' => "https://digital.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Kuliner',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/kuliner/saudagar-demo-kuliner.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/kuliner/saudagar-demo-kuliner.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/kuliner/saudagar-demo-kuliner.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/kuliner/saudagar-demo-kuliner.png?key={$key}",
                'preview_url' => "https://kuliner.saudagarwp.com?key={$key}",
            ],
            [
                'import_file_name' => 'Travel',
                'categories' => ['WooCommerce'],
                'import_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/travel/saudagar-demo-travel.xml?key={$key}",
                'import_widget_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/travel/saudagar-demo-travel.wie?key={$key}",
                'import_customizer_file_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/travel/saudagar-demo-travel.dat?key={$key}",
                'import_preview_image_url' => "https://webforia.id/wp-content/uploads/webforia/saudagarwp/travel/saudagar-demo-travel.png?key={$key}",
                'preview_url' => "https://travel.saudagarwp.com?key={$key}",
            ],
        ];
    }

    $import = array_merge($default, $templates);
    return $import;
}
add_filter('merlin_import_files', 'rt_import_files');

/*=================================================;
/* BACKUP THEME MODS BEFORE IMPORT
/*================================================= */
function rt_backup_theme_mods_before_import() {
    $date = date("Y-m-d");
    $rand = uniqid();

    update_option("_backup_saudagarwp_theme_mods_{$date}_{$rand}", get_option('theme_mods_saudagarwp'));
}
add_action('merlin_widget_importer_before_widgets_import', 'rt_backup_theme_mods_before_import');

/*=================================================;
/* SET DEFAULT AFTER IMPORT
/*================================================= */
function rt_set_after_all_import($import_index) {

    // Assign menu
    set_theme_mod('nav_menu_locations', [
        'primary' => get_term_by('name', 'Menu Primary', 'nav_menu')->term_id,
        'secondary' => get_term_by('name', 'Menu Secondary', 'nav_menu')->term_id,
        'tertiary' => get_term_by('name', 'Menu Tertiary', 'nav_menu')->term_id,
        'mobile' => get_term_by('name', 'Menu Mobile', 'nav_menu')->term_id,
    ]);

    // Set homepage and blog
    update_option('show_on_front', 'page');
    update_option('page_on_front', get_page_by_path('home')->ID);
    update_option('page_for_posts', get_page_by_path('blog')->ID);

    // Set WooCommerce Page
    update_option('woocommerce_shop_page_id', get_page_by_path('produk')->ID);
    update_option('woocommerce_cart_page_id', get_page_by_path('keranjang-belanja')->ID);
    update_option('woocommerce_checkout_page_id', get_page_by_path('checkout')->ID);
    update_option('woocommerce_myaccount_page_id', get_page_by_path('akun')->ID);

    // Set template home and checkout
    update_post_meta(get_page_by_path('home')->ID, '_wp_page_template', 'templates/front-page.php');
    update_post_meta(get_page_by_path('checkout')->ID, '_wp_page_template', 'templates/focus.php');

    // Set limit login
    update_option('limit_login_allowed_retries', 10);
    update_option('limit_login_allowed_lockouts', 10);

    // Replace WooCommerce Setting after import
    $requireds = array(
        'WPLANG' => 'id_ID',
        'date_format' => 'd/m/Y',
        'time_format' => 'g:i a',
        'woocommerce_allowed_countries' => 'specific',
        'woocommerce_specific_allowed_countries' => array('ID'),
        'woocommerce_ship_to_countries' => 'specific',
        'woocommerce_specific_ship_to_countries' => array('ID'),
        'woocommerce_default_country' => 'ID:JK',
        'woocommerce_currency' => 'IDR',
        'woocommerce_price_thousand_sep' => ',',
        'woocommerce_permalinks' => [
            'product_base' => '/produk',
            'category_base' => 'kategori-produk',
            'tag_base' => 'tag-produk',
        ],
        'woocommerce_email_footer_text' => '{site_title}',
        'woocommerce_currency_pos' => 'left',
        'woocommerce_price_thousand_sep' => ',',
        'woocommerce_price_decimal_sep' => '.',
        'woocommerce_price_num_decimals' => '0',
        'woocommerce_weight_unit' => 'g',
        'posts_per_page' => 9,
        'elementor_experiment' => 'active',
        'woocommerce_coming_soon' => 'no',
    );

    foreach ($requireds as $key => $setting) {
        update_option($key, $setting);
    }

    // Fill empty WooCommerce Setting after import
    $optionals = array(
        'woocommerce_bacs_settings' => [
            'enabled' => 'yes',
            'title' => 'Transfer Bank Manual',
            'description' => 'Lakukan pembayaran Anda langsung ke rekening bank kami, nomer Rekening akan ditampilkan setelah proses ini. Pesanan Anda akan dikirim setelah kami menerima pembayaran.',
            'instructions' => wp_sprintf("Anda telah memilih untuk membayar menggunakan transfer bank. Silakan menyelesaikan pembayaran melalui salah satu rekening kami. Klik <a href='%s'>disini</a> untuk melakukan konfirmasi pembayaran. Pesanan Anda tidak akan dikirim sampai kami menerima pembayaran.", site_url() . "/konfirmasi-pembayaran"),
        ],
        'woocommerce_bacs_accounts' => [
            [
                'account_name' => 'Budi Budiman',
                'account_number' => '123-456-789',
                'bank_name' => 'Bank Mandiri',
            ],
            [
                'account_name' => 'Budi Budiman',
                'account_number' => '123-456-789',
                'bank_name' => 'BCA',
            ],
        ],
        'ongkoskirim_id_store_city_id' => 173,
        'updraft_interval' => 'weekly',
        'updraft_retain' => 1,
        'updraft_interval_database' => 'daily',
    );

    foreach ($optionals as $key => $setting) {
        if (empty(update_option($key, $setting))) {
            update_option($key, $setting);
        }
    }

    // Disable auto installer plugin from hosting
    deactivate_plugins('/litespeed-cache/litespeed-cache.php', true);
    deactivate_plugins('/speedycache/speedycache.php', true);
    deactivate_plugins('/pagelayer/pagelayer.php', true);
    deactivate_plugins('/wp-super-cache/wp-cache.php', true);
    deactivate_plugins('/jetpack/jetpack.php', true);

    // Disable plugin ongkoskirim for digital shop
    if (in_array($import_index, [8, 10])) {
        deactivate_plugins('/ongkoskirim-id/ongkoskirim-id.php', true);
    }

    // disable another address for digital product
    if ($import_index === 8) {
        update_option('woocommerce_ship_to_destination', 'billing_only');
    }

    // Replace block checkout with classic checkout shortcode
    $page = get_page_by_path('checkout');

    if ($page) {
        // Update page
        wp_update_post([
            'ID'           => $page->ID,
            'post_content' => '[woocommerce_checkout]',
        ]);
    } else {
        return null;
    }
}
add_action('merlin_after_all_import', 'rt_set_after_all_import');

/*=================================================;
/* SET PERMALINK
/*================================================= */
function rt_demo_set_permalink() {
    // Set Permalink
    if (is_admin() && isset($_GET['page']) && $_GET['page'] === 'merlin') {
        global $wp_rewrite;
        $wp_rewrite->set_permalink_structure('/%postname%/');
        $wp_rewrite->flush_rules();
    }
}
add_action('init', 'rt_demo_set_permalink');

/*=================================================;
/* DISABLE WOOCOMMERCE WIZARD
/*================================================= */
add_filter('woocommerce_enable_setup_wizard', '__return_false');
