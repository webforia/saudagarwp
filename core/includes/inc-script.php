<?php
/*=================================================;
/* REGISTER ASSETS
/*================================================= */
function rt_scripts() {

    // Swiper Js
    wp_enqueue_style('swiper', get_template_directory_uri() . '/assets/swiper/v8/swiper-bundle.min.css', false, '8.4.5');
    wp_enqueue_script('swiper', get_template_directory_uri() . '/assets/swiper/v8/swiper-bundle.min.js', false, '8.4.5', true);
    wp_enqueue_script('retheme-swiper', get_template_directory_uri() . '/assets/swiper/v8/retheme-swiper.min.js', ['swiper'], '4.1.0', true);

    // animate css
    wp_enqueue_style('animate', get_template_directory_uri() . '/assets/css/animate.min.css', false, '4.1.2');
    wp_enqueue_script('animate', get_template_directory_uri() . '/assets/js/animate.min.js', false, '1.6.0', true);

    // Masonry
    if (rt_option('blog_archive_masonry', false) || rt_option('woocommerce_archive_masonry', false)) {
        wp_enqueue_style('retheme-masonry', get_template_directory_uri() . '/assets/css/masonry.min.css', false, '1.0.0');
        wp_enqueue_script('masonry', get_template_directory_uri() . '/assets/js/masonry.pkgd.min.js', false, '4.2.2', true);
        wp_enqueue_script('retheme-masonry', get_template_directory_uri() . '/assets/js/retheme-masonry.min.js', ['masonry'], '1.0.0', true);
    }

    // Countdown Timer
    if (rt_option('countdown_timer_shop', true)) {
        wp_enqueue_script('retheme-countdown', get_template_directory_uri() . '/assets/js/countdown.min.js', false, '2.0.0', true);
    }

    // Retheme Base
    wp_enqueue_script('retheme-helper', get_template_directory_uri() . '/assets/js/helper.min.js', false, '1.0.0', true);

    wp_enqueue_style('retheme-global-style', get_template_directory_uri() . '/assets/css/global-style.min.css', false, '1.0.0');
    wp_enqueue_style('retheme', get_template_directory_uri() . '/assets/css/retheme.min.css', false, '5.7.0');

    wp_enqueue_script('retheme-accordion', get_template_directory_uri() . '/assets/js/main/accordion.min.js', ['animate'], '2.0.0', true);
    wp_enqueue_script('retheme-form', get_template_directory_uri() . '/assets/js/main/form.min.js', ['animate'], '2.0.0', true);
    wp_enqueue_script('retheme-menu', get_template_directory_uri() . '/assets/js/main/menu.min.js', ['animate'], '2.0.0', true);
    wp_enqueue_script('retheme-modal', get_template_directory_uri() . '/assets/js/main/modal.min.js', ['animate'], '2.0.0', true);
    wp_enqueue_script('retheme-sidepanel', get_template_directory_uri() . '/assets/js/main/sidepanel.min.js', ['animate'], '2.0.0', true);
    wp_enqueue_script('retheme-tab', get_template_directory_uri() . '/assets/js/main/tab.min.js', ['animate'], '2.0.0', true);

    if (rt_option('tweak_responsive_embed', true)) {
        wp_enqueue_script('retheme-embed', get_template_directory_uri() . '/assets/js/main/embed.min.js', false, '1.0.0', true);
    }

    wp_enqueue_script('retheme', get_template_directory_uri() . '/assets/js/main/main.min.js', ['animate'], '4.1.0', true);

    // Comment
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_style('retheme-comment', get_template_directory_uri() . '/assets/css/comment.min.css', false, '1.0.0');
        wp_enqueue_script('retheme-comment', get_template_directory_uri() . '/assets/js/main/comment.min.js', ['animate'], '2.0.0', true);
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'rt_scripts', 99999);


/*=================================================;
/* MOVE JQUERY IN FOOTER
/*================================================= */
function rt_script_move_jquery_into_footer($wp_scripts) {

    if (is_admin()) {
        return;
    }

    if (rt_option('move_jquery_to_footer', true)) {
        $wp_scripts->add_data('jquery', 'group', 1);
        $wp_scripts->add_data('jquery-core', 'group', 1);
        $wp_scripts->add_data('jquery-migrate', 'group', 1);
    }
}
add_action('wp_default_scripts', 'rt_script_move_jquery_into_footer');

/*=================================================;
/* REMOVE EMOJI
/*================================================= */
function rt_script_remove_wp_emoji() {
    if (is_admin()) {
        return;
    }
    if (rt_option('remove_wp_emoji', true)) {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
    }
}
add_action('after_setup_theme', 'rt_script_remove_wp_emoji');

/*=================================================;
/* DISABLE GUTENBERG BLOCK STYLE ON FRONTEND
/*================================================= */
function rt_script_remove_block_css() {
    if (is_admin()) {
        return;
    }

    // Global
    if (rt_option('remove_block_style', true)) {
        wp_dequeue_style('wp-block-library'); // Wordpress core
        wp_dequeue_style('wp-block-library-theme'); // Wordpress core
        wp_dequeue_style('global-styles'); // Remove global Gutenberg style inline
    }

    // WooCommerce
    if (rt_option('remove_wc_block_style', true)) {
        wp_dequeue_style('wc-blocks-style');
    }
}
add_action('wp_enqueue_scripts', 'rt_script_remove_block_css', 100);

/*=================================================;
/* DISABLE ELEMENTOR GOOGLE FONTS
/*================================================= */
function rt_remove_elementor_google_fonts() {
    if (rt_option('remove_elementor_google_font', false)) {
        add_filter('elementor/frontend/print_google_fonts', '__return_false');
    }
}
add_action('init', 'rt_remove_elementor_google_fonts');

/*=================================================;
/* DISABLE
/*================================================= */
function rt_remove_elementor_icons() {

    if (rt_option('remove_elementor_icon', false)) {
        wp_dequeue_style('elementor-icons');
    }

    if (rt_option('remove_elementor_fontawesome', false)) {
        wp_dequeue_style('font-awesome');
    }
}
add_action('wp_enqueue_scripts', 'rt_remove_elementor_icons', 20);
