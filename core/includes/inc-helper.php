<?php

/*=================================================;
/* TEMPLATE PART
/*================================================= 
* Get template from template-parts folder
*/
function rt_get_template_part($template, $args = '', $name = '')
{
    get_template_part("template-parts/{$template}", $name, $args);

}

/*=================================================
 *  LIMIT STRING
/*================================================= */
/*
 * @the_excerpt paragraf
 * @excerpt_length limit number
 * return string
 */
function rt_limited_string($the_excerpt, $excerpt_length)
{
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt));
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if (count($words) > $excerpt_length):
        array_pop($words);
        $the_excerpt = implode(' ', $words);
        $the_excerpt .= '...';
    endif;

    return $the_excerpt;
}


/*=================================================;
/* PASSED URL GET METHOD
/*================================================= */
function rt_request_get($url)
{
    return !empty($_GET[$url]) ? $_GET[$url] : '';
}
/*=================================================;
/* PASSED URL GET METHOD
/*================================================= */
function rt_request_post($url)
{
    return !empty($_POST[$url]) ? $_POST[$url] : '';
}

/*=================================================;
/* PASSED URL GET METHOD
/*================================================= */
function rt_validated_value($value)
{
    return !empty($value) ? $value : '';
}

/*=================================================;
/* ANIMATE REVERSE
/*================================================= 
 * 
 * @param [type] $animate
 * @return string
 * @example fadeIn this function return fadeOut
 */
function rt_animate_reverse($animate)
{
    return str_replace('In', 'Out', $animate);
}