<?php

/**
 * retheme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package retheme
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rt_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on retheme, use a find and replace
     * to change 'retheme' to the name of your theme in all the template files.
     */
    load_theme_textdomain('saudagarwp', get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    // Gutenberg
    // @see https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-support/
    add_theme_support('wp-block-styles');
    add_theme_support('align-wide');
    add_theme_support('responsive-embeds');
    add_theme_support('editor-styles');

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @see https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('post-thumbnails');
    add_theme_support('html5', [
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'script',
        'style',
    ]);

    /**
     * Add theme support vendor breadcrumbs
     * @see https://themehybrid.com/docs/breadcrumb-trail
     */
    add_theme_support('breadcrumb-trail');

    if (!isset($content_width)) {
        $content_width = 1170;
    }


    /**
     * Add theme support selective refresh for customizer
     */
    add_theme_support('customize-selective-refresh-widgets');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus([
        'primary' => 'Menu Primary',
        'secondary' => 'Menu Secondary',
        'tertiary' => 'Menu Tertiary',
        'mobile' => 'Menu Mobile',
    ]);

    /**
     * Add custom image size
     */
    add_image_size('featured_medium', 400, 225, true);

    if (rt_is_premium()) {
        do_action('rt_after_setup_theme');

        update_option('webforia-whatsapp-chat_status', 'extended');
        update_option('webforia-wa-chat_status', 'extended');
    }

    add_filter('retheme_setup', '__return_true');
}
add_action('after_setup_theme', 'rt_setup');

/*=================================================;
/* THEME - BRAND META
/*================================================= */
/**
 * Add theme color brand on search board mobile browser
 */
function rt_theme_color() {

    $color = rt_option('brand_color_browser');

    if (!empty($color)) {
        echo '<meta name="theme-color" content="' . $color . '">';
    }
}
add_action('wp_head', 'rt_theme_color');

/*=================================================;
/* THEME - CHECK VERSION
/*================================================= */
function rt_check_theme_version() {
    $current_version = wp_get_theme()->get('Version');
    $old_version = get_option(RT_THEME_SLUG . '_theme_old_version');

    if ($old_version !== $current_version) {
        // update not to run twice
        update_option(RT_THEME_SLUG . '_theme_old_version', $current_version);
    }
}
add_action('after_setup_theme', 'rt_check_theme_version');

/*=================================================;
/* BODY CLASS
/*================================================= */
function rt_theme_body_class($classes) {

    $classes[] = 'retheme retheme-root';

    if (rt_is_premium()) {
        $classes[] = 'pro';
    }

    // add class device on body
    if (rt_is_only_mobile()) {
        $classes[] = 'is-mobile';
    } elseif (rt_is_only_tablet()) {
        $classes[] = 'is-tablet';
    } else {
        $classes[] = 'is-desktop';
    }

    return $classes;
}
add_filter('body_class', 'rt_theme_body_class');

/*=================================================;
/* REQUIRED PLUGIN
/*================================================= */
/**
 * Register the required plugins for this theme.
 */
function rt_theme_required_plugins() {
    // Get type user
    $premium = new Retheme\Activation;

    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins_global = [
        [
            'name' => 'Classic Editor',
            'slug' => 'classic-editor',
            'required'  => false,
        ],
        [
            'name' => 'WooCommerce',
            'slug' => 'woocommerce',
            'required'  => true,
        ],
        [
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'required'  => false,
        ],
        [
            'name' => 'Ongkos Kirim',
            'slug' => 'ongkoskirim-id',
            'source' => 'https://webforia.id/wp-content/uploads/webforia/ongkoskirim-id.zip',
            'required'  => false,
        ],
        [
            'name' => 'Loco Translate',
            'slug' => 'loco-translate',
            'required'  => false,
        ],
        [
            'name' => 'Admin Language Change',
            'slug' => 'simple-admin-language-change',
            'required'  => false,
        ],
        [
            'name' => 'Backup - UpdraftPlus',
            'slug' => 'updraftplus',
            'required'  => false,
        ],
        [
            'name' => 'Limit Login Attempts',
            'slug' => 'limit-login-attempts-reloaded',
            'required'  => false,
        ],
        [
            'name' => 'Webforia Checkout Fields',
            'slug' => 'webforia-checkout-field',
            'source' => 'https://webforia.id/wp-content/uploads/webforia/webforia-checkout-field.zip',
        ],
    ];

    $plugins_premium = array();

    if (rt_is_premium() && !$premium->is_product_variation('basic') && !$premium->is_product_variation('trial')) {
        $plugins_premium = [
            [
                'name' => 'Webforia Shop Booster',
                'slug' => 'webforia-shop-booster',
                'source' => 'https://webforia.id/wp-content/uploads/webforia/webforia-shop-booster-v2.zip',
            ],
            [
                'name' => 'Webforia Whatsapp Chat',
                'slug' => 'webforia-whatsapp-chat',
                'source' => 'https://webforia.id/wp-content/uploads/webforia/webforia-whatsapp-chat.zip',
            ]
        ];
    }

    $plugins_old = array();

    
    if (rt_is_premium() && !$premium->is_product_variation('basic') && !$premium->is_local() && $premium->get_date_create() < strtotime('2020-07-06')) {
        $plugins_old = [
            [
                'name' => 'Astro Element',
                'slug' => 'astro-element',
                'source' => 'https://webforia.id/wp-content/uploads/webforia/astro-element.zip',
            ],
            [
                'name' => 'Webforia User Price',
                'slug' => 'webforia-user-price',
                'source' => 'https://webforia.id/wp-content/uploads/webforia/webforia-user-price.zip',
            ]
        ];
    }

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    tgmpa(array_merge($plugins_global, $plugins_premium, $plugins_old), [
        'id' => 'rt_tgmpa', // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug' => 'theme-panel', // Parent menu slug.
        'capability' => 'edit_theme_options', // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => true, // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.
    ]);
}
add_action('tgmpa_register', 'rt_theme_required_plugins');

/*=================================================;
/* DEFAUT VARIABLE
/*================================================= */
function rt_get_theme($value, $arg = '') {

    $default = apply_filters('rt_setup_theme', [
        'typography_heading' => [
            'font-family' => 'Inter',
            'line-height' => '1.2',
            'variant' => '600',
            'text-transform' => 'none',
        ],
        'typography_regular' => [
            'font-family' => 'Inter',
            'font-size' => '14px',
            'line-height' => '1.75',
        ],
        'global_brand_color' => [
            'primary' => '#5733ce',
            'secondary' => '#5136d8',
        ],
        'global_font_color' => [
            'primary' => '#333333',
            'secondary' => '#7a7a7a',
            'tertiary' => '#999999',
        ],
        'global_color_link' => [
            'normal' => '#189dee',
            'hover' => '#189dee',
        ],
        'global_background_scheme' => [
            'primary' => '#fff',
            'secondary' => '#f9f9f9',
        ],
        'global_border_color' => '#ededed',
        'global_highlight_color' => '#fd704f',
        'global_font_light_color' => [
            'primary' => '#eaedf0',
            'secondary' => 'rgba(255,255,255,0.65)',
        ],
        'global_background_dark' => '#1c1c1f',
        'global_color_action' => '#069e08',

        'header_builder_option' => rt_option('header_builder_option', [
            'topbar_left_alignment' => 'left',
            'topbar_center_alignment' => 'center',
            'topbar_right_alignment' => 'right',
            'topbar_left_display' => 'normal',
            'topbar_center_display' => 'normal',
            'topbar_right_display' => 'normal',

            'middle_left_alignment' => 'left',
            'middle_center_alignment' => 'center',
            'middle_right_alignment' => 'right',
            'middle_left_display' => 'normal',
            'middle_center_display' => 'normal',
            'middle_right_display' => 'normal',

            'main_left_alignment' => 'left',
            'main_center_alignment' => 'center',
            'main_right_alignment' => 'right',
            'main_left_display' => 'grow',
            'main_center_display' => 'normal',
            'main_right_display' => 'grow',

            'sticky_left_alignment' => 'left',
            'sticky_center_alignment' => 'center',
            'sticky_right_alignment' => 'right',
            'sticky_left_display' => 'normal',
            'sticky_center_display' => 'normal',
            'sticky_right_display' => 'normal',

            'mobile_left_alignment' => 'left',
            'mobile_center_alignment' => 'center',
            'mobile_right_alignment' => 'right',
            'mobile_left_display' => 'normal',
            'mobile_center_display' => 'normal',
            'mobile_right_display' => 'normal',

            'main_left_element' => ['logo'],
            'main_center_element' => ['menu-primary'],
            'main_right_element' => ['search-icon', 'user-icon', 'cart-icon'],

            'sticky_left_element' => ['logo'],
            'sticky_center_element' => ['menu-primary'],
            'sticky_right_element' => ['search-icon', 'user-icon', 'cart-icon'],

            'middle_left_element' => [],
            'middle_center_element' => [],
            'middle_right_element' => [],

            'topbar_left_element' => ['html-1'],
            'topbar_center_element' => [],
            'topbar_right_element' => ['social'],

            'mobile_left_element' => ['logo-mobile'],
            'mobile_center_element' => [],
            'mobile_right_element' => ['toggle-menu'],

            'drawer_element' => ['search-form', 'menu-mobile'],
        ]),

    ]);

    return !empty($default[$value]) ? $default[$value] : '';
}
