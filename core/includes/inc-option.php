<?php
/*====================================================
/* GET OPTIONS
/*====================================================*/

/**
 * Retrieves a setting from various sources: metabox, option, or customizer.
 *
 * @param string $setting  The name of the setting to retrieve.
 * @param mixed  $default  The default value to return if setting not found.
 * @return mixed           The value of the setting.
 */
function rt_option($setting, $default = '') {

    // Get post meta
    $post_meta = get_post_meta(get_the_ID(), $setting, true);

    if (!empty($post_meta) && $post_meta !== 'default') {
        $option = $post_meta;
    } elseif (!empty(get_option($setting))) {
        $option = get_option($setting);
    } else {
        $option = get_theme_mod($setting, $default);
    }

    return apply_filters($setting, $option);
}


/*====================================================
/* THEME RUNNING
/*====================================================*/
/**
 * Check if user is running the product's Premium Version code
 *
 * @return bool true if on localhost or user has valid license
 */
function rt_is_premium() {
    $license = new Retheme\Activation();

    // Check if on localhost or in development mode
    if ($license->is_local() || apply_filters('retheme_dev', false)) {
        return true;
    } else {
        // Check if the user has a premium license
        return $license->is_premium();
    }
}

/*====================================================
/* UPDATE CHECKER
/*====================================================*/
function rt_update_checker(){
    $update = New \Retheme\Activation();
    $update->validate_license_status();
}
add_action('upgrader_process_complete', 'rt_update_checker');
add_action('after_switch_theme', 'rt_update_checker');
add_action('wp_update_themes', 'rt_update_checker');

/*=================================================;
/* MOBILE - DETECT
/*================================================= */
// mobile and tablet
function rt_is_mobile() {
    $detect = new Mobile_Detect;
    return $detect->isMobile() || $detect->isTablet();
}

// only mobile
function rt_is_only_mobile() {
    $detect = new Mobile_Detect;
    return $detect->isMobile() && !$detect->isTablet();
}

// only tablet
function rt_is_only_tablet() {
    $detect = new Mobile_Detect;
    return $detect->isTablet();
}
