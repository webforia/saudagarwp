<?php
/*=================================================
 * PAGE - TITLE
=================================================== */
function rt_get_page_title() {

    $title = get_the_title();

    if (is_home()) {
        $title = (get_option('show_on_front') === 'page') ? get_the_title(get_option('page_for_posts')) : __('Lastest Post', 'saudagarwp');
    }

    if (is_archive()) {
        $title = get_the_archive_title();
    }

    if (is_404()) {
        $title = __('404', 'saudagarwp');
    }

    if (rt_is_woocommerce('catalog')) {
        $title  = get_the_title(wc_get_page_id('shop'));
    }

    if (is_search()) {
        $title = sprintf(__('Search Results for: %s', 'saudagarwp'), get_search_query());
    }

    return apply_filters('rt_page_title', $title);
}

/*=================================================
 * PAGE - DESC
=================================================== */
function rt_get_page_desc() {
    $desc = '';
    if (is_archive()) {
        $desc = get_the_archive_description();
    }
    if (rt_is_woocommerce('shop')) {
        $desc = '';
    }
    return apply_filters('rt_page_desc', $desc);
}
