<?php
/*=================================================
 *  LAYOUT CLASS
/*================================================= */

/**
 * Add class in body each page for layout
 */
function rt_template_class($classes) {
    $content_layout = rt_content_layout();

    if (is_page_template('templates/full.php')) {
        $classes[] = 'template--builder-container';
    }

    // content layout
    $classes[] = "template--{$content_layout}";

    // guternberg
    if ($content_layout == 'normal' || $content_layout == 'compact') {
        $classes[] = "gutenberg-support";
    }

    // hidden sidebar shop on mobile
    if (rt_is_woocommerce('shop') && rt_option('woocommerce_mobile_sidebar', false) == false) {
        $classes[] = "page-aside-mobile-hidden";
    }

    return $classes;
}
add_filter('body_class', 'rt_template_class', 5);

/*=================================================
 *  HEADER
/*================================================= */

function rt_header() {
    if (is_customize_preview()) {
        echo "<div class='rt-header-customizer'>";
        rt_get_template_part('header/header');
        echo "</div>";
    } else if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('header')) {
        rt_get_template_part('header/header');
    }
}
add_action('rt_header', 'rt_header', 5);

/*=================================================
 *  PAGE TITLE
/*================================================= */
function rt_page_header() {
    if (rt_option('page_header', true) && rt_option('page_header_layout', 'in-content') == 'above' && is_page_template(['templates/front-page.php', 'templates/full.php']) != true) {
        rt_get_template_part('global/page-header');
    }
}
add_action('rt_header', 'rt_page_header', 10);


/*=================================================
 *  PAGE TITLE INSIDE CONTENT
/*================================================= */
function rt_page_header_inside() {
    if (rt_option('page_header', true) && rt_option('page_header_layout', 'in-content') == 'in-content' && is_page_template(['templates/front-page.php', 'templates/full.php']) != true) {
        rt_get_template_part('global/page-header');
    }
}
add_action('rt_before_loop', 'rt_page_header_inside', 5);

/*=================================================
 *  LOOP
/*================================================= */
function rt_before_loop() {
    if (is_archive() || is_home() || is_search()) {
        rt_get_template_part('global/loop-start');
    }
}
add_action('rt_before_loop', 'rt_before_loop', 25);

function rt_after_loop() {
    if (is_archive() || is_home() || is_search()) {
        rt_get_template_part('global/loop-end');
    }
}
add_action('rt_after_loop', 'rt_after_loop', 25);

/*=================================================;
/* PAGINATION ON ARCHIVE LOOP
/*================================================= */
function rt_pagination_archive() {
    global $wp_query;

    $query = get_queried_object();

    // Check taxonomy have archive
    if (!empty($query->slug)) {
        $query_by['tax_query'][] = array(
            'taxonomy' => $query->taxonomy,
            'field' => 'slug',
            'terms' => $query->slug,
            'operator' => 'IN',
        );
    }

    $query_by['target'] = "post_archive";
    $query_by['post_type'] = "post";
    $query_by['posts_per_page'] = rt_option('posts_per_page', 7);
    $query_by['template_part'] = 'template-parts/post/post';
    $query_by['pagination_style'] = rt_option('blog_archive_pagination', 'number');
    $query_by['total'] = $wp_query->max_num_pages;
    $query_by['post_total'] = $wp_query->found_posts;
    $query_by['format'] = '?paged=%#%';
    $query_by['current'] = max(1, get_query_var('paged'));

    echo rt_pagination(apply_filters('rt_loop_query', $query_by));
}
add_action('rt_after_loop', 'rt_pagination_archive',30);

/*=================================================
 *  CONTENT LAYOUT
/*=================================================
 * Get content layout
 */
function rt_content_layout() {

    $content_layout = 'sidebar-right';

    // post archive
    if (is_home() || is_archive()) {
        $content_layout = rt_option('blog_archive_layout', 'sidebar-right');
    }

    // single post
    if (is_single() || is_singular()) {
        $content_layout = rt_option('blog_single_layout', 'sidebar-right');
    }

    // page
    if (is_page() || is_404()) {
        $content_layout = rt_option('page_layout', 'sidebar-right');
    }

    // WooCommerce general page
    if (rt_is_woocommerce('account') || rt_is_woocommerce('cart') || rt_is_woocommerce('checkout')) {
        $content_layout = 'normal';
    }

    // Product archive
    if (rt_is_woocommerce('shop')) {
        $content_layout = rt_option('woocommerce_archive_layout', 'sidebar-right');
    }

    // Produck single layout
    if (rt_is_woocommerce('product')) {
        $content_layout = rt_option('woocommerce_single_layout', 'normal');
    }

    // homepage / frontpage builder
    if (is_page_template('templates/front-page.php')) {
        $content_layout = rt_option('homebuilder_content_layout', 'normal');
    }

    // Search page
    if (is_search()) {
        if (rt_option('search_main_result', 'product') == 'product') {
            $content_layout = rt_option('woocommerce_archive_layout', 'sidebar-right');
        } else {
            $content_layout = rt_option('blog_archive_layout', 'sidebar-right');
        }
    }

    // template page
    if(is_page_template(['templates/full.php', 'templates/no-sidebar.php']) ){
        $content_layout = 'normal';
    }


    // set sidebar filter
    return apply_filters('rt_content_layout', $content_layout);
}
/*=================================================
 *  SIDEBAR STATUS
/*=================================================
 * Added sidebar layout template
 * @return sidebar layout
 * */
function rt_sidebar() {
    if (rt_content_layout() == 'sidebar-left' || rt_content_layout() == 'sidebar-right') {
        if (rt_is_woocommerce('pages')) {
            get_sidebar('shop');
        } else {
            get_sidebar();
        }
    }
}
add_action('rt_sidebar', 'rt_sidebar', 5);

/*=================================================
 *  FOOTER
/*=================================================
 * Added footer layout template
 * Elementor support widget area
 * @return footer layout
 */
function rt_footer() {
    if (!function_exists('elementor_theme_do_location') || !elementor_theme_do_location('footer')) {
        rt_get_template_part('footer/footer');
    }
}
add_action('rt_footer', 'rt_footer', 5);

/*=================================================
 * GLOBAL TOP
=================================================== */
function rt_gotop() {
    rt_get_template_part('global/gotop');
}
add_action('rt_footer', 'rt_gotop', 10);

/*=================================================;
/* MOBILE DRAWER
/*================================================= */
/**
 * Added mobile menu of canvas
 *
 * @return html
 */
function rt_add_global_element() {
    if (rt_option('header_drawer_menu_style', 'dropdown') === 'sidepanel') {
        rt_get_template_part('header/header-drawer-panel');
    }

    if (rt_option('header_menu_off_canvas_menu', 'none') !== 'none') {
        rt_get_template_part('header/header-off-canvas');
    }
}
add_action('rt_footer', 'rt_add_global_element');