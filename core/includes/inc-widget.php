<?php

/*=================================================;
/* REGISTER WIDGET LOCATION
/*================================================= */
function rt_register_widget_location()
{

    register_sidebar(array(
        'name' => __('Sidebar', 'saudagarwp'),
        'id' => 'retheme_sidebar',
        'before_widget' => '<div id="%1$s" class="rt-widget rt-widget--aside %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ));

    if (class_exists('woocommerce')) {
        register_sidebar(array(
            'name' => __('WooCommerce Sidebar', 'saudagarwp'),
            'id' => 'retheme_woocommerce_sidebar',
            'before_widget' => '<div id="%1$s" class="rt-widget rt-widget--aside %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
            'after_title' => '</h4></div>',
        ));
    }

    register_sidebar(array(
        'name' => __('Footer Column 1', 'saudagarwp'),
        'id' => 'retheme_footer_1',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 2', 'saudagarwp'),
        'id' => 'retheme_footer_2',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 3', 'saudagarwp'),
        'id' => 'retheme_footer_3',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 4', 'saudagarwp'),
        'id' => 'retheme_footer_4',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 5', 'saudagarwp'),
        'id' => 'retheme_footer_5',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ));

    register_sidebar(array(
        'name' => __('Footer Column 6', 'saudagarwp'),
        'id' => 'retheme_footer_6',
        'before_widget' => '<div id ="%1$s" class="rt-widget rt-widget--footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="rt-widget__header"><h4 class="rt-widget__title">',
        'after_title' => '</h4></div>',
    ));

}
add_action('widgets_init', 'rt_register_widget_location');

/*=================================================;
/* SET SIZE TAG
/*================================================= */
function rt_tag_cloud_widget($args)
{
    $args['largest'] = 12;
    $args['smallest'] = 12;
    $args['unit'] = 'px';

    return $args;
}
add_filter('widget_tag_cloud_args', 'rt_tag_cloud_widget');

/*=================================================;
/* SET DEFAULT FOOTER WIDGET
/*================================================= */
function rt_footer_widget_item($widget_id = '')
{
    if (is_active_sidebar($widget_id)) {
        dynamic_sidebar($widget_id);
    }
}

/*=================================================;
/* WIDGET DEFAULT
/*================================================= */
function rt_dynamic_widget($widget_name)
{
    if (is_active_sidebar($widget_name)) {
        dynamic_sidebar($widget_name);
    }
}
