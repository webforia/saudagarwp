<?php
/*=================================================
* GET IMAGE ALL SIZE
=================================================== 
* add new image size
*/
function rt_get_image_sizes(){

    $image_size['featured_medium'] = 'featured_medium';

    foreach (get_intermediate_image_sizes() as $key => $size) {
        $image_size[$size] = $size;
    }
    return $image_size;
}


/*=================================================
 * IMAGE PLACE HOLDER
/*================================================= */
function rt_image_placeholder()
{
    if (rt_option('image_placeholder', true)) {
        $url = esc_url(get_template_directory_uri() . "/assets/img/placeholder.png");
        echo "<img src='{$url}' class='img-responsive' alt='image-placeholder'>";
    }
   
}

function rt_image_placeholder_url()
{
    if (rt_option('image_placeholder', true)) {
        echo esc_url(get_template_directory_uri() . "/assets/img/placeholder.png");
    }
}

/*=================================================
* ADD CUSTOM IMAGE SIZE ON IMAGE
=================================================== */
function rt_add_custom_image_sizes($image){

    $image[] = 'featured_medium';
    
    return $image;
}

add_filter('intermediate_image_sizes', 'rt_add_custom_image_sizes');