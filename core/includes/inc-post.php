<?php
/*=================================================;
/* GET ARCHIVE ALL POST
/*================================================= */
/**
 * This function detect all archive on post type post
 * like category, tag, archive default
 *
 * @return boolean
 */
function rt_is_post_archive()
{
    if (is_archive() || is_home() || is_search()) {
        return true;
    }
    
}

/*=================================================;
/* POST ARCHIVE OPTION
/*================================================= */
function rt_post_option($value){
    $element = rt_option('blog_element', ['category', 'thumbnail', 'content', 'readmore']);
    $meta = rt_option('blog_meta', ['meta-author', 'meta-date', 'meta-comment']);

    return in_array($value, array_merge($element, $meta));
}

/*=================================================;
/* POST SINGLE OPTION
/*================================================= */
function rt_single_option($value)
{
    $element = rt_option('single_element', ['thumbnail', 'tag', 'authorbox', 'navigation', 'comment', 'related']);
    $meta = rt_option('single_meta', ['meta-author', 'meta-date', 'meta-category']);

    return in_array($value, array_merge($element, $meta));

}


/*=================================================
 *  POST WRAPPER CLASS
/*================================================= */
/**
 * Added class to before loop class post
 * Setting column layout on post archive
 * Create filter rt_post_loop_class
 * 
 * @param string $classes
 * @return void
 */
function rt_post_loop_class()
{

    $classes[] = 'rt-post-archive grids';

    if (rt_option('blog_archive_style', 'grid') == 'grid') {

        $classes[] = "grids-md-".rt_option('blog_archive_column', 3);
        $classes[] = "grids-sm-".rt_option('blog_archive_column_tablet', 3);
        $classes[] = "grids-xs-".rt_option('blog_archive_column_mobile', 2);

        /** Masonry layout enable */
        if (rt_option('blog_archive_masonry', false)) {
            $classes[] = 'grid-masonry js-masonry';
        }

    } else{
        $classes[] = 'grids-md-1';
    }

    // Separates classes with a single space, collates classes for post DIV.
    echo 'class="' . esc_attr( implode(' ', apply_filters( 'rt_post_loop_class', $classes ) ) ) . '"';

}

/*=================================================
 *  BODY CLASS
/*================================================= */
function rt_post_body_class($classes)
{
    if (is_singular('post')) {
        $classes[] = 'template--single';
    }
    if (rt_is_post_archive()) {
        $classes[] = 'post-archive';
    }

    return $classes;
}
add_filter('body_class', 'rt_post_body_class');

/*=================================================;
/* LOOPS CLASS
/*=================================================
 * Added custom class on items loop all CPT
 */
function rt_post_class($classes){
    $classes[] = 'post-item';
    return $classes;
}
add_filter('post_class', 'rt_post_class');

/*=================================================;
/* ARCHIVE - REMOVE PREFIX PAGE TITLE
/*=================================================
 * remove prefix archive on category
 */
function rt_archive_remove_prefix_title($title)
{
    if (is_category() || is_tag()) {
        $title = single_cat_title('', false);
    }
    return $title;

}
add_filter('get_the_archive_title', 'rt_archive_remove_prefix_title');

/*=================================================;
/* SINGLE - REMOVE PAGE HEADER
/*================================================= */
/**
 * remove global page header on single post
 *
 * @param [type] $args
 * @return void
 */
function rt_single_remove_page_header($args)
{
    if (is_singular('post')) {
        return false;
    }
    return $args;

}
add_filter('page_header', 'rt_single_remove_page_header');

