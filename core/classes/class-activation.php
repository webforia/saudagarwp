<?php

/**
 * Activation Manager
 *
 * @package     Retheme
 * @category    Core
 * @author      Webforia Studio
 * @version     4.1.0
 */

namespace Retheme;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

// No need to proceed if this class already exists.
if (!class_exists('Retheme\Activation')) {

    class Activation {
        public $product_name = '';
        public $product_slug = '';
        public $product_key = '';
        public $product_ref = '';
        public $domain = '';
        public $product_extends = array();

        public function __construct($args = array()) {
            $this->product_name = RT_THEME_NAME;
            $this->product_slug = RT_THEME_SLUG;
            $this->product_key = !empty($args['product_key']) ? $args['product_key'] : get_option("{$this->product_slug}_key");
            $this->domain = preg_replace('#^https?://(?:www\.)?#', '', site_url());

            $this->product_extends = [
                'webforia-shop-booster',
                'webforia-whatsapp-chat',
                'webforia-wa-chat',
                'webforia-user-price',
                'webforia-checkout-field',
                'astro-element',
            ];
        }

        /**
         * Handles license management with the server.
         *
         * Sends a request to the license manager server without cache.
         *
         * @param string $action The action to perform (e.g., activate, deactivate).
         * @return object|false JSON response object on success, false on failure.
         */
        public function license_manager($action) {

            // request serverhost without cache
            $serverhost = "https://webforia.id/?time=" . time();
            $secret_key = "5c0a2dd7949698.97710041s";

            // API query parameters
            $api_params['slm_action'] = "slm_{$action}";
            $api_params['secret_key'] = $secret_key;
            $api_params['license_key'] = $this->product_key;
            $api_params['registered_domain'] = $this->domain;
            $api_params['item_reference'] = $this->product_name;

            // Build query URL
            $query = esc_url_raw(add_query_arg($api_params, $serverhost));

            // Send request to the license manager server
            $response = wp_remote_get($query, [
                'timeout'   => 100,
                'sslverify' => false,
            ]);

            // Check for errors
            if (is_wp_error($response)) {
                return false;
            }

            // Decode and return JSON response
            return json_decode(wp_remote_retrieve_body($response));
        }

        /**
         * Get the maximum allowed domains for a license.
         *
         * @param object|null $license The license object.
         * @return int The number of max allowed domains, or 0 if unavailable.
         */
        public function max_allowed_domains($license = '') {
            if (is_object($license) && !empty($license->max_allowed_domains)) {
                return (int) $license->max_allowed_domains;
            }

            return 0;
        }

        /**
         * Gets the registered domains for the license.
         *
         * @return array The registered domains.
         */
        public function registered_domains($license) {

            if (is_object($license) && !empty($license->registered_domains)) {
                return array_map(function ($domain) {
                    return $domain->registered_domain;
                }, $license->registered_domains);
            }

            return [];
        }

        /**
         * Check if a domain is active on the license.
         *
         * @param string $site_url The domain to check.
         * @return bool True if the domain is registered, otherwise false.
         */
        public function has_domain($license) {
            $site_url = $this->domain;

            // Get registered domains and ensure it's an array
            $registered_domains = $this->registered_domains($license);

            if (!is_array($registered_domains)) {
                return false;
            }

            return in_array($site_url, $registered_domains, true);
        }

        /**
         * Get license status from option db
         * Possible values: active, unregistered, expired, trial.
         * 
         * @return string
         */
        public function get_status() {

            if (!is_string($this->product_slug) || empty($this->product_slug)) {
                return 'Unregistered';
            }

            $product_status = "{$this->product_slug}_status";

            $status = get_option($product_status, 'Unregistered');

            return is_string($status) ? $status : 'Unregistered';
        }

        /**
         * Check if user is only running the product’s Premium Version code
         * 
         * @return bool True if premium version is active, otherwise false.
         */
        public function is_premium() {
            if (in_array($this->get_status(), ['active', 'trial', 'expired']) || $this->is_local()) {
                return true;
            }

            return false;
        }

        /**
         * Check if the license contains the specified product.
         *
         * @param object|null $license The license object.
         * @return bool True if the product exists in the license, false otherwise.
         */
        public function has_product($license) {
            if (is_object($license) && !empty($license->product_ref)) {
                $keywords = ['saudagar', 'saudagar wp', 'saudagarwp', $this->product_slug];
                foreach ($keywords as $keyword) {
                    if (stripos($license->product_ref, $keyword) !== false) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Check if the given variation matches the product variation option.
         *
         * @param string $variation The variation to check.
         * @return bool True if it matches, false otherwise.
         */
        public function is_product_variation($variation) {
            $product_variation = "{$this->product_slug}_product_variation";

            return get_option($product_variation) === $variation;
        }

        /**
         * Set license attributes and update relevant WordPress options.
         *
         * This function stores license details such as status, expiration date, and domain registration.
         * It also handles different license types, including trial and expired licenses.
         *
         * @param object $license License object containing license details.
         */
        public function store_license_data($license) {
            // Define option keys for storing license data
            $product_status = "{$this->product_slug}_status";
            $product_key = "{$this->product_slug}_key";
            $product_date_expired = "{$this->product_slug}_date_expired";
            $product_date_created = "{$this->product_slug}_date_created";
            $product_date_renewed = "{$this->product_slug}_date_renewed";
            $product_variation = "{$this->product_slug}_product_variation";
            $product_update = "{$this->product_slug}_product_update";

            // Store license details in WordPress options
            if ($this->get_status() !== 'trial' && $this->get_status() !== 'trial expired') {
                update_option($product_status, 'active');
                update_option($product_update, true);
                update_option($product_key, $this->product_key);
                update_option($product_date_created, $license->date_created);
                update_option($product_date_renewed, $license->date_renewed);
                update_option($product_date_expired, $license->date_expiry);
            }


            // Extract product variation from product reference
            $product_slug = str_replace(" ", "", strtolower($license->product_ref));
            $variation = explode("-", $product_slug);

            // Store product variation if available
            if (isset($variation[1])) {
                update_option($product_variation, trim($variation[1]));
            }

            // Handle trial license status
            if ($this->is_product_variation('trial')) {
                update_option($product_status, 'trial');
            }

            // Handle expired license status
            if ($this->get_expired_date_duration() < 1) {
                update_option($product_status, 'expired');
                update_option($product_update, false);
            }

            // Handle trial expiration status
            if ($this->is_product_variation('trial') && $this->get_expired_date_duration() < 1) {
                update_option($product_status, 'trial expired');
                update_option($product_update, false);
            }

            // Activate bundled extended plugins
            if (!empty($this->product_extends) && is_array($this->product_extends)) {
                foreach ($this->product_extends as $extend) {
                    update_option("{$extend}_status", 'extended');
                    update_option("{$extend}_key", $this->product_key);
                    update_option("{$extend}_product_update", true);
                }
            }
        }

        /**
         * Deactivate the license for the current domain.
         *
         * @return array|null Returns an array with the result and message if successful, null otherwise.
         */
        public function deactivate() {
            $license = $this->license_manager('check');

            if (empty($license)) {
                // Server connection issue
                $data['result'] = 'error';
                $data['message'] = __("Gagal terhubung ke server. Silakan periksa koneksi internet Anda dan coba kembali dalam 10 hingga 15 menit.", 'saudagarwp');
            } else {
                $license = $this->license_manager('deactivate');

                // Define option keys for product status, key, and domain registration
                $product_status = "{$this->product_slug}_status";
                $product_key = "{$this->product_slug}_key";
                $product_domain_register = "{$this->product_slug}_domain_register";

                // Update options to mark the product as unregistered.
                update_option($product_status, 'unregistered');
                update_option($product_domain_register, true);
                update_option($product_key, '');

                // Deactivate any extended bundled plugins associated with the product.
                if (!empty($this->product_extends) && is_array($this->product_extends)) {
                    foreach ($this->product_extends as $extend) {
                        update_option("{$extend}_status", 'unregistered');
                        update_option("{$extend}_key", '');
                        update_option("{$extend}_product_update", false);
                    }
                }
                // Return license deactivation result.
                $data['result'] = $license->result;
                $data['message'] = $license->message;
            }

            return $data;
        }

        /**
         * Activate the license for the current domain.
         *
         * @return array Result and message of the activation process.
         */
        public function activate() {
            $message = [
                'server_not_connect' => __("Gagal terhubung ke server. Silakan periksa koneksi internet Anda dan coba kembali dalam 10 hingga 15 menit.", 'saudagarwp'),
                'invalid_license_key' => wp_sprintf(__('Lisensi tidak ditemukan. Pastikan Anda memasukan kunci lisensi seperti yang tersedia di <a href="%s" target="_blank">Member Area</a>.', 'saudagarwp'), esc_url('https://webforia.id/akun')),
                'limit_domain' => wp_sprintf(__('Lisensi telah melebih batas domain. Lisensi telah digunakan di website lain! Silakan periksa daftar domain Anda di <a href="%s" target="_blank">Member Area</a>.', 'saudagarwp'), esc_url('https://webforia.id/akun')),
                'license_active' => __('Lisensi Anda telah berhasil diaktifkan. Akses ke update, support, dan fitur Premium kini telah diaktifkan.', 'saudagarwp'),
                'product_not_found' =>  wp_sprintf(__('Lisensi tidak valid atau tema <strong>%s</strong> tidak terdaftar untuk lisensi ini.'), $this->product_name),
            ];

            // Check license status from the server
            $license = $this->license_manager('check');

            if (empty($license)) {
                // Server connection issue
                $data['result'] = 'error';
                $data['message'] = $message['server_not_connect'];
            } elseif ($license->result === 'error') {
                // License system error
                $data['result'] = 'error';
                $data['message'] = ($license->message === 'Invalid license key') ? $message['invalid_license_key'] : $license->message;
            } elseif (!$this->has_product($license)) {
                // Product validation failed
                $data['result'] = 'error';
                $data['message'] = $message['product_not_found'];
            } elseif (count($this->registered_domains($license)) >= $this->max_allowed_domains($license) && !$this->has_domain($license)) {
                // License domain limit exceeded
                $data['result'] = 'error';
                $data['message'] = $message['limit_domain'];
            } else {

                // Activate the license
                $this->license_manager('activate');

                // Set the current domain as registered in the license
                update_option("{$this->product_slug}_domain_register", true);

                // Store the license data in WordPress options
                $this->store_license_data($license);

                // Return activation response
                $data['result'] = $license->result;
                $data['message'] = ($license->message === 'License key details retrieved.') ? $message['license_active'] : $license->message;
            }

            return $data;
        }

        /**
         * Date license create
         *
         * @param string $format Optional date format.
         * @return string Formatted date or empty string if not set.
         */
        public function get_date_create($format = '') {
            $date = strtotime(get_option("{$this->product_slug}_date_created"));

            if ($format) {
                $date = date("d F Y", $date);
            }

            return $date;
        }

        /**
         * Get the license expiration date.
         *
         * @param string $format Optional date format (default: "d F Y").
         * @return string Formatted date or empty string if not set.
         */
        public function get_expired_date($format = '') {
            $date = strtotime(get_option("{$this->product_slug}_date_expired"));

            if ($format) {
                $date = date("d F Y", $date);
            }

            return $date;
        }

        /**
         * Get the remaining days until the license expires.
         *
         * @return int Number of days left. Returns 0 if expired or invalid.
         */
        public function get_expired_date_duration() {

            $now = strtotime('now');
            $end = $this->get_expired_date();
            $number_days = ($end - $now) / (60 * 60 * 24);

            return round($number_days);
        }

        /**
         * Validate the license status and update the domain registration status.
         *
         * This function checks if the license is valid and updates the domain registration status accordingly.
         */
        public function validate_license_status() {

            $license = $this->license_manager('check');
            $product_domain_register = "{$this->product_slug}_domain_register";

            if (empty($license)) {
                return; // Stop execution if the license check fails.
            }

            if ($license->result === 'error' && $license->message === 'Invalid license key') {
                update_option($product_domain_register, false);
                return;
            }

            // Update domain registration status based on license validity
            if ($this->is_premium() && !$this->has_domain($license)) {
                update_option($product_domain_register, false);
            } else {
                update_option($product_domain_register, true);
            }

            // Update the retrieved license data
            if ($license->result !== 'error') {
                $this->store_license_data($license);
            }
        }

        /**
         * Check if the website is running on localhost, staging, or development
         *
         * @return bool True if localhost or staging, false if production
         */
        public function is_local() {
            if (apply_filters('retheme_dev_sandbox', false)) {
                $host = $_SERVER['HTTP_HOST'] ?? '';
                $server_ip = $_SERVER['SERVER_ADDR'] ?? '';

                // List of possible local environments
                $local_hosts = ['localhost', '127.0.0.1'];

                // Patterns for staging/development domains
                $staging_patterns = [
                    '/\.local$/i',        // Domain ends with .local
                    '/^dev\./i',          // Subdomain starts with dev.
                    '/^local\./i',        // Subdomain starts with local.
                    '/^staging\./i',      // Subdomain starts with staging.
                    '/^test\./i',         // Subdomain starts with test.
                    '/^preview\./i',      // Subdomain starts with preview.
                ];

                // Check if running on localhost
                if (in_array($host, $local_hosts, true) || in_array($server_ip, $local_hosts, true)) {
                    return true;
                }

                // Check if domain matches staging patterns
                foreach ($staging_patterns as $pattern) {
                    if (preg_match($pattern, $host)) {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
