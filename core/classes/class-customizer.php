<?php
/**
 * Customizer WP methods
 *
 * @package     Retheme
 * @category    Core
 * @author      Webforia Studio
 * @version     2.3.0
 */
namespace Retheme;

use Retheme\Helper;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

// No need to proceed if this class already exists.
if (!class_exists('Retheme\Customizer_Base')) {
    class Customizer_Base
    {

        public $breakpoint_large = '@media (min-width: 992px)';
        public $breakpoint_medium = '@media (max-width: 768px)';
        public $breakpoint_small = '@media (max-width: 576px)';
        public $textdomain = 'retheme';

        /**
         * Add panel
         * required kirki
         */
        public function add_panel($name, $args)
        {
            \Kirki::add_panel($name, $args);
        }

        /**
         * Add section
         * required kirki
         */
        public function add_section($panel = '', $sections = '')
        {
            if ($sections) {
                foreach ($sections as $section_id => $section) {

                    \Kirki::add_section("{$section_id}_section", [
                        'title' => $section[0],
                        'description' => !empty($section[1]) ? $section[1] : '',
                        'panel' => $panel,
                    ]);

                }
            }
        }

        /**
         * Add field control
         */
        public function add_field($args)
        {

            \Kirki::add_field('retheme_customizer', $args);

        }

        /**
         * Add header control
         */
        public function add_header($args = array())
        {

            $this->add_field(wp_parse_args(array(
                'type' => 'custom',
                'settings' => 'control_header_' . $args['settings'],
                'class' => 'header',
            ), $args));
        }

        public function add_tabs($args = array())
        {
            $this->add_field(wp_parse_args(array(
                'type' => 'custom',
                'settings' => 'control_tabs_' . $args['settings'],
                'class' => 'tabs',
                'default' => '<ul class="retheme_tabs">
                                <li class="tab-setting">Settings</li>
                                <li class="tab-style">Styles</li>
                              </ul>',
            ), $args));
        }

        public function upgrade_pro($args = array())
        {
            $product = esc_url_raw(RT_THEME_URL);

            $this->add_field(wp_parse_args(array(
                'type' => 'custom',
                'settings' => 'control_upgrade_' . $args['settings'],
                'default' => "<div><p>Upgrade to Premium for use this option. <a href='{$product}' target='__target'>Upgrade Now</a></p></div>",
            ), $args));
        }

        /**
         * Responsive control with mobile choose
         * @desc - Add tablet and mobile control
         * @param $args - argument from control
         * @return HTML - control field
         */
        public function add_field_responsive($args = array())
        {

            $default_tablet = !empty($args['default_tablet']) ? $args['default_tablet'] : $args['default'];
            $default_mobile = !empty($args['default_mobile']) ? $args['default_mobile'] : $args['default'];

            // output
            $output_tablet = array();
            $output_mobile = array();

            if (!empty($args['output'])) {

                $output_tablet = $args['output'];

                foreach ($output_tablet as $key => $output) {
                    $output_tablet[$key]['media_query'] = $this->breakpoint_medium;
                }

                $output_mobile = $args['output'];

                foreach ($output_mobile as $key => $output) {
                    $output_mobile[$key]['media_query'] = $this->breakpoint_small;
                }
            }

            /** Show responsive control */
            $this->add_field(wp_parse_args(array(
                'device' => 'desktop',
                'default' => $args['default'],
            ), $args));

            $this->add_field(wp_parse_args(array(
                'settings' => "{$args['settings']}_tablet",
                'device' => 'tablet',
                'output' => $output_tablet,
                'default' => $default_tablet,
            ), $args));

            $this->add_field(wp_parse_args(array(
                'settings' => "{$args['settings']}_mobile",
                'device' => 'mobile',
                'output' => $output_mobile,
                'default' => $default_mobile,
            ), $args));

        }

        /**
         * Color Group Control
         *
         * @param array $args
         * @return void
         */
        public function add_field_color($args = array())
        {
            $element = !empty($args['element']) ? $args['element'] : '';

            if (!empty($args['pseudo'])) {
                $this->add_field(wp_parse_args($args, array(
                    'label' => __('Color', 'saudagarwp'),
                    'settings' => $args['settings'],
                    'type' => 'multicolor',
                    'choices' => [
                        'normal' => __('Normal', 'saudagarwp'),
                        'hover' => __('Hover', 'saudagarwp'),
                    ],
                    'default' => [
                        'normal' => ' ',
                        'hover' => ' ',
                    ],
                    'output' => array(
                        array(
                            'element' => $element,
                            'property' => 'color',
                            'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                            'choice' => 'normal',
                        ),
                        array(
                            'element' => $this->selector($element, $args['pseudo']),
                            'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                            'property' => 'color',
                            'choice' => 'hover',
                        ),

                    ),
                    'transport' => 'auto',
                )));
            } else {
                $this->add_field(wp_parse_args($args, array(
                    'label' => __('Color', 'saudagarwp'),
                    'type' => 'color',
                    'choices' => array(
                        'alpha' => true,
                    ),
                    'output' => array(
                        array(
                            'element' => $element,
                            'property' => 'color',
                            'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        ),
                    ),
                    'transport' => 'auto',
                )));

            }

        }

        /**
         * Background Group Control
         *
         * @param array $args
         * @return void
         */

        public function add_field_background($args = array())
        {
            $element = !empty($args['element']) ? $args['element'] : '';

            $this->add_field_color(wp_parse_args($args, array(
                'label' => __('Background Color', 'saudagarwp'),
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'background-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => $this->selector($element),
                        'property' => 'background-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'hover',
                    ),

                ),
            )));

        }
        /**
         * Link Group Control
         *
         * @param array $args
         * @return void
         */
        public function add_field_link($args = array())
        {
            $element = !empty($args['element']) ? $args['element'] : '';

            $this->add_field_color(wp_parse_args($args, array(
                'label' => __('Link', 'saudagarwp'),
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => $this->selector($element, $args['pseudo']),
                        'property' => 'color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'hover',
                    ),

                ),
            )));

        }

        /**
         * Border Color Control
         *
         * @param array $args
         * @return void
         */
        public function add_field_border_color($args = array())
        {
            $element = !empty($args['element']) ? $args['element'] : '';
            $pseudo = !empty($args['pseudo']) ? $args['pseudo'] : '';
            $suffix = !empty($args['suffix']) ? $args['suffix'] : '';

            $this->add_field_color(wp_parse_args($args, array(
                'label' => __('Border Color', 'saudagarwp'),
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'border-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => $this->selector($element, $pseudo),
                        'property' => 'border-color',
                        'suffix' => $suffix,
                        'choice' => 'hover',
                    ),

                ),
            )));
        }

        /**
         * Border Radius Control
         *
         * @param array $args
         * @return void
         */
        public function add_field_border_radius($args = array())
        {
            $label = !empty($args['label']) ? $args['label'] : 'Border Radius';
            $element = !empty($args['element']) ? $args['element'] : '';

            /**
             * Merge default array with array from control
             * @param array $args, $default
             */
            $this->add_field(wp_parse_args($args, array(
                'label' => __($label, 'saudagarwp'),
                'type' => 'slider',
                'choices' => array(
                    'min' => '0',
                    'max' => '100',
                    'step' => '1',
                ),
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'border-radius',
                        'units' => 'px',
                    ),
                ),
                'transport' => 'auto',
            )));
        }

        /**
         * Animation Control
         *
         * @param array $args
         * @return void
         */
        public function add_field_animation($args = array())
        {
            $element = !empty($args['element']) ? $args['element'] : '';
            /**
             * Merge default array with array from control
             * @param array $args, $default
             */
            $this->add_field(wp_parse_args($args, array(
                'type' => 'select',
                'label' => __('Animation', 'admin_domain'),
                'settings' => $args['settings'],
                'default' => 'fadeIn',
                'choices' => Helper::get_animation_in(),
            )));

            $this->add_field(array(
                'type' => 'number',
                'label' => __('Duration', 'saudagarwp'),
                'settings' => $args['settings'] . '_duration',
                'default' => 300,
                'choices' => array(
                    'min' => 120,
                    'max' => 1000,
                ),
            ));

        }

        public function add_field_padding($args = array())
        {

            $this->add_field_responsive(wp_parse_args($args, array(
                'type' => 'dimensions',
                'label' => __('Padding', 'saudagarwp'),
                'settings' => $args['settings'],
                'section' => $args['section'],
                'description' => 'Use CSS Unit px or %',
                'default' => array(
                    'left' => '',
                    'top' => '',
                    'right' => '',
                    'bottom' => '',
                ),
                'output' => array(
                    array(
                        'element' => $args['element'],
                        'property' => 'padding',
                    ),

                ),
                'transport' => 'auto',
            )));
        }

        public function add_field_margin($args = array())
        {

            $this->add_field_responsive(wp_parse_args($args, array(
                'type' => 'dimensions',
                'label' => __('Margin', 'saudagarwp'),
                'settings' => $args['settings'],
                'section' => $args['section'],
                'description' => 'Use CSS Unit px or %',
                'default' => array(
                    'left' => '',
                    'top' => '',
                    'right' => '',
                    'bottom' => '',
                ),
                'output' => array(
                    array(
                        'element' => $args['element'],
                        'property' => 'margin',
                    ),

                ),
                'transport' => 'auto',
            )));
        }

        /**
         * Desc
         */
        public function add_desc($args)
        {
            $this->add_field(array(
                'type' => 'custom',
                'settings' => $args['settings'],
                'section' => $args['section'],
                'default' => '<div class="retheme-desc">' . $args['label'] . '</div>',
            ));
        }

        /*
         * Merge selector for hover
         */
        public function selector($selector, $pseudo = '')
        {
            $data = explode(",", $selector);
            $element = array();

            if ($data) {
                foreach ($data as $key => $value) {

                    $element[] = $value . ':hover';
                    // $element[] = $value . ':active';
                    // $element[] = $value . ':focus';

                }
                return implode(', ', $element);
            }

        }

        /**
         * Get default form theme mods
         *
         * this function use for get value from global settings
         * @param [type] $name
         * @param string $choose
         * @return void
         */
        public function get_default_mod($name, $choose = '')
        {
            $theme_mod = '';

            if (!empty(get_theme_mod($name, rt_get_theme($name))[$choose])) {
                $theme_mod = get_theme_mod($name, rt_get_theme($name))[$choose];
            }
            if (empty($choose)) {
                $theme_mod = get_theme_mod($name, rt_get_theme($name));
            }

            return $theme_mod;
        }

        // end class
    }
}
