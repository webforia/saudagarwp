<?php

/**
 * Elementor Helper methods
 *
 * @package     Retheme
 * @category    Core
 * @author      Webforia Studio
 * @version     2.3.0
 */

namespace Retheme;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Widget_Base;
use Retheme\Helper;
use Retheme\HTML;

// Exit if accessed directly.
if (!defined('ABSPATH')) {
    exit;
}

add_action('elementor/widgets/widgets_registered', function () {

    // No need to proceed if this class already exists.
    if (!class_exists('Retheme\Elementor_Base')) {
        class Elementor_Base extends Widget_Base {

            public $textdomain = 'retheme';

            public function get_name() {
                return 'retheme-base';
            }

            public function setting_carousel($args = '') {

                $settings = wp_parse_args($args, [
                    'carousel' => 'yes',
                    'slide' => 'yes',
                    'gap' => 'yes',
                    'navigation' => 'yes',
                ]);

                $this->start_controls_section(
                    'setting_carousel',
                    [
                        'label' => __('Carousel', 'saudagarwp'),
                    ]
                );

                if ($settings['carousel'] === 'yes') {
                    $this->add_control(
                        'carousel',
                        [
                            'label' => __('Carousel', 'saudagarwp'),
                            'type' => Controls_Manager::SWITCHER,
                            'default' => 'no',
                            'label_off' => __('Off', 'saudagarwp'),
                            'label_on' => __('On', 'saudagarwp'),
                            'return_value' => 'yes',

                        ]
                    );
                }
                if ($settings['slide'] === 'yes') {
                    $slides_to_show = range(1, 10);
                    $slides_to_show = array_combine($slides_to_show, $slides_to_show);
                    $this->add_responsive_control(
                        'slider_item',
                        [
                            'label' => __('Slides Per View', 'saudagarwp'),
                            'type' => Controls_Manager::SELECT,
                            'options' => $slides_to_show,
                            'devices' => ['desktop', 'tablet', 'mobile'],
                            'desktop_default' => 4,
                            'tablet_default' => 3,
                            'mobile_default' => 1,

                        ]
                    );
                }
                if ($settings['gap'] === 'yes') {

                    $this->add_control(
                        'slider_gap',
                        [
                            'label' => __('Slide Spacing', 'saudagarwp'),
                            'description' => __('spacing between Carousel item', 'saudagarwp'),
                            'type' => Controls_Manager::SLIDER,
                            'size_units' => ['px'],
                            'range' => [
                                'px' => [
                                    'min' => 0,
                                    'max' => 30,
                                    'step' => 1,
                                ],
                            ],
                            'default' => [
                                'unit' => 'px',
                                'size' => 20,
                            ],
                        ]
                    );
                }
                if ($settings['navigation'] === 'yes') {
                    $this->add_control(
                        'slider_nav',
                        [
                            'label' => __('Navigation', 'saudagarwp'),
                            'type' => Controls_Manager::SELECT,
                            'default' => 'beside',
                            'options' => [
                                'none' => 'None',
                                'beside' => 'On Side',
                            ],
                        ]
                    );
                }

                $this->add_control(
                    'slider_pagination',
                    [
                        'label' => __('Pagination', 'saudagarwp'),
                        'type' => Controls_Manager::SWITCHER,
                        'default' => 'no',
                        'label_off' => __('Off', 'saudagarwp'),
                        'label_on' => __('On', 'saudagarwp'),
                        'return_value' => 'yes',

                    ]
                );

                $this->add_control(
                    'slider_loop',
                    [
                        'label' => __('Infinite Loop', 'saudagarwp'),
                        'type' => Controls_Manager::SWITCHER,
                        'default' => 'no',
                        'label_off' => __('Off', 'saudagarwp'),
                        'label_on' => __('On', 'saudagarwp'),
                        'return_value' => 'yes',

                    ]
                );

                $this->add_control(
                    'slider_auto_play',
                    [
                        'label' => __('Auto Play', 'saudagarwp'),
                        'type' => Controls_Manager::SWITCHER,
                        'default' => 'no',
                        'label_on' => __('On', 'saudagarwp'),
                        'label_off' => __('Off', 'saudagarwp'),
                        'return_value' => 'yes',
                    ]
                );

                $this->end_controls_section();

                $this->start_controls_section(
                    'style_slider_navigation',
                    [
                        'label' => __('Carousel', 'saudagarwp'),
                        'tab' => Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_control(
                    'pagination_color',
                    [
                        'label' => __('Pagination Color', 'saudagarwp'),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .swiper-pagination-bullet' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_control(
                    'pagination_color_hover',
                    [
                        'label' => __('Pagination Color :Active', 'saudagarwp'),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .swiper-pagination-bullet.swiper-pagination-bullet-active' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );
                $this->end_controls_section();
            }


            /**
             * Pagination control
             *
             * @return void
             */
            public function setting_pagination() {

                $this->start_controls_section(
                    'pagination',
                    [
                        'label' => __('Pagination', 'saudagarwp'),
                    ]
                );

                $this->add_control(
                    'pagination_style',
                    [
                        'label' => __('Pagination Style', 'saudagarwp'),
                        'type' => Controls_Manager::SELECT,
                        'default' => 'no_pagination',
                        'options' => [
                            'no_pagination' => 'No Pagination',
                            'loadmore' => 'Load More',
                        ],
                    ]
                );

                $this->end_controls_section();
            }

            public function setting_header_block() {
                $this->start_controls_section(
                    'header',
                    [
                        'label' => __('Header', 'saudagarwp'),
                    ]
                );
                $this->add_control(
                    'header_title',
                    [
                        'label' => __('Heading', 'saudagarwp'),
                        'type' => Controls_Manager::TEXT,
                        'default' => __('Heading title', 'saudagarwp'),
                    ]
                );
                $this->add_control(
                    'header_style',
                    [
                        'label' => __('Style', 'saudagarwp'),
                        'type' => Controls_Manager::SELECT,
                        'default' => 'style-1',
                        'options' => [
                            'style-1' => __('Style 1', 'saudagarwp'),
                            'style-2' => __('Style 2', 'saudagarwp'),
                            'style-3' => __('Style 3', 'saudagarwp'),
                            'style-4' => __('Style 4', 'saudagarwp'),
                        ],
                    ]
                );
                $this->end_controls_section();

                /* add style header block */
                $this->start_controls_section(
                    'style_header',
                    [
                        'label' => __('Header', 'saudagarwp'),
                        'tab' => Controls_Manager::TAB_STYLE,
                    ]
                );

                $this->add_control(
                    'style_header_color',
                    [
                        'label' => __('Color', 'saudagarwp'),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rt-header-block .rt-header-block__title' => 'color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_control(
                    'style_header_color_line_primary',
                    [
                        'label' => __('Color Line Primary', 'saudagarwp'),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rt-header-block--style-1 .rt-header-block__title' => 'border-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-2 .rt-header-block__title' => 'border-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-3 .rt-header-block__title' => 'background-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-4 .rt-header-block__title' => 'border-color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_control(
                    'style_header_color_line_second',
                    [
                        'label' => __('Color Line Second', 'saudagarwp'),
                        'type' => Controls_Manager::COLOR,
                        'selectors' => [
                            '{{WRAPPER}} .rt-header-block--style-1 ' => 'border-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-3 ' => 'border-color: {{VALUE}};',
                            '{{WRAPPER}} .rt-header-block--style-4::before' => 'background-color: {{VALUE}};',
                        ],
                    ]
                );

                $this->add_responsive_control(
                    'header_block_margin',
                    [
                        'label' => __('Margin Bottom', 'saudagarwp'),
                        'type' => Controls_Manager::SLIDER,
                        'size_units' => ['px', '%'],
                        'selectors' => [
                            '{{WRAPPER}} .rt-header-block' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                        ],
                    ]
                );

                $this->end_controls_section();
            }

            /**
             * HELPER elementor loop query
             */
            public static function elementor_loop($settings = array()) {
                // get query argument
                $the_query = new \WP_Query(Helper::query($settings));

        
                $args = [
                    'id' => $settings['post_type'],
                    'post_type' => !empty($settings['post_type']) ? $settings['post_type'] : 'post',
                    'posts_per_page' => !empty($settings['posts_per_page']) ? $settings['posts_per_page'] : 9,
                    'pagination_style' => !empty($settings['pagination_style']) ? $settings['pagination_style'] : 'number',
                    'class_wrapper' => !empty($settings['class_wrapper']) ? $settings['class_wrapper'] : '',
                    'carousel' => !empty($settings['carousel']) ? $settings['carousel'] : null,
                    'setting_column' => !empty($settings['setting_column']) ? $settings['setting_column'] : 3,
                    'setting_column_tablet' => !empty($settings['setting_column_tablet']) ? $settings['setting_column_tablet'] : 2,
                    'setting_column_mobile' => !empty($settings['setting_column_mobile']) ? $settings['setting_column_mobile'] : 1,
                    'layout_masonry' => !empty($settings['layout_masonry']) ? $settings['layout_masonry'] : 'no',
                    'template_part' => !empty($settings['template_part']) ? $settings['template_part'] : '',
                    'slider_item' => !empty($settings['slider_item']) ? $settings['slider_item'] : 4,
                    'slider_item_tablet' => !empty($settings['slider_item_tablet']) ? $settings['slider_item_tablet'] : 3,
                    'slider_item_mobile' => !empty($settings['slider_item_mobile']) ? $settings['slider_item_mobile'] : 1,
                    'slider_nav' => !empty($settings['slider_nav']) ? $settings['slider_nav'] : 'no',
                    'slider_pagination' => !empty($settings['slider_pagination']) ? $settings['slider_pagination'] : 'no',
                    'slider_loop' => !empty($settings['slider_loop']) ? $settings['slider_loop'] : 'no',
                    'slider_auto_play' => !empty($settings['slider_auto_play']) ? $settings['slider_auto_play'] : 'no',
                    'slider_gap' => [
                        'size' => 20,
                    ],
                ];

                // class wrapper
                $classes[] = $args['class_wrapper'];

                // start loop
                if ($the_query->have_posts()) :

                    // layout column
                    if ($args['carousel'] != 'yes') {

                        // class wrapper
                        $column_md = $args['setting_column'];
                        $column_sm = $args['setting_column_tablet'];
                        $column_xs = $args['setting_column_mobile'];

                        $classes[] = 'grids';
                        $classes[] = "grids-md-{$column_md}";
                        $classes[] = "grids-sm-{$column_sm}";
                        $classes[] = "grids-xs-{$column_xs}";
                        $classes[] = ($args['layout_masonry'] == 'yes') ? 'grid-masonry js-masonry' : '';

                        echo HTML::open([
                            'id' => "block_{$args['id']}",
                            'class' => $classes,
                        ]);

                        while ($the_query->have_posts()) : $the_query->the_post();
                            include locate_template($args['template_part'] . '.php');
                        endwhile;

                        echo HTML::close();


                        // add pagination
                        echo HTML::pagination(wp_parse_args($args, [
                            'target' => "block_{$args['id']}",
                            'total' => $the_query->max_num_pages,
                            'post_total' => $the_query->found_posts,
                            'format' => '?paged=%#%',
                            'current' => max(1, get_query_var('paged')),
                            'add_args' => [
                                'post_type' => $args['post_type'],
                            ],
                        ]));
                    }

                    // layout carousel
                    if ($args['carousel'] == 'yes') {

                        $classes[] = 'rt-swiper--card';

                        // Header nav
                        $slider_nav = array();
                        if ($args['slider_nav'] == 'header') {
                            $slider_nav = [
                                'navigation' => [
                                    'nextEl' => "#header-{$args['id']} .js-slider-next",
                                    'prevEl' => "#header-{$args['id']} .js-slider-prev",
                                ],
                            ];
                        }

                        // remove internal nav
                        if ($args['slider_nav'] != 'beside') {
                            $classes[] = 'rt-swiper--nonav';
                        }


                        // Pagination
                        $slider_pagination = array();
                        if ($args['slider_pagination'] == 'yes') {
                            $slider_pagination = [
                                'pagination' => [
                                    'el' => '.swiper-pagination',
                                    'clickable' => true,
                                ],
                            ];
                            $classes[] = 'rt-swiper--pagination-outer';
                        }

                        // auto play

                        $slider_option = array_merge($slider_nav, $slider_pagination, $args);


                        echo HTML::before_slider(wp_parse_args($slider_option, [
                            'id' => "rt-swiper-{$args['id']}",
                            'class' => implode(' ', $classes),
                            'items-lg' => $args['slider_item'],
                            'items-md' => $args['slider_item_tablet'],
                            'items-sm' => $args['slider_item_mobile'],
                            'loop' => ($args['slider_loop'] == 'yes') ? true : false,
                            'spaceBetween' => $args['slider_gap']['size'],
                            'autoplay' => ($args['slider_auto_play'] == 'yes') ? true : false,
                            'sameheight' => true,
                        ]));

                        while ($the_query->have_posts()) : $the_query->the_post();
                            echo HTML::open(['class' => 'swiper-slide']);
                            include locate_template($args['template_part'] . '.php');
                            echo HTML::close();
                        endwhile;

                        echo HTML::after_slider();
                    }

                    wp_reset_postdata();
                else :
                    do_action('rt_post_none');
                endif;
            }

            /* end class */
        }
    }
});
